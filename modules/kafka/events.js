const Joi = require("@hapi/joi");
const { 
  transactionProcessing, 
  checkRules,
  blockAccounts,
  currencyExchange,
  receiveMessage
}  = require("./topics");

module.exports = {
  
  // TRANSACTION PROCESSING

  NEW_TRANSACTION: {
    name: "NEW_TRANSACTION",
    topic: transactionProcessing,
    schema: Joi.object().keys({
      txInfo: Joi.object().keys({}),
      definition: Joi.string()
    })
  },  
  UPDATE_TRANSACTION: {
    name: "UPDATE_TRANSACTION",
    topic: transactionProcessing,
    schema: Joi.object().keys({      
      data: Joi.object().keys({}),
      definition: Joi.string()
    })
  },
  CURRENCY_EXCHANGE: {
    name: "CURRENCY_EXCHANGE",
    topic: currencyExchange,
    schema: Joi.object().keys({      
      data: Joi.object().keys({}),
      definition: Joi.string()
    })
  },
  CHECK_RULES: {
    name: "CHECK_RULES",
    topic: checkRules,
    schema: Joi.object().keys({      
      data: Joi.object().keys({}),
      definition: Joi.string()
    })
  },
  BLOCK_ACCOUNTS: {
    name: "BLOCK_ACCOUNTS",
    topic: blockAccounts,
    schema: Joi.object().keys({      
      data: Joi.object().keys({}),
      definition: Joi.string()
    })
  },
  END_TRANSACTION: {
    name: "END_TRANSACTION",
    topic: transactionProcessing,
    schema: Joi.object().keys({      
      data: Joi.object().keys({}),
      definition: Joi.string(),
      stack: Joi.string()
    })
  },

  // MESSAGES RECEIVING

  RECEIVE_MESSAGE_SUCCESS: {
    name: "RECEIVE_MESSAGE_SUCCESS",
    topic: receiveMessage,
    schema: Joi.object().keys({      
      data: Joi.object().keys({}),
      definition: Joi.string(),
      stack: Joi.string()
    })
  },
  RECEIVE_MESSAGE_FAILED: {
    name: "RECEIVE_MESSAGE_FAILED",
    topic: receiveMessage,
    schema: Joi.object().keys({      
      data: Joi.object().keys({}),
      definition: Joi.string(),
      stack: Joi.string()
    })
  }
}