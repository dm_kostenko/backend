const { end, fail } = require("../helpers")
const db = require("../../../../../helpers/db/bankApi");


module.exports = async({ transaction, type, error }) => {
  let connection;
  try { 
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    if(error) {
      await fail(transaction, type, connection)
    }
    else {
      await end(transaction, type, connection)
    }

    await db._transaction.commit(connection);
  }
  catch(err) {
    if(connection)
      await db._transaction.rollback(connection);
  }
  finally {
    if(connection)
      connection.release();
  }
}