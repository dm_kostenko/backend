const { TransactionProcessing } = require("../../../../../helpers/kafka");
const { services } = require("../../../../../config/kafka");
const { processingSequence } = require("../config");
const { transactionProcessing, checkRules, blockAccounts } = require("../config").topics.producer;
const { END_TRANSACTION, BLOCK_ACCOUNTS } = require("../config").events.producer;

const producer = new TransactionProcessing.Producer();

const willBlock = (checking) => {
  if(checking.checkRules && services[blockAccounts])
   return (!(checking.checkRules.aml && checking.checkRules.blacklist) && !checking.blockAccounts)   // failed AML or Blacklist but doesn't blocked yet
  return false;
}

const wereBlocked = (checking) => {
  return checking.blockAccounts
}

module.exports = async({ transaction, type, checking, error }) => {
  try {
    if(error)
      throw(error)
      
    const [ currentStep ] = processingSequence[type].filter(step => !step.check(checking))
    if(willBlock(checking))
      producer.send({
        topic: blockAccounts,
        event: BLOCK_ACCOUNTS,
        payload: { transaction, type, checking, error }
      })
    else if(wereBlocked(checking))
      producer.send({ 
        topic: transactionProcessing,
        event: END_TRANSACTION,
        payload: { transaction, type, checking, error: "Blocked accounts" }
      })
    else if(currentStep)
      switch(currentStep.type) {
        case "service":
          producer.send({
            topic: currentStep.topic,
            event: currentStep.event.name,
            payload: { transaction, type, checking, error }
          })
          return      
        default: break;
      }
    else
      producer.send({ 
        topic: transactionProcessing,
        event: END_TRANSACTION,
        payload: { transaction, type, checking, error }
      })
  } catch (err) {
    producer.send({ 
      topic: transactionProcessing,
      event: END_TRANSACTION,
      payload: { transaction, type, checking, error: err.toString() }
    })

  }
}