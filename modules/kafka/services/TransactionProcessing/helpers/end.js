const db = require("../../../../../helpers/db/bankApi");
const { 
  EXTERNAL_TRANSACTION_UNAPPROVED, 
  EXTERNAL_TRANSACTION_UNCONFIRMED,
  EXTERNAL_TRANSACTION_APPROVED
} = require("../../../../../helpers/constants/statuses").externalTransaction;
const { 
  INTERNAL_TRANSACTION_SUCCESS
} = require("../../../../../helpers/constants/statuses").internalTransaction;
const { creditTransfer } = require("../../../../../helpers/transactions");
const { closeReserved, sendCommission } = require("../../../../../helpers/transactions/account");


module.exports = async(data, type, connection) => {
  switch(type) {
    case "FFCCTRNS":
      // let status = EXTERNAL_TRANSACTION_UNAPPROVED;
      // if (data.authType)
      //   switch (data.authType) {
      //   case "entity":
      //     status = EXTERNAL_TRANSACTION_UNCONFIRMED;
      //     break;
  
      //   default:
      //     status = EXTERNAL_TRANSACTION_UNAPPROVED;
      //     break;
      //   }
      console.log('4: ')
      console.log(data)
      if(data.status === EXTERNAL_TRANSACTION_APPROVED)
        await creditTransfer.prepareXml({ 
          guid: data.guid,
          status: data.status,
          system: data.ClrSysPrtry,
          author_guid: data.authorGuid
        }, connection);
      else        
        await db.external_transaction_history.upsert({ 
          input_guid: data.guid,
          input_status: data.status
        }, connection);
        
      break;
    case "Internal":
      const [ receiverAccount ] = await db.account.get({ 
        input_number: data.receiverAccount
      });

      const [ senderAccount ] = await db.account.get({ 
        input_number: data.senderAccount
      });

      const [ receiverBalance ] = await db.currency_balance.get({ 
        input_account_number: receiverAccount.number, 
        input_currency_code: data.receiverCurrency 
      });
      
      await closeReserved({
        senderAccountNumber: senderAccount.number,
        currency: data.senderCurrency,
        amount: data.senderAmount
      }, connection)

      await db.currency_balance.upsert({ 
        input_account_number: data.receiverAccount, 
        input_currency_code: data.receiverCurrency, 
        input_balance: Math.round(receiverBalance.balance) + Math.round(data.receiverAmount),
        input_author_guid: data.authorGuid
      }, connection);

      await db.internal_transaction_history.upsert({
        input_guid: data.guid,
        input_status: INTERNAL_TRANSACTION_SUCCESS
      }, connection);

      const [{ rate }] = await db.currency_rate.get({
        input_currency_code: data.senderCurrency
      });

      const eurCommission = Math.round((data.baseAmount - data.senderAmount) / rate)
      
      await sendCommission({ commission: eurCommission }, connection);

      break;
    case "FFPCRQST":
      break;
    case "PRTRN":
      await db.external_transaction_history.upsert({ 
        input_guid: data.guid,
        input_status: EXTERNAL_TRANSACTION_UNAPPROVED
      }, connection);
      break;
    case "ROINVSTG":
      break;
    default: throw("Invalid type")
  }
}