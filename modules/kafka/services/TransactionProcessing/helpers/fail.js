const db = require("../../../../../helpers/db/bankApi");
const { 
  EXTERNAL_TRANSACTION_FAILED 
} = require("../../../../../helpers/constants/statuses").externalTransaction;
const { 
  INTERNAL_TRANSACTION_FAILED 
} = require("../../../../../helpers/constants/statuses").internalTransaction;
const { cancelReserved } = require("../../../../../helpers/transactions/account");

module.exports = async(data, type, connection) => {
  switch(type) {
    case "FFCCTRNS": {
      await db.external_transaction_history.upsert({ 
        input_guid: data.guid,
        input_status: EXTERNAL_TRANSACTION_FAILED
      }, connection);

      await cancelReserved({
        senderAccountNumber: data.senderAccount.slice(9),
        currency: data.initCurrency,
        amount: data.initAmount
      }, connection);

      break;
    }
    case "Internal":
      await db.internal_transaction_history.upsert({
        input_guid: data.guid,
        input_status: INTERNAL_TRANSACTION_FAILED
      }, connection);
      
      await cancelReserved({
        senderAccountNumber: data.senderAccount,
        currency: data.senderCurrency,
        amount: data.senderAmount
      }, connection);

      break;
    case "FFPCRQST":      
      await db.external_transaction_history.upsert({ 
        input_guid: data.guid,
        input_status: EXTERNAL_TRANSACTION_FAILED
      }, connection);
      break;
    case "PRTRN":
      await db.external_transaction_history.upsert({ 
        input_guid: data.guid,
        input_status: EXTERNAL_TRANSACTION_FAILED
      }, connection);
      if(data.reasonForReturn === "FOCR" && data.isReserved)
        await cancelReserved({
          senderAccountNumber: data.receiverAccount,
          currency: data.currency,
          amount: data.amount
        }, connection);

      break;
    case "ROINVSTG":      
      await db.external_transaction_history.upsert({ 
        input_guid: data.guid,
        input_status: EXTERNAL_TRANSACTION_FAILED
      }, connection);
      break;
    default: throw("Invalid type")
  }
}