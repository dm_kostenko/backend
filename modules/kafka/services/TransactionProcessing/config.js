const { 
  transactionProcessing,
  currencyExchange,
  checkRules,
  blockAccounts
} = require("../../topics");
const { 
  CHECK_RULES,
  BLOCK_ACCOUNTS,
  CURRENCY_EXCHANGE, 
  UPDATE_TRANSACTION,
  END_TRANSACTION
} = require("../../events");

module.exports = {
  events: {
    consumer: {
      END_TRANSACTION: END_TRANSACTION.name,
      UPDATE_TRANSACTION: UPDATE_TRANSACTION.name
    },
    producer: {
      CHECK_RULES: CHECK_RULES.name,
      BLOCK_ACCOUNTS: BLOCK_ACCOUNTS.name,
      CURRENCY_EXCHANGE: CURRENCY_EXCHANGE.name,
      END_TRANSACTION: END_TRANSACTION.name
    }
  },
  topics: {
    consumer: [
      transactionProcessing
    ],
    producer: {
      checkRules,
      blockAccounts,
      currencyExchange,
      transactionProcessing
    }
  },
  processingSequence: {
    FFCCTRNS: [
      {
        type: "service",
        name: checkRules,
        topic: checkRules,
        event: CHECK_RULES,
        check: (checking) => {
          return checking.checkRules ? checking.checkRules.aml && checking.checkRules.blacklist && checking.checkRules.rules : false;
        }
      },
      {
        type: "service",
        name: currencyExchange,
        topic: currencyExchange,
        event: CURRENCY_EXCHANGE,
        check: (checking) => {
          return checking.currencyExchange
        }        
      }
    ],
    Internal: [
      {
        type: "service",
        name: currencyExchange,
        topic: currencyExchange,
        event: CURRENCY_EXCHANGE,
        check: (checking) => {
          return checking.currencyExchange
        }     
      }
    ],
    FFPCRQST: [],
    PRTRN: [],
    ROINVSTG: []
  }
  
}