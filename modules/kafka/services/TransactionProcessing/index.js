const { TransactionProcessing } = require("../../../../helpers/kafka");
const controllers = require("./controllers");
// const events = require("../../events");
const consumerTopics = require("./config").topics.consumer;
const { printEvent } = require("../../helpers");

let consumerEvents = require("./config").events.consumer;
// consumerEvents = consumerEvents.map(event => ( events[event] ));

const consumer = new TransactionProcessing.Consumer({ topics: consumerTopics, name: "transactionProcessingConsumer" });

try {
  consumer.onMessage((message) => {
    if(Object.keys(consumerEvents).includes(message.key) && controllers[message.key]) {
      printEvent(message)
      controllers[message.key](JSON.parse(message.value));
    }
  });
}
catch(err) {
  throw(err)
}

