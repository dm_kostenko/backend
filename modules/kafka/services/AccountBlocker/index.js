const { AccountBlocker } = require("../../../../helpers/kafka");
const controllers = require("./controllers");
const consumerTopics = require("./config").topics.consumer;
let consumerEvents = require("./config").events.consumer;
const { printEvent } = require("../../helpers");


const consumer = new AccountBlocker.Consumer({ topics: consumerTopics, name: "accountBlockerConsumer" });
try {
  consumer.onMessage((message) => {
    if(Object.keys(consumerEvents).includes(message.key) && controllers[message.key]) {
      printEvent(message)
      controllers[message.key](JSON.parse(message.value));
    }
  });
}
catch(err) {
  throw(err)
}

