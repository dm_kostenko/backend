const { 
  blockAccounts,
  transactionProcessing
} = require("../../topics");
const { 
  BLOCK_ACCOUNTS, 
  UPDATE_TRANSACTION
} = require("../../events");

module.exports = {
  events: {
    consumer: {
      BLOCK_ACCOUNTS: BLOCK_ACCOUNTS.name
    },
    producer: {
      UPDATE_TRANSACTION: UPDATE_TRANSACTION.name
    }
  },
  topics: {
    consumer: [
      blockAccounts,
    ],
    producer: {
      transactionProcessing
    }
  }
}