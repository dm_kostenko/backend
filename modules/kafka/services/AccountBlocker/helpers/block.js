const db = require("../../../../../helpers/db/bankApi");


module.exports = async(loginGuid, connection) => {
  await db.login.block({
    input_guid: loginGuid
  }, connection);
}