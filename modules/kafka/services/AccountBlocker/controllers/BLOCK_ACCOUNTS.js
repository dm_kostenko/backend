const { AccountBlocker } = require("../../../../../helpers/kafka");
const { transactionProcessing } = require("../config").topics.producer;
const { UPDATE_TRANSACTION } = require("../config").events.producer;
const { block } = require("../helpers")
const db = require("../../../../../helpers/db/bankApi");

const producer = new AccountBlocker.Producer();

module.exports = async({ transaction, type, checking }) => {
  let connection;
  try {    
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    if(transaction) {
      await block(transaction.authorGuid);
      checking.blockAccounts = true;
    }
    producer.send({
      topic: transactionProcessing,
      event: UPDATE_TRANSACTION,
      payload: { transaction, type, checking, error: "Block accounts" }
    });

    await db._transaction.commit(connection);
  }
  catch(err) {
    if(connection)
      await db._transaction.rollback(connection);
    producer.send({
      topic: transactionProcessing,
      event: UPDATE_TRANSACTION,
      payload: { transaction, type, checking, error: err.toString() }
    });
  }
  finally {
    if(connection)
      connection.release();
  }
}