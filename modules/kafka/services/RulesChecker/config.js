const { 
  checkRules,
  transactionProcessing
} = require("../../topics");
const { 
  CHECK_RULES, 
  UPDATE_TRANSACTION
} = require("../../events");

module.exports = {
  events: {
    consumer: {
      CHECK_RULES: CHECK_RULES.name
    },
    producer: {
      UPDATE_TRANSACTION: UPDATE_TRANSACTION.name
    }
  },
  topics: {
    consumer: [
      checkRules,
    ],
    producer: {
      transactionProcessing
    }
  }
}