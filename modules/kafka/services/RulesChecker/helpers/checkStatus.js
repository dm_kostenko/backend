const db = require("../../../../../helpers/db/bankApi");
const { EXTERNAL_TRANSACTION_APPROVED } = require("../../../../../helpers/constants/statuses").externalTransaction;

module.exports = async(data, type) => {

  if(data.test) {
    data.status = EXTERNAL_TRANSACTION_APPROVED
    return data
  }
    
  let amount;

  switch(type) {
    case "FFCCTRNS":
      if(data.currency !== "EUR") {
        const [ { rate } ] = await db.currency_rate.get({
          input_currency_code: data.currency
        });
        amount = Math.round(data.amount / rate);
      }
      else
        amount = data.amount;
      break;
    default: 
      return data;
  };

  const statusRules = await db.transaction_rule.get({
    input_direction: "outgoing",
    input_transaction_type: type.toUpperCase(),
    input_rule_type: "status"
  });

  let success_status = "success_individual_status";
  let failed_status = "success_individual_status";
  let below = "below_individual";
  switch(data.authType) {
    case "entity":
      success_status = "success_entity_status";
      failed_status = "failed_entity_status";
      below = "below_entity";
      break;
    case "individual":
      success_status = "success_individual_status";
      failed_status = "failed_individual_status";
      below = "below_individual";
      break;
    default: break;      
  }
  const successStatus = statusRules.find(rule => rule.name === success_status);
  const failedStatus = statusRules.find(rule => rule.name === failed_status);
  const _below = statusRules.find(rule => rule.name === below);

  data.status = amount < Math.round(_below.value) ? successStatus.value : failedStatus.value;

  return data;
}