const { RulesChecker } = require("../../../../../helpers/kafka");
const { transactionProcessing } = require("../config").topics.producer;
const { UPDATE_TRANSACTION } = require("../config").events.producer;
const { checkStatus } = require("../helpers")

const producer = new RulesChecker.Producer();

module.exports = async({ transaction, type, checking, error }) => {
  try {
    if(transaction) {
      // all checkings
      transaction = await checkStatus(transaction, type);
      checking.checkRules = {
        aml: true,
        blacklist: true,
        rules: true
      }
    }
    producer.send({
      topic: transactionProcessing,
      event: UPDATE_TRANSACTION,
      payload: { transaction, type, checking, error }
    });
  }
  catch(err) {
    producer.send({
      topic: transactionProcessing,
      event: UPDATE_TRANSACTION,
      payload: { transaction, type, checking, error: err.toString() }
    });
  }
}