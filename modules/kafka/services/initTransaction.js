const Producer = require("../../../helpers/kafka/Producer");
const { 
  UPDATE_TRANSACTION
} = require("../events");
const { transactionProcessing } = require("../topics");
const db = require("../../../helpers/db/bankApi");
const { create } = require("../helpers");
const { checkRates } = require("../helpers")
const { iban } = require("../../../config");
const { checkIBANs } = require("../../../helpers/parseIBAN")

const producer = new Producer({ name: "Init" });

const init = async(req, res, next, type, _connection, remoteConnection) => {

  const author_guid = req.auth && req.auth.loginGuid ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  let connection;
  let transaction;
  
  let { 
    currency,
    amount,
    amountWithRates,
    isNext,
    // saveForLater,
    // FFCCTRNS
    // paymentTransactionId = require("uuid/v1")(),
    // paymentInstructionId,
    paymentEndToEndId = "NOTPROVIDED",
    serviceLevel = "SEPA",
    chargeBearer = "SLEV",
    debtorName,
    debtorAccount,
    debtorBic,
    creditorName,
    creditorAccount,
    creditorBic,
    ClrSysPrtry = "LITAS-MIG", // LITAS-RLS or LITAS-MIG
    remittanceInformation = "Contract No.123456",
    // test,
    
    // Internal
    sender_currency = "EUR",
    receiver_currency = "EUR",
    sender_number,
    receiver_number,
    value,

    // FFPCRQST
    OrgnlTxId,  // and for ROINVSTG
    // paymentCancellationId = require("uuid/v1")(),
    CxlRsnInf, // Reason - DUPL, TECH or FRAD
    CxlRsnInfAddtInf, // If reason FRAD
    originatorClient, // and for PRTRN, ROINVSTG
    originatorBank,   // and for PRTRN, ROINVSTG

    // PRTRN
    originalTxId,
    // returnmentId = require("uuid/v1")(),
    chrgsInfAmt,
    reasonForReturn,  // allowed values: AC01, AC04, AC06, AG01, AG02, AM05, BE04, FOCR, MD07, MS02, MS03, RC01, RR01, RR02, RR03, RR04.
    rtrRsnInfAddtInf,
    isResponse,   // for ROINVSTG response, if it will fails
    returnImmediately,

    // ROINVSTG
    CxlStsRsnInf,
    CxlStsRsnInfAddtInf,
    // cancellationStsId = require("uuid/v1")()
  } = req.body;

  const getBicAndName = async iban => {
    const code = iban.slice(4, 9)
    const [ record ] = await db.institute.get({
      input_code: code
    })
    if(record && record.bic && record.name)
      return {
        creditorBic: record.bic, 
        creditorName: record.name
      }
    else 
      return {
        empty: true
      }
  }

  try {

    let commission;
    // let { error, test, internal } = checkIBANs(creditorAccount, debtorAccount)
    let error = null,  test = null, internal = null;
    if(error && !returnImmediately)
      throw { error }
    if(type === "FFCCTRNS" && internal) 
      type = "Internal"
    console.log('1: ', value)
    switch(type) {
      case "FFCCTRNS":
        if(!creditorBic || !creditorName) {
          let { creditorName: _creditorName, creditorBic: _creditorBic, empty } = await getBicAndName(creditorAccount)
          if(empty)
            if(res)
              return res.status(200).json({ empty })
            else
              return { empty }
          creditorName = _creditorName
          creditorBic = _creditorBic 
        }

        if(!test) {
          // commission = await checkRates({
          //   currency,
          //   amount: Math.round(amount * 100),
          //   senderAccount: debtorAccount,
          //   receiverAccount: creditorAccount,
          //   senderName: debtorName,
          //   senderBIC: debtorBic,
          //   receiverName: creditorName,
          //   receiverBIC: creditorBic,
          //   loginGuid: author_guid
          // }, "outgoing_FFCCTRNS");

          // if(!amountWithRates) {
          //   if(res)
          //     return res.status(200).json({ commission })
          //   else
          //     return { commission }
          // }
          // else if(amountWithRates) {
          //   const _amountWithRates = +amount + +commission
          //   if(_amountWithRates !== amountWithRates) {
          //     if(res)
          //       return res.status(400).json({
          //         error: "Failed amount with commission",
          //         details: [{ message: "Commission for this transaction was changed. Please try again." }]
          //       })
          //     else
          //       return {
          //         error: "Failed amount with commission",
          //         details: [{ message: "Commission for this transaction was changed. Please try again." }]
          //       }
          //   }
          // }
        }
        if(!isNext) {
          if(res)
            return res.status(200).json({ next: true, creditorName })
          else
            return { next: true, creditorName }
        }
        break;
      case "Internal":
        if(!test) {
          commission = await checkRates({
            senderCurrency: sender_currency || "EUR",
            receiverCurrency: receiver_currency || "EUR",
            senderNumber: sender_number || debtorAccount,
            receiverNumber: receiver_number || creditorAccount,
            amount: Math.round(value * 100) || Math.round(amount * 100),
            loginGuid: author_guid
          }, "Internal");

          if(!amountWithRates) {
            if(res)
              return res.status(200).json({ commission })
            else
              return { commission }
          }
          else if(amountWithRates) {
            const _amountWithRates = +value + +commission
            if(_amountWithRates !== amountWithRates) {
              if(res)
                return res.status(400).json({
                  error: "Failed amount with commission",
                  details: [{ message: "Commission for this transaction was changed. Please try again." }]
                })
              else
                return {
                  error: "Failed amount with commission",
                  details: [{ message: "Commission for this transaction was changed. Please try again." }]
                }
            }
          }
        }
        if(!isNext) {
          if(res)
            return res.status(200).json({ next: true, creditorName })
          else
            return { next: true, creditorName }
        }
        break;
      default:
        break;
    }
    if(!_connection) {
      connection = await db.getConnection();

      await db._transaction.begin(connection);
    }
    else
      connection = _connection

      console.log('2: ', amount ? Math.round(amount * 100) : Math.round(value * 100))
    transaction = await create({
      // paymentTransactionId,
      // paymentInstructionId,
      paymentEndToEndId,
      currency, 
      sender_currency,
      receiver_currency,
      commission,     // in sender account currency
      amount: Math.round(amountWithRates * 100),
      baseAmount: amount ? Math.round(amount * 100) : Math.round(value * 100),
      senderAccount: debtorAccount || sender_number,   // IBAN, not account number (FFCCTRNS)
      receiverAccount: creditorAccount || receiver_number,   // IBAN, not account number (FFCCTRNS)
      senderIBAN: debtorAccount,
      receiverIBAN: creditorAccount,
      authorGuid: author_guid,
      serviceLevel,
      chargeBearer,
      senderName: debtorName,
      senderBIC: debtorBic,
      receiverName: creditorName,
      receiverBIC: creditorBic,
      remittanceInformation,
      authType: req.auth && req.auth.type ? req.auth.type : null,
      OrgnlTxId,
      // paymentCancellationId,
      CxlRsnInf,
      CxlRsnInfAddtInf,
      ClrSysPrtry,
      originatorClient,
      originatorBank,
      originalTxId,
      // returnmentId,
      chrgsInfAmt: Math.round(chrgsInfAmt * 100),
      reasonForReturn,
      isResponse,
      rtrRsnInfAddtInf,
      CxlStsRsnInf,
      CxlStsRsnInfAddtInf,
      test,
      returnImmediately
      // cancellationStsId
    }, type, connection);


    if(transaction._response) {
      const { req, type } = transaction;
      if(!remoteConnection)
        await db._transaction.commit(connection);
      init(req, undefined, () => {}, type);
    }
    else if(transaction.error) {
      throw { 
        message: transaction.errorMessage, 
        error: transaction._error 
      }
    }
    else {
      if(!remoteConnection)
        await db._transaction.commit(connection);

      producer.send({
        topic: transactionProcessing,
        event: UPDATE_TRANSACTION.name,
        payload: { transaction, type, checking: {} }
      })

      console.log("\nNEW_TRANSACTION" + "\x1b[32m%s\x1b[0m", " (OK)\n")
      if(res)
        res.status(200).json(transaction);
      else
        return { transaction, status: "ok" }
    }
  } 
  catch (err) {
    if(connection && !remoteConnection)
      await db._transaction.rollback(connection);
    if(err.message ? !err.message.producer : true) {
      producer.send({
        topic: transactionProcessing,
        event: UPDATE_TRANSACTION.name,
        payload: { transaction: (transaction && transaction.error) ? {} : transaction, type, error: err }
      })

      console.log("\nNEW_TRANSACTION" + "\x1b[31m%s\x1b[0m", ` (FAILED: ${JSON.stringify(err)})\n`);
    }
    if(err._error)
      next(err._error)
    next(err)
  }
  finally {
    if(connection && !remoteConnection)
      connection.release();
  }
}


module.exports = init;