const db = require("../../../../../helpers/db/bankApi");


module.exports = async(data, type, connection) => {
  switch(type) {
    case "FFCCTRNS":
      if(data.currency !== "EUR") {
        const [ { rate, date } ] = await db.currency_rate.get({
          input_currency_code: data.currency
        }, connection);
        const oldAmount = data.baseAmount;
        const oldCurrency = data.currency;
        data.currency = "EUR";
        data.baseAmount = Math.round(data.baseAmount / rate);
        
        await db.ffcctrns_currency_exchange.upsert({
          input_transaction_history_guid: data.guid,
          input_from_currency_code: oldCurrency,
          input_to_currency_code: "EUR",
          input_rate: rate,
          input_date: date.toJSON().slice(0,10),
          input_from_amount: oldAmount,
          input_to_amount: data.baseAmount,
          input_author_guid: data.authorGuid
        }, connection);
      }
      await db.ffcctrns_transaction.upsert({
        input_guid: data.guid,
        input_cdttrftxinfintrbksttlmamt: data.baseAmount
      }, connection);
      break;
    case "Internal":
      data.receiverAmount = data.senderAmount;
      if(data.senderCurrency !== data.receiverCurrency) {        
        const [ { rate: senderCurrencyRate, date } ] = await db.currency_rate.get({
          input_currency_code: data.senderCurrency
        }, connection);
        let receiverCurrencyRate = 1;
        if(data.receiverCurrency !== "EUR")
          [ { rate: receiverCurrencyRate } ] = await db.currency_rate.get({
            input_currency_code: data.receiverCurrency
          }, connection);
        const oldAmount = data.baseAmount;
        const rate = (senderCurrencyRate / receiverCurrencyRate).toFixed(5);
        data.receiverAmount = Math.round(data.baseAmount / rate);

        await db.internal_currency_exchange.upsert({
          input_transaction_history_guid: data.guid,
          input_from_currency_code: data.senderCurrency,
          input_to_currency_code: data.receiverCurrency,
          input_rate: rate,
          input_date: date.toJSON().slice(0,10),
          input_from_amount: oldAmount,
          input_to_amount: data.receiverAmount,
          input_author_guid: data.authorGuid
        }, connection);
      }

      await db.internal_transaction_history.upsert({
        input_guid: data.guid,
        input_value: data.receiverAmount
      }, connection);
      break;
    default: throw("Invalid type")
  }
  return data;
}