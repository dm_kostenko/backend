const { CurrencyExchange } = require("../../../../helpers/kafka");
const controllers = require("./controllers");
// const events = require("../../events");
const consumerTopics = require("./config").topics.consumer;
let consumerEvents = require("./config").events.consumer;
// consumerEvents = consumerEvents.map(event => ( events[event] ));
const { printEvent } = require("../../helpers");

const consumer = new CurrencyExchange.Consumer({ topics: consumerTopics, name: "currencyExchangeConsumer" });
try {
  consumer.onMessage((message) => {
    if(Object.keys(consumerEvents).includes(message.key) && controllers[message.key]) {
      printEvent(message)
      controllers[message.key](JSON.parse(message.value));
    }
  });
}
catch(err) {
  throw(err)
}

