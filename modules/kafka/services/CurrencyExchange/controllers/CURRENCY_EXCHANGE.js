const { CurrencyExchange } = require("../../../../../helpers/kafka");
const { transactionProcessing } = require("../config").topics.producer;
const { UPDATE_TRANSACTION } = require("../config").events.producer;
const { exchangeCurrency } = require("../helpers")
const db = require("../../../../../helpers/db/bankApi");

const producer = new CurrencyExchange.Producer();

module.exports = async({ transaction, type, checking, error }) => {
  let connection;
  try {
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    transaction = await exchangeCurrency(transaction, type, connection);
    checking.currencyExchange = true;

    producer.send({
      topic: transactionProcessing,
      event: UPDATE_TRANSACTION,
      payload: { transaction, type, checking, error }
    });

    await db._transaction.commit(connection);
  }
  catch(err) {    
    if(connection)
      await db._transaction.rollback(connection);
    producer.send({
      topic: transactionProcessing,
      event: UPDATE_TRANSACTION,
      payload: { transaction, type, checking, error: err.toString() }
    });
  }
  finally {
    if(connection)
      connection.release();
  }
}