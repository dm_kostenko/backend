const { 
  currencyExchange,
  transactionProcessing
} = require("../../topics");
const { 
  CURRENCY_EXCHANGE, 
  UPDATE_TRANSACTION
} = require("../../events");

module.exports = {
  events: {
    consumer: {
      CURRENCY_EXCHANGE: CURRENCY_EXCHANGE.name
    },
    producer: {
      UPDATE_TRANSACTION: UPDATE_TRANSACTION.name
    }
  },
  topics: {
    consumer: [
      currencyExchange,
    ],
    producer: {
      transactionProcessing
    }
  }
}