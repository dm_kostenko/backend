const db = require("../../../../../helpers/db/bankApi");


module.exports = async(message, type) => {
  const amount = message.transactions[0].amount * 100;
  const statusRules = await db.transaction_rule.get({
    input_direction: "incoming",
    input_transaction_type: type.toUpperCase(),
    input_rule_type: "status"
  });

  const successStatus = statusRules.find(rule => rule.name === "success_status");
  const failedStatus = statusRules.find(rule => rule.name === "failed_status");
  const below = statusRules.find(rule => rule.name === "below");
  
  return amount < Math.round(below.value) ? successStatus.value : failedStatus.value;
}