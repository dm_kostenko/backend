const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs

const importBALANCE = async (message, data, connection) => {
  try {
    const { messageHeader, transactions } = message;
    for(let i = 0; i < transactions.length; i++) {
      await db.balance.upsert({
        input_sender: messageHeader.sender,
        input_receiver: messageHeader.receiver,
        input_priority: messageHeader.priority,
        input_msgid: messageHeader.msgId,
        input_date: messageHeader.date,
        input_system: messageHeader.system,
        input_docid: transactions[i].docId,
        input_datestype: transactions[i].dateType,
        input_type: transactions[i].type,
        input_businessarea: messageHeader.businessArea,
        input_bic: transactions[i].bic,
        input_account: transactions[i].iban,
        input_currency: transactions[i].currency,
        input_openingbalance: Math.round(transactions[i].openingBalance * 100),
        input_openingavailablebalance: Math.round(transactions[i].openingAvailableBalance * 100),
        input_closingbalance: Math.round(transactions[i].closingBalance * 100),
        input_closingavailablebalance: Math.round(transactions[i].closingAvailableBalance * 100),
        input_data: data,
        input_author_guid: author_guid
      }, connection)
    }
  }
  catch(err) {
    throw(err)
  }
}

module.exports = importBALANCE;