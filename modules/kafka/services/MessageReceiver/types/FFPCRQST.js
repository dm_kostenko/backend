const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs
// const { EXTERNAL_TRANSACTION_WILL_BE_RETURNED } = require("../../../../../helpers/constants/statuses").externalTransaction;
const { XML_IMPORT_IMPORTED } = require("../../../../../helpers/constants/statuses").xmlImport;

const importFFPCRQST = async ({ messageHeader, transactions: [ transaction ] }, originalXml, connection) => {
  try {
    const status = XML_IMPORT_IMPORTED;
    await db.ffpcrqst_transaction.createIncomingTransaction({
      input_sender_iban: transaction.debtorAccount,
      input_receiver_iban: transaction.creditorAccount,
      input_currency: "EUR",
      input_value: transaction.OrgnlIntrBkSttlmAmt * 100,
      input_commission: "0",
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: messageHeader.system,
      input_docid: messageHeader.msgId, 
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      // input_reportmsgid,
      // input_reportdttm,
      // input_grsts,
      // input_rsnprty,
      // input_txsts,
      // input_rsncd,
      input_data: originalXml,
      // input_assgnmtid,
      // input_assgnmtcredttm,
      // input_ctrldatanboftxs,
      input_undrlygtxinfcxlid: transaction.CancellationTrId,
      input_undrlygtxinforgnlgrpinforgnlmsgid: transaction.OrgnlMsgId,    // FFCCTRNS msg id
      input_undrlygtxinforgnlgrpinforgnlmsgnmid: transaction.OrgnlMsgNmId, 
      input_undrlygtxinforgnlgrpinforgnlmsgendtoendid: transaction.OrgnlEndToEndId,
      input_undrlygtxinforgnlgrpinforgnltxid: transaction.OrgnlTxId,
      input_undrlygtxinforgnlintrbksttlmamt: transaction.OrgnlIntrBkSttlmAmt * 100,
      input_undrlygtxinforgnlintrbksttlmdt: transaction.OrgnlIntrBkSttlmDt,
      input_undrlygtxinfcxlrsninforgtrnm: transaction.originatorClient,
      input_undrlygtxinfcxlrsninforgtridorgidbicorbei: transaction.originatorBank,
      input_undrlygtxinfcxlrsninfrsncd: transaction.CxlRsnInf === "DUPL" ? transaction.CxlRsnInf : undefined,
      input_undrlygtxinfcxlrsninfrsnprty: transaction.CxlRsnInf !== "DUPL" ? transaction.CxlRsnInf : undefined,
      input_undrlygtxinfcxlrsninfrsnaddtinf: transaction.CxlRsnInf === "FRAD" ? transaction.CxlRsnInfAddtInf : undefined,
      // input_undrlygtxinforgnltxref,
      input_undrlygtxinforgnltxrefsttlminfsttlmmtd: transaction.SttlmMtd,
      input_undrlygtxinforgnltxrefsttlminfclrsysprtry: transaction.ClrSysPrtry,
      input_undrlygtxinforgnltxrefpmttpinfsvclvlcd: transaction.serviceLevel,
      input_undrlygtxinforgnltxrefdbtrnm: transaction.debtorName,
      input_undrlygtxinforgnltxrefdbtracctidiban: transaction.debtorAccount,
      input_undrlygtxinforgnltxrefdbtragtfininstnidbic: transaction.debtorBic,
      input_undrlygtxinforgnltxrefcdtragtfininstnidbic: transaction.creditorBic,
      input_undrlygtxinforgnltxrefcdtrnm: transaction.creditorName,
      input_undrlygtxinforgnltxrefcdtracctidiban: transaction.creditorAccount,
      input_status: status,
      input_author_guid: author_guid
    }, connection);

    switch(transaction.CxlRsnInf) {
      case "DUPL":
        // checks
        break;
      case "TECH":
        // checks
        break;
      case "FRAD":
        // checks
        break;
      default:
        break;
    }

    // const [{ guid }] = await db.ffcctrns_transaction.get({
    //   input_msgid: transaction.OrgnlMsgId
    // });

    // await db.external_transaction_history.upsert({
    //   input_guid: guid,
    //   input_status: EXTERNAL_TRANSACTION_WILL_BE_RETURNED
    // }, connection);

    // return {
    //   type: "PRTRN",
    //   req: {
    //     body: {
    //       originalTxId: guid,
    //       originatorClient: "TBF Finance, UAB",
    //       reasonForReturn: "FOCR",
    //       chrgsInfAmt: "0",
    //       rtrRsnInfAddtInf: "Return after cancellation request",
    //       isResponse: true
    //     },
    //     auth: {
    //       loginGuid: author_guid
    //     }
    //   }
    // }
  }
  catch(err) {
    throw(err)
  }

};

module.exports = importFFPCRQST;