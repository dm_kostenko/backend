const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs

const importBTCDCNTF = async (message, connection) => {
  try {
    const { messageHeader, transactions, groupHeader, accountInfo } = message;
    transactions.forEach(async(doc) => {
      try {
        await db.btcdcntf.upsert({
          input_sender: messageHeader.sender,
          input_receiver: messageHeader.receiver,
          input_priority: messageHeader.priority,
          input_msgid: messageHeader.msgId,
          input_date: messageHeader.date,
          input_system: messageHeader.system,
          input_businessarea: messageHeader.businessArea,
          input_accountid: accountInfo.Id,
          input_accountiban: accountInfo.iban,
          input_accountcurrency: accountInfo.currency,
          input_instrid: doc.instrId,
          input_pmtinfid: doc.pmtInfId,
          input_endtoendid: doc.endToEndId,
          input_transactionid: doc.transactionId,
          input_amount: Math.round(doc.amount * 100),
          input_creditdebitindex: doc.creditDebitIndex,
          input_domaincode: doc.domainCode,
          input_domainfamilycode: doc.domainFamilyCode,
          input_domainsubfamilycode: doc.domainSubFamilyCode,
          input_initgptybic: doc.initgPtyBIC,
          input_debtoriban: doc.debtorIban,
          input_creditoriban: doc.creditorIban,
          input_debtorbic: doc.debtorBic,
          input_creditorbic: doc.creditorBic,
          input_acceptancedatetime: doc.acceptanceDateTime,
          input_banksettlementdate: doc.bankSettlementDate,
          input_transactiondatetime: doc.transactionDateTime,
          input_author_guid: author_guid
        }, connection)
      }
      catch(err) {
        throw(err)
      }
    })
  }
  catch(err) {
    throw(err)
  }
}

module.exports = importBTCDCNTF;