const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs

const importDEBIT = async (message, data, connection) => {
  try {
    const { messageHeader, transactions } = message;
    transactions.forEach(async(doc) => {
      try {
        await db.debit.upsert({
          input_sender: messageHeader.sender,
          input_receiver: messageHeader.receiver,
          input_priority: messageHeader.priority,
          input_msgid: messageHeader.msgId,
          input_date: messageHeader.date,
          input_system: messageHeader.system,
          input_businessarea: messageHeader.businessArea,
          input_docid: doc.docId,
          input_transactionbusinessarea: doc.businessArea,
          input_referencetype: doc.referenceType,
          input_referencesender: doc.referenceSender,
          input_referencedocid: doc.referenceDocId,
          input_payerbic: doc.payerBIC,
          input_payeraccount: doc.payerAccount,
          input_payerccodeicode: doc.payerCcodeIcode,
          input_payername: doc.payerName,
          input_payeebic: doc.payeeBIC,
          input_payeeaccount: doc.payeeAccount,
          input_payeeccodeicode: doc.payeeCcodeIcode,
          input_payeename: doc.payeeName,
          input_currency: doc.currency,
          input_amount: (doc.amount * 100).toFixed(0),
          input_remittanceinformationdate: doc.remittanceInformationDate,
          input_remittanceinformationid: doc.remittanceInformationId,
          input_remittanceinformationpaymentreason: doc.remittanceInformationPaymentReason,
          input_data: data,
          input_author_guid: author_guid
        }, connection)
      }
      catch(err) {
        throw(err)
      }
    })
  }
  catch(err) {
    throw(err)
  }
}

module.exports = importDEBIT;