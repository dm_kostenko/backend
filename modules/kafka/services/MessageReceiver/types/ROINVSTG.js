const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs
const { XML_IMPORT_IMPORTED } = require("../../../../../helpers/constants/statuses").xmlImport;


const importROINVSTG = async ({ messageHeader, assgmntHeader, transactions: [ transaction ] }, originalXml, connection) => {
  try {
    const status = XML_IMPORT_IMPORTED;
  
    await db.roinvstg_transaction.createIncomingTransaction({
      input_sender_iban: transaction.creditorAccount,
      input_receiver_iban: transaction.debtorAccount,
      input_currency: "EUR",
      input_value: transaction.orgnlIntrBkSttlmAmt * 100,
      input_commission: "0",
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: messageHeader.system,
      input_docid: messageHeader.msgId,
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      // input_reportmsgid,
      // input_reportdttm,
      // input_grsts,
      // input_rsnprty,
      // input_txsts,
      // input_rsncd,
      input_data: originalXml,
      input_assgnmtid: assgmntHeader.Id,
      input_assgnmtassgnragtfininstnidbic: assgmntHeader.AssignerBic,
      input_assgnmtassgneagtfininstnidbic: assgmntHeader.AssigneeBic,
      input_assgnmtcredttm: assgmntHeader.CreDtTm,
      input_stsconf: assgmntHeader.StsConf,
      input_cxldtlstxinfandstscxlstsid: transaction.cancellationStsId,
      input_cxldtlstxinfandstsorgnlgrpinforgnlmsgid: transaction.orgnlMsgId,
      input_cxldtlstxinfandstsorgnlgrpinforgnlmsgnmid: transaction.orgnlMsgNmId,
      input_cxldtlstxinfandstsorgnlendtoendid: transaction.orgnlEndToEndId,
      input_cxldtlstxinfandstsorgnltxid: transaction.orgnlTxId,
      input_cxldtlstxinfandststxcxlsts: transaction.transactionCancellationSts,
      input_cxldtlstxinfandstscxlstsrsninforgtrnm: transaction.originatorClient,
      input_cxldtlstxinfandstscxlstsrsninforgtridorgidbicorbei: transaction.originatorBank,
      input_cxldtlstxinfandstscxlstsrsninfrsncd: [ "LEGL", "CUST" ].includes(transaction.reasonForCancellation) ? transaction.reasonForCancellation : undefined,
      input_cxldtlstxinfandstscxlstsrsninfrsnprtry: ![ "LEGL", "CUST" ].includes(transaction.reasonForCancellation) ? transaction.reasonForCancellation : undefined,
      input_cxldtlstxinfandstscxlstsrsninfaddtlinf: transaction.addInfForCancellation,
      input_cxldtlstxinfandstsorgnltxrefintrbksttlmamt: transaction.orgnlIntrBkSttlmAmt * 100,
      input_cxldtlstxinfandstsorgnltxrefintrbksttlmdt: transaction.orgnlIntrBkSttlmDt,
      input_cxldtlstxinfandstsorgnltxrefsttlminfsttlmmtd: transaction.sttlmMtd,
      input_cxldtlstxinfandstsorgnltxrefsttlminfclrsysprtry: transaction.clrSysPrtry,
      input_cxldtlstxinfandstsorgnltxrefpmttpinfsvclvlcd: transaction.serviceLevel,
      input_cxldtlstxinfandstsorgnltxrefdbtrnm: transaction.debtorName,
      input_cxldtlstxinfandstsorgnltxrefdbtracctidiban: transaction.debtorAccount,
      input_cxldtlstxinfandstsorgnltxrefdbtragtfininstnidbic: transaction.debtorBic,
      input_cxldtlstxinfandstsorgnltxrefcdtragtfininstnidbic: transaction.creditorBic,
      input_cxldtlstxinfandstsorgnltxrefcdtrnm: transaction.creditorName,
      input_cxldtlstxinfandstsorgnltxrefcdtracctidiban: transaction.creditorAccount,
      input_status: status,
      input_author_guid: author_guid
    }, connection);
  }
  catch(err) {
    throw(err)
  }
}

module.exports = importROINVSTG;