const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs
const { XML_IMPORT_IMPORTED } = require("../../../../../helpers/constants/statuses").xmlImport;
const { creditTransfer } = require("../../../../../helpers/transactions")
const { iban } = require("../../../../../config");

const importPRTRN = async (message, originalXml, connection) => {
  try {
    const { messageHeader, groupHeader, transactions: [ transaction ] } = message;
    const status = XML_IMPORT_IMPORTED;
    await db.prtrn_transaction.createIncomingTransaction({
      input_version: messageHeader.version,
      input_sender_iban: transaction.originalDebtorAccount, 
      input_receiver_iban: transaction.originalCreditorAccount,
      input_currency: "EUR",
      input_value: transaction.rtrdIntrBkSttlmAmt * 100,
      input_commission: "0",
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId, 
      input_system: messageHeader.system,
      input_docid: messageHeader.msgId, 
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      input_grphdrmsgid: groupHeader.MsgId,
      input_grphdrcredttm: groupHeader.CreDtTm, 
      input_grphdrnboftxs: groupHeader.NbOfTxs,
      input_grphdrttlintrbksttlmamt: groupHeader.TtlRtrdIntrBkSttlmAmt * 100, 
      input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
      input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
      input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
      input_grphdrinstgagfininstnidbic: groupHeader.FinInstnIdBIC,
      input_data: originalXml,
      input_txinfrtrid: transaction.returnmentId,
      input_txinforgnlgrpinforgnlmsgid: transaction.originalMsgId,
      input_txinforgnlgrpinforgnlmsgnmid: transaction.originalMsgNmId,
      input_txinforgnlinstrid: transaction.originalInstrId,
      input_txinforgnlendtoendid: transaction.originalEndToEndId,
      input_txinforgnltxid: transaction.originalTxId,
      input_txinforgnlintrbksttlmamt: transaction.originalIntrBkSttlmAmt * 100,
      input_txinfrtrdintrbksttlmamt: transaction.rtrdIntrBkSttlmAmt * 100,
      input_txinfrtrdinstdamt: transaction.rtrdInstdAmt * 100,
      input_txinfchrgbr: transaction.chargeBearer,
      input_txinfchrgsinfamt: transaction.сhrgsInfAmt * 100,
      input_txinfrtrrsninforgtrnm: transaction.originatorClient,
      input_txinfrtrrsninforgtridorgidbicorbei: transaction.originatorBank,
      input_txinfrtrrsninfrsncd: transaction.reasonForReturn,
      input_txinfrtrrsninfaddtinf: transaction.rtrRsnInfAddtInf,
      input_txinforgnltxrefintrbksttlmdt: transaction.originalSettlementDate,
      input_txinforgnltxrefsttlminfsttlmmtd: transaction.originalSettlementMethod,
      input_txinforgnltxrefpmttpinfsvclvlcd: transaction.originalServiceLevel,
      input_txinforgnltxrefdbtrnm: transaction.originalDebtorName,
      input_txinforgnltxrefdbtracctidiban: transaction.originalDebtorAccount,
      input_txinforgnltxrefdbtragtfininstnidbic: transaction.originalDebtorBic,
      input_txinforgnltxrefcdtrnm: transaction.originalCreditorName,
      input_txinforgnltxrefcdtragtfininstnidbic: transaction.originalCreditorBic,
      input_txinforgnltxrefcdtracctidiban: transaction.originalCreditorAccount,
      // input_txsts,
      // input_rsnprty,
      // input_rsncd,
      input_status: status,
      input_author_guid: author_guid
    }, connection);

    const [{ guid }] = await db.ffcctrns_transaction.get({
      input_msgid: transaction.originalMsgId
    })

    const [{ currency, sender_iban, receiver_iban }] = await db.external_transaction_history.get({
      input_guid: guid
    });

    let isCommissionTransfer = false;
    if([ iban.settlement.internal, iban.customer.internal ].includes(sender_iban) || [ iban.settlement.internal, iban.customer.internal ].includes(receiver_iban))
      isCommissionTransfer = true

    await creditTransfer.return({
      transactionId: transaction.originalMsgId,
      senderIban: sender_iban,
      amount: transaction.rtrdIntrBkSttlmAmt * 100,
      currency,
      isCommissionTransfer
    }, connection);
  }
  catch(err) {
    throw(err)
  }
};

module.exports = importPRTRN;