module.exports = {
  importFFCCTRNS: require("./FFCCTRNS"),
  importFFPSRPRT: require("./FFPSRPRT"),
  importPRTRN: require("./PRTRN"),
  importFFPCRQST: require("./FFPCRQST"),
  importROINVSTG: require("./ROINVSTG"),
  importBALANCE: require("./BALANCE"),
  importBTCDCNTF: require("./BTCDCNTF"),
  importDEBIT: require("./DEBIT"),
  importDICTIONR: require("./DICTIONR")
}