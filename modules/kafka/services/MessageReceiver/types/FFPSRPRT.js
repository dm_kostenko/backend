
const { XML_IMPORT_IMPORTED } = require("../../../../../helpers/constants/statuses").xmlImport;
const { 
  creditTransfer, 
  paymentCancellation, 
  paymentReturn,
  resolutionOfInvestigation
} = require("../../../../../helpers/transactions");
const db = require("../../../../../helpers/db/bankApi");
const { bic, iban } = require("../../../../../config");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs



const importFFPSRPRT = async ({ messageHeader, /* groupHeader, */ transactions, OrgnlGrpInfAndSts }, connection) => {
  try {
    const [ /* FFPSRPRT, */ FFCCTRNS, FFPCRQST, PRTRN, ROINVSTG ] = 
      [ /* "pacs.002.001.03", */ "pacs.008.001.02", "camt.056.001.01", "pacs.004.001.02", "camt.029.001.03" ];
    let upsertedTransaction;
    let isCommissionTransfer;
    if (OrgnlGrpInfAndSts.OrgnlMsgNmId === FFCCTRNS) {
      const orgnlMsgId = OrgnlGrpInfAndSts.OrgnlMsgId === "NOTPROVIDED" ? transactions[0].paymentTransactionId : OrgnlGrpInfAndSts.OrgnlMsgId

      let [ transaction ] = await db.ffcctrns_transaction.get({
        input_msgid: orgnlMsgId
      })

      if (!transaction)
        throw { message: `Transaction for msgId ${orgnlMsgId} didn't found in database` };

      if((transaction.sender === transaction.receiver) && (transaction.sender === bic))    // commission transfer returning
        return      
      
      // if (transaction.grsts)
      //   throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} is already has status ${transaction.grsts}` };
      let { guid } = transaction;

      [ upsertedTransaction ] = await db.ffcctrns_transaction.upsert({
        input_guid: transaction.guid,
        input_msgid: orgnlMsgId, 
        input_grsts: OrgnlGrpInfAndSts.GrpSts,
        input_rsnprty: OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry,
        input_grphdrmsgid: orgnlMsgId,
        input_reportmsgid: messageHeader.msgId,
        input_reportdttm: messageHeader.date,
        input_txsts: transactions[0] ? transactions[0].transactionStatus : undefined,
        input_rsncd: transactions[0] ? transactions[0].reasonCode : undefined,
        input_author_guid: author_guid,
      }, connection);

      let [ ffcctrnsTransactionRecord ] = await db.external_transaction_history.get({
        input_guid: guid
      })

      let { currency, value, commission, sender_iban, receiver_iban } = ffcctrnsTransactionRecord;

      isCommissionTransfer = false;
      if([ iban.settlement.internal, iban.customer.internal ].includes(sender_iban) || [ iban.settlement.internal, iban.customer.internal ].includes(receiver_iban))
        isCommissionTransfer = true;

      switch (OrgnlGrpInfAndSts.GrpSts) {

      // FFCCTRNS is accepted
      case "ACCP":
        console.log('accp')
        console.log(guid)
        await creditTransfer.accept({ 
          transactionId: guid 
        }, connection);
        break;

      // FFCCTRNS is settled
      case "ACSC":
        await creditTransfer.set({ 
          transactionId: guid,
          senderIban: sender_iban,
          amount: Math.round(value) + Math.round(commission),
          currency,
          isCommissionTransfer
        }, connection)
        break;
      case "PART":
        // Request for cancellation is transferred to the receiver:
        if(transactions[0].transactionStatus === "PDNG" && 
          OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry === "RCLL") {

          // Some logic for pending FFPCRQST
          
        }

        break;
      case "RJCT":
        // Cancel after FFPCRQST request
        if(transactions[0].transactionStatus === "RJCT" &&
          [ transactions[0].reasonCode, transactions[0].reasonPrtry ].includes("CANC")) {
          await creditTransfer.cancel({
            transactionId: guid,
            senderIban: sender_iban,
            amount: Math.round(value) + Math.round(commission),
            currency,
            isCommissionTransfer
          }, connection);

          // Set new FFPCRQST status ?

        }
        // Reject FFCCTRNS
        else
          await creditTransfer.reject({
            transactionId: guid,
            senderIban: sender_iban,
            amount: Math.round(value) + Math.round(commission),
            currency,
            isCommissionTransfer
          }, connection);
        break;  
      default:
        break;
      }
    } 
    else if (OrgnlGrpInfAndSts.OrgnlMsgNmId === FFPCRQST) {

      const [ transaction ] = await db.ffpcrqst_transaction.get({
        input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId
      })
      if (!transaction)
        throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} didn't found in database` };
      
      // if (transaction.grsts)
      //   throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} has already status ${transaction.grsts}` };
      
      const { 
        guid, 
        // undrlygtxinforgnltxrefdbtracctidiban: sender_iban, 
        // undrlygtxinforgnlintrbksttlmamt: value, 
        // undrlygtxinforgnlgrpinforgnlmsgid: cancelledMsgId,
        // currency = "EUR" 
      } = transaction;

      [ upsertedTransaction ] = await db.ffpcrqst_transaction.upsert({
        input_guid: transaction.guid,
        input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId, 
        input_grsts: OrgnlGrpInfAndSts.GrpSts,
        input_rsnprty: OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry,
        // input_assgnmtid: OrgnlGrpInfAndSts.OrgnlMsgId,
        input_reportmsgid: messageHeader.msgId,
        // input_reportdttm: messageHeader.date,
        // input_undrlygtxinfcxlid: transactions[0] ? transactions[0].paymentTransactionId : undefined,
        // input_txsts: transactions[0] ? transactions[0].transactionStatus : undefined, 
        // input_rsncd: transactions[0] ? transactions[0].reasonCode : undefined,
        input_status: XML_IMPORT_IMPORTED,
        input_author_guid: author_guid,
      }, connection);

      switch (OrgnlGrpInfAndSts.GrpSts) {
      case "ACCP":
        paymentCancellation.accept({ 
          transactionId: guid 
        }, connection);
        break;    
      case "RJCT":
        paymentCancellation.reject({ 
          transactionId: guid 
        }, connection);
        break;
    
      default:
        break;
      }
    }
    else if (OrgnlGrpInfAndSts.OrgnlMsgNmId === PRTRN) {

      let [ transaction ] = await db.prtrn_transaction.get({
        input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId
      })
      // console.log(transaction)
      if (!transaction) {
        [ transaction ] = await db.prtrn_transaction.get({
          input_txinforgnltxid: transactions[0].paymentTransactionId
        })
        if(!transaction)
          throw { message: `Transaction for txId ${transactions[0].paymentTransactionId} didn't found in database` };
      }
      
      // if (transaction.grsts !== "ACCP")
      //   throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} has status ${transaction.grsts}, not ACCP` };

      let { 
        guid, 
        txinforgnltxrefcdrracctidiban: sender_iban, 
        // grphdrttlintrbksttlmamt: value,
        txinforgnlgrpinforgnlmsgid: returnedMsgId,
        // currency = "EUR" 
      } = transaction;

      
      [ upsertedTransaction ] = await db.prtrn_transaction.upsert({
        input_guid: guid,
        // input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId, 
        input_grsts: OrgnlGrpInfAndSts.GrpSts,
        input_rsnprty: OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry,
        // input_assgnmtid: OrgnlGrpInfAndSts.OrgnlMsgId,
        input_reportmsgid: messageHeader.msgId,
        input_reportdttm: messageHeader.date,
        input_txinfrtrid: transactions[0] ? transactions[0].paymentTransactionId : undefined,
        input_txsts: transactions[0] ? transactions[0].transactionStatus : undefined, 
        input_rsncd: transactions[0] ? transactions[0].reasonCode : undefined,
        input_status: XML_IMPORT_IMPORTED,
        input_author_guid: author_guid,
      }, connection);

      const [ prtrnTransactionRecord ] = await db.external_transaction_history.get({
        input_guid: transaction.guid
      });

      const [ originalTransaction ] = await db.external_transaction_history.get({
        input_msgid: returnedMsgId
      });

      const { txinfrtrrsninfrsncd: reason } = upsertedTransaction;
      const { currency, value, commission, receiver_iban } = prtrnTransactionRecord;
      sender_iban = prtrnTransactionRecord.sender_iban
      
      isCommissionTransfer = false;
      if([ iban.settlement.internal, iban.customer.internal ].includes(sender_iban) || [ iban.settlement.internal, iban.customer.internal ].includes(receiver_iban))
        isCommissionTransfer = true;

      switch (OrgnlGrpInfAndSts.GrpSts) {
      // PRTRN is accepted
      case "ACCP":
        console.log('accp')
        console.log(guid)
        paymentReturn.accept({ 
          transactionId: guid 
        }, connection);
        break;
      // PRTRN is settled
      case "ACSC":
        paymentReturn.set({ 
          transactionId: guid,
          senderIban: sender_iban,
          amount: Math.round(value) - Math.round(originalTransaction.commission) + Math.round(commission),
          currency,
          returnedMsgId,
          reason,
          isCommissionTransfer
        }, connection)
        break;
      // PRTRN is rejected
      case "RJCT":
        paymentReturn.reject({ 
          transactionId: guid,
          senderIban: sender_iban,
          amount: Math.round(value) - Math.round(originalTransaction.commission) + Math.round(commission),
          currency,
          reason,
          isCommissionTransfer
        }, connection)
        break;
      default:
        break;
      }

    }
    else if (OrgnlGrpInfAndSts.OrgnlMsgNmId === ROINVSTG) {
      const [ transaction ] = await db.roinvstg_transaction.get({
        input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId
      })

      if (!transaction)
        throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} didn't found in database` };

      const { guid } = transaction;
      
      [ upsertedTransaction ] = await db.roinvstg_transaction.upsert({
        input_guid: guid,
        input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId, 
        input_rsnprty: OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry || OrgnlGrpInfAndSts.StsRsnInf.Rsn.Cd,
        // input_assgnmtid: OrgnlGrpInfAndSts.OrgnlMsgId,
        input_reportmsgid: messageHeader.msgId,
        // input_reportdttm: messageHeader.date,
        // input_undrlygtxinfcxlid: transactions[0] ? transactions[0].paymentTransactionId : undefined,
        // input_txsts: transactions[0] ? transactions[0].transactionStatus : undefined, 
        // input_rsncd: transactions[0] ? transactions[0].reasonCode : undefined,
        input_status: XML_IMPORT_IMPORTED,
        input_author_guid: author_guid
      }, connection);
      
      switch (OrgnlGrpInfAndSts.GrpSts) {

      // ROINVSTG is accepted
      case "ACCP":
        resolutionOfInvestigation.accept({ 
          transactionId: guid,
          investigatedMsgId: upsertedTransaction.cxldtlstxinfandstsorgnlgrpinforgnlmsgid
        }, connection);
        break;

      // ROINVSTG is rejected
      case "RJCT":
        resolutionOfInvestigation.reject({ 
          transactionId: guid
        }, connection)
        break;
      default:
        break;
      }
    }

    else 
      throw { message: `FFPSRPRT for ${OrgnlGrpInfAndSts.OrgnlMsgNmId} cannot be imported yet` };

    if (!upsertedTransaction)
      throw { message: "Transaction is undefined" };
  }
  catch(err) {
    console.log(err)
    throw(err)
  }
};

module.exports = importFFPSRPRT;