const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs

const importDICTIONR = async (message, connection) => {
  try {
    const { transactions } = message;
    for(let i = 0; i < transactions.length; i++) {
      try {
        await db.institute.upsert({
          input_bic: transactions[i].BIC,
          input_code: transactions[i].Code,
          input_name: transactions[i].Name,
          input_author_guid: author_guid
        }, connection)
      }
      catch(err) {
        throw(err)
      }
    }
  }
  catch(err) {
    throw(err)
  }
}

module.exports = importDICTIONR;