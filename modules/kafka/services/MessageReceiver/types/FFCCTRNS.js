
const db = require("../../../../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs
const { 
  XML_IMPORT_ERRORED,
  XML_IMPORT_DECLINED,
  XML_IMPORT_IMPORTED
} = require("../../../../../helpers/constants/statuses").xmlImport;
const { creditTransfer } = require("../../../../../helpers/transactions");
const { checkRates } = require("../../../helpers")
const { bic, iban } = require("../../../../../config");


const validate = async({
  receiverIban,
  receiverName,
  senderIban,
  senderName,
  receiverBIC,
  senderBIC
}) => {  
  // if((senderBIC === receiverBIC) && (senderBIC === bic))  // commission transfer
  //   return {
  //     commissionTransfer: true
  //   }
  if(!receiverIban)
    return { 
      validation: false,
      code: "BE04",
      message: "Missing Creditor Address"
    }

  const [ receiverAccount ] = await db.account.get({
    input_iban: receiverIban
  });
  if(!receiverAccount) {
    if([ iban.customer.internal, iban.settlement.internal ].includes(receiverIban))
      return {
        validation: true,
        noReceiverAccount: true
      }
    else
      return { 
        validation: false,
        code: "AC01",
        message: "Incorrect Account Number or the Account Number does not exist",
      }
  }
  /*
  *** DOES NOT IMPLEMENTED YET ***

  if(receiverAccount.status === "Closed")
    return {
      validation: false,
      code: "AC04",
      message: "The account is closed"
    }
  if(receiverAccount.status === "Blocked")
    return {
      validation: false,
      code: "AC06",
      message: "The account is blocked; the reason is not specified"
    }
  */
  if(receiverBIC !== bic)     // our BIC
    return { 
      validation: false,
      code: "RC01",
      message: "Bank Identifier Incorrect"
    }
  if(!senderName || !senderIban)
    return {
      validation: false,
      code: "RR02",
      message: "Regulatory reason. There is no full name or address of the debtor"
    }
  if(!receiverName)
    return {
      validation: false,
      code: "RR03",
      message: "Regulatory reason. There is no full name or address of the creditor"
    }  
  return { validation: true }
}


const importFFCCTRNS = async ({ messageHeader, groupHeader, transactions, status }, originalXml/*, connection*/) => {

  let returnArray = []

  if(status === XML_IMPORT_DECLINED)
    returnArray.push({
      type: "PRTRN",
      req: {
        body: {
          originalTxId: messageHeader.msgId,
          originatorClient: "TBF Finance, UAB",
          reasonForReturn: "MS03",    // Reason not specified
          returnImmediately: true
        }
      }
    })
  else {
      console.log(transactions.length)
      for(let i = 0; i < transactions.length; i++) {

        let result;
        let _transaction;
        let _status;
        let connection;

        try {

          connection = await db.getConnection();
          await db._transaction.begin(connection);

          result = await validate({
            receiverIban: transactions[i].creditorAccount,
            receiverName: transactions[i].creditorName,
            senderIban: transactions[i].debtorAccount,
            senderName: transactions[i].debtorName,
            receiverBIC: transactions[i].creditorBic,
            senderBIC: transactions[i].debtorBic,
          });

          console.log(result)

          let commission = await checkRates({
            amount: Math.round(transactions[i].amount * 100),
            senderAccount: transactions[i].debtorAccount,
            receiverAccount: transactions[i].creditorAccount,
            senderName: transactions[i].debtorName,
            senderBIC: transactions[i].debtorBic,
            receiverName: transactions[i].creditorName,
            receiverBIC: transactions[i].creditorBic,
            currency: "EUR"
          }, "incoming_FFCCTRNS");

          _status = result.validation ? status : XML_IMPORT_ERRORED;

          if(!result.commissionTransfer)
            [ _transaction ] = await db.ffcctrns_transaction.createIncomingTransaction({
              input_sender_iban: transactions[i].debtorAccount,
              input_receiver_iban: transactions[i].creditorAccount,
              input_currency: "EUR",
              input_value: transactions[i].amount * 100,
              input_commission: result.noReceiverAccount ? 0 : commission * 100,
              input_version: messageHeader.version,
              input_sender: messageHeader.sender, 
              input_receiver: messageHeader.receiver,
              input_priority: messageHeader.priority,
              input_msgid: messageHeader.msgId, 
              input_system: messageHeader.system,
              input_docid: messageHeader.msgId, 
              input_datestype: messageHeader.date,
              input_businessarea: messageHeader.businessArea,
              input_grphdrmsgid: groupHeader.MsgId,
              input_grphdrcredttm: groupHeader.CreDtTm, 
              input_grphdrnboftxs: groupHeader.NbOfTxs,
              input_grphdrttlintrbksttlmamt: groupHeader.TtlIntrBkSttlmAmt * 100, 
              input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
              input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
              input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
              input_grphdrinstgagfininstnidbic: groupHeader.FinInstnIdBIC,
              input_data: originalXml,
              input_cdttrftxinfpmtidinstrid: transactions[i].paymentInstructionId, 
              input_cdttrftxinfpmtidendtoendid: transactions[i].paymentEndToEndId, 
              input_cdttrftxinfpmtidtxid: transactions[i].paymentTransactionId, 
              input_cdttrftxinfpmtidpmttpinfsvclvlcd: transactions[i].serviceLevel.Cd, 
              // input_cdttrftxinfpmttpinflclinstrm, 
              // input_cdttrftxinfpmttpinfctgypurp, 
              input_cdttrftxinfintrbksttlmamt: transactions[i].amount * 100, 
              input_cdttrftxinfchrgbr: transactions[i].chargeBearer, 
              // input_cdttrftxinfinstgagtfininstnidbic, 
              input_cdttrftxinfdbtrnm: transactions[i].debtorName,
              input_cdttrftxinfdbtracctidiban: transactions[i].debtorAccount, 
              input_cdttrftxinfdbtragtfininstnidbic: transactions[i].debtorBic, 
              input_cdttrftxinfcdtragtfininstnidbic: transactions[i].creditorBic, 
              input_cdttrftxinfcdtrnm: transactions[i].creditorName, 
              input_cdttrftxinfcdtracctidiban: transactions[i].creditorAccount, 
              // input_cdttrftxinfnnnid, 
              // input_cdttrftxinfpurpcd, 
              // input_cdttrftxinfrmtinfustrd, 
              // input_cdttrftxinfrmtinfstrd, 
              // input_cdttrftxinfrmtinfstrdcdtrrefinftpcdorprtrycd, 
              // input_cdttrftxinfrmtinfstrdcdtrrefinftpissr, 
              input_cdttrftxinfrmtinfstrdcdtrrefinfref: transactions[i].remittanceInformation,
              input_status: _status,
              input_author_guid: author_guid,
            }, connection);

          if(result.validation && !result.noReceiverAccount)
            await creditTransfer.receive({
              receiverIban: transactions[i].creditorAccount,
              amount: (transactions[i].amount * 100) - (commission * 100),
              commission: commission * 100,
              currency: "EUR"
            }, connection);
          else if(!result.commissionTransfer && !result.noReceiverAccount)
            returnArray.push({
              type: "PRTRN",
              req: {
                body: {
                  originalTxId: transactions[i].paymentTransactionId,
                  originatorClient: "TBF Finance, UAB",
                  reasonForReturn: result.code,
                  сhrgsInfAmt: result.code === "FOCR" ? "0" : undefined,
                  rtrRsnInfAddtInf: result.code === "FOCR" ? "additional info" : undefined,
                  returnImmediately: true
                },
                auth: {
                  loginGuid: author_guid
                }
              }
            })
            await db._transaction.commit(connection);
          }
          catch(err) {
            console.log(err)
            if(err.error && err.error.detail.includes("already exists")) {
              if (connection)
                await db._transaction.rollback(connection);
              returnArray.push({
                type: "PRTRN",
                req: {
                  body: {
                    originalTxId: transactions[i].paymentTransactionId,
                    originatorClient: "TBF Finance, UAB",
                    reasonForReturn: "AM05",    // duplication
                    returnImmediately: true
                  }
                },
                err
              })
            }
          }
          finally {
            if (connection)
              connection.release();
          }
        }
      }
    console.log(returnArray)
    return returnArray
}

module.exports = importFFCCTRNS;