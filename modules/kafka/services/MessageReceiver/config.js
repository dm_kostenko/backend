const { receiveMessage } = require("../../topics")
const { RECEIVE_MESSAGE_FAILED, RECEIVE_MESSAGE_SUCCESS } = require("../../events")

module.exports = {
  events: {
    producer: {
      RECEIVE_MESSAGE_FAILED: RECEIVE_MESSAGE_FAILED.name,
      RECEIVE_MESSAGE_SUCCESS: RECEIVE_MESSAGE_SUCCESS.name
    }
  },
  topics: {
    producer: { receiveMessage }
  },
  incomingXmlParser: async (guid, status, { res, next }, producer) => {

    const { RECEIVE_MESSAGE_SUCCESS, RECEIVE_MESSAGE_FAILED } = require("./config").events.producer;
    const { receiveMessage } = require("./config").topics.producer;
    const { FFCCTRNS, FFPCRQST, FFPSRPRT, PRTRN, ROINVSTG, BALANCE, BTCDCNTF, DEBIT, DICTIONR, XmlMessageParser } = require("../../../messages");
    const { 
      XML_IMPORT_IMPORTED, 
      XML_IMPORT_ERRORED,
      XML_IMPORT_RECEIVED 
    } = require("../../../../helpers/constants/statuses").xmlImport;
    const {
      importFFCCTRNS,
      importFFPSRPRT,
      importPRTRN,
      importFFPCRQST,
      importROINVSTG,
      importBALANCE,
      importBTCDCNTF,
      importDEBIT,
      importDICTIONR
    } = require("./types");
    const { getStatus } = require("./helpers");
    const { bic } = require("../../../../config")
    const db = require("../../../../helpers/db/bankApi");
    const { init } = require("..");
    const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs

    let connection;
    let message;
    
    try {
      const [ receivedDocument ] = await db.inbox.get({ input_guid: guid });
      const { data } = receivedDocument;
  
      const messageParser = new XmlMessageParser(unescape(data));
      message = await messageParser.getMessage();
  
      connection = await db.getConnection();
      await db._transaction.begin(connection);
  
      // if(message.messageHeader.receiver !== bic)
      //   throw {
      //     message: `Failed bic: "${message.messageHeader.receiver}" instead of "${bic}"`
      //   }
  
      let obj;
      let arr;
  
  
      switch (message.constructor) {
      case FFCCTRNS:
        if(status)
          message.status = status;
        else
          message.status = await getStatus(message, "FFCCTRNS");   
  
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: message.status, 
          input_details: message.status, 
          input_author_guid: author_guid 
        }, connection); 
  
        if(message.status === XML_IMPORT_IMPORTED)
          arr = await importFFCCTRNS(message, data, connection);
        break;
        
      case FFPCRQST:      
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection); 
  
        obj = await importFFPCRQST(message, data, connection);
        break;
  
      case PRTRN:      
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection); 
  
        await importPRTRN(message, data, connection);
        break;
  
      case FFPSRPRT:      
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection); 
        let _error = { message: "" }
        if(message.transactions.length > 0) {
          for(let i = 0; i < message.transactions.length; i++) {
            try {
              await importFFPSRPRT({ 
                messageHeader: message.messageHeader, 
                transactions: [ message.transactions[i] ], 
                OrgnlGrpInfAndSts: message.OrgnlGrpInfAndSts
              }, connection);
            }
            catch(err) {
              _error.message = _error.message.length > 0 ? `, ${err.message}` : err.message
            }
          }
        }
        else {
          try {
            await importFFPSRPRT(message, connection);
          }
          catch(err) {
            _error = {
              ...err
            }
          }
        }
        if(_error && _error.message && _error.message.length > 0)
          throw(_error)
        break;
  
      case ROINVSTG:
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection); 
  
        await importROINVSTG(message, data, connection);
        break;
  
      case BALANCE:
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection);
  
        await importBALANCE(message, data, connection);
        break;
  
      case BTCDCNTF:
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection);
  
        await importBTCDCNTF(message, connection);
        break;
      
      case DEBIT:
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection);
  
        await importDEBIT(message, data, connection);
        break;

      case DICTIONR:
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_RECEIVED, 
          input_details: "Received successfully", 
          input_author_guid: author_guid 
        }, connection);
  
        await importDICTIONR(message, connection);
        break;
    
      default:
        throw { message: "Message type unsupported" };
      }
      
      if(![ /* FFPCRQST, PRTRN, */ FFCCTRNS ].includes(message.constructor))
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message.constructor.name || "undefined", 
          input_status: XML_IMPORT_IMPORTED, 
          input_details: "Imported successfully", 
          input_author_guid: author_guid 
        }, connection);
      
      console.log('***')
      await db._transaction.commit(connection);
  
      if(producer)
        producer.send({
          topic: receiveMessage,
          event: RECEIVE_MESSAGE_SUCCESS,
          payload: { guid }
        })
      console.log(arr)
      if(obj) {
        let { req, type, err } = obj;
        if(!err)
          if(producer)
            console.log(RECEIVE_MESSAGE_SUCCESS + "\x1b[32m%s\x1b[0m", " (OK)\n")
        init(req, undefined, () => {}, type)
        if(err)
          throw(err)
      }
      else if(arr && arr.length > 0) {
        let errArr = []
        for(let i = 0; i < arr.length; i++) {
          let { req, type, err } = arr[i];
          if(!err)
            if(producer)
              console.log(RECEIVE_MESSAGE_SUCCESS + "\x1b[32m%s\x1b[0m", " (OK)\n")
          init(req, undefined, () => {}, type)
          if(err)
            errArr.push(err)
        }
        if(errArr.length > 0)
          throw(errArr)
      }
      else
        if(producer)
          console.log(RECEIVE_MESSAGE_SUCCESS + "\x1b[32m%s\x1b[0m", " (OK)\n")
  
      // await db._transaction.commit(connection);    
  
      if(res)
        res.status(200).json({ guid });
  
    } catch (err) {
      console.log(err)
      // if(err.xmlParser && err.req && err.type)
      //   init(err.req, undefined, () => {}, err.type)
      if (connection)
        await db._transaction.rollback(connection);
      if(Array.isArray(err) && err.length > 0) {
        for(let i = 0; i < err.length; i++) {
          await db.inbox.upsert({ 
            input_guid: guid, 
            input_type: message && message.constructor ? message.constructor.name : "undefined", 
            input_status: XML_IMPORT_ERRORED,
            input_details: typeof(err[i]) === "object" ? JSON.stringify(err[i]) : escape(err[i].message || err[i]), 
            input_author_guid: author_guid 
          }, connection);
      
          if(producer) {
            producer.send({
              topic: receiveMessage,
              event: RECEIVE_MESSAGE_FAILED,
              payload: { guid, error: typeof(err[i]) === "object" ? JSON.stringify(err[i]) : err[i].toString() }
            })
            console.log(RECEIVE_MESSAGE_FAILED + "\x1b[31m%s\x1b[0m", ` (FAILED: ${err[i]})\n`)
          }
        }
      }
      else {
        await db.inbox.upsert({ 
          input_guid: guid, 
          input_type: message && message.constructor ? message.constructor.name : "undefined", 
          input_status: XML_IMPORT_ERRORED,
          input_details: typeof(err) === "object" ? JSON.stringify(err) : escape(err.message || err), 
          input_author_guid: author_guid 
        }, connection);
    
        if(producer) {
          producer.send({
            topic: receiveMessage,
            event: RECEIVE_MESSAGE_FAILED,
            payload: { guid, error: typeof(err) === "object" ? JSON.stringify(err) : err.toString() }
          })
          console.log(RECEIVE_MESSAGE_FAILED + "\x1b[31m%s\x1b[0m", ` (FAILED: ${err})\n`)
        }
      }
  
      if(next)
        next(err)
    } 
    finally {
      if (connection)
        connection.release();
    }
  }
}