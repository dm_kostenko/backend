const { MessageReceiver } = require("../../../../helpers/kafka");
let producer;
const { incomingXmlParser } = require("./config")
const db = require("../../../../helpers/db/bankApi");

producer = new MessageReceiver.Producer();
console.log("\x1b[32m%s\x1b[0m","\nmessageReceiver service started\n");


const subscribeOnNewDocuments = () => db.getConnection().then(client => {
  client.on("notification", ({ /* channel, */ payload }) => incomingXmlParser(payload, null, {}, producer));
  client.query("LISTEN new_inbox");
});

subscribeOnNewDocuments().catch((/* err */) => setTimeout(subscribeOnNewDocuments, 5 * 60 * 1000));

