const db = require("../../../helpers/db/bankApi");
const { getCETDate, calcTime } = require("../../../helpers/target2Timing")
const messageId = require("../../messageIdGenerator/")
const { 
  EXTERNAL_TRANSACTION_PENDING,  
  EXTERNAL_TRANSACTION_UNAPPROVED,
  EXTERNAL_TRANSACTION_RETURNED
} = require("../../../helpers/constants/statuses").externalTransaction;
const {   
  INTERNAL_TRANSACTION_PENDING
} = require("../../../helpers/constants/statuses").internalTransaction;
const {   
  XML_IMPORT_IMPORTED,
  XML_IMPORT_ERRORED
} = require("../../../helpers/constants/statuses").xmlImport;
const { iban } = require("../../../config")

const { reserve } = require("../../../helpers/transactions/account");

module.exports = async(data, type, connection) => {

  let newTransaction;
  let senderAccount;
  let receiverAccount;
  let extTransaction;
  let isResOfInvest;
  let msgId;

  try {
    switch(type) {
      case "FFCCTRNS":       
        if(![iban.customer.internal, iban.settlement.internal].includes(data.senderAccount)) {
          [ senderAccount ] = await db.account.get({ 
            input_iban: data.senderAccount 
          });      

          if(!senderAccount)
            throw { message: `Sender account with ${data.senderAccount} IBAN didn't found` };

          await reserve({ 
            senderAccountNumber: senderAccount.number, 
            currency: data.currency, 
            amount: data.amount 
          }, connection);
        }

        data.initCurrency = data.currency;
        data.initAmount = data.amount;

        msgId = await messageId.generate();

        // data.senderIBAN = "LT931020103000031800"
        
        [ newTransaction ] = await db.ffcctrns_transaction.createTransaction({
          input_login_guid: data.authorGuid,
          input_sender_iban: data.senderIBAN,
          input_receiver_iban: data.receiverIBAN,
          input_currency: data.currency,
          input_value: data.baseAmount,
          input_commission: data.amount - data.baseAmount,
          input_cdttrftxinfpmtidinstrid: msgId,
          input_cdttrftxinfpmtidendtoendid: data.paymentEndToEndId,
          input_cdttrftxinfpmtidtxid: msgId,
          input_cdttrftxinfpmtidpmttpinfsvclvlcd: data.serviceLevel,
          // input_cdttrftxinfpmttpinflclinstrm: ,
          input_cdttrftxinfintrbksttlmamt: "0",    //  NOT PROCESSED EXCHANGE YET
          input_cdttrftxinfchrgbr: data.chargeBearer,
          // input_cdttrftxinfinstgagtfininstnidbic: ,
          input_cdttrftxinfdbtrnm: data.senderName,
          input_cdttrftxinfdbtracctidiban: data.senderIBAN,
          input_cdttrftxinfdbtragtfininstnidbic: data.senderBIC,
          input_cdttrftxinfcdtragtfininstnidbic: data.receiverBIC,
          input_cdttrftxinfcdtrnm: data.receiverName,
          input_cdttrftxinfcdtracctidiban: data.receiverIBAN,
          // input_cdttrftxinfnnnid: ,
          // input_cdttrftxinfpurpcd: ,
          // input_cdttrftxinfrmtinfustrd: ,
          // input_cdttrftxinfrmtinfstrd: ,
          // input_cdttrftxinfrmtinfstrdcdtrrefinftpcdorprtrycd: ,
          // input_cdttrftxinfrmtinfstrdcdtrrefinftpissr: ,
          input_cdttrftxinfrmtinfstrdcdtrrefinfref: data.remittanceInformation,
          input_status: EXTERNAL_TRANSACTION_PENDING,
          input_author_guid: data.authorGuid,
        }, connection);

        data = {
          paymentTransactionId: msgId,
          paymentInstructionId: msgId,
          paymentEndToEndId: data.paymentEndToEndId,
          currency: data.currency,
          initCurrency: data.currency,
          amount: data.amount,
          baseAmount: data.baseAmount,
          initAmount: data.amount,
          senderAccount: data.senderAccount,
          receiverAccount: data.receiverAccount,
          senderIBAN: data.senderIBAN,
          receiverIBAN: data.receiverIBAN,
          authorGuid: data.authorGuid,
          serviceLevel: data.serviceLevel,
          chargeBearer: data.chargeBearer,
          senderName: data.senderName,
          senderBIC: data.senderBIC,
          receiverName: data.receiverName,
          receiverBIC: data.receiverBIC,
          remittanceInformation: data.remittanceInformation,
          authType: data.authType,
          status: EXTERNAL_TRANSACTION_PENDING,
          ClrSysPrtry: data.ClrSysPrtry
        }

        console.log('3: \n')
        console.log(data)
        break;

      case "Internal":
        [ receiverAccount ] = await db.account.get({ 
          input_number: data.receiverAccount, 
          input_login_guid: data.authorGuid 
        });

        const [ _receiverAccount ] = await db.account.getByLogin({ 
          input_number: data.receiverAccount, 
          input_login_guid: data.authorGuid 
        });

        receiverAccount = receiverAccount || _receiverAccount;

        if (!receiverAccount)
          throw { message: "Receiver account didn't found" };

        const [ receiverBalance ] = await db.currency_balance.get({ 
          input_account_number: receiverAccount.number, 
          input_currency: data.receiver_currency 
        });

        if (!receiverBalance)
          throw { message: `Receiver account ${receiverAccount.number} with currency ${data.receiver_currency} didn't found` };

        [ senderAccount ] = await db.account.get({ 
          input_number: data.senderAccount
        });

        if(!senderAccount)
          throw { message: `Sender account with ${data.senderAccount} IBAN didn't found` };

        await reserve({ 
          senderAccountNumber: senderAccount.number, 
          currency: data.sender_currency, 
          amount: data.amount 
        }, connection);

        data.senderCurrency = data.sender_currency;
        data.receiverCurrency = data.receiver_currency;
        data.senderAmount = data.amount;
        
        [ newTransaction ] = await db.internal_transaction_history.upsert({
          input_sender_iban: senderAccount.iban,
          input_receiver_iban: receiverAccount.iban,
          input_currency: data.receiverCurrency,
          input_value: "0",
          input_commission: data.amount - data.baseAmount,
          input_status: INTERNAL_TRANSACTION_PENDING,
          input_author_guid: data.authorGuid
        });

        data = {
          guid: newTransaction.guid,
          senderAccount: data.senderAccount,
          receiverAccount: data.receiverAccount,
          receiverCurrency: data.receiver_currency,
          senderCurrency: data.sender_currency,
          senderAmount: data.amount,
          baseAmount: data.baseAmount,
          authorGuid: data.authorGuid,
          authType: data.authType,
          status: INTERNAL_TRANSACTION_PENDING
        }

        break;
      case "FFPCRQST":

      
        let [ origTransaction ] = await db.ffcctrns_transaction.get({ 
          input_msgid: data.OrgnlTxId 
        }, connection);

        if (!origTransaction)
          throw { message: `Transaction ${data.OrgnlTxId} does not exist` };

        [ extTransaction ] = await db.external_transaction_history.get({ 
          input_guid: origTransaction.guid 
        }, connection);

        if (!extTransaction)
          throw { message: `Transaction ${data.OrgnlTxId} does not exist` };
      

        // if(!origTransaction)
        //   throw { message: `Transaction ${data.originalTxId} does not have FFCCTRNS type` }

        let { 
          grphdrmsgid,
          cdttrftxinfpmtidendtoendid,
          cdttrftxinfpmtidtxid,
          cdttrftxinfpmtidpmttpinfsvclvlcd,
          cdttrftxinfintrbksttlmamt,
          cdttrftxinfdbtrnm,
          cdttrftxinfdbtracctidiban,
          cdttrftxinfdbtragtfininstnidbic,
          cdttrftxinfcdtragtfininstnidbic,
          cdttrftxinfcdtrnm,
          cdttrftxinfcdtracctidiban,
          grphdrintrbksttlmdt,
          grphdrsttlminfsttlmmtd,
        } = origTransaction;

        msgId = await messageId.generate();
      
        [ newTransaction ] = await db.ffpcrqst_transaction.createTransaction({
          input_sender_iban: extTransaction.sender_iban,
          input_receiver_iban: extTransaction.receiver_iban,
          input_currency: extTransaction.currency,
          input_value: extTransaction.value,
          input_commission: "0",
          input_author_guid: data.authorGuid,
          // input_assgnmtid,
          input_undrlygtxinfcxlid: msgId,
          input_undrlygtxinforgnlgrpinforgnlmsgid: grphdrmsgid,
          input_undrlygtxinforgnlgrpinforgnlmsgnmid: "pacs.008.001.02",
          input_undrlygtxinforgnlgrpinforgnlmsgendtoendid: cdttrftxinfpmtidendtoendid || "NOTPROVIDED",
          input_undrlygtxinforgnlgrpinforgnltxid: cdttrftxinfpmtidtxid,
          input_undrlygtxinforgnlintrbksttlmamt: cdttrftxinfintrbksttlmamt,
          input_undrlygtxinforgnlintrbksttlmdt: calcTime(grphdrintrbksttlmdt).toISOString().slice(0,10),
          input_undrlygtxinfcxlrsninforgtrnm: data.originatorClient,
          input_undrlygtxinfcxlrsninforgtridorgidbicorbei: data.originatorBank,
          input_undrlygtxinfcxlrsninfrsncd: data.CxlRsnInf === "DUPL" ? data.CxlRsnInf : undefined,
          input_undrlygtxinfcxlrsninfrsnprty: data.CxlRsnInf !== "DUPL" ? data.CxlRsnInf : undefined,
          input_undrlygtxinfcxlrsninfrsnaddtinf: data.CxlRsnInf === "FRAD" ? data.CxlRsnInfAddtInf : undefined,
          // input_undrlygtxinforgnltxref: ,
          input_undrlygtxinforgnltxrefsttlminfsttlmmtd: grphdrsttlminfsttlmmtd,
          input_undrlygtxinforgnltxrefsttlminfclrsysprtry: data.ClrSysPrtry,
          input_undrlygtxinforgnltxrefpmttpinfsvclvlcd: cdttrftxinfpmtidpmttpinfsvclvlcd,
          input_undrlygtxinforgnltxrefdbtrnm: cdttrftxinfdbtrnm,
          input_undrlygtxinforgnltxrefdbtracctidiban: cdttrftxinfdbtracctidiban,
          input_undrlygtxinforgnltxrefdbtragtfininstnidbic: cdttrftxinfdbtragtfininstnidbic,
          input_undrlygtxinforgnltxrefcdtragtfininstnidbic: cdttrftxinfcdtragtfininstnidbic,
          input_undrlygtxinforgnltxrefcdtrnm: cdttrftxinfcdtrnm,
          input_undrlygtxinforgnltxrefcdtracctidiban: cdttrftxinfcdtracctidiban,
          // input_txsts,
          // input_rsnprty,
          // input_rsncd,
          input_status: EXTERNAL_TRANSACTION_UNAPPROVED,
          input_author_guid: data.authorGuid
        }, connection);
        
        data = {
          OrgnlTxId: data.OrgnlTxId,
          paymentCancellationId: msgId,
          CxlRsnInf: data.CxlRsnInf,
          CxlRsnInfAddtInf: data.CxlRsnInfAddtInf,
          ClrSysPrtry: data.ClrSysPrtry,
          originatorClient: data.originatorClient,
          originatorBank: data.originatorBank,
          authorGuid: data.authorGuid,
          status: EXTERNAL_TRANSACTION_UNAPPROVED
        }
        break;

      case "PRTRN": 

        isResOfInvest = data.reasonForReturn === "FOCR" && data.isResponse;

        let [ ffcctrns ] = await db.ffcctrns_transaction.get({
          input_msgid: data.originalTxId
        })

        if(!ffcctrns) {
          [ ffcctrns ] = await db.ffcctrns_transaction.get({
            input_cdttrftxinfpmtidtxid: data.originalTxId
          });
          if(!ffcctrns)
            throw { 
              message: `Transaction ${data.originalTxId} does not exist`,
              code: "NOOR"  // The funds were not received
            };
        }

        const [ externalTransaction ] = await db.external_transaction_history.get({ 
          input_guid: ffcctrns.guid 
        }, connection);

        if (!externalTransaction)
          throw { 
            message: `Transaction ${data.originalTxId} does not exist`,
            code: "NOOR"  // The funds were not received
          };
        
        if(externalTransaction.status === EXTERNAL_TRANSACTION_RETURNED)
          throw {
            message: `Transaction ${data.originalTxId} was already returned`,
            code: "ARDT"    // The funds have already been returned
          }

        if(![ XML_IMPORT_IMPORTED, XML_IMPORT_ERRORED ].includes(externalTransaction.status))
          throw { 
            message: `Transaction ${data.originalTxId} status is ${externalTransaction.status} instead of ${XML_IMPORT_IMPORTED} or ${XML_IMPORT_ERRORED}`,
          };
          
          
        let isReserved;
        [ receiverAccount ] = await db.account.get({
          input_iban: externalTransaction.receiver_iban
        });

        // if(!receiverAccount && !data.returnImmediately)
        //   throw { 
        //     message: `Sender account with ${externalTransaction.receiver_iban} IBAN didn't found`,
        //     code: "AC04"    // The account is closed
        //   };


        // if(data.reasonForReturn === "FOCR") {
        //   await reserve({ 
        //     senderAccountNumber: receiverAccount.number, 
        //     currency: externalTransaction.currency, 
        //     amount: externalTransaction.value 
        //   }, connection);

        //   isReserved = true;
        // }

        const [ origTx ] = await db.ffcctrns_transaction.get({ 
          input_guid: externalTransaction.guid 
        }, connection);

        if(!origTx)
          throw { message: `Transaction ${externalTransaction.guid} does not have FFCCTRNS type` }

        msgId = await messageId.generate();
     
        const [ tx ] = await db.prtrn_transaction.createTransaction({
          input_sender_iban: externalTransaction.receiver_iban,   // because tx is returned (reversed)
          input_receiver_iban: externalTransaction.sender_iban,
          input_currency: externalTransaction.currency,
          input_value: externalTransaction.value,
          input_commission: "0",      // zero yet
          // input_grphdrmsgid: ,
          input_txinfrtrid: msgId,
          input_txinforgnlgrpinforgnlmsgid: origTx.grphdrmsgid,
          input_txinforgnlgrpinforgnlmsgnmid: "pacs.008.001.02",
          input_txinforgnlinstrid: origTx.cdttrftxinfpmtidinstrid,
          input_txinforgnlendtoendid: origTx.cdttrftxinfpmtidendtoendid,
          input_txinforgnltxid: origTx.cdttrftxinfpmtidtxid,
          input_txinforgnlintrbksttlmamt: origTx.cdttrftxinfintrbksttlmamt,
          input_txinfrtrdintrbksttlmamt: data.reasonForReturn === "FOCR" ? origTx.cdttrftxinfintrbksttlmamt - data.chrgsInfAmt : origTx.cdttrftxinfintrbksttlmamt,
          input_txinfrtrdinstdamt: origTx.cdttrftxinfintrbksttlmamt,
          input_txinfchrgbr: "SLEV",
          input_txinfchrgsinfamt: data.reasonForReturn === "FOCR" ? data.chrgsInfAmt : undefined,
          // input_txinfchrgsinfptyfininstnidbic: ,
          // input_txinfinstgagtfininstnidbic: ,
          input_txinfrtrrsninforgtrnm: data.originatorClient,
          input_txinfrtrrsninforgtridorgidbicorbei: data.originatorBank,
          input_txinfrtrrsninfrsncd: data.reasonForReturn,
          input_txinfrtrrsninfaddtinf: data.reasonForReturn === "FOCR" ? data.rtrRsnInfAddtInf : undefined,
          // input_txinforgnltxref: ,
          input_txinforgnltxrefintrbksttlmdt: calcTime(origTx.grphdrintrbksttlmdt).toISOString().slice(0, 10),
          input_txinforgnltxrefsttlminfsttlmmtd: origTx.grphdrsttlminfsttlmmtd,
          input_txinforgnltxrefpmttpinfsvclvlcd: origTx.cdttrftxinfpmtidpmttpinfsvclvlcd,
          input_txinforgnltxrefdbtrnm: origTx.cdttrftxinfdbtrnm,
          input_txinforgnltxrefdbtracctidiban: origTx.cdttrftxinfdbtracctidiban,
          input_txinforgnltxrefdbtragtfininstnidbic: origTx.cdttrftxinfdbtragtfininstnidbic,
          input_txinforgnltxrefcdtragtfininstnidbic: origTx.cdttrftxinfcdtragtfininstnidbic,
          input_txinforgnltxrefcdtrnm: origTx.cdttrftxinfcdtrnm,
          input_txinforgnltxrefcdtracctidiban: origTx.cdttrftxinfcdtracctidiban,
          // input_txsts: ,
          // input_rsnprty: ,
          // input_rsncd: ,
          input_status: EXTERNAL_TRANSACTION_PENDING,
          input_author_guid: data.authorGuid,
        }, connection);

        newTransaction = tx;

        data = {
          originalTxId: data.originalTxId,
          returnmentId: msgId,
          сhrgsInfAmt: data.сhrgsInfAmt,
          originatorClient: data.originatorClient,
          originatorBank: data.originatorBank,
          reasonForReturn: data.reasonForReturn,
          rtrRsnInfAddtInf: data.rtrRsnInfAddtInf,
          currency: externalTransaction.currency,
          receiverAccount: receiverAccount ? receiverAccount.number : origTx.cdttrftxinfcdtracctidiban,   // the last if IBAN is incorrect or empty
          amount: externalTransaction.value,
          status: EXTERNAL_TRANSACTION_UNAPPROVED,
          authorGuid: data.authorGuid,
          isReserved,
          test: data.test
        }
    
        break;
      
      case "ROINVSTG":

        let cancTr = {};
        let [ origTr ] = await db.ffcctrns_transaction.get({ 
          input_cdttrftxinfpmtidtxid: data.OrgnlTxId
        });

        [ extTransaction ] = await db.external_transaction_history.get({
          input_guid: origTr.guid
        });

        if (!extTransaction)            
          extTransaction = {};

        if(!origTr) {
          origTr = {};
          [ cancTr ] = await db.ffpcrqst_transaction.get({
            input_undrlygtxinforgnlgrpinforgnlmsgid: data.OrgnlTxId
          });
          if(!cancTr)
            throw { message: `FFCCTRNS transaction ${data.OrgnlTxId} is undefined and FFPCRQST transaction too` };
        }     
        else if(![ XML_IMPORT_IMPORTED, XML_IMPORT_ERRORED ].includes(extTransaction.status)) {
          throw { message: `Transaction ${data.OrgnlTxId} status is ${extTransaction.status} instead of ${XML_IMPORT_IMPORTED} or ${XML_IMPORT_ERRORED}` };
        }

        msgId = await messageId.generate();

        [ newTransaction ] = await db.roinvstg_transaction.createTransaction({
          input_sender_iban: extTransaction.receiver_iban || origTr.cdttrftxinfcdtracctidiban || cancTr.undrlygtxinforgnltxrefcdtracctidiban,
          input_receiver_iban: extTransaction.sender_iban || origTr.cdttrftxinfdbtracctidiban || cancTr.undrlygtxinforgnltxrefdbtracctidiban,
          input_currency: extTransaction.currency || "EUR",
          input_value: extTransaction.value || origTr.grphdrttlintrbksttlmamt || cancTr.undrlygtxinforgnlintrbksttlmamt,
          input_commission: "0",
          input_cxldtlstxinfandstscxlstsid: msgId,
          input_cxldtlstxinfandstsorgnlgrpinforgnlmsgid: origTr.grphdrmsgid || cancTr.undrlygtxinforgnlgrpinforgnlmsgid,
          input_cxldtlstxinfandstsorgnlgrpinforgnlmsgnmid: "pacs.008.001.02",
          input_cxldtlstxinfandstsorgnlendtoendid: origTr.cdttrftxinfpmtidendtoendid || cancTr.undrlygtxinforgnlgrpinforgnlmsgendtoendid,
          input_cxldtlstxinfandstsorgnltxid: origTr.cdttrftxinfpmtidtxid || cancTr.undrlygtxinforgnlgrpinforgnltxid,
          input_cxldtlstxinfandststxcxlsts: "RJCR",
          input_cxldtlstxinfandstscxlstsrsninforgtrnm: data.originatorClient,
          input_cxldtlstxinfandstscxlstsrsninforgtridorgidbicorbei: data.originatorBank,
          input_cxldtlstxinfandstscxlstsrsninfrsncd: [ "LEGL", "CUST" ].includes(data.CxlStsRsnInf) ? data.CxlStsRsnInf : undefined,
          input_cxldtlstxinfandstscxlstsrsninfrsnprtry: ![ "LEGL", "CUST" ].includes(data.CxlStsRsnInf) ? data.CxlStsRsnInf : undefined,
          input_cxldtlstxinfandstscxlstsrsninfaddtlinf: data.CxlStsRsnInf === "LEGL" ? data.CxlStsRsnInfAddtInf : undefined,
          input_cxldtlstxinfandstsorgnltxrefintrbksttlmamt: origTr.cdttrftxinfintrbksttlmamt || cancTr.undrlygtxinforgnlintrbksttlmamt,
          input_cxldtlstxinfandstsorgnltxrefintrbksttlmdt: origTr.grphdrintrbksttlmdt 
                                                        ? calcTime(origTr.grphdrintrbksttlmdt).toISOString().slice(0, 10) 
                                                        : cancTr.undrlygtxinforgnlintrbksttlmdt 
                                                          ? calcTime(cancTr.undrlygtxinforgnlintrbksttlmdt).toISOString().slice(0, 10) 
                                                          : "",
          input_cxldtlstxinfandstsorgnltxrefsttlminfsttlmmtd:  origTr.grphdrsttlminfsttlmmtd || cancTr.undrlygtxinforgnltxrefsttlminfsttlmmtd,
          input_cxldtlstxinfandstsorgnltxrefsttlminfclrsysprtry: origTr.grphdrsttlminfclrsysprtry || cancTr.undrlygtxinforgnltxrefsttlminfclrsysprtry,
          input_cxldtlstxinfandstsorgnltxrefpmttpinfsvclvlcd: origTr.cdttrftxinfpmtidpmttpinfsvclvlcd || cancTr.undrlygtxinforgnltxrefpmttpinfsvclvlcd,
          input_cxldtlstxinfandstsorgnltxrefdbtrnm: origTr.cdttrftxinfdbtrnm || cancTr.undrlygtxinforgnltxrefdbtrnm,
          input_cxldtlstxinfandstsorgnltxrefdbtracctidiban: origTr.cdttrftxinfdbtracctidiban || cancTr.undrlygtxinforgnltxrefdbtracctidiban,
          input_cxldtlstxinfandstsorgnltxrefdbtragtfininstnidbic: origTr.cdttrftxinfdbtragtfininstnidbic || cancTr.undrlygtxinforgnltxrefdbtragtfininstnidbic,
          input_cxldtlstxinfandstsorgnltxrefcdtragtfininstnidbic: origTr.cdttrftxinfcdtragtfininstnidbic || cancTr.undrlygtxinforgnltxrefcdtragtfininstnidbic,
          input_cxldtlstxinfandstsorgnltxrefcdtrnm: origTr.cdttrftxinfcdtrnm || cancTr.undrlygtxinforgnltxrefcdtrnm,
          input_cxldtlstxinfandstsorgnltxrefcdtracctidiban: origTr.cdttrftxinfcdtracctidiban || cancTr.undrlygtxinforgnltxrefcdtracctidiban,
          input_status: EXTERNAL_TRANSACTION_UNAPPROVED,
          input_author_guid: data.authorGuid
        }, connection);

        data = {
          originalTxId: data.OrgnlTxId,
          reasonForReturn: data.CxlStsRsnInf,
          additionalInformation: data.CxlStsRsnInfAddtInf,
          authorGuid: data.authorGuid,
          status: EXTERNAL_TRANSACTION_UNAPPROVED
        }        

        break;
      default: throw { message: "Invalid transaction type" }
    }
    data.guid = newTransaction.guid;
    return data;
  }
  catch(err) {
    if(type === "PRTRN" && err.code && isResOfInvest) {
      return {
        _response: true,
        type: "ROINVSTG",
        req: {
          body: {
            OrgnlTxId: data.originalTxId,
            CxlStsRsnInf: err.code,
            originatorClient: data.originatorClient
          },
          auth: {
            loginGuid: data.authorGuid
          }
        }
      }
    }
    if(err.message)
      return {
        error: true,
        errorMessage: err.message,
        _error: err
      }
  }
}