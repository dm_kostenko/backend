const timeoutFunction = (callback, f) => {
  if(!f) {
    return new Promise(resolve => {
      setTimeout(() => {
        try {
          timeoutFunction(callback, true)
        }
        catch(err) {
          if(err.error === "timeout")
            resolve(err)
          else
            throw(err)
        }
      }, 30000);

      callback(resolve)
      
    })
  }
  else {
    throw { error: "timeout" };
  }
}

module.exports.timeoutFunction = timeoutFunction;

// const callback = async (resolve) => {
//   setTimeout(() => {
//     resolve("ok")
//   }, 1000)
// }

// const func = async () => {
//   try {
//     let a = await timeoutFunc(callback);
//     console.log(a)
//   }
//   catch(err) {
//   }
// }

// func()
