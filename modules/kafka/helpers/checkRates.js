const db = require("../../../helpers/db/bankApi");
const config = require("../../../config/kafka");

const ratesConfig = {
  outgoing_FFCCTRNS: {
    isEnable: true,
    getValues: (data) => {
      return Object.keys(data).map(key => ({    
        rule_type: key,
        field: key
      }))
      .filter(item => item)
    }
  },
  incoming_FFCCTRNS: {
    isEnable: true,
    getValues: (data) => {
      return Object.keys(data).map(key => ({    
        rule_type: key,
        field: key
      }))
      .filter(item => item)
    }
  },
  Internal: {
    isEnable: true,
    getValues: (data) => {
      return Object.keys(data).map(key => ({  
        rule_type: key,
        field: key
      }))
      .filter(item => item)
    }
  },
  currency_exchange: {
    isEnable: config.services.currencyExchange,
    getValues: (data) => {
      if(data.senderCurrency && data.receiverCurrency)
        return [ 
          {
            rule_type: `${data.senderCurrency}_${data.receiverCurrency}`,
            field: "amount"
          }
        ];
      if(data.currency !== "EUR")
        return [ 
          {
            rule_type: `${data.currency}_EUR`,
            field: "amount"
          } 
        ];
      return [];
    }
  }
};

const sequence = {
  outgoing_FFCCTRNS: [ "outgoing_FFCCTRNS", "currency_exchange" ],
  incoming_FFCCTRNS: [ "incoming_FFCCTRNS" ],
  Internal: [ "Internal", "currency_exchange" ]
}

const compare = {
  below: (field, value) => Math.round(field) < Math.round(value),
  above: (field, value) => Math.round(field) >= Math.round(value),
  equals: (field, value) => field === value,
  mask: (field, mask) => {
    // if(field.length !== mask.length)
    //   throw { 
    //     message: `Invalid mask length ${mask.length} instead of ${field.length}`,
    //     producer: true
    //   }

    let c = true;
    const minLen = mask.length < field.length ? mask.length : field.length
    const maxLen = mask.length < field.length ? field.length : mask.length

    for(let i = 0; i < minLen; i++)
      if(mask[i] !== "*")
        c = c && (mask[i] === field[i])
    if(minLen === mask.length)
      return c
    else
      return false
  },
  other: (amount, value) => true
}

const parseParams = (params) => {
  const percentRateObj = params.find(param => param.name === "rate" && param.value_type === "percent");
  const numberRateObj = params.find(param => param.name === "rate" && param.value_type === "number");
  const compareObj = params.find(param => param.name !== "rate");
  return {
    [`${compareObj.name}Value`]: compareObj.value,
    [`${compareObj.name}PercentRate`]: percentRateObj.value,
    [`${compareObj.name}NumberRate`]: numberRateObj.value,
  }     
}

const getRate = async(field, type, rule_type, guid) => {

  try {
    const rules = await db.rate.getRules({
      input_guid: guid,
      input_type: type,
      input_rule_type: rule_type
    });

    let params = {};

    for(let i = 0; i < rules.length; i++) {
      const _params = await db.rate_rule_param.get({
        input_rate_rule_guid: rules[i].guid
      });
      params = {
        ...params,
        ...parseParams(_params)
      };
    }

    const compareNames = [ "above", "below", "equals", "mask", "other" ];
    let rate = {};

    compareNames.forEach(name => {
      if(params[`${name}Value`])
        if(compare[name](field, params[`${name}Value`]) && Object.keys(rate).length === 0)
          rate = {
            percentValue: params[`${name}PercentRate`],
            numberValue: params[`${name}NumberRate`]
          };
    });

    return {
      numberRate: rate.numberValue || 0,
      percentRate: (rate.percentValue / 10000) || 0
    };
  }
  catch(err) {
    throw(err)
  }
}

module.exports = async(data, type) => {
  try {
    // let amount;
    let baseAmount;
    let currency_rate = 1;
    switch(type) {
      case "outgoing_FFCCTRNS":
        baseAmount = data.amount
        if(data.currency !== "EUR") {
          [ { rate: currency_rate } ] = await db.currency_rate.get({
            input_currency_code: data.currency
          });
          data.amount = Math.round(data.amount / currency_rate);
        }
        // else
        //   amount = data.amount;
        // data.amount = amount
        break;
      case "incoming_FFCCTRNS":
        // amount = data.amount;
        break;
      case "Internal":
        // amount = data.amount;
        break;
      default: 
        return data;
    }

    let numberRate = 0;
    let percentRate = 0;
    let guid;

    [{ rate_guid: guid }] = await db.login.get({
      input_guid: data.loginGuid
    });
    
    if(!guid)
      [{ guid }] = await db.rate.get({
        input_name: "Default rate"
      });
    
    for(let j = 0; j < sequence[type].length; j++)
      if(ratesConfig[sequence[type][j]].isEnable) {
        const values = ratesConfig[sequence[type][j]].getValues(data);
        for(let i = 0; i < values.length; i++) {
          const currentRateObj = await getRate(data[values[i].field], sequence[type][j], values[i].rule_type, guid);
          numberRate += Math.round(currentRateObj.numberRate);
          percentRate += parseFloat(currentRateObj.percentRate);
        }
      }
    const eurCommission = (parseFloat((data.amount * (1 + percentRate) + Math.round(numberRate)) / 100).toFixed(2) - (data.amount / 100)).toFixed(2);
    return (parseFloat(eurCommission / currency_rate)).toFixed(2)
  }
  catch(err) {
    throw(err)
  }
}