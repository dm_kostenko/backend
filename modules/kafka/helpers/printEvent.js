module.exports = (message) => {
  const payload = JSON.parse(message.value);
  if(payload.error)
    console.log(message.key + "\x1b[31m%s\x1b[0m", ` (FAILED: ${payload.error})\n`); // red
  else
    console.log(message.key + "\x1b[32m%s\x1b[0m", " (OK)\n")   // green
}