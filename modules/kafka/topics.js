module.exports = {
  transactionProcessing: "transactionProcessing",
  checkRules: "checkRules",
  blockAccounts: "blockAccounts",
  currencyExchange: "currencyExchange",
  receiveMessage: "receiveMessage"
}