const AuthTypeValidator = require("./AuthTypeValidator");
const RoutesValidator = require("./RoutesValidator");
const SessionValidator = require("./SessionValidator");
const UserInfoValidator = require("./UserInfoValidator");
const MailValidator = require("./MailValidator");
const SmsValidator = require("./SmsValidator");

const GeneralValidator = (config) => {
  let errors = [];
  const { err: authTypeErr, authType } = AuthTypeValidator(config.authType);
  if(authTypeErr)
    errors.push(authTypeErr);

  const { errors: routesErrors, routes } = RoutesValidator(config.routes);
  if(routesErrors)
    errors.push(...routesErrors);

  const { errors: sessionErrors, session } = SessionValidator(config.session);
  if(sessionErrors)
    errors.push(...sessionErrors);

  const { errors: userInfoErrors, user } = UserInfoValidator(config.authType, config.user);
  if(userInfoErrors)
    errors.push(...userInfoErrors);

  const { err: mailErr, mail } = MailValidator(config.authType, config.mail);
  if(mailErr)
    errors.push(mailErr);

  const { err: smsErr, sms } = SmsValidator(config.authType, config.sms);
  if(smsErr)
    errors.push(smsErr);

  if(errors.length)
    return { errors };
    
  const editConfig = {
    ...config,
    authType,
    routes,
    session,
    user,
    mail,
    sms
  };
  return { editConfig };

};

module.exports = GeneralValidator;