
const SmsValidator = (authType, sms) => {
  if(authType.includes("sms")) {
    if(typeof(sms.send) === "function")
      return { sms };
    return { err: "Sms validation failed" };
  }
  return { sms };
};

module.exports = SmsValidator;