
const MailValidator = (authType, mail) => {
  if(authType.includes("mail")) {
    if(mail.send && typeof(mail.send) === "function")
      return { mail };
    return { err: "Mail validation failed" };
  }
  return { mail };
};

module.exports = MailValidator;