
const validateRoute = (route) => {
  if(route && route[0] === "/" && route.length > 3)
    return true;
  return false;
};

const areRoutesIntersect = (login, logout, refreshToken, insecure = []) => {
  let _login = login;
  let _logout = logout;
  let _insecure = insecure; 
  let _refreshToken = refreshToken;

  if(typeof(_login) === "string")
    _login = [ _login ];
  if(typeof(_logout) === "string")
    _logout = [ _logout ];
  if(typeof(_refreshToken) === "string")
    _refreshToken = [ _refreshToken ];
  if(typeof(_insecure) === "string")
    _insecure = [ _insecure ];

  const fullLength = _login.length + _logout.length + _refreshToken.length + _insecure.length;
  const set = new Set();
  if(_login.length)
    _login.forEach(route => set.add(route));
  if(_logout.length)
    _logout.forEach(route => set.add(route));
  if(_refreshToken.length)
    _refreshToken.forEach(route => set.add(route));
  if(_insecure.length)
    _insecure.forEach(route => set.add(route));
  if(fullLength === set.size)
    return true;
  return false;
};


const RoutesValidator = (routes) => {
  let errors = [];
  if(!routes.login)
    errors.push("Login route(s) is (are) empty");

  if(!routes.logout)
    errors.push("Logout route(s) is (are) empty");

  if(!routes.refreshToken)
    errors.push("Refresh token route(s) is (are) empty");

  if(!Array.isArray(routes.login) && typeof(routes.login) !== "string")
    errors.push("Login route type must be an array or a string");

  if(!Array.isArray(routes.logout) && typeof(routes.logout) !== "string")
    errors.push("Logout route type must be an array or a string");

  if(!Array.isArray(routes.refreshToken) && typeof(routes.refreshToken) !== "string")
    errors.push("Refresh token route type must be an array or a string");

  if(Array.isArray(routes.login) && routes.login.length === 0)
    errors.push("Login route can not be empty");

  if(Array.isArray(routes.logout) && routes.logout.length === 0)
    errors.push("Login route can not be empty");

  if(Array.isArray(routes.refreshToken) && routes.refreshToken.length === 0)
    errors.push("Refresh token route can not be empty");

  if(!areRoutesIntersect(routes.login, routes.logout, routes.refreshToken, routes.insecure))
    errors.push("Routes fields are intersect");

  if(Array.isArray(routes.login)) {
    routes.login.forEach(route => {
      if(!validateRoute(route))
        errors.push(`Login route '${route.toUpperCase()}' validation failed`);
    });
  }
  
  if(Array.isArray(routes.logout) === "object") {
    routes.logout.forEach(route => {
      if(!validateRoute(route))
        errors.push(`Logout route '${route.toUpperCase()}' validation failed`);
    });
  }

  if(Array.isArray(routes.login) === "string") {
    if(!validateRoute(routes.login))
      errors.push(`Login route '${routes.login.toUpperCase()}' validation failed`);
  }

  if(Array.isArray(routes.logout) === "string") {
    if(!validateRoute(routes.logout))
      errors.push(`Logout route '${routes.logout.toUpperCase()}' validation failed`);
  }

  if(errors.length)
    return { errors };

  return { routes };
};

module.exports = RoutesValidator;