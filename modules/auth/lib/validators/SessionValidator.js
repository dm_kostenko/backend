
const SessionValidator = (session) => {
  let errors = [];
  if(!session.header)
    session.header = "Authorization";
  if(!session.accessTokenTTL)
    session.accessTokenTTL = "20m";
  if(!session.refreshTokenTTL)
    session.refreshTokenTTL = "7d";
  if(typeof(session.get) !== "function")
    errors.push("Session get function validation failed");
  if(typeof(session.upsert) !== "function")
    errors.push("Session upsert function validation failed");
  if(typeof(session.delete) !== "function")
    errors.push("Session delete function validation failed");
  if(errors.length)
    return { errors };
  return { session };
};

module.exports = SessionValidator;