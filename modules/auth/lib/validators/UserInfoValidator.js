
const LoginPasswordUserInfoValidator = (user) => {
  let errors = [];

  if(typeof(user.getLoginData) !== "function")
    errors.push("User getLoginData function validation failed");

  if(typeof(user.checkPassword) !== "function")
    errors.push("User checkPassword function validation failed");

  if(errors.length)
    return { errors };
  return { user }; 
};

const LoginSmsUserInfoValidator = (user) => {
  let errors = [];

  if(typeof(user.getLoginId) !== "function")
    errors.push("User getLoginId function validation failed");

  if(typeof(user.getOtp) !== "function")
    errors.push("User getOtp function validation failed");

  if(!user.saveOtp || typeof(user.saveOtp) !== "function")
    errors.push("User saveOtp function validation failed");

  if(typeof(user.deleteOtp) !== "function")
    errors.push("User deleteOtp function validation failed");

  if(typeof(user.getPhone) !== "function")
    errors.push("User getPhone function validation failed");

  if(errors.length)
    return { errors };
    
  return { user };    
};

const LoginMailUserInfoValidator = (user) => {
  let errors = [];

  if(typeof(user.getLoginId) !== "function")
    errors.push("User getLoginId function validation failed");

  if(typeof(user.getOtp) !== "function")
    errors.push("User getOtp function validation failed");

  if(typeof(user.saveOtp) !== "function")
    errors.push("User saveOtp function validation failed");

  if(typeof(user.deleteOtp) !== "function")
    errors.push("User deleteOtp function validation failed");  
  if(typeof(user.getMail) !== "function")
    errors.push("User getMail function validation failed");

  if(errors.length)
    return { errors };
  return { user };    
};

const LoginPasswordSmsUserInfoValidator = (user) => {
  let errors = [];

  if(typeof(user.checkPassword) !== "function")
    errors.push("User checkPassword function validation failed");

  if(typeof(user.getLoginId) !== "function")
    errors.push("User getLoginId function validation failed");

  if(typeof(user.getOtp) !== "function")
    errors.push("User getOtp function validation failed");

  if(typeof(user.saveOtp) !== "function")
    errors.push("User saveOtp function validation failed");

  if(typeof(user.deleteOtp) !== "function")
    errors.push("User deleteOtp function validation failed");  
  if(typeof(user.getPhone) !== "function")
    errors.push("User getPhone function validation failed");

  if(errors.length)
    return { errors };
  return { user };    
};

const LoginPasswordMailUserInfoValidator = (user) => {
  let errors = [];

  if(typeof(user.checkPassword) !== "function")
    errors.push("User checkPassword function validation failed");

  if(typeof(user.getLoginId) !== "function")
    errors.push("User getLoginId function validation failed");

  if(typeof(user.getOtp) !== "function")
    errors.push("User getOtp function validation failed");

  if(typeof(user.saveOtp) !== "function")
    errors.push("User saveOtp function validation failed");

  if(typeof(user.deleteOtp) !== "function")
    errors.push("User deleteOtp function validation failed");  
  if(typeof(user.getMail) !== "function")
    errors.push("User getMail function validation failed");

  if(errors.length)
    return { errors };
  return { user };    
};



const UserInfoValidator = (authType, user) => {
  switch(authType) {
  case "login-password": return LoginPasswordUserInfoValidator(user);
  case "login-sms": return LoginSmsUserInfoValidator(user);
  case "login-mail": return LoginMailUserInfoValidator(user);
  case "login-password-sms": return LoginPasswordSmsUserInfoValidator(user);
  case "login-password-mail": return LoginPasswordMailUserInfoValidator(user);
  default: {
    return {
      err: `Wrong authentication type '${authType}'`
    };
  }
  }
};

module.exports = UserInfoValidator;