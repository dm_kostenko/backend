const authTypes = require("../constants/authTypes");

const AuthTypeValidator = (authType) => {
  if(authTypes.includes(authType))
    return { authType };
  return {
    err: `Wrong authentication type '${authType}'`
  };
};

module.exports = AuthTypeValidator;