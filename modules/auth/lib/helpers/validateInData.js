const Joi = require("@hapi/joi");
const ipSchema = Joi.string().ip({ version: [ "ipv4", "ipv6" ] }).required();
const userAgentSchema = Joi.string().required();
const loginSchema = Joi.string()/*.alphanum()*/.min(3).max(30).required();
const passwordSchema = Joi.string().min(6).max(20).required();
const otpSchema = Joi.string().regex(/^[0-9]+$/, "numbers").length(6);
const emailSchema = Joi.string().email().required();
const phoneSchema = Joi.string().regex(/^[+]?[0-9 ]*[(]?[0-9 ]{1,6}[)]?[-\s\./0-9 ]*$/).required();

module.exports = {
  loginValidate: (login) => {
    const { error } = Joi.validate(login, loginSchema);
    return !error;
  },  
  
  passwordValidate: (password) => {
    const { error } = Joi.validate(password, passwordSchema);
    return !error;
  },  
  
  otpValidate: (otp) => {
    const { error } = Joi.validate(otp, otpSchema);
    return !error;
  },
  
  ipValidate: (ip) => {
    const { error } = Joi.validate(ip, ipSchema);
    return !error; 
  },
  
  userAgentValidate: (user_agent) => {
    const { error } = Joi.validate(user_agent, userAgentSchema);
    return !error; 
  },

  emailValidate: (email) => {
    const { error } = Joi.validate(email, emailSchema);
    return !error; 
  },

  phoneValidate: (phone) => {
    const { error } = Joi.validate(phone, phoneSchema);
    return !error; 
  }
};