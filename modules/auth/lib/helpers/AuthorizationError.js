class AuthorizationError extends Error {
  constructor(message, status = 500){
    super(message);
    this.name = "AuthorizationError";
    this.status = status;
  }
}

module.exports = AuthorizationError;