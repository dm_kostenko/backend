const authenticator = require("otplib/authenticator");
const crypto = require("crypto");

authenticator.options = { crypto, step: 30, window: 1 };

module.exports.generateSecret = () => authenticator.generateSecret();
module.exports.isOtpValid = (otp, secret) => authenticator.check(otp, secret);
module.exports.keyuri = (user, service, secret) => authenticator.keyuri(user, service, secret);