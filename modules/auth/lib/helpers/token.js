const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

module.exports = {
  generateAccessToken: async (req, config, loginId) => {
    let accessTokenPayload = {
      payload: {
        id: loginId,
        ip: req.headers["x-real-ip"] || req.connection.remoteAddress,
        user_agent: req.headers["user-agent"]
      }    
    };
    if(config.user.getAccessTokenPayload !== undefined)
      accessTokenPayload = {
        ...accessTokenPayload,
        userPayload: {
          ...await config.user.getAccessTokenPayload(loginId, req.auth)
        }
      };
    const accessToken = jwt.sign(
      accessTokenPayload, 
      config.keys.private, 
      { 
        expiresIn: config.session.accessTokenTTL,
        algorithm: "RS256" 
      });
    return accessToken;
  },

  generateRefreshToken: async (req, config, loginId) => {
    let refreshTokenPayload = {
      payload: {
        id: loginId,      
        ip: req.headers["x-real-ip"] || req.connection.remoteAddress,
        user_agent: req.headers["user-agent"]
      }
    };
    if(config.user.getRefreshTokenPayload !== undefined)
      refreshTokenPayload = {
        ...refreshTokenPayload,
        userPayload: {
          ...await config.user.getRefreshTokenPayload(loginId, req.auth)
        }
      };
    const refreshToken = jwt.sign(
      refreshTokenPayload, 
      config.keys.private, 
      { 
        expiresIn: config.session.refreshTokenTTL,
        algorithm: "RS256" 
      });
    return refreshToken;
  },

  changeOtpToken: (config, dbOtp) => {   
    if(config.session.timesToCheck <= dbOtp.wrongTimes)
      return { 
        error: { 
          timesToCheck: true 
        } 
      }
    const expiresIn = dbOtp.exp - Math.floor(Date.now() / 1000);
    if(expiresIn <= 0)
      return { 
        error: { 
          exp: true
        } 
      }
    const otpToken = jwt.sign(
      {
        otp: dbOtp.otp,
        wrongTimes: parseInt(dbOtp.wrongTimes) + 1
      },
      config.keys.private,
      {
        expiresIn,
        algorithm: "RS256"
      }
    );
    return { otpToken };
  },

  generateOtpToken: (config, otp) => {
    const otpToken = jwt.sign(
      {
        otp: bcrypt.hashSync(otp, 10),
        wrongTimes: 0
      },
      config.keys.private,
      {
        expiresIn: "2m",
        algorithm: "RS256"
      }
    );
    return otpToken;
  },

  getAccessToken: (req, config) => {
    try {
      if(req.headers[config.session.header.toLowerCase()])
        return {
          accessToken: req.headers[config.session.header.toLowerCase()].split(" ")[1]
        };
      else
        return {
          err: `Can not read header ${config.session.header.toUpperCase()}`
        };
    }
    catch(err) {
      return { err };
    }
  },
  
  verifyToken: (config, token) => {
    return jwt.verify(token, config.keys.public, { algorithm: "RS256" }, (err, decoded) => {
      if(err) {
        return { err };
      }
      return { decoded };
    });
  },

  verifyOtpToken: (config, token) => {
    if(!token)
      return { decoded: "Empty" };
    return jwt.verify(token, config.keys.public, { algorithm: "RS256" }, (err, decoded) => {
      if(err) {
        return { err };
      }
      return { decoded };
    });
  }

};