module.exports = {
  verifyUserInfo: (req, decodedAccessToken, decodedRefreshToken) => {
    return (((req.headers["x-real-ip"] || req.connection.remoteAddress) === decodedAccessToken.payload.ip &&
              decodedAccessToken.payload.ip === decodedRefreshToken.payload.ip) &&
            (req.headers["user-agent"] === decodedAccessToken.payload.user_agent &&
            decodedAccessToken.payload.user_agent === decodedRefreshToken.payload.user_agent));
  }
};