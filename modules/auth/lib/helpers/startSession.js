const AuthorizationError = require("./AuthorizationError");
const { ipValidate, userAgentValidate } = require("./validateInData");
const { 
  generateRefreshToken, 
  generateAccessToken
} = require("./token");


const startSession = async (req, config, loginId) => {
  await config.session.delete(loginId);
  const refreshToken = await generateRefreshToken(req, config, loginId);
  const ip = req.headers["x-real-ip"] || req.connection.remoteAddress;
  const user_agent = req.headers["user-agent"];
  
  if(!ipValidate(ip))
    throw new AuthorizationError("IP validation failed", 400);

  if(!userAgentValidate(user_agent))
    throw new AuthorizationError("User-agent validation failed", 400);

  await config.session.upsert({
    id: loginId,
    ip: req.headers["x-real-ip"] || req.connection.remoteAddress,
    user_agent: req.headers["user-agent"],
    token: refreshToken
  });
  const accessToken = await generateAccessToken(req, config, loginId);
  
  return { accessToken, refreshToken };
};


module.exports = startSession;