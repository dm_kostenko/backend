const LoginRouteHandler = require("./LoginRouteHandler");
const LogoutRouteHandler = require("./LogoutRouteHandler");
const RefreshTokenRouteHandler = require("./RefreshTokenRouteHandler");
const SecureRouteHandler = require("./SecureRouteHandler");
const ChangeLoginHandler = require("./ChangeLoginHandler");
const OtpRouteHandler = require("./OtpRouteHandler");
const ForgotPasswordRouteHandler = require("./ForgotPasswordRouteHandler");
const CheckRecoveryTokenRouteHandler = require("./CheckRecoveryTokenRouteHandler");
const SetRecoveryPasswordRouteHandler = require("./SetRecoveryPasswordRouteHandler");


const getRoute = (req) => req.path;

const RequestHandler = (req, res, next, config) => {
  const route = getRoute(req);
  console.log(route)
  if(config.routes.login === route || config.routes.login.includes(route))
    return LoginRouteHandler(req, res, next, config);

  else if(config.routes.logout === route || config.routes.logout.includes(route)) 
    return LogoutRouteHandler(req, res, next, config);

  else if(config.routes.refreshToken === route || config.routes.refreshToken.includes(route)) 
    return RefreshTokenRouteHandler(req, res, next, config);
  
  else if(config.routes.forgotPassword === route || config.routes.forgotPassword.includes(route)) 
    return ForgotPasswordRouteHandler(req, res, next, config);

  else if(config.routes.checkRecoveryToken === route || config.routes.checkRecoveryToken.includes(route)) 
    return CheckRecoveryTokenRouteHandler(req, res, next, config);

  else if(config.routes.recoveryPassword === route || config.routes.recoveryPassword.includes(route)) 
    return SetRecoveryPasswordRouteHandler(req, res, next, config);
  
  else if(config.routes.validateToken === route || config.routes.validateToken.includes(route)) 
    return SecureRouteHandler(req, res, (err) => { 
      if (err)
        next(err);
      res.status(200).send();
    }, config);

  else if(config.routes.insecure && (config.routes.insecure === route || config.routes.insecure.includes(route)))
    return next();
  
  else if (config.routes.changeLogin === route || config.routes.changeLogin.includes(route)) {
    return SecureRouteHandler(req, res, async (err) => { 
      if (err)
        next(err);
      LoginRouteHandler(req, res, next, config);
      // ChangeLoginHandler(req, res, next, config);
    }, config);
  }
  else if (config.routes.otp === route || config.routes.otp.includes(route)) {
    return SecureRouteHandler(req, res, async (err) => { 
      if (err)
        next(err);
      if(req.method === "GET")
        return OtpRouteHandler.get(req, res, next, config);  
      else if (req.method === "POST")
        return OtpRouteHandler.post(req, res, next, config);
      
      next(err);
    }, config);
  }
  SecureRouteHandler(req, res, next, config);
};

module.exports = RequestHandler;