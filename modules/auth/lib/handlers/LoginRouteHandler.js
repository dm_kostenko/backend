const { loginValidate, passwordValidate, otpValidate } = require("../helpers/validateInData");
const { isOtpValid } = require("../helpers/otplib");
const AuthorizationError = require("../helpers/AuthorizationError");
const startSession = require("../helpers/startSession");
const jwt = require("jsonwebtoken");
const cryptoRandomString = require("crypto-random-string");
const bcrypt = require("bcrypt");

const { 
  generateOtpToken, 
  changeOtpToken,
  verifyOtpToken 
} = require("../helpers/token");


const hideMail = (mail) => {
  const parts = mail.split("@");
  let left = parts[0].slice(0, 3) + "*".repeat(parts[0].length - 3);
  return left + "@" + parts[1];
};

const getLogin = req => {
  try {
    let login;
    if(req.body.login && typeof(req.body.login) === "string")
      login = req.body.login;
    else if(req.body.username && typeof(req.body.username) === "string") 
      login = req.body.username;
    
    if(!loginValidate(login))
      throw new AuthorizationError("Incorrect login or password", 400);
    
    return login;    
  } catch (err) {
    throw new AuthorizationError(err.message || "Authorization error");
  }
};

const getPassword = req => {
  let password;
  if(req.body.password && typeof(req.body.password) === "string")
    password = req.body.password;

  if(!passwordValidate(password))
    throw new AuthorizationError("Incorrect login or password", 400);

  return password;
};

const getOtp = (req) => {
  let otp;
  if(req.body.otp)
    otp = req.body.otp;

  if(!otpValidate(otp))
    throw new AuthorizationError("OTP validation failed", 400);

  return otp;
};


const LoginRouteHandler = async (req, res, next, config) => {
  try {
    const login = getLogin(req);
    const password = getPassword(req);
    const dbLoginData = await config.user.getLoginData({ username: login });
    console.log(dbLoginData)
    console.log(login)
    const checkPassword = await config.user.checkPassword(dbLoginData.id, password);
    console.log(checkPassword)
    if(!dbLoginData || /* !config.user.checkPassword(password, dbLoginData.password) */ !checkPassword.status)
      throw new AuthorizationError("Incorrect login or password", 400);   

    switch(dbLoginData.auth_type || "login-password") {
    case "login-password": { 
      if(checkPassword.first_time_login || checkPassword.credentials_expired || checkPassword.credentials_expire_after) {
        const recoveryData = await config.user.setRecoveryToken(dbLoginData.id);
        if (!recoveryData.token) throw new AuthorizationError("Cannot get token");

        return res.status(200).json({
          // token_type: "Bearer",
          // accessToken,
          // refreshToken,
          token: recoveryData.token,
          credentials_expire_after: !checkPassword.credentials_expired ? checkPassword.credentials_expire_after : undefined,
          credentials_expired: checkPassword.credentials_expired,
          first_time_login: checkPassword.first_time_login 
        })
      }
      const { accessToken, refreshToken } = await startSession(req, config, dbLoginData.id);      
      await config.logger("login", req, jwt.decode(accessToken).userPayload);
      console.log({ 
        token_type: "Bearer", 
        accessToken, 
        refreshToken,
        credentials_expire_after: !checkPassword.credentials_expired ? checkPassword.credentials_expire_after : undefined,
        credentials_expired: checkPassword.credentials_expired,
        first_time_login: checkPassword.first_time_login 
      })
      return res.status(200).json({ 
        token_type: "Bearer", 
        accessToken, 
        refreshToken,
        credentials_expire_after: !checkPassword.credentials_expired ? checkPassword.credentials_expire_after : undefined,
        credentials_expired: checkPassword.credentials_expired,
        first_time_login: checkPassword.first_time_login 
      });
    }
    case "login-password-mail": {
      const inOtp = getOtp(req);
      const dbLoginData = await config.user.getLoginData({ username: login });
      const dbOtpJwt = await config.session.getOtp(dbLoginData.id);
      const { decoded: dbOtp, err: errorVerifyOtp } = verifyOtpToken(config, dbOtpJwt);
      if(dbOtpJwt && inOtp && dbOtp) {        
        if(!bcrypt.compareSync(inOtp, dbOtp.otp)) {
          const { otpToken, error: errChangeOtp } = changeOtpToken(config, dbOtp);
          if(errChangeOtp && errChangeOtp.exp)
            throw new AuthorizationError("Code is expired", 400);
          if(errChangeOtp && errChangeOtp.timesToCheck) {
            config.session.deleteOtp(dbLoginData.id);
            throw new AuthorizationError("Requests limit exceeded", 400);
          }
          config.session.saveOtp(dbLoginData.id, otpToken);  
          throw new AuthorizationError("Incorrect code", 400);
        }
        await config.session.deleteOtp(dbLoginData.id);
        if(checkPassword.first_time_login || checkPassword.credentials_expired || checkPassword.credentials_expire_after) {
          const recoveryData = await config.user.setRecoveryToken(dbLoginData.id);
          if (!recoveryData.token) throw new AuthorizationError("Cannot get token");

          return res.status(200).json({
            // token_type: "Bearer",
            // accessToken,
            // refreshToken,
            token: recoveryData.token,
            credentials_expire_after: !checkPassword.credentials_expired ? checkPassword.credentials_expire_after : undefined,
            credentials_expired: checkPassword.credentials_expired,
            first_time_login: checkPassword.first_time_login 
          })
        }
        const { accessToken, refreshToken } = await startSession(req, config, dbLoginData.id);
        return res.status(200).json({
          token_type: "Bearer",
          accessToken,
          refreshToken,
          credentials_expire_after: !checkPassword.credentials_expired ? checkPassword.credentials_expire_after : undefined,
          credentials_expired: checkPassword.credentials_expired,
          first_time_login: checkPassword.first_time_login 
        });
      }
      else if (dbOtpJwt && inOtp && errorVerifyOtp && errorVerifyOtp.name === "TokenExpiredError")
        throw new AuthorizationError("Code is expired", 400);    
      else if (errorVerifyOtp && inOtp)
        throw new AuthorizationError("Wrong code", 400);
      else if (!inOtp) {
        const otp = cryptoRandomString({ 
          length: 6, 
          characters: "0123456789"
        });
        const otpRecord = generateOtpToken(config, otp);
        config.session.saveOtp(dbLoginData.id, otpRecord);    

        const { status, error } = await config.mail.send(otp, dbLoginData.mail);
        if(error)
          throw new AuthorizationError("Two-factor authorization failed: mail cannot be send", 500);
        return res.status(200).json({
          status: `Success, check email ${hideMail(dbLoginData.mail)}`
        });
      }
      else
        throw new AuthorizationError("Wrong data", 400);
    }
    case "login-password-otp": {
      const { session: { getOtpSalt } } = config;
      const inOtp = getOtp(req);
      const { otpsalt: currentSalt, otpsaltstatus } = await getOtpSalt(dbLoginData.id);

      if (otpsaltstatus !== "confirmed"){
        const { accessToken, refreshToken } = await startSession(req, config, dbLoginData.id);
        return res.status(200).json({
          auth_type: "login-password-otp",
          token_type: "Bearer",
          accessToken,
          refreshToken,
          status: `Otp secret status is ${otpsaltstatus}. Please, generate and confirm new secret`
        })
      }

      if (!inOtp && req.headers["one-time-password-checking-ignore"] !== "true")
        return res.status(200).json({ status: "One time password required" });
      if (isOtpValid(inOtp, currentSalt) || ((process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev") && req.headers["one-time-password-checking-ignore"] === "true")){
        if(checkPassword.first_time_login || checkPassword.credentials_expired || checkPassword.credentials_expire_after) {
          const recoveryData = await config.user.setRecoveryToken(dbLoginData.id);
          if (!recoveryData.token) throw new AuthorizationError("Cannot get token");

          return res.status(200).json({
            // token_type: "Bearer",
            // accessToken,
            // refreshToken,
            auth_type: "login-password-otp",
            token: recoveryData.token,
            // status: `Otp secret status is ${otpsaltstatus}. Please, generate and confirm new secret`,
            credentials_expire_after: !checkPassword.credentials_expired ? checkPassword.credentials_expire_after : undefined,
            credentials_expired: checkPassword.credentials_expired,
            first_time_login: checkPassword.first_time_login 
          })
        }
        const { accessToken, refreshToken } = await startSession(req, config, dbLoginData.id);

        return res.status(200).json({
          token_type: "Bearer",
          accessToken,
          refreshToken,
          auth_type: "login-password-otp",
          // status: `Otp secret status is ${otpsaltstatus}. Please, generate and confirm new secret`,
          credentials_expire_after: !checkPassword.credentials_expired ? checkPassword.credentials_expire_after : undefined,
          credentials_expired: checkPassword.credentials_expired,
          first_time_login: checkPassword.first_time_login 
        })
      }
      throw new AuthorizationError("Wrong authentication data");
    }
    default: {
      throw new AuthorizationError("Wrong authentication type", 500);
    }
    }
  }
  catch(err) {
    next(err);
  }
};

module.exports = LoginRouteHandler;