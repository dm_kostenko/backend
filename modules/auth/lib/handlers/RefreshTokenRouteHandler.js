const { generateRefreshToken, generateAccessToken ,verifyToken } = require("../helpers/token");
const { verifyUserInfo } = require("../helpers/userInfo");
const AuthorizationError = require("../helpers/AuthorizationError");

const RefreshTokenRouteHandler = async (req, res, next, config) => {
  try {
    const { refreshToken } = req.body;
    if(!refreshToken) 
      throw new AuthorizationError("Refresh token is incorrect", 403);

    const { decoded, err } = verifyToken(config, refreshToken);
    if(err)       
      throw new AuthorizationError("Refresh token is incorrect", 403);

    const session = await config.session.get(decoded.payload.id);
    if(!session)
      throw new AuthorizationError("Refresh token is incorrect", 403);

    const { token: refreshTokenFromDB } = session;
    if(refreshToken !== refreshTokenFromDB)
      throw new AuthorizationError("Refresh token is incorrect", 403);

    const { decoded: decodedFromDB } = verifyToken(config, refreshTokenFromDB);
    if(!verifyUserInfo(req, decoded, decodedFromDB))
      throw new AuthorizationError("Refresh token is incorrect", 403);
    
    req.auth = decoded.userPayload;
    const newRefreshToken = await generateRefreshToken(req, config, decoded.payload.id);
    await config.session.upsert({
      id: decoded.payload.id,
      ip: decoded.payload.ip,
      user_agent: decoded.payload.user_agent,
      token: newRefreshToken
    });
    const newAccessToken = await generateAccessToken(req, config, decoded.payload.id);
    
    res.status(200).json({ token_type: "Bearer", accessToken: newAccessToken, refreshToken: newRefreshToken });
  }
  catch(err) {
    next(err);
  }
};

module.exports = RefreshTokenRouteHandler;