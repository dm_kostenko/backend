const { getAccessToken, verifyToken } = require("../helpers/token");
const { verifyUserInfo } = require("../helpers/userInfo");
const AuthorizationError = require("../helpers/AuthorizationError");

const SecureRouteHandler = async (req, res, next, config) => {
  try {
    const { accessToken, err } = getAccessToken(req, config);
    console.log(accessToken)
     if(err && (process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev")) {
      req.auth = req.auth || { loginGuid: req.body.loginGuid };
      return next(); 
    } 
    else if(err)
      throw new AuthorizationError("Wrong Access Token", 403);
    
    const { decoded: decodedAccessToken, err: errAccessToken } = verifyToken(config, accessToken);
    if(errAccessToken) {
      console.log(errAccessToken)
      throw new AuthorizationError("Token broken or expired", 401);
    }
    
    const session = await config.session.get(decodedAccessToken.payload.id);
    if(!session)
      throw new AuthorizationError("There is no session", 403);

    const { decoded: decodedRefreshToken, err: errRefreshToken } = verifyToken(config, session.token);
    if(errRefreshToken)
      throw new AuthorizationError("DB Refresh token expired", 403);

    if(!verifyUserInfo(req, decodedAccessToken, decodedRefreshToken))
      throw new AuthorizationError("IP or/and user-agent were changed", 403);
      
    req.auth = decodedAccessToken.userPayload;
    const expiredPwd = await config.user.checkExpiredPassword(decodedAccessToken.userPayload);
    if(expiredPwd)
      throw new AuthorizationError("Token broken or expired", 401);
    next()

  }
  catch(err) {
    next(err);
  }
};

module.exports = SecureRouteHandler;