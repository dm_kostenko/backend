const jwt = require("jsonwebtoken");
const { getAccessToken, verifyToken } = require("../helpers/token");
const AuthorizationError = require("../helpers/AuthorizationError");

const LogoutRouteHandler = async (req, res, next, config) => {
  try {
    const { accessToken, err } = getAccessToken(req, config);
    if(err)
      throw new AuthorizationError("Wrong Access Token", 403);

    let { decoded: decodedAccessToken, err: errAccessToken } = verifyToken(config, accessToken);

    if(errAccessToken && errAccessToken.name === "TokenExpiredError")
      decodedAccessToken = jwt.decode(accessToken);
    else if(errAccessToken)
      throw new AuthorizationError("Wrong Access Token", 403);

    config.session.delete(decodedAccessToken.payload.id);
    
    await config.logger("logout", req, decodedAccessToken.userPayload);
    res.status(200).json({ msg: "Logout succeed" });
  }
  catch(err) {
    next(err);
  }
};

module.exports = LogoutRouteHandler;