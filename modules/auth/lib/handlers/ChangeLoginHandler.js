const startSession = require("../helpers/startSession");
const jwt = require("jsonwebtoken");
const AuthorizationError = require("../helpers/AuthorizationError");

const ChangeLoginHandler = async (req, res, next, config) => {
  try {
    const { logins = [] } = req.auth;
    const { loginGuid } = req.body;
    if (!loginGuid)
      throw new AuthorizationError("Incorrect data", 400);
    if (!logins.includes(loginGuid))
      throw new AuthorizationError(`Cannot change login to ${loginGuid}`);
    const { accessToken, refreshToken } = await startSession(req, config, loginGuid);      
    await config.logger("login", req, jwt.decode(accessToken).userPayload);
    return res.status(200).json({ token_type: "Bearer", accessToken, refreshToken });
  }
  catch(err) {
    next(err);
  }
};

module.exports = ChangeLoginHandler;