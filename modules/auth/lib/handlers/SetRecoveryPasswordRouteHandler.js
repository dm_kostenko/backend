const AuthorizationError = require("../helpers/AuthorizationError");

const SetRecoveryPasswordRouteHandler = async (req, res, next, config) => {
  try {
    const token = req.body.token;
    const checkData = await config.user.checkRecoveryToken(token);

    if (!checkData.status) throw new AuthorizationError("Token expired");
    if (!checkData.id) throw new AuthorizationError("id undefined");
    if (!req.body.password) throw new AuthorizationError("Password required");

    await config.user.setNewPassword(checkData.id, req.body.password);
    
    return res.status(200).json({
      status: "Password changed"
    });
  }
  catch (err) {
    next(err);
  }
};

module.exports = SetRecoveryPasswordRouteHandler;