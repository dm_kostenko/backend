const AuthorizationError = require("../helpers/AuthorizationError");

const CheckRecoveryTokenRouteHandler = async (req, res, next, config) => {
  try {
    const token = req.body.token;

    if (!token) throw new AuthorizationError("Token not specified");

    const checkData = await config.user.checkRecoveryToken(token);
    return res.status(200).json({
      status: checkData.status
    });
  }
  catch (err) {
    next(err);
  }
};

module.exports = CheckRecoveryTokenRouteHandler;