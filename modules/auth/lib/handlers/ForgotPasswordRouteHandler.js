const AuthorizationError = require("../helpers/AuthorizationError");
const { loginValidate, emailValidate, phoneValidate } = require("../helpers/validateInData");

const getDataFromBody = req => {
  try {
    if (req.body.username){
      if (!loginValidate(req.body.username)) throw new AuthorizationError("Incorrect login", 400);
      return { username: req.body.username };
    }
    else if (req.body.email){
      if (!emailValidate(req.body.email)) throw new AuthorizationError("Incorrect email", 400);
      return { email: req.body.email };
    }
    else if (req.body.phone){
      if (!phoneValidate(req.body.phone)) throw new AuthorizationError("Incorrect phone", 400);
      return { phone: req.body.phone };
    }
    else throw new AuthorizationError("Data undefined", 400);
  } catch (err) {
    throw new AuthorizationError(err.message || "Authorization error");
  }
};

const ForgotPasswordRouteHandler = async (req, res, next, config) => {
  try {
    const body = getDataFromBody(req);
    const dbLoginData = await config.user.getLoginData(body);
    if (!dbLoginData)
      throw new AuthorizationError("User not found", 400);

    const recoveryData = await config.user.setRecoveryToken(dbLoginData.id);
    if (!recoveryData.token) throw new AuthorizationError("Cannot get token");

    const link = config.user.baseRecoveryLink + recoveryData.token;
    
    const { error } = await config.recoveryPasswordMail.send(link, dbLoginData.mail, dbLoginData.language);
    console.log(error)
    if (error) throw new AuthorizationError("Recovery password failed: mail cannot be send", 500);

    let respText = "Link has been sent";
    if (recoveryData.email) respText += ` to ${recoveryData.email}`;

    return res.status(200).json({
      status: respText
    });
  }
  catch (err) {
    next(err);
  }
};

module.exports = ForgotPasswordRouteHandler;