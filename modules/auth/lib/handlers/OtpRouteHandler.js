const { generateSecret, isOtpValid, keyuri } = require("../helpers/otplib");
const AuthorizationError = require("../helpers/AuthorizationError");

const get = async (req, res, next, config) => {
  try {
    const { loginGuid, username } = req.auth;
    const { session: { setOtpSalt, serviceName: service } } = config;
    if (!loginGuid) 
      throw new AuthorizationError("Must be logged in");

    const newSalt = generateSecret();
    await setOtpSalt(loginGuid, newSalt);
    const otpAuth = keyuri(`Username: ${username}, Login: ${loginGuid}`, service, newSalt);
    return res.status(200).json({ otpAuth, otpSalt: newSalt });
  }
  catch(err) {
    next(err);
  }
};

const post = async (req, res, next, config) => {
  try {
    if (!req.auth) 
      if (process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev")
        return next;
      else 
        throw { message: "Must be authorized" };
      
    const { loginGuid } = req.auth;
    const { otp, disable } = req.body;
    const { session: { setOtpSalt, getOtpSalt, disableOtp } } = config;

    if (disable) {
      await disableOtp(loginGuid);

      return res.status(200).json({ message: "OTP disabled" });
    }


    const { otpsalt: currentSalt } = await getOtpSalt(loginGuid);
    if (!currentSalt)
      throw new AuthorizationError("Secret doesn't exist");

    if (isOtpValid(otp, currentSalt)) {
      await setOtpSalt(loginGuid, currentSalt, "confirmed");
      return res.status(200).json({ data: "Otp secret was updated" });
    }

    throw new AuthorizationError("Otp secret cannot be confirmed. Otp incorrect or expired");
  }
  catch(err) {
    next(err);
  }
};



module.exports = { get, post };