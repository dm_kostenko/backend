const RequestHandler = require("./lib/handlers/RequestHandler");
const GeneralValidator = require("./lib/validators/GeneralValidator");


module.exports = (config) => {
  const { errors, editConfig } = GeneralValidator(config);
  if(errors) {
    console.log("\x1b[31m%s\x1b[7m","");
    console.log("\x1b[31m%s\x1b[0m", "\nAuth module validation: FAILED\n");
    console.log("\x1b[31m%s\x1b[0m", " - " + errors.join("\n - ") + "\n\n");
    return () => {};
  }
  else {
    console.log("\x1b[32m%s\x1b[0m","\nAuth module validation: OK\n");
    return async (req, res, next) => await RequestHandler(req, res, next, editConfig);
  }
};