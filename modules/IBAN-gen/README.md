How to use?
-----------

It is necessary to transfer the bank code and bank account to the function arguments.

Example
-------
```js
const iban = require("./index.js");
console.log(iban.generate("89271234001"));
```