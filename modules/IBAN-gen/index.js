const { companyCode } = require("../../config")


mod97 = (digit_string) => {
  var m = 0;
  for (var i = 0; i < digit_string.length; ++i)
    m = (m * 10 + parseInt(digit_string.charAt(i))) % 97;
  return m;
}

ChecksumIBAN = (iban) => {
  var code = iban.substring(0, 2);
  var checksum = iban.substring(2, 4);
  var bban = iban.substring(4);

  // Assemble digit string
  var digits = "";
  for (var i = 0; i < bban.length; ++i) {
    var ch = bban.charAt(i).toUpperCase();
    if ("0" <= ch && ch <= "9")
      digits += ch;
    else
      digits += capital2digits(ch);
  };
  for (var i = 0; i < code.length; ++i) {
    var ch = code.charAt(i);
    digits += capital2digits(ch);
  };
  digits += checksum;

  // Calculate checksum
  checksum = 98 - mod97(digits);
  return fill0("" + checksum, 2);
};

fill0 = (s, l) => {
  while (s.length < l)
    s = "0" + s;
  return s;
}

capital2digits = (ch) => {
  var capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  for (var i = 0; i < capitals.length; ++i)
    if (ch == capitals.charAt(i))
      break;
  return i + 10;
};

CalcIBAN = (account) => {
  if (account && account.length !== 11) {
    const index = 11 - account.length;
    for (let i = 0; i < index; ++i)
      account = "0" + account;
    account += subAccount;
  }
  else if (!account)
    account = makeAccount();
  var checksum = ChecksumIBAN("LT00" + companyCode + account);
  return "LT" + checksum + companyCode + account;
};

makeAccount = (account) => {
  if (account) {
    if (account.substr(account.length - 3) == "999") {
      return("Invalid account");
    }    
    let newSubAccount = (+account + 1).toString();
    if (newSubAccount.length !== 11 && newSubAccount.length < 11) {
      const index = 11 - newSubAccount.length;
      for (let i = 0; i < index; ++i)
      newSubAccount = "0" + newSubAccount;
    }
    return newSubAccount;
  }
  else {
    var result = "";
    var characters = "0123456789";
    var charactersForFirstDigit = characters.substr(1);
    result += charactersForFirstDigit.charAt(Math.floor(Math.random() * charactersForFirstDigit.length));
    const subAccount = "001";
    for (var i = 0; i < 7; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    };
    return result + subAccount;
  }
};

module.exports.generateIBAN = CalcIBAN;
module.exports.generateAccount = makeAccount;