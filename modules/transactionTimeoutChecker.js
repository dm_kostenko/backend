const db = require("../helpers/db/api/");
const emitter = require("./transactionsProcessor");
const initiator = "timeoutAutoCheck";
const msInMinute = 60000;
const transactionTimeoutChecker = async timeoutMinutes => {
  const [ pendingTransactions ] = await db.transactionProcessing.get({ input_status: "pending" });
  
  pendingTransactions.map(transaction => {
    const lastUpdate = new Date(transaction.updated_at || transaction.created_at);
    const differenceInMinutes = Math.round((Date.now() - lastUpdate) / msInMinute);
    if (differenceInMinutes > timeoutMinutes) 
      emitter.emit("transactionFailed", initiator, transaction, { initiator });
  });
};

module.exports = (timeoutMinutes, intervalMinutes) => setInterval(async () => transactionTimeoutChecker(timeoutMinutes), intervalMinutes * msInMinute);
