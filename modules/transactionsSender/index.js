const axios = require("axios")
const https = require("https")
const db = require("../../helpers/db/bankApi")
const { certs: { cert } } = require("../../config/")

let options = { baseURL: process.env.SENDER_SERVICE_URL }

if([ "uat", "prod" ].includes(process.env.NODE_ENV))
  options.httpsAgent = new https.Agent({ ca: cert })

const instance = axios.create(options)

const encrypt = (docs) => {
  // encrypt docs
  return docs
}

const sender = async() => {
  try {
    const docs = await db.demon.get_outbox();
    if(docs.length > 0) {
      const encryptedDocs = encrypt(docs);
      const response = await instance.post("/", { encryptedDocs });
      if(response.data.status === "OK" && response.data.savedDocs) {
        await Promise.all(response.data.savedDocs.map(async(doc) => {
          await db.demon.update_status({
            input_guid: doc.guid,
            input_status: "Sent"
          })
        }))
      }
    }
  }
  catch(err) {
    throw(err)
  }
}

setInterval(sender, parseInt(process.env.INTERVAL) || 60000)