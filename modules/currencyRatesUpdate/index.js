const getCurrencyRates = require("./api/getCurrencyRates");
const db = require("../../helpers/db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";
const { getCETDate, isHoliday } = require("../../helpers/target2Timing");


const updateRates = async() => {
  let connection;

  try {
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    const { rates, date } = await getCurrencyRates();
    rates.forEach(async(item) => {
      await db.currency_rate.upsert({
        input_currency_code: item.currency,
        input_date: date,
        input_rate: item.rate,
        input_author_guid: author_guid
      }, connection)
    })
    await db.currency_rate.upsert({
      input_currency_code: "EUR",
      input_date: date,
      input_rate: "1",
      input_author_guid: author_guid
    }, connection)

    await db._transaction.commit(connection);

  }
  catch(err) {
    if(connection)
      await db._transaction.rollback(connection);
    throw(err)
  }
  finally {
    if(connection)
      connection.release()
  }
};
setInterval(() => {
  const date = getCETDate();
  const hours = date.getUTCHours();
  const minutes = date.getUTCMinutes();
  const seconds = date.getUTCSeconds();

  const isTime = hours === 18 && minutes === 5 && seconds === 0;
  
  if(isTime && !isHoliday())
    updateRates();
}, 1000);