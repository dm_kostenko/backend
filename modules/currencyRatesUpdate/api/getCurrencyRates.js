const xml2js = require("xml2js");
const axios = require("axios");

const _xml2json = async(xml) => {
  return new Promise((resolve, reject) => xml2js.parseString(xml, { trim: true, explicitArray: false, async: true }, (err, json) => err ? reject(err) : resolve(json)));
}

const getCurrencyRates = async() => {
  const { data } = await axios.get("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
  const json = await _xml2json(data);
  const rates = json["gesmes:Envelope"].Cube.Cube.Cube.map(item => item.$)
  return { 
    rates, 
    date: json["gesmes:Envelope"].Cube.Cube.$.time
  };
}


module.exports = getCurrencyRates;