const EventEmitter = require("events").EventEmitter;

const emitLog = new EventEmitter();

module.exports = emitLog;

//Initialize events
require("./lib/events");