const log = require("../../../../helpers/logger");
const secureData = [ "email", "email_canonical", "phone", "password", "salt", "name_on_card", "first_name", "last_name", "holder", "verification_value", "exp_year" ];
const cardNumber = [ "number", "card_number", "cardNumber" ];

function copyRecursive(obj){
  let output, value, key;
  output = Array.isArray(obj) ? [] : {};
  for (key in obj) {
    value = obj[key];
    if (typeof value === "object") output[key] = copyRecursive(value);
    else if (secureData.includes(key)) output[key] = "*******";
    else output[key] = cardNumber.includes(key) ? "************" + obj[key].substr(-4) : value;
  }
  return output;
}

module.exports.filter = (obj) => {
  try {
    if (!obj) return obj;
    if (typeof obj !== "object") obj = JSON.parse(obj);
    return copyRecursive(obj);
  } catch(err){
    log.error(err.stack);
  }
};