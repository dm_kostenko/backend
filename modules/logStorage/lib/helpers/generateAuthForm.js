module.exports.generateAuthForm = ({ merchant, group, partner, loginGuid }) => {
  let auth = {
    type: "NO TYPE",
    guid: null
  };

  if (merchant){
    auth.type = "merchant";
    auth.guid = merchant.merchant_guid;
    auth.group_guid = merchant.group_guid;
    auth.partner_guid = merchant.partner_guid;
  }
  else if (group){
    auth.type = "group";
    auth.guid = group.group_guid;
    auth.partner_guid = group.partner_guid;
  }
  else if (partner){
    auth.type = "partner";
    auth.guid = partner.partner_guid;
  } else if (loginGuid){
    auth.type = "admin";
    auth.guid = loginGuid;
  }

  return auth;
};