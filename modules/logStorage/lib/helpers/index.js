module.exports = {
  generateAuthForm: require("./generateAuthForm").generateAuthForm,
  filter: require("./filter").filter
};