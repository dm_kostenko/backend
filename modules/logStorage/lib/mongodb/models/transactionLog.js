const { mongoose } = require("../connect");
const Schema = mongoose.Schema;

const transactionLog = new Schema({
  step_info: { type: String },
  request_id: {
    type: String,
    required: true
  },
  partner_guid: { type: String, index: true },
  partner_name: { type: String },
  group_guid: { type: String, index: true },
  group_name: { type: String },
  merchant_guid: { type: String, index: true },
  merchant_name: { type: String },
  transaction_guid: { type: String },
  shop_guid: { type: String },
  shop_name: { type: String },
  message: { type: Object },
  createdAt: {
    type: Date,
    required: true,
    index: true
  }
}, { capped: true, size: 2048, max: 3000 });

transactionLog.statics.get = async function({
  request_id,
  partner_guid,
  partner_name,
  group_guid,
  group_name,
  merchant_guid,
  merchant_name,
  transaction_guid,
  step_info,
  shop_guid,
  shop_name,
  from_date,
  to_date,
  page = 1,
  items = 100
}){
  let query = {
    request_id: request_id ? { $regex: request_id } : undefined,
    partner_guid: partner_guid ? { $regex: partner_guid } : undefined,
    partner_name: partner_name ? { $regex: partner_name } : undefined, 
    group_guid: group_guid ? { $regex: group_guid } : undefined,
    group_name: group_name ? { $regex: group_name } : undefined,
    merchant_guid: merchant_guid ? { $regex: merchant_guid } : undefined,
    merchant_name: merchant_name ? { $regex: merchant_name } : undefined,
    transaction_guid: transaction_guid ? { $regex: transaction_guid } : undefined,
    step_info: step_info ? { $regex: step_info } : undefined,
    shop_guid: shop_guid ? { $regex: shop_guid } : undefined,
    shop_name: shop_name ? { $regex: shop_name } : undefined,
    createdAt: (from_date || to_date) ? {
      $gte: from_date ? new Date(from_date) : undefined,
      $lte: to_date ? new Date(to_date) : undefined 
    } : undefined
  };

  query = JSON.parse(JSON.stringify( query ));

  let count = this.countDocuments(query).exec();
  let data = this
    .find(query)
    .sort({ createdAt: "desc" })
    .skip((page - 1) * items)
    .limit(parseInt(items))
    .exec();

  [ count, data ] = await Promise.all([ count, data ]);

  return {
    count,
    data
  };
};

module.exports.transactionLog = mongoose.model("transactionLog", transactionLog);
