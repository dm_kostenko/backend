const { mongoose } = require("../connect");
const Schema = mongoose.Schema;

const transactionStepLog = new Schema({
  id:{
    type: String,
    required: true
  },
  step_guid:{
    type: String,
    required: true
  },
  transaction_processing_guid:{
    type: String,
    required: true
  },
  name:{
    type: String,
    required: true
  },
  status: { type: String },
  error: { type: Object },
  params: { type: Object },
  createdAt: {
    type: Date,
    required: true
  }
}, { capped: true, size: 2048, max: 3000 });


transactionStepLog.statics.get = async function(transaction_guid, page = 1, items = 100){
  let query = { transaction_processing_guid: transaction_guid };
  query = JSON.parse(JSON.stringify( query ));

  let count = this.countDocuments(query).exec();
  let data = this
    .find(query)
    .sort({ createdAt: "desc" })
    .skip((page - 1) * items)
    .limit(parseInt(items))
    .exec();

  [ count, data ] = await Promise.all([ count, data ]);

  return {
    count,
    data
  };
};

module.exports.transactionStepLog = mongoose.model("transactionStepLog", transactionStepLog);
