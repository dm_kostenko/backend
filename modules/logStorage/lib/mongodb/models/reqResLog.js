const { mongoose } = require("../connect");
const Schema = mongoose.Schema;

const reqResLogSchema = new Schema({
  request_id:{ type: String },
  group_guid:{ type: String },
  partner_guid:{ type: String },
  url: {
    type: String,
    required: true
  },
  ip: { type: String },
  headers: { type: Object },
  parameters: { type: Object },
  userType: { type: String },
  guid: { type: String },
  reqBody: { type: Object },
  resStatusCode: { type: Number },
  resBody: { type: Object },
  createdAt: {
    type: Date,
    required: true,
    index: true
  }
}, { capped: true, size: 2048, max: 3000 });

reqResLogSchema.statics.get = async function({
  from_date,
  to_date,
  userType, guid,
  url, ip, resStatusCode,
  request_id,
  group_guid,
  partner_guid,
  page = 1,
  items = 100
}){
  let query = {
    group_guid: group_guid,
    partner_guid: partner_guid,
    userType: userType,
    resStatusCode: resStatusCode,
    request_id: request_id ? { $regex: request_id } : undefined,
    guid: guid ? { $regex: guid } : undefined,
    url: url ? { $regex: url } : undefined,
    ip: ip ? { $regex: ip } : undefined,
    createdAt: (from_date || to_date) ? {
      $gte: from_date ? new Date(from_date) : undefined,
      $lte: to_date ? new Date(to_date) : undefined 
    } : undefined
  };

  query = JSON.parse(JSON.stringify( query ));

  let count = this.countDocuments(query).exec();
  let data = this
    .find(query)
    .sort({ createdAt: "desc" })
    .skip((page - 1) * items)
    .limit(parseInt(items))
    .exec();

  [ count, data ] = await Promise.all([ count, data ]);

  return {
    count,
    data
  };
};


module.exports.reqResLog = mongoose.model("reqResLog", reqResLogSchema);
