const { reqResLog, transactionLog, transactionStepLog } = require("./mongodb/models");
const log = require("../../../helpers/logger");
const { filter, generateAuthForm } = require("./helpers");
const db = require("../../../helpers/db/api/");

module.exports.loginLogoutLog = async (req, authPayload) => {
  try {
    const auth = generateAuthForm(authPayload);
    const utc = new Date();
    const mongolog = new reqResLog({
      request_id: req.id,
      group_guid: auth.group_guid,
      partner_guid: auth.partner_guid,
      url: req.originalUrl,
      ip: req.headers["x-real-ip"] || req.connection.remoteAddress,
      headers: req.headers,
      parameters: req.params,
      userType: auth.type,
      guid: auth.guid,
      reqBody: {},
      resStatusCode: 200,
      resBody: {},
      createdAt: utc.setHours( utc.getHours() + 7)
    });
    mongolog.save();
  } catch(err){
    log.error(`LogError: ${err} EMITLOG: "loginLogoutLog"`);
    console.log(`LogError: ${err} EMITLOG: "loginLogoutLog"`);
  }

};

module.exports.writeReqResLog = async (req, res, next) => {
  try {
    if (req.method !== "POST") return;

    const defaultWrite = res.write;
    const defaultEnd = res.end;
    const chunks = [];
    let body;
  
    res.write = (...restArgs) => {
      chunks.push(Buffer.from(restArgs[0]));
      defaultWrite.apply(res, restArgs);
    };
  
    res.end = (...restArgs) => {
      if (restArgs[0]) {
        chunks.push(Buffer.from(restArgs[0]));
      }
      body = Buffer.concat(chunks).toString("utf8");
  
      if (req.auth){
        const auth = generateAuthForm(req.auth);
        const utc = new Date();
        const mongolog = new reqResLog({
          request_id: req.id,
          group_guid: auth.group_guid,
          partner_guid: auth.partner_guid,
          url: req.originalUrl,
          ip: req.headers["x-real-ip"] || req.connection.remoteAddress,
          headers: req.headers,
          parameters: req.params,
          userType: auth.type,
          guid: auth.guid,
          reqBody: filter(req.body),
          resStatusCode: res.statusCode,
          resBody: filter(body),
          createdAt: utc.setHours( utc.getHours() + 7)
        }); 
        mongolog.save();
      }
      defaultEnd.apply(res, restArgs);
    };
  } catch(err){
    log.error(`LogError: ${err} EMITLOG: "writeReqResLog"`);
    console.log(`LogError: ${err} EMITLOG: "writeReqResLog"`);
  }
};

module.exports.transactionLog = async ({ step_info, request_id, transaction_guid, shop_guid, message }) => {
  try {
    let infoByShopGuid;

    if (!shop_guid){
      log.error("LogError: ShopGuid undefined EMITLOG: 'transactionLog'");
      console.log("LogError: ShopGuid undefined EMITLOG: 'transactionLog'");
    } else {
      [[ infoByShopGuid ]]= await db.shop.info({ input_guid: shop_guid });
    }

    const utc = new Date();
    const mongolog = new transactionLog({
      step_info,
      partner_guid: infoByShopGuid ? infoByShopGuid.partner_guid : undefined,
      partner_name: infoByShopGuid ? infoByShopGuid.partner_name : undefined,
      group_guid: infoByShopGuid ? infoByShopGuid.group_guid : undefined,
      group_name: infoByShopGuid ? infoByShopGuid.group_name : undefined,
      merchant_guid: infoByShopGuid ? infoByShopGuid.merchant_guid : undefined,
      merchant_name: infoByShopGuid ? infoByShopGuid.merchant_name : undefined,
      request_id,
      transaction_guid,
      shop_guid,
      shop_name: infoByShopGuid ? infoByShopGuid.name : undefined,
      message: filter(message),
      createdAt: utc.setHours( utc.getHours() + 7)
    });
    mongolog.save();
  } catch(err){
    log.error(`LogError: ${err} EMITLOG: "transactionLog"`);
    console.log(`LogError: ${err} EMITLOG: "transactionLog"`);
  }
};

module.exports.stepTransactionLog = async (requestId, processingStep, params, status, error) => {
  try {
    const utc = new Date();
    const mongolog = new transactionStepLog({
      id: requestId,
      step_guid: processingStep.guid,
      transaction_processing_guid: processingStep.transaction_processing_guid,
      name: processingStep.name,
      status: status,
      error: error,
      params: params,
      createdAt: utc.setHours( utc.getHours() + 7)
    });
    mongolog.save();
  } catch(err){
    log.error(`LogError: ${err} EMITLOG: "stepTransactionLog"`);
    console.log(`LogError: ${err} EMITLOG: "stepTransactionLog"`);
  }
};