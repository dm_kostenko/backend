const emitLog = require("../emitLog");
const { loginLogoutLog, writeReqResLog, transactionLog, stepTransactionLog } = require("./handlers");

emitLog.on("loginLogoutLog", loginLogoutLog);
emitLog.on("writeReqResLog", writeReqResLog);
emitLog.on("transactionLog", transactionLog);
emitLog.on("stepTransactionLog", stepTransactionLog);