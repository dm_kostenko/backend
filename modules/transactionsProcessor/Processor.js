const db = require("../../helpers/db/api/");
const { ProcessorError } = require("../../errorTypes");

class Processor{
  constructor(shopid, gatewayid, currencyid, transactionid, transactionType){
    this.shopid = shopid;
    this.gatewayid = gatewayid;
    this.currencyid = currencyid;
    this.transactionid = transactionid;
    this.transactionType = transactionType;
  }

  async processRates(status, amount, transaction_type_guid){
    await this._setStatus(status);

    const [ base_amount, buy_rate, sell_rate, rate ] = await Promise.all([
      this._checkTransactionType(amount),
      this._getTransactionRate("buy", transaction_type_guid),
      this._getTransactionRate("sell", transaction_type_guid),
      this._getRate()
    ]);

    const to_bank = Math.round((base_amount * buy_rate) / 100);
    const to_processor = Math.round((base_amount * sell_rate) / 100);
    const hold_amount = this.status ? Math.round((base_amount * rate.hold_rate / 100)) : 0;
    const to_client = base_amount - to_bank - to_processor - hold_amount;

    await db.transactionOverview.upsert({
      input_transaction_processing_guid: this.transactionid,
      input_base_amount: base_amount,
      input_to_processor: to_processor,
      input_to_bank: to_bank,
      input_to_client: to_client,
      input_hold: hold_amount,
      input_author_guid: this.shopid
    });
  }

  async _setStatus(status){
    if (status === "Success") this.status = 1;
    else if (status === "Failed") this.status = 0;
    else throw new ProcessorError(`Unknown transaction status=${status}`);
  }

  async _checkTransactionType(base_amount){
    switch(this.transactionType){
    case "Authorization":
      if (Math.sign(base_amount) < 0){
        console.log("WarningTransaction: Transaction type=Authorization, but amount < 0");
        return Math.abs(base_amount);
      }
      else return base_amount;
    case "Payment":
      if (Math.sign(base_amount) < 0){
        console.log("WarningTransaction: Transaction type=Payment, but amount < 0");
        return Math.abs(base_amount);
      }
      else return base_amount;
    case "Capture":
      if (Math.sign(base_amount) < 0){
        console.log("WarningTransaction: Transaction type=Capture, but amount < 0");
        return Math.abs(base_amount);
      }
      else return base_amount;
    case "Refund":
      if (Math.sign(base_amount) > 0){
        console.log("WarningTransaction: Transaction type=Refund, but amount > 0");
        return (-1) * Math.abs(base_amount);
      }
      else return base_amount;
    default: throw new ProcessorError(`Unknown transaction type=${this.transactionType}`);
    }
  }
  

  async _getRate(){
    const [[ rate ]] = await db.rate.get({
      input_shop_guid: this.shopid,
      input_gateway_guid: this.gatewayid,
      input_currency_guid: this.currencyid
    });
    
    if (!rate){
      console.log(`Unknown rate for shop=${this.shopid} gateway=${this.gatewayid} currency=${this.currencyid}`);
      return {
        hold_rate: 0,
        hold_period: 0,
        apply_from: Date.now(),
        apply_to: Date.now() + 777
      };
    }

    return {
      hold_rate: rate.hold_rate,
      hold_period: rate.hold_period,
      apply_from: rate.apply_from,
      apply_to: rate.apply_to
    };
  }

  async _getTransactionRate(type, transaction_type_guid){
    const [[ rate ]] = await db.transactionRate.get({
      input_shop_guid: this.shopid,
      input_gateway_guid: this.gatewayid,
      input_currency_guid: this.currencyid,
      input_transaction_guid: transaction_type_guid,
      input_type: type
    });

    if (!rate){
      console.log(`Unknown ${type} rate for shop=${this.shopid} gateway=${this.gatewayid} currency=${this.currencyid}`);
      return 0;
    }

    return this.status ? rate.success : rate.failure;
  }
}

module.exports = Processor;