const emitter = require("..");
const emitLog = require("../../logStorage/emitLog");
const db = require("../../../helpers/db/api/");
const steps = require("./steps");
const { getDescendantProp } = require("../../../helpers/tools");

module.exports.stepProcess = async (transaction, data) => {
  try {
    const { params, initiator } = data;
    const { currentStep } = data.template;
    const [[ processingStep ]] = await db.stepProcessing.upsert({ 
      input_transaction_processing_guid: transaction.guid,
      input_author_guid: initiator,
      input_name: currentStep.step_name,
      input_type: currentStep.step_type,
      input_status: "Started"
    });
    emitLog.emit("stepTransactionLog", data.requestId, processingStep, params, "Started");
    
    if (typeof steps[currentStep.step_type] !== "function")
      throw { message: `Handler ${currentStep.step_type} isn't correct` };

    const saveParams = await steps[currentStep.step_type]({ data, processingStep, transaction_guid: transaction.guid });
    if (Array.isArray(saveParams) && saveParams.length) {
      await Promise.all(saveParams.map(async param => {
        const [ key, path ] = param.split(":"); 
        await db.stepParamProcessing.upsert({ 
          input_step_processing_guid: processingStep.guid,
          input_name: key,
          input_value: path ? getDescendantProp(data.params, path) : data.params[key] || "undefined",
          input_author_guid: initiator,
        });
      }));
    }

    if (data.params.redirect3DData && !data.redirectedTo3D){
      emitter.emit("transactionRedirect3dRequired", transaction, data);
      data.redirectedTo3D = true;
    }
    emitter.emit("stepSucceed", processingStep, data);
    if (data.template.steps.length) {
      data.template.currentStep = data.template.steps.shift();
      emitter.emit("stepProcess", transaction, data);
    } else 
      emitter.emit("transactionSucceed", transaction, data);
  } catch (error) {
    if (error.processingStep) {
      emitter.emit("stepFailed", error, transaction, error.processingStep, data);
      emitLog.emit("stepTransactionLog", data.requestId, error.processingStep, error.processedParams, "Failed");
    } else 
      emitter.emit("transactionFailed", error, transaction, data);
  }
};

module.exports.stepSucceed = async (step, data) => {
  try {
    const { initiator } = data;
    const [[ processingStep ]] = await db.stepProcessing.upsert({ input_guid: step.guid, input_author_guid: initiator, input_status: "Success" });
    emitLog.emit("stepTransactionLog", data.requestId, processingStep, undefined, "Success");
  } catch (error) {
    emitter.emit("unexpectedBehavior", error);
  }
};

module.exports.stepFailed = async (error, transaction, step, data) => {
  try {
    const { initiator } = data;
    const [[ processingStep ]] =  await db.stepProcessing.upsert({ input_guid: step.guid, input_status: "Failed", input_author_guid: initiator });
    emitLog.emit("stepTransactionLog", data.requestId, processingStep, undefined, "Failed", error);
    emitter.emit("transactionFailed", error, transaction, data);
  } catch (error) {
    emitter.emit("unexpectedBehavior", error);
  }
};