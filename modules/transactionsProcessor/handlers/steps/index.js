module.exports = {
  Initialization: require("./initialization"),
  End: async () => {},
  GatewayCreatePaymentTest: async () => {},
  GatewayGetResponseTest: async () => {},
  GatewayProcessPaymentTest: async () => {},
  request: require("./request"),  
  getSavedParams: require("./getSavedParams")
};