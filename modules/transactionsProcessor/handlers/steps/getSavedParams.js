const db = require("../../../../helpers/db/api/");

module.exports = async ({ data }) => {
  const { params } = data;
  const { currentStep } = data.template;

  const config = JSON.parse(currentStep.params.getParams);
  const [[ savedStep ]] = await db.stepProcessing.get({ input_transaction_processing_guid: params.transactionId, input_name: config.step_name });
  if (!savedStep)
    throw `Step ${config.step_name} not found in db.stepProcessing for ${params.transactionId}`;
    
  const [ savedParams ] = await db.stepParamProcessing.get({ input_step_processing_guid: savedStep.guid  });
  if (!savedParams || !savedParams.length)
    throw `Params for ${savedStep.step_processing_guid} not found in db.stepParamProcessing`;
  
  const savedParamsAsObject = savedParams.reduce((acc, savedParam) => ({ ...acc, [savedParam.name]: savedParam.value }), {});

  data.params = { ...params, ...savedParamsAsObject };
  return []; 
};