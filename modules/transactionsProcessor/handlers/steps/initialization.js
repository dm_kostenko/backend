const db = require("../../../../helpers/db/api/");
const { fillForm } = require("../../../../helpers/tools"); 
const notRequiredParams = [ "gatewayGuid", "notification_url", "tracking_id", "return_url" ];

module.exports = async ({ data, processingStep }) => {
  const { params, initiator } = data;
  const { currentStep } = data.template;
  const map = JSON.parse(currentStep.params.map);
  const templatedData = fillForm(map, params);

  await Promise.all(Object.keys(templatedData).map(async key => {
    await db.stepParamProcessing.upsert({ 
      input_step_processing_guid: processingStep.guid,
      input_name: key,
      input_value: templatedData[key] || "undefined",
      input_author_guid: initiator,
    });
  }));
  const [ processedParams ] = await db.stepParamProcessing.get({ input_step_processing_guid: processingStep.guid });

  processedParams.forEach(param => {
    if (param.value === "undefined" && !notRequiredParams.indexOf(param.name)) 
      throw { processingStep, processedParams };
  });

  return processedParams;
};