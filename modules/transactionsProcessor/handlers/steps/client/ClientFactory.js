const RestClient = require("./RestClient");

const ClientFactory = config => {
  if (config.type === "rest")
    return new RestClient(config);
  else 
    throw "Client cannot be initialized";
};

module.exports = ClientFactory;