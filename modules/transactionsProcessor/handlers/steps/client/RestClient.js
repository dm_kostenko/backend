const axios = require("axios");
const { getDescendantProp, findDeepNestedProperty } = require("../../../../../helpers/tools");

class RestClient {
  constructor({ auth, timer, request, response, doLog }){
    this.client = axios;

    this.auth = auth;
    this.timer = timer;
    this.request = request;
    this.response = response;
    this.doLog = doLog;
  }

  setBearerToken() {
    this.client.defaults.headers.common.authorization = `Bearer ${this.auth.token}`;
    return this;
  }

  successConditionCheck(data) {
    const results = [];
    const [[ field, value ]] = Object.entries(this.timer.successCondition);
    findDeepNestedProperty(data, field, value, results);
    return results.length ? results : false;
  }

  failConditionCheck(data) {
    const results = [];
    const [[ field, value ]] = Object.entries(this.timer.failCondition);
    findDeepNestedProperty(data, field, value, results);
    return results.length ? results : false;
  }

  redirectConditionCheck(data) {
    const results = [];
    const [[ field, value ]] = Object.entries(this.timer.redirectCondition);
    findDeepNestedProperty(data, field, value, results);
    return results.length ? results : false;
  }

  async doSingleRequest() {
    const { method, url, headers, body  } = this.request;
    this.setBearerToken(this.auth.token);

    this.doLog("Request to Gateway", { method, url, data: body, headers });
    const { data } = await this.client({ method, url, data: body, headers });
    this.doLog("Response from Gateway", data);
    return data;
  }

  async doTimerRequest() {
    const { interval, limit } = this.timer;
    let counter = 0;
    const timerRequests = (resolve, reject) => {
      const repetativeFunction = setInterval(async () => {
        if (counter++ > limit) {
          clearInterval(repetativeFunction);
          reject({ reason: "Limit overrun" });
        }
        const data = await this.doSingleRequest();
        const successCheck = this.successConditionCheck(data); 
        if (successCheck){
          clearInterval(repetativeFunction);
          resolve({ successPayment: successCheck[0] });
        }
        
        const failCheck = this.failConditionCheck(data);
        if (failCheck){
          clearInterval(repetativeFunction);
          reject({ failedPayment: failCheck[0] });
        }

        if (this.timer.redirectCondition){
          const redirectCheck = this.redirectConditionCheck(data);
          if (redirectCheck){
            clearInterval(repetativeFunction);
            resolve({ redirect3DData: redirectCheck[0] });
          }
        }
      }, interval);
    };

    const timerEnded = await new Promise(timerRequests);
    return timerEnded;
  }

  async doRequest(){
    if (!this.timer){
      const responseData = await this.doSingleRequest();
      if (!this.response.map)
        return true;
      if (this.response.map.length)
        return this.response.map.reduce((result, field) => {
          if (field.split(":").length > 1) {
            const [ key, path ] = field.split(":");
            return { ...result, [key]: getDescendantProp(responseData, path) };
          } else return { ...result, [field]: responseData[field] };
        }, {});
      else return responseData;
    }
    else {
      return await this.doTimerRequest();
    }
  }
}
module.exports = RestClient;