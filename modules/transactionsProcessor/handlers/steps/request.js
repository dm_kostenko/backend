const { fillHTMLTemplate } = require("../../../../helpers/tools"); 
const ClientFactory = require("./client/ClientFactory");
const emitLog = require("../../../logStorage/emitLog");

module.exports = async ({ data, transaction_guid }) => {
  const { params, initiator } = data;
  const { currentStep } = data.template;

  const filledConfig = fillHTMLTemplate(currentStep.params.requestConfig, params);
  const filledObject = JSON.parse(filledConfig);

  const Client = ClientFactory({ ...filledObject, doLog: (step_info, message) => {
    emitLog.emit("transactionLog", {
      step_info: `${currentStep.step_name}: ${step_info}`,
      message,
      request_id: data.requestId,
      transaction_guid,
      shop_guid: initiator
    });
  } });
  
  const response = await Client.doRequest();
  data.params = { ...params, ...response };
  return filledObject.saveParams ? filledObject.saveParams : []; 
};