const emitter = require("..");
const db = require("../../../helpers/db/api/");
const Processor = require("../Processor");

module.exports.create = async data => {
  let transaction;
  try {
    const { initiator } = data;
    const { accountGuid, transaction_guid: transactionGuid } = data.template;
    const status = "Pending";
    [[ transaction ]] = await db.transactionProcessing.upsert({ input_account_guid: accountGuid, input_transaction_guid: transactionGuid, input_status: status, input_author_guid: initiator });
    if (data.template.steps.length) {
      data.template.currentStep = data.template.steps.shift();
      emitter.emit("stepProcess", transaction, data);
    } else
      throw { message: "Steps doesn't implemented" };
  } catch (error) {
    transaction ? emitter.emit("transactionFailed", error, transaction, data) : emitter.emit("unexpectedBehavior", error);
  }
};

module.exports.redirect3dRequired = async (transaction, data) => {
  try {
    const { initiator } = data;
    const status = "3Dwaiting";
    await db.transactionProcessing.upsert({ input_guid: transaction.guid, input_status: status, input_author_guid: initiator });
    emitter.emit(`transactionRedirect3dRequired${data.requestId}`, transaction.guid, data.params.redirect3DData);
  } catch (error) {
    emitter.emit("unexpectedBehavior", error);
  }
};

module.exports.succeed = async (transaction, data) => {
  try {
    const { initiator } = data;
    const { shopGuid, gatewayGuid, currencyGuid, transaction_guid: transaction_type_guid } = data.template;
    const status = "Success";
    await db.transactionProcessing.upsert({ input_guid: transaction.guid, input_status: status, input_author_guid: initiator });

    const processor = new Processor(shopGuid, gatewayGuid, currencyGuid, transaction.guid, data.params.type);
    await processor.processRates(status, data.params.amount, transaction_type_guid).catch(err => {
      console.error(err);
    });

    emitter.emit(`transactionSucceed${data.requestId}`, transaction.guid);
  } catch (error) {
    emitter.emit("unexpectedBehavior", error);
  }
};

module.exports.failed = async (error, transaction, data) => {
  try {
    const { initiator } = data;
    const { shopGuid, gatewayGuid, currencyGuid, transaction_guid: transaction_type_guid } = data.template;
    const status = "Failed";
    console.error(error);
    await db.transactionProcessing.upsert({ input_guid: transaction.guid, input_status: status, input_author_guid: initiator });

    const processor = new Processor(shopGuid, gatewayGuid, currencyGuid, transaction.guid, data.params.type);
    await processor.processRates(status, data.params.amount, transaction_type_guid).catch(err => {
      console.error(err);
    });

    emitter.emit(`transactionFailed${data.requestId}`, transaction.guid);
  } catch (error) {
    emitter.emit("unexpectedBehavior", error);
  }
};