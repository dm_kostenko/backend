const EventEmitter = require("events").EventEmitter;

class TransactionsEmitter extends EventEmitter {
  constructor(unexpectedBehaviorCallback) {
    super();
    if (unexpectedBehaviorCallback)
      this.on("unexpectedBehavior", unexpectedBehaviorCallback);
    else
      this.on("unexpectedBehavior", error => {
        console.error("Unexpected behavior");
        console.error(error);
      });
  } 

  emit(event, ...args) {
    if (!this.listeners(event).length)
      throw { message: `Event doesn't implemented: ${event}`, args };  
    else
      super.emit(event, ...args);
  }
}

const emitter = new TransactionsEmitter();
module.exports = emitter;

//Initialize events
require("./events");
