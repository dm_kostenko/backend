const emitter = require("../");
const { transactionHandlers } = require("../handlers");

emitter.on("transactionCreate", transactionHandlers.create);
emitter.on("transactionRedirect3dRequired", transactionHandlers.redirect3dRequired);
emitter.on("transactionSucceed", transactionHandlers.succeed);
emitter.on("transactionFailed", transactionHandlers.failed);