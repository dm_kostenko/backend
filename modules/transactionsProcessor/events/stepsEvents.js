const emitter = require("..");
const { stepHandlers } = require("../handlers");

emitter.on("stepSucceed", stepHandlers.stepSucceed);
emitter.on("stepFailed", stepHandlers.stepFailed);

emitter.on("stepProcess", stepHandlers.stepProcess);