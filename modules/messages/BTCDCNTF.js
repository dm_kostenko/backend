const Message = require("./Message");
const type = "BTCDCNTF";
const version = "camt.054.001.05";

class BTCDCNTF extends Message {
  constructor({ parsedXml }){
    const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
    super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type, version });    
    const { CreDtTm } = parsedXml.Msg.Docs.Doc.Btcdcntf.Document.BkToCstmrDbtCdtNtfctn.GrpHdr;

    this._calculateGroupHeader({
      MsgId,
      CreDtTm
    })
    this.parseDocs(parsedXml.Msg.Docs.Doc.Btcdcntf.Document.BkToCstmrDbtCdtNtfctn);
  }

  _calculateGroupHeader({
    MsgId,
    CreDtTm
  }){
    this.groupHeader = {
      MsgId: MsgId,
      CreDtTm
    };
  }

  parseDocs (xmlDocs) {
    const { 
      Id, 
      CreDtTm,
      Acct: {
        Id: {
          IBAN: iban
        },
        Ccy: currency,
        Ownr: {
          Id: {
            OrgId: {
              AnyBIC: bic
            }
          }
        }
      }
    } = xmlDocs.Ntfctn;

    this.accountInfo = {
      Id,
      CreDtTm,
      iban,
      currency,
      bic
    };

    const docs = xmlDocs.Ntfctn.Ntry.NtryDtls.TxDtls;
    if (Array.isArray(docs))
      docs.forEach(doc => this.parseDoc(doc));
    else
      this.parseDoc(docs);
  } 

  parseDoc(xmlDoc) {
    xmlDoc = xmlDoc || {}
    this.addDoc({
      instrId: xmlDoc.Refs ? (xmlDoc.Refs.InstrId || "123321") : "123321", // crutch
      pmtInfId: xmlDoc.Refs ? (xmlDoc.Refs.PmtInfId || "none") : "none",
      endToEndId:  xmlDoc.Refs ? xmlDoc.Refs.EndToEndId : undefined,
      transactionId: xmlDoc.Refs ? xmlDoc.Refs.TxId : "123321", // crutch
      amount: xmlDoc.Amt ? xmlDoc.Amt._ : 0,
      creditDebitIndex: xmlDoc.CdtDbtInd,
      domainCode: xmlDoc.BkTxCd ? xmlDoc.BkTxCd.Domn.Cd : undefined,
      domainFamilyCode: xmlDoc.BkTxCd ? xmlDoc.BkTxCd.Domn.Fmly.Cd : undefined,
      domainSubFamilyCode: xmlDoc.BkTxCd ? xmlDoc.BkTxCd.Domn.Fmly.SubFmlyCd : undefined,
      initgPtyBIC: xmlDoc.RltdPties ? xmlDoc.RltdPties.InitgPty.Id.OrgId.AnyBIC : undefined, // ?
      debtorIban: xmlDoc.RltdPties ? xmlDoc.RltdPties.DbtrAcct.Id.IBAN : undefined,
      creditorIban: xmlDoc.RltdPties ? xmlDoc.RltdPties.CdtrAcct.Id.IBAN : undefined,
      debtorBic: xmlDoc.RltdAgts ? xmlDoc.RltdAgts.DbtrAgt.FinInstnId.BICFI : undefined,
      creditorBic: xmlDoc.RltdAgts ? xmlDoc.RltdAgts.CdtrAgt.FinInstnId.BICFI : undefined,
      acceptanceDateTime: xmlDoc.RltdDts ? xmlDoc.RltdDts.AccptncDtTm : undefined,
      bankSettlementDate: xmlDoc.RltdDts ? xmlDoc.RltdDts.IntrBkSttlmDt : undefined,
      transactionDateTime: xmlDoc.RltdDts ? xmlDoc.RltdDts.TxDtTm : undefined,     
    });
  }

  addDoc({
    instrId,
    pmtInfId,
    endToEndId,
    transactionId,
    amount,
    creditDebitIndex,
    domainCode,
    domainFamilyCode,
    domainSubFamilyCode,
    initgPtyBIC,
    debtorIban,
    creditorIban,
    debtorBic,
    creditorBic,
    acceptanceDateTime,
    bankSettlementDate,
    transactionDateTime
  }) {
    super.addTransaction({
      instrId,
      pmtInfId,
      endToEndId,
      transactionId,
      amount,
      creditDebitIndex,
      domainCode,
      domainFamilyCode,
      domainSubFamilyCode,
      initgPtyBIC,
      debtorIban,
      creditorIban,
      debtorBic,
      creditorBic,
      acceptanceDateTime,
      bankSettlementDate,
      transactionDateTime
    })
  }
}

module.exports = BTCDCNTF;