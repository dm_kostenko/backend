const Message = require("./Message")
const type = "BkToCstmrStmt";
const version = "camt.053.001.02";

const isObject = obj => obj.constructor === Object;
const isEmptyObject = obj => Object.keys(obj).length === 0 && obj.constructor === Object;


class BTCSTMT extends Message {
  constructor({
    MsgId,
    CreDtTm,
    StmtId,
    StmtCreDtTm,
    StmtFrDtTm,
    StmtToDtTm,
    StmtAcctIdIban,
    StmtAcctCcy,
    StmtAcctOwnrNm,
    StmtAcctOwnrIdOrgIdOthrId,
    StmtAcctOwnrIdOrgIdOthrSchmeNmCd,
    StmtAcctSvcrFinInstnIdBic,
    StmtAcctSvcrFinInstnIdNm,
    StmtAcctSvcrFinInstnIdPstlAdrStrtNm,
    StmtAcctSvcrFinInstnIdPstlAdrPstCd,
    StmtAcctSvcrFinInstnIdPstlAdrTwnNm,
    StmtAcctSvcrFinInstnIdPstlAdrCtry,
    StmtAcctSvcrFinInstnIdOthrId,
    StmtAcctSvcrFinInstnIdOthrSchmeNmCd,
    StmtOpBalCd,
    StmtOpBalAmt,
    StmtOpBalCcy,
    StmtOpBalCdtDbtInd,
    StmtOpBalDt,
    StmtClBalCd,
    StmtClBalAmt,
    StmtClBalCcy,
    StmtClBalCdtDbtInd,
    StmtClBalDt,
    StmtTxsSummryTtlNtriesNbOfNtries,
    StmtTxsSummryTtlCdtNtriesNbOfNtries,
    StmtTxsSummryTtlCdtNtriesSum,
    StmtTxsSummryTtlDbtNtriesNbOfNtries,
    StmtTxsSummryTtlDbtNtriesSum,
    StmtNtryArray = [
      {
        Amt,
        Ccy,
        CdtDbtInd,  // CRDT or DBIT
        Sts,
        BookgDt,
        ValDt,
        BkTxCd,
        BkTxFmlyCd,
        BkTxSubFmlyCd,
        NtryDtlsTxDtlsAcctSvcrRef,
        NtryDtlsTxDtlsTxId,
        NtryDtlsAmtDtlsInstdAmt,
        NtryDtlsAmtDtlsInstdCcy,
        NtryDtlsAmtDtlsTxAmt,
        NtryDtlsAmtDtlsTxCcy,
        NtryDtlsRltdPtiesDbtrNm,
        NtryDtlsRltdPtiesDbtrAcctIban,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdBic,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdNm,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrStrtNm,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrPstCd,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrTwnNm,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrCtry,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrId,
        NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrSchmeNmCd,
        NtryDtlsRltdPtiesCdtrNm,
        NtryDtlsRltdPtiesCdtrAcctIban,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdBic,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdNm,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrStrtNm,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrPstCd,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrTwnNm,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrCtry,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrId,
        NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrSchmeNmCd,
        NtryDtlsRmtInfUstrd
      }
    ]
  }) {
    super({})
    this._calculateGroupHeader({ MsgId, CreDtTm });
    this._addInfo({
      StmtId,
      StmtCreDtTm,
      StmtFrDtTm,
      StmtToDtTm,
      StmtAcctIdIban,
      StmtAcctCcy,
      StmtAcctOwnrNm,
      StmtAcctOwnrIdOrgIdOthrId,
      StmtAcctOwnrIdOrgIdOthrSchmeNmCd,
      StmtAcctSvcrFinInstnIdBic,
      StmtAcctSvcrFinInstnIdNm,
      StmtAcctSvcrFinInstnIdPstlAdrStrtNm,
      StmtAcctSvcrFinInstnIdPstlAdrPstCd,
      StmtAcctSvcrFinInstnIdPstlAdrTwnNm,
      StmtAcctSvcrFinInstnIdPstlAdrCtry,
      StmtAcctSvcrFinInstnIdOthrId,
      StmtAcctSvcrFinInstnIdOthrSchmeNmCd,
      StmtOpBalCd,
      StmtOpBalAmt,
      StmtOpBalCcy,
      StmtOpBalCdtDbtInd,
      StmtOpBalDt,
      StmtClBalCd,
      StmtClBalAmt,
      StmtClBalCcy,
      StmtClBalCdtDbtInd,
      StmtClBalDt,
      StmtTxsSummryTtlNtriesNbOfNtries,
      StmtTxsSummryTtlCdtNtriesNbOfNtries,
      StmtTxsSummryTtlCdtNtriesSum,
      StmtTxsSummryTtlDbtNtriesNbOfNtries,
      StmtTxsSummryTtlDbtNtriesSum,
    })

    StmtNtryArray.forEach(Ntry => this._addTransaction(Ntry))
      
  }

  _calculateGroupHeader({
    MsgId,
    CreDtTm
  }){
    this.groupHeader = {
      MsgId,
      CreDtTm
    };
  }

  _addInfo({
    StmtId,
    StmtCreDtTm,
    StmtFrDtTm,
    StmtToDtTm,
    StmtAcctIdIban,
    StmtAcctCcy = "EUR",
    StmtAcctOwnrNm,
    StmtAcctOwnrIdOrgIdOthrId,
    StmtAcctOwnrIdOrgIdOthrSchmeNmCd,
    StmtAcctSvcrFinInstnIdBic,
    StmtAcctSvcrFinInstnIdNm,
    StmtAcctSvcrFinInstnIdPstlAdrStrtNm,
    StmtAcctSvcrFinInstnIdPstlAdrPstCd,
    StmtAcctSvcrFinInstnIdPstlAdrTwnNm,
    StmtAcctSvcrFinInstnIdPstlAdrCtry,
    StmtAcctSvcrFinInstnIdOthrId,
    StmtAcctSvcrFinInstnIdOthrSchmeNmCd,
    StmtOpBalCd,
    StmtOpBalAmt,
    StmtOpBalCcy = "EUR",
    StmtOpBalCdtDbtInd,
    StmtOpBalDt,
    StmtClBalCd,
    StmtClBalAmt,
    StmtClBalCcy = "EUR",
    StmtClBalCdtDbtInd,
    StmtClBalDt,
    StmtTxsSummryTtlNtriesNbOfNtries,
    StmtTxsSummryTtlCdtNtriesNbOfNtries,
    StmtTxsSummryTtlCdtNtriesSum,
    StmtTxsSummryTtlDbtNtriesNbOfNtries,
    StmtTxsSummryTtlDbtNtriesSum,
  }) {
    this.Stmt = {
      Id: StmtId,
      CreDtTm: StmtCreDtTm,
      FrToDt: {
        FrDtTm: StmtFrDtTm,
        ToDtTm: StmtToDtTm
      },
      Acct: {
        Id: {
          IBAN: StmtAcctIdIban
        },
        Ccy: StmtAcctCcy,
        Ownr: {
          Nm: StmtAcctOwnrNm,
          Id: {
            OrgId: {
              Othr: {
                Id: StmtAcctOwnrIdOrgIdOthrId,
                SchmeNm: {
                  Cd: StmtAcctOwnrIdOrgIdOthrSchmeNmCd
                }
              }
            }
          }
        },
        Svcr: {
          FinInstnId: {
            BIC: StmtAcctSvcrFinInstnIdBic,
            Nm: StmtAcctSvcrFinInstnIdNm,
            PstlAdr: {
              StrtNm: StmtAcctSvcrFinInstnIdPstlAdrStrtNm,
              PstCd: StmtAcctSvcrFinInstnIdPstlAdrPstCd,
              TwnNm: StmtAcctSvcrFinInstnIdPstlAdrTwnNm,
              Ctry: StmtAcctSvcrFinInstnIdPstlAdrCtry
            },
            Othr: {
              Id: StmtAcctSvcrFinInstnIdOthrId,
              SchmeNm: {
                Cd: StmtAcctSvcrFinInstnIdOthrSchmeNmCd
              }
            }
          }
        }
      },
      Bal: [
        {
          Tp: {
            CdOrPrtry: {
              Cd: StmtOpBalCd
            }
          },
          Amt: {
            _: StmtOpBalAmt.toString(),
            $: {
              Ccy: StmtOpBalCcy
            }
          },
          CdtDbtInd: StmtOpBalCdtDbtInd,
          Dt: {
            Dt: StmtOpBalDt
          }
        },
        {
          Tp: {
            CdOrPrtry: {
              Cd: StmtClBalCd
            }
          },
          Amt: {
            _: StmtClBalAmt.toString(),
            $: {
              Ccy: StmtClBalCcy
            }
          },
          CdtDbtInd: StmtClBalCdtDbtInd,
          Dt: {
            Dt: StmtClBalDt
          }
        }
      ],
      TxsSummry: {
        TtlNtries: {
          NbOfNtries: StmtTxsSummryTtlNtriesNbOfNtries
        },
        TtlCdtNtries: {
          NbOfNtries: StmtTxsSummryTtlCdtNtriesNbOfNtries,
          Sum: StmtTxsSummryTtlCdtNtriesSum
        },
        TtlDbtNtries: {
          NbOfNtries: StmtTxsSummryTtlDbtNtriesNbOfNtries,
          Sum: StmtTxsSummryTtlDbtNtriesSum
        }
      },
      Ntry: {}
    }
  }

  _addTransaction({
    Amt,
    Ccy = "EUR",
    CdtDbtInd,  // CRDT or DBIT
    Sts,
    BookgDt,
    ValDt,
    BkTxCd,
    BkTxFmlyCd,
    BkTxSubFmlyCd,
    NtryDtlsTxDtlsAcctSvcrRef,
    NtryDtlsTxDtlsTxId,
    NtryDtlsAmtDtlsInstdAmt,
    NtryDtlsAmtDtlsInstdCcy = "EUR",
    NtryDtlsAmtDtlsTxAmt,
    NtryDtlsAmtDtlsTxCcy = "EUR",
    NtryDtlsRltdPtiesDbtrNm,
    NtryDtlsRltdPtiesDbtrAcctIban,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdBic,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdNm,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrStrtNm,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrPstCd,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrTwnNm,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrCtry,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrId,
    NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrSchmeNmCd,
    NtryDtlsRltdPtiesCdtrNm,
    NtryDtlsRltdPtiesCdtrId,
    NtryDtlsRltdPtiesCdtrSchmeNmCd,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdBic,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdNm,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrStrtNm,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrPstCd,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrTwnNm,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrCtry,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrId,
    NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrSchmeNmCd,
    NtryDtlsRmtInfUstrd
  }) {
    let NtryItem = {
      Amt: {
        _: Amt.toString(),
        $: {
          Ccy
        }
      },
      CdtDbtInd,
      Sts,
      BookgDt: {
        DtTm: BookgDt
      },
      ValDt: {
        DtTm: ValDt
      },
      BkTxCd: {
        Domn: {
          Cd: BkTxCd,
          Fmly: {
            Cd: BkTxFmlyCd,
            SubFmlyCd: BkTxSubFmlyCd
          }
        }
      },
      NtryDtls: {
        TxDtls: {
          Refs: {
            AcctSvcrRef: NtryDtlsTxDtlsAcctSvcrRef,
            TxId: NtryDtlsTxDtlsTxId
          },
          AmtDtls: {
            InstdAmt: {
              Amt: {
                _: NtryDtlsAmtDtlsInstdAmt.toString(),
                $: {
                  Ccy: NtryDtlsAmtDtlsInstdCcy
                }
              }
            },
            TxAmt: {
              Amt: {
                _: NtryDtlsAmtDtlsTxAmt.toString(),
                $: {
                  Ccy: NtryDtlsAmtDtlsTxCcy
                }
              }
            }
          },
          RltdPties: {},
          RltdAgts: {},
          RmtInf: {
            Ustrd: NtryDtlsRmtInfUstrd
          }
        }
      }
    }

    if(NtryDtlsRltdPtiesDbtrNm)
      NtryItem.NtryDtls.TxDtls.RltdPties.Dbtr = {
        Nm: NtryDtlsRltdPtiesDbtrNm
      }

    if(NtryDtlsRltdPtiesDbtrAcctIban)
      NtryItem.NtryDtls.TxDtls.RltdPties.DbtrAcct = {
        Id: {
          IBAN: NtryDtlsRltdPtiesDbtrAcctIban
        }
      }

    if(NtryDtlsRltdAgtsDbtrAgtFinInstnIdBic ||
      NtryDtlsRltdAgtsDbtrAgtFinInstnIdNm ||
      NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrStrtNm ||
      NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrPstCd ||
      NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrTwnNm ||
      NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrCtry ||
      NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrId ||
      NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrSchmeNmCd)
      NtryItem.NtryDtls.TxDtls.RltdAgts = {
        DbtrAgt: {
          FinInstnId: {
            BIC: NtryDtlsRltdAgtsDbtrAgtFinInstnIdBic,
            Nm: NtryDtlsRltdAgtsDbtrAgtFinInstnIdNm,
            PstlAdr: {
              StrtNm: NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrStrtNm,
              PstCd: NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrPstCd,
              TwnNm: NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrTwnNm,
              Ctry: NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrCtry
            },
            Othr: {
              Id: NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrId,
              SchmeNm: {
                Cd: NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrSchmeNmCd
              }
            }
          }
        }
      }
    
    if(NtryDtlsRltdPtiesCdtrNm)
      NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr = {
        Nm: NtryDtlsRltdPtiesCdtrNm
      }

    if(NtryDtlsRltdPtiesCdtrId)
      NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr = {
        ...NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr,
        Id: {
          OrgId: {
            Othr: {
              Id: NtryDtlsRltdPtiesCdtrId
            }
          }
        }
      }

    if(NtryDtlsRltdPtiesCdtrSchmeNmCd) {
      if(NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr && NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr.Id)
        NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr = {
          ...NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr,
          Id: {
            OrgId: {
              Othr: {
                ...NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr.Id.OrgId.Othr,
                SchmeNm: {
                  Cd: NtryDtlsRltdPtiesCdtrSchmeNmCd
                }
              }
            }
          }
        }
      else
        NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr = {
          ...NtryItem.NtryDtls.TxDtls.RltdPties.Cdtr,
          Id: {
            OrgId: {
              Othr: {
                SchmeNm: {
                  Cd: NtryDtlsRltdPtiesCdtrSchmeNmCd
                }
              }
            }
          }
        }
    }

    if(NtryDtlsRltdAgtsCdtrAgtFinInstnIdBic ||
      NtryDtlsRltdAgtsCdtrAgtFinInstnIdNm ||
      NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrStrtNm ||
      NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrPstCd ||
      NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrTwnNm ||
      NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrCtry ||
      NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrId ||
      NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrSchmeNmCd)
      NtryItem.NtryDtls.TxDtls.RltdAgts = {
        ...NtryItem.NtryDtls.TxDtls.RltdAgts,
        CdtrAgt: {
          FinInstnId: {
            BIC: NtryDtlsRltdAgtsCdtrAgtFinInstnIdBic,
            Nm: NtryDtlsRltdAgtsCdtrAgtFinInstnIdNm,
            PstlAdr: {
              StrtNm: NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrStrtNm,
              PstCd: NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrPstCd,
              TwnNm: NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrTwnNm,
              Ctry: NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrCtry
            },
            Othr: {
              Id: NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrId,
              SchmeNm: {
                Cd: NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrSchmeNmCd
              }
            }
          }
        }
      }


    if(isObject(this.Stmt.Ntry)) {
      if(isEmptyObject(this.Stmt.Ntry))
        this.Stmt.Ntry = NtryItem
      else
        this.Stmt.Ntry = [
          this.Stmt.Ntry,
          NtryItem
        ]
    }
    else
      this.Stmt.Ntry.push(NtryItem)

  }

  toXml() {
    const document = {
      Document: {
        $: {
          Version: version
        },
        [type] : {
          GrpHdr: this.groupHeader,
          Stmt: this.Stmt
        }
      }
    }

    return this.buildXml(document);
  }
}

module.exports = BTCSTMT;
