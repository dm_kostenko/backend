const Message = require("./Message");
const type = "DEBIT";
const version = "2007-1-0";

class DEBIT extends Message {
  constructor({ parsedXml }){
    const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
    super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type, version });    
    this.parseDocs(parsedXml.Msg.Docs.Doc);
  }

  parseDocs (xmlDocs) {
    if (Array.isArray(xmlDocs))
      xmlDocs.forEach(doc => this.parseDoc(doc));
    else
      this.parseDoc(xmlDocs);
  } 

  parseDoc(xmlDoc) {
    this.addDoc({
      docId: xmlDoc.Header.DocId,
      type: xmlDoc.Header.Type,
      businessArea: xmlDoc.Header.BusinessArea,
      referenceType: xmlDoc.Header.Reference.Type,
      referenceSender: xmlDoc.Header.Reference.Sender,
      referenceDocId: xmlDoc.Header.Reference.DocId,
      payerBIC: xmlDoc.Debitu.Payer.BIC,
      payerAccount: xmlDoc.Debitu.Payer.Account,
      payerCcodeIcode: xmlDoc.Debitu.Payer.CCode.ICode,
      payerName: xmlDoc.Debitu.Payer.Name,
      payeeBIC: xmlDoc.Debitu.Payee.BIC,
      payeeAccount: xmlDoc.Debitu.Payee.Account,
      payeeCcodeIcode: xmlDoc.Debitu.Payee.CCode.ICode,
      payeeName: xmlDoc.Debitu.Payee.Name,
      currency: xmlDoc.Debitu.Currency,
      amount: xmlDoc.Debitu.Amount,
      remittanceInformationDate: xmlDoc.Debitu.RemittanceInformation.Date,
      remittanceInformationId: xmlDoc.Debitu.RemittanceInformation.ID,
      remittanceInformationPaymentReason: xmlDoc.Debitu.RemittanceInformation.PaymentReason
    });
  }

  addDoc({
    docId,
    type,
    businessArea,
    referenceType,
    referenceSender,
    referenceDocId,
    payerBIC,
    payerAccount,
    payerCcodeIcode,
    payerName,
    payeeBIC,
    payeeAccount,
    payeeCcodeIcode,
    payeeName,
    currency,
    amount,
    remittanceInformationDate,
    remittanceInformationId,
    remittanceInformationPaymentReason
  }) {
    super.addTransaction({
      docId,
      type,
      businessArea,
      referenceType,
      referenceSender,
      referenceDocId,
      payerBIC,
      payerAccount,
      payerCcodeIcode,
      payerName,
      payeeBIC,
      payeeAccount,
      payeeCcodeIcode,
      payeeName,
      currency,
      amount,
      remittanceInformationDate,
      remittanceInformationId,
      remittanceInformationPaymentReason
    })
  }
}

module.exports = DEBIT;