const xml2js = require("xml2js");
const FFCCTRNS = require("./FFCCTRNS");
const FFPCRQST = require("./FFPCRQST");
const FFPSRPRT = require("./FFPSRPRT");
const PRTRN = require("./PRTRN");
const ROINVSTG = require("./ROINVSTG");
const BALANCE = require("./BALANCE");
const BTCDCNTF = require("./BTCDCNTF");
const DEBIT = require("./DEBIT");
const DICTIONR = require("./DICTIONR")


class XmlMessageParser {
  constructor(str) {
    this.xml = str;
  }

  async getMessageTypeAndDate () {
    try {
      let parsedXml = await this._xml2json(this.xml);
      if(!parsedXml)
        return {
          date: '',
          type: ''
        }
      if (parsedXml.EDoc)
        parsedXml = parsedXml.EDoc;
      return {
        type: parsedXml.Msg.$.Type,
        date: parsedXml.Msg.Header.Date
      }
    }
    catch(err) {
      throw(err)
    }
  }



  async getBalanceData (account) {
    try {
      let parsedXml = await this._xml2json(this.xml);
      if (parsedXml.EDoc)
        parsedXml = parsedXml.EDoc;
      let Doc = parsedXml.Msg.Docs.Doc;
      if(Array.isArray(Doc))
        Doc = parsedXml.Msg.Docs.Doc.find(doc => doc.CBalPri ? doc.CBalPri.Account === account : doc.CBalPat.Account === account)
      if(parsedXml.Msg.$.Type === "BALANCE")
        return {
          iban: Doc.CBalPat ? Doc.CBalPat.Account : Doc.CBalPri.Account,
          openingBalance: Doc.CBalPat ? Doc.CBalPat.Opening.Balance : Doc.CBalPri.Opening.Balance,
          closingBalance: Doc.CBalPat ? Doc.CBalPat.Closing.Balance : Doc.CBalPri.Closing.Balance,
          date: parsedXml.Msg.Header.Date,
          f: Doc.CBalPat
        }
    }
    catch(err) {
      throw(err)
    }
  }

  async getMessage () {
    try {
      let parsedXml = await this._xml2json(this.xml);
      if (parsedXml.EDoc)
        parsedXml = parsedXml.EDoc;
      switch (parsedXml.Msg.$.Type) {
      case "FFCCTRNS":
        return new FFCCTRNS({ parsedXml });
      case "FFPCRQST":
        return new FFPCRQST({ parsedXml });
      case "FFPSRPRT":
        return new FFPSRPRT({ parsedXml });
      case "PRTRN":
        return new PRTRN({ parsedXml });
      case "ROINVSTG":
        return new ROINVSTG({ parsedXml });
      case "BALANCE":
        return new BALANCE({ parsedXml })
      case "BTCDCNTF":
        return new BTCDCNTF({ parsedXml })
      case "DEBIT":
        return new DEBIT({ parsedXml })
      case "DICTIONR":
        return new DICTIONR({ parsedXml })
      default:
        throw { message: `Unsupported type ${parsedXml.Msg.$.Type}` };
      }
    }
    catch(err) {
      throw(err)
      // if(err.message.includes("Unsupported type"))
      //   throw(err)
      // throw {
      //   xmlParser: true,
      //   type: "PRTRN",
      //   req: {
      //     body: {
      //       originalTxId: transaction.paymentTransactionId,
      //       originatorClient: "TBF Finance, UAB",
      //       reasonForReturn: "AG02"
      //     }
      //   },
      //   err
      // }
    }
  }

  async _xml2json (xml) {
    return new Promise((resolve, reject) => xml2js.parseString(xml, { trim: true, explicitArray: false, async: true }, (err, json) => err ? reject(err) : resolve(json)));
  }
}

module.exports = XmlMessageParser;