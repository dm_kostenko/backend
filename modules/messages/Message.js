const xml2js = require("xml2js");
const xmlBuilder = new xml2js.Builder({ headless: true });
const { getDateTime } = require("../../helpers/target2Timing")
const { bic } = require("../../config")

isIncoming = (sender, receiver) => (receiver === bic) && (sender !== bic)

module.exports = class Message {
  constructor({ msgId, type, version, sender, receiver, system, date = getDateTime(), priority = 51, businessArea = "SEPASCT" }){
    this.messageHeader = {
      msgId,
      type,
      version,
      date,
      priority,
      businessArea,
      sender,
      receiver: isIncoming(sender, receiver) ? receiver : "LIABLT2XMSD",
      system //LITAS-RLS or LITAS-MIG
    };
    this.transactions = [];
  }

  buildXml(document) {
    try {
    const obj = xmlBuilder.buildObject(document);
    console.log(obj);
    return obj;
    }
    catch(err) {
      throw(err)
    }
  }

  addTransaction(transaction){
    this.transactions.push(transaction);
    return this;
  }
};