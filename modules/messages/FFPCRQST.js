const Message = require("./Message");
const type = "FFPCRQST";
const version = "camt.056.001.01";

class FFPCRQST extends Message {
  constructor({ msgId, sender, receiver, system, parsedXml }){
    if (parsedXml) {
      const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
      super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type, version });
      const { Id, Assgnr, Assgne, CreDtTm } = parsedXml.Msg.Docs.Doc.Ffpcrqst.Document.FIToFIPmtCxlReq.Assgnmt;
      const { NbOfTxs } = parsedXml.Msg.Docs.Doc.Ffpcrqst.Document.FIToFIPmtCxlReq.CtrlData;
      this._calculateAssgmntHeader({ 
        Id,
        AssignerBic: Assgnr.Agt.FinInstnId.BIC,
        AssigneeBic: Assgne.Agt.FinInstnId.BIC,
        CreDtTm,
        NbOfTxs
      });

      this.parseTransactions(parsedXml.Msg.Docs.Doc.Ffpcrqst.Document.FIToFIPmtCxlReq.Undrlyg.TxInf);
    }
    else 
      super({ msgId, sender, receiver, system, type, version });
  }

  _calculateAssgmntHeader({
    Id,
    AssignerBic,
    AssigneeBic,
    CreDtTm,
    NbOfTxs,
  }){
    this.assgmntHeader = {
      Id: Id || this.messageHeader.msgId,
      AssignerBic: AssignerBic || this.messageHeader.sender,
      AssigneeBic: /*AssigneeBic || this.messageHeader.receiver*/ AssigneeBic || "LIABLT2XMSD",
      CreDtTm: CreDtTm || this.messageHeader.date,
      NbOfTxs: NbOfTxs || this.transactions.length
    };
  }

  parseTransactions(xmlTransactions) {
    if (Array.isArray(xmlTransactions))
      xmlTransactions.forEach(transaction => this.parseTransaction(transaction));
    else
      this.parseTransaction(xmlTransactions);
  }

  parseTransaction(xmlTransaction) {
    this.addTransaction({
      CancellationTrId: xmlTransaction.CxlId,
      OrgnlMsgId: xmlTransaction.OrgnlGrpInf.OrgnlMsgId,
      OrgnlMsgNmId: xmlTransaction.OrgnlGrpInf.OrgnlMsgNmId,
      OrgnlEndToEndId: xmlTransaction.OrgnlEndToEndId,
      OrgnlTxId: xmlTransaction.OrgnlTxId,
      OrgnlIntrBkSttlmAmt: xmlTransaction.OrgnlIntrBkSttlmAmt._,
      OrgnlIntrBkSttlmDt: xmlTransaction.OrgnlIntrBkSttlmDt,
      originatorClient: xmlTransaction.CxlRsnInf.Orgtr.Nm ,
      originatorBank: xmlTransaction.CxlRsnInf.Orgtr.Id ? xmlTransaction.CxlRsnInf.Orgtr.Id.OrgId.BICOrBEI : undefined,
      CxlRsnInf: xmlTransaction.CxlRsnInf.Rsn.Prtry,            //Reason - DUPL, TECH or FRAD
      CxlRsnInfAddtInf: xmlTransaction.CxlRsnInf.AddtlInf,     //If reason FRAD
      SttlmMtd: xmlTransaction.OrgnlTxRef.SttlmInf.SttlmMtd,             // = "CLRG",
      ClrSysPrtry: xmlTransaction.OrgnlTxRef.SttlmInf.ClrSys.Prtry,          // LITAS-RLS or LITAS-MIG
      serviceLevel: xmlTransaction.OrgnlTxRef.PmtTpInf.SvcLvl.Cd,         // = "SEPA",
      debtorName: xmlTransaction.OrgnlTxRef.Dbtr.Nm,
      debtorAccount: xmlTransaction.OrgnlTxRef.DbtrAcct.Id.IBAN,
      debtorBic: xmlTransaction.OrgnlTxRef.DbtrAgt.FinInstnId.BIC,
      creditorName: xmlTransaction.OrgnlTxRef.Cdtr.Nm,
      creditorAccount: xmlTransaction.OrgnlTxRef.CdtrAcct.Id.IBAN,
      creditorBic: xmlTransaction.OrgnlTxRef.CdtrAgt.FinInstnId.BIC,
    });
  }

  addTransaction({
    CancellationTrId,
    OrgnlMsgId,
    OrgnlMsgNmId,
    OrgnlEndToEndId,
    OrgnlTxId,
    OrgnlIntrBkSttlmAmt,
    OrgnlIntrBkSttlmDt,
    CxlRsnInf, //Reason - DUPL, TECH or FRAD
    CxlRsnInfAddtInf, //If reason FRAD
    SttlmMtd = "CLRG",
    ClrSysPrtry, // LITAS-RLS or LITAS-MIG
    serviceLevel = "SEPA",
    debtorName,
    debtorAccount,
    debtorBic,
    creditorName,
    creditorAccount,
    creditorBic,
    originatorClient,
    originatorBank
  }) {
    super.addTransaction({
      CancellationTrId,
      OrgnlMsgId,
      OrgnlMsgNmId,
      OrgnlEndToEndId,
      OrgnlTxId,
      OrgnlIntrBkSttlmAmt,
      OrgnlIntrBkSttlmDt,
      CxlRsnInf,
      CxlRsnInfAddtInf,
      SttlmMtd,
      ClrSysPrtry,
      serviceLevel,
      debtorName,
      debtorAccount,
      debtorBic,
      creditorName,
      creditorAccount,
      creditorBic,
      originatorClient,
      originatorBank
    });
  }

  toXml() {
    // this._calculateAssgmntHeader({});

    const messageHeader = {
      Sender: this.messageHeader.sender,
      Receiver: this.messageHeader.receiver,
      Priority: this.messageHeader.priority,
      MsgId: this.messageHeader.msgId,
      Date: this.messageHeader.date,
      System: this.messageHeader.system
    };

    const docHeader = {
      DocId: this.messageHeader.msgId,
      Dates: {
        Type: "FormDoc",
        Date: this.messageHeader.date
      },
      Type: this.messageHeader.type,
      Priority: this.messageHeader.priority,
      BusinessArea: this.messageHeader.businessArea
    };

    const groupHeader = {
      Assgnmt: {
        Id: this.assgmntHeader.Id,
        Assgnr: { Agt: { FinInstnId: { BIC: this.assgmntHeader.AssignerBic } } },
        Assgne: { Agt: { FinInstnId: { BIC: this.assgmntHeader.AssigneeBic } } },
        CreDtTm: this.assgmntHeader.CreDtTm
      },
      CtrlData: { NbOfTxs: this.assgmntHeader.NbOfTxs }
    };

    const body = this.transactions.map(transaction => {
      const transactionSchema = {
        CxlId: transaction.CancellationTrId,
        OrgnlGrpInf: {
          OrgnlMsgId: transaction.OrgnlMsgId,
          OrgnlMsgNmId: transaction.OrgnlMsgNmId
        },
        OrgnlInstrId: transaction.OrgnlTxId,
        OrgnlEndToEndId: transaction.OrgnlEndToEndId,
        OrgnlTxId: transaction.OrgnlTxId,
        OrgnlIntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: transaction.OrgnlIntrBkSttlmAmt / 100 },
        OrgnlIntrBkSttlmDt: transaction.OrgnlIntrBkSttlmDt,
        CxlRsnInf: { 
          Orgtr: {  },
          Rsn: { } 
        }, 
        OrgnlTxRef: {
          SttlmInf: { SttlmMtd: transaction.SttlmMtd, ClrSys: { Prtry: transaction.ClrSysPrtry } },
          PmtTpInf: { SvcLvl: { Cd: transaction.serviceLevel } },
          Dbtr: { Nm: transaction.debtorName },
          DbtrAcct: { Id: { IBAN: transaction.debtorAccount } },
          DbtrAgt: { FinInstnId: { BIC: transaction.debtorBic } },
          CdtrAgt: { FinInstnId: { BIC: transaction.creditorBic } },
          Cdtr: { Nm: transaction.creditorName },
          CdtrAcct: { Id: { IBAN: transaction.creditorAccount } },
        }
      };

      if (transaction.CxlRsnInf === "DUPL")
        transactionSchema.CxlRsnInf.Rsn.Cd = transaction.CxlRsnInf;
      else
        transactionSchema.CxlRsnInf.Rsn.Prtry = transaction.CxlRsnInf;

      if (transaction.CxlRsnInf === "FRAD")
        transactionSchema.CxlRsnInf.Rsn.AddtInf = transaction.CxlRsnInf;

      if (transaction.originatorClient && [ "AM09", "AC03" ].includes(transaction.CxlRsnInf))
        transactionSchema.CxlRsnInf.Orgtr = { Nm: transaction.originatorClient };
      else 
        transactionSchema.CxlRsnInf.Orgtr = { Id: { OrgId: { BICOrBEI: this.messageHeader.sender } } };

      return transactionSchema;
    });

    const document = {
      Msg: {
        $: { ID: "Edoc", Type: this.messageHeader.type, Version: this.messageHeader.version + ".2021" },
        Header: messageHeader,
        Docs: {
          Doc: [ {  
            Header: docHeader,
            Ffpcrqst: {
              Document: {
                FIToFIPmtCxlReq: {
                  ...groupHeader,
                  Undrlyg: {
                    TxInf: body
                  }
                }
              }
            }
          } ]
        }
      }
    };

    return this.buildXml(document);
  }
}

module.exports = FFPCRQST;