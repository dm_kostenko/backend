const Message = require("./Message");

class FFPSRPRT extends Message {
  constructor({ parsedXml }){
    const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
    super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type: "FFPSRPRT", version: "pacs.002.001.03", });

    const { /*MsgId,*/ CreDtTm, 
      /*NbOfTxs, TtlIntrBkSttlmAmt, IntrBkSttlmDt, SttlmInf, InstdAgt*/ } = parsedXml.Msg.Docs.Doc.Ffpsrprt.Document.FIToFIPmtStsRpt.GrpHdr;
       
    this._calculateGroupHeader({ 
      MsgId, 
      CreDtTm, 
      // NbOfTxs, 
      // TtlIntrBkSttlmAmt: TtlIntrBkSttlmAmt._, 
      // IntrBkSttlmDt, 
      // SttlmMtd: SttlmInf.SttlmMtd, 
      // ClrSysPrtry: SttlmInf.ClrSys.Prtry, 
      // FinInstnIdBIC: InstdAgt && InstdAgt.FinInstnId && InstdAgt.FinInstnId.BIC ? InstdAgt.FinInstnId.BIC : undefined  
    }, false);
        
    this.setOrgnlGrpInfAndSts(parsedXml.Msg.Docs.Doc.Ffpsrprt.Document.FIToFIPmtStsRpt.OrgnlGrpInfAndSts)
    if (parsedXml.Msg.Docs.Doc.Ffpsrprt.Document.FIToFIPmtStsRpt.TxInfAndSts)
      this.parseTransactions(parsedXml.Msg.Docs.Doc.Ffpsrprt.Document.FIToFIPmtStsRpt.TxInfAndSts);
  }

  _calculateGroupHeader({
    MsgId,
    CreDtTm
  }){
    this.groupHeader = {
      MsgId: MsgId || this.messageHeader.msgId,
      CreDtTm: CreDtTm || this.messageHeader.date,
      // NbOfTxs: NbOfTxs || autofill ? this.transactions.length: undefined,
      // TtlIntrBkSttlmAmt: TtlIntrBkSttlmAmt || autofill ? this.transactions.reduce((fullAmount, transaction) => fullAmount + transaction.amount, 0): undefined,
      // IntrBkSttlmDt: IntrBkSttlmDt || autofill ? new Date().toJSON().slice(0, 10): undefined,
      // SttlmMtd: SttlmMtd || autofill ? "CLRG": undefined,
      // ClrSysPrtry: ClrSysPrtry || autofill ? "LITAS-RLS": undefined, // LITAS-RLS/LITASMIG
      // FinInstnIdBIC: FinInstnIdBIC || autofill ? this.messageHeader.sender: undefined,
    };
  }

  setOrgnlGrpInfAndSts (OrgnlGrpInfAndSts) {
    this.OrgnlGrpInfAndSts = OrgnlGrpInfAndSts; 
  }
  
  parseTransactions(xmlTransactions) {
    if (Array.isArray(xmlTransactions))
      xmlTransactions.forEach(transaction => this.parseTransaction(transaction));
    else
      this.parseTransaction(xmlTransactions);
  } 

  parseTransaction(xmlTransaction) {
    this.addTransaction({
      paymentInstructionId: xmlTransaction.OrgnlInstrId,
      paymentEndToEndId: xmlTransaction.OrgnlEndToEndId,
      paymentTransactionId: xmlTransaction.OrgnlTxId,
      sepaTransactionId: xmlTransaction.StsId,
      transactionStatus: xmlTransaction.TxSts,
      reasonCode: xmlTransaction.StsRsnInf.Rsn.Cd,
      reasonPrtry: xmlTransaction.StsRsnInf.Rsn.Prtry,
      instdAgtFinInstnId: "" /*xmlTransaction.InstdAgt.FinInstnId.BIC*/,
      intrBkSttlmAmt: xmlTransaction.OrgnlTxRef.IntrBkSttlmAmt._,
      intrBkSttlmDt: xmlTransaction.OrgnlTxRef.IntrBkSttlmDt,
      debtorBic:xmlTransaction.OrgnlTxRef.DbtrAgt.FinInstnId.BIC,
      creditorBic:xmlTransaction.OrgnlTxRef.CdtrAgt.FinInstnId.BIC,
    });
  }

  addTransaction({
    paymentInstructionId,
    paymentEndToEndId,
    paymentTransactionId,
    sepaTransactionId,
    transactionStatus,
    reasonCode,
    reasonPrtry,
    instdAgtFinInstnId,
    intrBkSttlmAmt,
    intrBkSttlmDt,
    debtorBic,
    creditorBic,
  }) {
    super.addTransaction({
      paymentInstructionId,
      paymentEndToEndId,
      paymentTransactionId,
      sepaTransactionId,
      transactionStatus,
      reasonCode,
      reasonPrtry,
      instdAgtFinInstnId,
      intrBkSttlmAmt,
      intrBkSttlmDt,
      debtorBic,
      creditorBic,
    });
    return this;
  }

  // toXml() {
  //   // this._calculateGroupHeader();

  //   const messageHeader = {
  //     Sender: this.messageHeader.sender,
  //     Receiver: this.messageHeader.receiver,
  //     Priority: this.messageHeader.priority,
  //     MsgId: this.messageHeader.msgId,
  //     Date: this.messageHeader.date,
  //     System: this.messageHeader.system
  //   };

  //   const docHeader = {
  //     DocId: this.messageHeader.msgId,
  //     Dates: {
  //       Type: "FormDoc",
  //       Date: this.messageHeader.date
  //     },
  //     Type: this.messageHeader.type,
  //     Priority: this.messageHeader.priority,
  //     BusinessArea: this.messageHeader.businessArea
  //   };

  //   const groupHeader = {
  //     MsgId: this.groupHeader.MsgId,
  //     CreDtTm: this.groupHeader.CreDtTm,
  //     NbOfTxs: this.groupHeader.NbOfTxs,
  //     TtlIntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: this.groupHeader.TtlIntrBkSttlmAmt },
  //     IntrBkSttlmDt: this.groupHeader.IntrBkSttlmDt,
  //     SttlmInf: {
  //       SttlmMtd: this.groupHeader.SttlmMtd,
  //       ClrSys: { Prtry: this.groupHeader.ClrSysPrtry }
  //     },
  //     InstgAgt: { FinInstnId: { BIC: this.groupHeader.FinInstnIdBIC } }
  //   };

  //   const body = this.transactions.map(transaction => {
  //     const transactionSchema = {
  //       PmtId: {
  //         InstrId: transaction.paymentInstructionId,
  //         EndToEndId: transaction.paymentEndToEndId,
  //         TxId: transaction.paymentTransactionId
  //       },
  //       PmtTpInf: { SvcLvl: { Cd: transaction.serviceLevel } },
  //       IntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: transaction.amount },
  //       ChrgBr: transaction.ChrgBr,
  //       Dbtr: { Nm: transaction.debtorName },
  //       DbtrAcct: { Id: { IBAN: transaction.debtorAccount } },
  //       DbtrAgt: { FinInstnId: { BIC: transaction.debtorBic } },
  //       Cdtr: { Nm: transaction.creditorName },
  //       CdtrAcct: { Id: { IBAN: transaction.creditorAccount } },
  //       CdtrAgt: { FinInstnId: { BIC: transaction.creditorBic } },
  //     };
  //     if (transaction.remittanceInformation)
  //       transactionSchema.RmtInf.Ustrd = transaction.remittanceInformation;

  //     return transactionSchema;
  //   });

  //   const document = {
  //     Msg: {
  //       $: { ID: "EDOC", Type: this.messageHeader.type, Version: this.messageHeader.version },
  //       Header: messageHeader,
  //       Docs: {
  //         Doc: [ {  
  //           Header: docHeader,
  //           FFPSRPRT: {
  //             Document: {
  //               FIToFICstmrCdtTrf: {
  //                 GrpHdr: groupHeader,
  //                 CdtTrfTxInf: body
  //               }
  //             }
  //           }
  //         } ]
  //       }
  //     }
  //   };

  //   return this.buildXml(document);
  // }
}

module.exports = FFPSRPRT;