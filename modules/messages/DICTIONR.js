const Message = require("./Message");
const type = "DICTIONR";
const version = "";

class DICTIONR extends Message {
  constructor({ parsedXml }){
    const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
    super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type, version });    
    this.parseParticipants(parsedXml.Msg.Docs.Doc.Parts.Participant);
  }

  parseParticipants (participants) {
    if (Array.isArray(participants))
      participants.forEach(participant => this.parseParticipant(participant));
    else
      this.parseParticipant(participants);
  } 

  parseParticipant(participant) {
    if (Array.isArray(participant.Branch))
      participant.Branch.forEach(branch => this.addBranch({
        BIC: participant.BIC,
        ...branch
      }));
    else if(participant.Branch)
      this.addBranch({
        BIC: participant.BIC,
        ...participant.Branch
      });
  }

  addBranch({
    BIC,
    Code,
    Name
  }) {
    super.addTransaction({
      BIC,
      Code,
      Name
    })
  }
}

module.exports = DICTIONR;