const Message = require("./Message");
const type = "FFCCTRNS";
const version = "pacs.008.001.02";
const { getNextWorkingDay } = require("../../helpers/target2Timing")

class FFCCTRNS extends Message {
  constructor({ msgId, sender, receiver, system, parsedXml }){
    if (parsedXml){
      const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
      super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type, version });
      const { /*MsgId,*/ CreDtTm, NbOfTxs, TtlIntrBkSttlmAmt, IntrBkSttlmDt, SttlmInf, InstdAgt } = parsedXml.Msg.Docs.Doc.Ffcctrns.Document.FIToFICstmrCdtTrf.GrpHdr;
      this._calculateGroupHeader({ 
        MsgId, 
        CreDtTm, 
        NbOfTxs, 
        TtlIntrBkSttlmAmt: TtlIntrBkSttlmAmt._, 
        IntrBkSttlmDt, 
        SttlmMtd: SttlmInf.SttlmMtd, 
        ClrSysPrtry: SttlmInf.ClrSys.Prtry, 
        FinInstnIdBIC: InstdAgt && InstdAgt.FinInstnId && InstdAgt.FinInstnId.BIC ? InstdAgt.FinInstnId.BIC : undefined  
      });

      this.parseTransactions(parsedXml.Msg.Docs.Doc.Ffcctrns.Document.FIToFICstmrCdtTrf.CdtTrfTxInf);
    }
    else 
      super({ msgId, sender, receiver, type, version, system });
  }

  _calculateGroupHeader({
    MsgId,
    CreDtTm,
    NbOfTxs,
    TtlIntrBkSttlmAmt,
    IntrBkSttlmDt,
    SttlmMtd,
    ClrSysPrtry,
    FinInstnIdBIC,
  }){
    this.groupHeader = {
      MsgId: MsgId || this.messageHeader.msgId,
      CreDtTm: CreDtTm || this.messageHeader.date,
      NbOfTxs: NbOfTxs || this.transactions.length,
      TtlIntrBkSttlmAmt: TtlIntrBkSttlmAmt || this.transactions.reduce((fullAmount, transaction) => fullAmount + parseFloat(transaction.amount), 0),
      IntrBkSttlmDt: IntrBkSttlmDt || getNextWorkingDay(),
      SttlmMtd: SttlmMtd || "CLRG",
      ClrSysPrtry: ClrSysPrtry || this.messageHeader.system, // LITAS-RLS/LITASMIG
      FinInstnIdBIC: FinInstnIdBIC || this.messageHeader.sender,
    };
  }

  parseTransactions(xmlTransactions) {
    if (Array.isArray(xmlTransactions))
      xmlTransactions.forEach(transaction => this.parseTransaction(transaction));
    else
      this.parseTransaction(xmlTransactions);
  } 

  parseTransaction(xmlTransaction) {
    const remittanceInformation = xmlTransaction && xmlTransaction.RmtInf && xmlTransaction.RmtInf.Ustrd ? xmlTransaction.RmtInf.Ustrd : undefined;
    this.addTransaction({
      paymentInstructionId: xmlTransaction.PmtId.InstrId,
      paymentEndToEndId: xmlTransaction.PmtId.EndToEndId,
      paymentTransactionId: xmlTransaction.PmtId.TxId,
      serviceLevel: xmlTransaction.PmtTpInf.SvcLvl,
      amount: xmlTransaction.IntrBkSttlmAmt._,
      chargeBearer: xmlTransaction.ChrgBr,
      debtorName: xmlTransaction.Dbtr.Nm,
      debtorAccount:xmlTransaction.DbtrAcct.Id.IBAN,
      debtorBic:xmlTransaction.DbtrAgt.FinInstnId.BIC,
      creditorName:xmlTransaction.Cdtr.Nm,
      creditorAccount:xmlTransaction.CdtrAcct.Id.IBAN,
      creditorBic:xmlTransaction.CdtrAgt.FinInstnId.BIC,
      remittanceInformation
    });
  }

  addTransaction({
    paymentInstructionId,
    paymentEndToEndId = "NOTPROVIDED",
    paymentTransactionId,
    serviceLevel = "SEPA",
    amount,
    chargeBearer = "SLEV",
    debtorName,
    debtorAccount,
    debtorBic,
    creditorName,
    creditorAccount,
    creditorBic,
    remittanceInformation = "Contract No.123456"
  }) {
    super.addTransaction({
      paymentInstructionId,
      paymentEndToEndId,
      paymentTransactionId,
      serviceLevel,
      amount,
      chargeBearer,
      debtorName,
      debtorAccount,
      debtorBic,
      creditorName,
      creditorAccount,
      creditorBic,
      remittanceInformation
    });
    return this;
  }

  toXml() {
    try {
    // this._calculateGroupHeader();
    console.log('to xml')
    const messageHeader = {
      Sender: this.messageHeader.sender,
      Receiver: this.messageHeader.receiver,
      Priority: this.messageHeader.priority,
      MsgId: this.messageHeader.msgId,
      Date: this.messageHeader.date,
      System: this.messageHeader.system
    };

    console.log('message header')

    const docHeader = {
      DocId: this.messageHeader.msgId,
      Dates: {
        Type: "FormDoc",
        Date: this.messageHeader.date
      },
      Type: this.messageHeader.type,
      Priority: this.messageHeader.priority,
      BusinessArea: this.messageHeader.businessArea
    };

    console.log('doc header')

    const groupHeader = {
      MsgId: this.groupHeader.MsgId,
      CreDtTm: this.groupHeader.CreDtTm,
      NbOfTxs: this.groupHeader.NbOfTxs,
      TtlIntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: this.groupHeader.TtlIntrBkSttlmAmt / 100 },
      IntrBkSttlmDt: this.groupHeader.IntrBkSttlmDt,
      SttlmInf: {
        SttlmMtd: this.groupHeader.SttlmMtd,
        ClrSys: { Prtry: this.groupHeader.ClrSysPrtry }
      },
      InstgAgt: { FinInstnId: { BIC: this.groupHeader.FinInstnIdBIC } }
    };
    console.log('group header')


    const body = this.transactions.map(transaction => {
      console.log('6: ', transaction.amount)
      const transactionSchema = {
        PmtId: {
          InstrId: transaction.paymentInstructionId,
          EndToEndId: transaction.paymentEndToEndId,
          TxId: transaction.paymentTransactionId
        },
        PmtTpInf: { SvcLvl: { Cd: transaction.serviceLevel } },
        IntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: transaction.amount / 100 },
        ChrgBr: transaction.chargeBearer,
        Dbtr: { 
          Nm: transaction.debtorName,
          PstlAdr: {
            Ctry: "LT",
            AdrLine: [
              "Konstitucijos Pr. 21a",
              "00000 08130 Vilnius"
            ]
          },
          Id: {
            OrgId: {
              Othr: {
                Id: "31800",
                SchmeNm: {
                  Cd: "COID"
                }
              }
            }
          }
        },
        DbtrAcct: { Id: { IBAN: transaction.debtorAccount } },
        DbtrAgt: { FinInstnId: { BIC: transaction.debtorBic } },
        CdtrAgt: { FinInstnId: { BIC: transaction.creditorBic } },
        Cdtr: { Nm: transaction.creditorName },
        CdtrAcct: { Id: { IBAN: transaction.creditorAccount } },
      };
      if (transaction.remittanceInformation){
        transactionSchema.RmtInf = {};
        transactionSchema.RmtInf.Ustrd = transaction.remittanceInformation;
      }
      return transactionSchema;
    });

    const document = {
      Msg: {
        $: { ID: "Edoc", Type: this.messageHeader.type, Version: this.messageHeader.version + ".2021" },
        Header: messageHeader,
        Docs: {
          Doc: [ {  
            Header: docHeader,
            Ffcctrns: {
              Document: {
                FIToFICstmrCdtTrf: {
                  GrpHdr: groupHeader,
                  CdtTrfTxInf: body
                }
              }
            }
          } ]
        }
      }
    };

    return this.buildXml(document);
  }
  catch(err) {
    throw(err)
  }
  }
}

module.exports = FFCCTRNS;

// const message = new FFCCTRNS({ msgId: "D119070801827981", sender: "TBFULT21XXX", receiver: "LIABLT2XMSD" });

// const xml = message.addTransaction({
//   paymentInstructionId: "D119070801829878",
//   paymentTransactionId: "SU569",
//   amount: 100,
//   debtorName: "Voisin Dupuis SAS",
//   debtorAccount: "LT033180000050100232",
//   debtorBic: "TBFULT21XXX",
//   creditorName: "Bandomasis bankas Nr.1",
//   creditorAccount: "LT950100100000123456",
//   creditorBic: "BANDLT21XXX",
// }).addTransaction({
//   paymentInstructionId: "D119070801829878",
//   paymentTransactionId: "SU569",
//   amount: 100,
//   debtorName: "Voisin Dupuis SAS",
//   debtorAccount: "LT033180000050100232",
//   debtorBic: "TBFULT21XXX",
//   creditorName: "Bandomasis bankas Nr.1",
//   creditorAccount: "LT950100100000123456",
//   creditorBic: "BANDLT21XXX",
// }).toXml()

// require("fs").writeFileSync("./aa.xml", xml)
