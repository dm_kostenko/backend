const Message = require("./Message");
const { getNextWorkingDay } = require("../../helpers/target2Timing")

class ROINVSTG extends Message {
  constructor({ msgId, sender, receiver, system, parsedXml }){
    if(parsedXml) {
      const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
      super({ 
        sender: Sender,
        receiver: Receiver,
        priority: Priority,
        msgId: MsgId,
        date: Date,
        system: System,
        type: "ROINVSTG",
        version: "camt.029.001.03"
      });

      const { RsltnOfInvstgtn } = parsedXml.Msg.Docs.Doc.Roinvstg.Document;

      this._calculateAssgmntHeader({
        MsgId: RsltnOfInvstgtn.Assgnmt.Id,
        Sender: RsltnOfInvstgtn.Assgnmt.Assgnr.Agt.FinInstnId.BIC,
        Receiver: RsltnOfInvstgtn.Assgnmt.Assgne.Agt.FinInstnId.BIC,
        CreDtTm: RsltnOfInvstgtn.Assgnmt.CreDtTm,
        StsConf: RsltnOfInvstgtn.Sts.Conf
      });

      this.parseTransactions(parsedXml.Msg.Docs.Doc.Roinvstg.Document.RsltnOfInvstgtn.CxlDtls.TxInfAndSts);
    }
    else
      super({ msgId, sender, receiver, type: "ROINVSTG", version: "camt.029.001.03", system });
  }

  _calculateAssgmntHeader({
    MsgId,
    Sender,
    Receiver,
    CreDtTm,
    StsConf
  }){
    this.assgmntHeader = {
      Id: MsgId || this.messageHeader.msgId,
      AssignerBic: Sender || this.messageHeader.sender,
      AssigneeBic: Receiver || this.messageHeader.receiver,
      CreDtTm: CreDtTm || this.messageHeader.date,
      StsConf: StsConf || "RJCR"
    };
  }

  addTransactionCancellation({
    cancellationStsId,
    orgnlMsgId,
    orgnlMsgNmId = "pacs.008.001.02",
    orgnlEndToEndId,
    orgnlTxId,
    transactionCancellationSts = "RJCR",
    originatorClient,
    originatorBank,
    reasonForCancellation, // TxInfAndSts/CxlStsRsnInf/Rsn/Cd: LEGL, CUST.  TxInfAndSts/CxlStsRsnInf /Rsn/Prtry ARDT, AC04, AM04, NOAS, NOOR. 
    addInfForCancellation,
    orgnlIntrBkSttlmAmt,
    orgnlIntrBkSttlmDt,
    sttlmMtd = "CLRG",
    clrSysPrtry, // LITAS-RLS or LITAS-MIG
    serviceLevel = "SEPA",
    debtorName,
    debtorAccount,
    debtorBic,
    creditorName,
    creditorAccount,
    creditorBic,
  }) {
    super.addTransaction({
      cancellationStsId,
      orgnlMsgId,
      orgnlMsgNmId,
      orgnlEndToEndId,
      orgnlTxId,
      transactionCancellationSts,
      originatorClient,
      originatorBank,
      reasonForCancellation,
      addInfForCancellation,
      orgnlIntrBkSttlmAmt,
      orgnlIntrBkSttlmDt,
      sttlmMtd,
      clrSysPrtry,
      serviceLevel,
      debtorName,
      debtorAccount,
      debtorBic,
      creditorName,
      creditorAccount,
      creditorBic
    });
  }

  parseTransactions(xmlTransactions) {
    if (Array.isArray(xmlTransactions))
      xmlTransactions.forEach(transaction => this.parseTransaction(transaction));
    else
      this.parseTransaction(xmlTransactions);
  }

  parseTransaction(xmlTransaction) {
    this.addTransaction({
      cancellationStsId: xmlTransaction.CxlStsId,
      orgnlMsgId: xmlTransaction.OrgnlGrpInf.OrgnlMsgId,
      orgnlMsgNmId: xmlTransaction.OrgnlGrpInf.OrgnlMsgNmId,
      orgnlEndToEndId: xmlTransaction.OrgnlEndToEndId,
      orgnlTxId: xmlTransaction.OrgnlTxId,
      transactionCancellationSts: xmlTransaction.TxCxlSts,
      originatorClient: xmlTransaction.CxlStsRsnInf.Orgtr.Nm,
      originatorBank: xmlTransaction.CxlStsRsnInf.Orgtr.Id ? xmlTransaction.CxlStsRsnInf.Orgtr.Id.OrgId.BICOrBEI: undefined,
      reasonForCancellation: xmlTransaction.CxlStsRsnInf.Rsn.Prtry,
      addInfForCancellation: xmlTransaction.CxlStsRsnInf.AddtlInf,
      orgnlIntrBkSttlmAmt: xmlTransaction.OrgnlTxRef.IntrBkSttlmAmt._,
      orgnlIntrBkSttlmDt: xmlTransaction.OrgnlTxRef.IntrBkSttlmDt,
      sttlmMtd: xmlTransaction.OrgnlTxRef.SttlmInf.SttlmMtd,
      clrSysPrtry: xmlTransaction.OrgnlTxRef.SttlmInf.ClrSys.Prtry,
      serviceLevel: xmlTransaction.OrgnlTxRef.PmtTpInf.SvcLvl.Cd,
      debtorName: xmlTransaction.OrgnlTxRef.Dbtr.Nm,
      debtorAccount: xmlTransaction.OrgnlTxRef.DbtrAcct.Id.IBAN,
      debtorBic: xmlTransaction.OrgnlTxRef.DbtrAgt.FinInstnId.BIC,
      creditorName: xmlTransaction.OrgnlTxRef.Cdtr.Nm,
      creditorAccount: xmlTransaction.OrgnlTxRef.CdtrAcct.Id.IBAN,
      creditorBic: xmlTransaction.OrgnlTxRef.CdtrAgt.FinInstnId.BIC,
    })
  }


  toXml() {
    this._calculateAssgmntHeader({});

    const messageHeader = {
      Sender: this.messageHeader.sender,
      Receiver: this.messageHeader.receiver,
      Priority: this.messageHeader.priority,
      MsgId: this.messageHeader.msgId,
      Date: this.messageHeader.date,
      System: this.messageHeader.system
    };

    const docHeader = {
      DocId: this.messageHeader.msgId,
      Dates: {
        Type: "FormDoc",
        Date: this.messageHeader.date
      },
      Type: this.messageHeader.type,
      Priority: this.messageHeader.priority,
      BusinessArea: this.messageHeader.businessArea
    };

    const groupHeader = {
      Assgnmt: {
        Id: this.assgmntHeader.Id,
        Assgnr: { Agt: { FinInstnId: { BIC: this.assgmntHeader.AssignerBic } } },
        Assgne: { Agt: { FinInstnId: { BIC: this.assgmntHeader.AssigneeBic } } },
        CreDtTm: this.assgmntHeader.CreDtTm
      },
      Sts: { Conf: this.assgmntHeader.StsConf }
    };

    const body = this.transactions.map(transaction => {
      const transactionSchema = {
        TxInfAndSts: {
          CxlStsId: transaction.cancellationStsId,
          OrgnlGrpInf: {
            OrgnlMsgId: transaction.orgnlMsgId,
            OrgnlMsgNmId: transaction.orgnlMsgNmId
          },
          OrgnlInstrId: transaction.orgnlTxId,
          OrgnlEndToEndId: transaction.orgnlEndToEndId,
          OrgnlTxId: transaction.orgnlTxId,
          TxCxlSts: transaction.transactionCancellationSts,
          CxlStsRsnInf: {},
          /* 
            CxlDtls/TxInfAndSts/CxlStsRsnInf/Orgtr/Nm  Transaction originator Name, First Name or Title Initiator (client) 
            that issues a negative answer to cancellation request. Either this field or TxInf /RtrRsnInf/Orgtr/Id/OrgId/BICOrBEI should be completed.
          */
          OrgnlTxRef: {
            IntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: transaction.orgnlIntrBkSttlmAmt / 100 },
            IntrBkSttlmDt: transaction.orgnlIntrBkSttlmDt,
            SttlmInf: { SttlmMtd: transaction.sttlmMtd, ClrSys: { Prtry: transaction.clrSysPrtry } },
            PmtTpInf: { SvcLvl: { Cd: transaction.serviceLevel } },
            Dbtr: { Nm: transaction.debtorName },
            DbtrAcct: { Id: { IBAN: transaction.debtorAccount } },
            DbtrAgt: { FinInstnId: { BIC: transaction.debtorBic } },
            CdtrAgt: { FinInstnId: { BIC: transaction.creditorBic } },
            Cdtr: { Nm: transaction.creditorName },
            CdtrAcct: { Id: { IBAN: transaction.creditorAccount } },
          }
        }
      };
      
      if (transaction.originatorClient)
        transactionSchema.TxInfAndSts.CxlStsRsnInf = {
          ...transactionSchema.TxInfAndSts.CxlStsRsnInf,
          Orgtr: {
            Nm: transaction.originatorClient
          }
        }
      else if (transaction.originatorBank)
        transactionSchema.TxInfAndSts.CxlStsRsnInf = {
          ...transactionSchema.TxInfAndSts.CxlStsRsnInf,
          Orgtr: {
            Id: {
              OrgId: {
                BICOrBEI: transaction.originatorBank
              }
            }
          }
        }
        // reasonForCancellation, 
        // TxInfAndSts/CxlStsRsnInf/Rsn/Cd: LEGL, CUST.  
        // TxInfAndSts/CxlStsRsnInf/Rsn/Prtry ARDT, AC04, AM04, NOAS, NOOR. 
      
      if (transaction.reasonForCancellation === "LEGL" || transaction.reasonForCancellation === "CUST")
        transactionSchema.TxInfAndSts.CxlStsRsnInf = {
          ...transactionSchema.TxInfAndSts.CxlStsRsnInf,
          Rsn: {
            Cd: transaction.reasonForCancellation
          }
        }
      else
        transactionSchema.TxInfAndSts.CxlStsRsnInf = {
          ...transactionSchema.TxInfAndSts.CxlStsRsnInf,
          Rsn: {
            Prtry: transaction.reasonForCancellation
          }
        }

      if (transaction.reasonForCancellation === "LEGL")
        transactionSchema.TxInfAndSts.CxlStsRsnInf = {
          ...transactionSchema.TxInfAndSts.CxlStsRsnInf,
          AddtlInf: transaction.addInfForCancellation
        }

      return transactionSchema;
    });

    const document = {
      Msg: {
        $: { ID: "Edoc", Type: this.messageHeader.type, Version: this.messageHeader.version + ".2019" },
        Header: messageHeader,
        Docs: {
          Doc: [ {  
            Header: docHeader,
            Roinvstg: {
              Document: {
                RsltnOfInvstgtn: {
                  ...groupHeader,
                  CxlDtls: body
                }
              }
            }
          } ]
        }
      }
    };

    return this.buildXml(document);
  }
}

module.exports = ROINVSTG;