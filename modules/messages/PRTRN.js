const Message = require("./Message");
const type = "PRTRN";
const version = "pacs.004.001.02";
const { getNextWorkingDay } = require("../../helpers/target2Timing")

class PRTRN extends Message {
  constructor({ msgId, sender, receiver, parsedXml, system }){
    if (parsedXml) {
      const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
      super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type, version });
      const { CreDtTm, NbOfTxs, TtlRtrdIntrBkSttlmAmt, IntrBkSttlmDt, SttlmInf, InstdAgt } = parsedXml.Msg.Docs.Doc.Prtrn.Document.PmtRtr.GrpHdr;
      this._calculateGroupHeader({ 
        MsgId, 
        CreDtTm, 
        NbOfTxs, 
        TtlRtrdIntrBkSttlmAmt: TtlRtrdIntrBkSttlmAmt._, 
        IntrBkSttlmDt, 
        SttlmMtd: SttlmInf.SttlmMtd, 
        ClrSysPrtry: SttlmInf.ClrSys.Prtry, 
        FinInstnIdBIC: InstdAgt && InstdAgt.FinInstnId && InstdAgt.FinInstnId.BIC ? InstdAgt.FinInstnId.BIC : undefined  
      });

      this.parseTransactions(parsedXml.Msg.Docs.Doc.Prtrn.Document.PmtRtr.TxInf);
    }
    else 
      super({ msgId, sender, receiver, type, version, system });
  }

  _calculateGroupHeader({
    MsgId,
    CreDtTm,
    NbOfTxs,
    TtlRtrdIntrBkSttlmAmt,
    IntrBkSttlmDt,
    SttlmMtd,
    ClrSysPrtry,
    FinInstnIdBIC,
  }){
    this.groupHeader = {
      MsgId: MsgId || this.messageHeader.msgId,
      CreDtTm: CreDtTm || this.messageHeader.date,
      NbOfTxs: NbOfTxs || this.transactions.length,
      TtlRtrdIntrBkSttlmAmt: TtlRtrdIntrBkSttlmAmt || this.transactions.reduce((fullAmount, transaction) => fullAmount + parseFloat(transaction.rtrdIntrBkSttlmAmt), 0),
      IntrBkSttlmDt: IntrBkSttlmDt || getNextWorkingDay(),
      SttlmMtd: SttlmMtd || "CLRG",
      ClrSysPrtry: ClrSysPrtry || "LITAS-MIG", // LITAS-RLS/LITASMIG
      FinInstnIdBIC: FinInstnIdBIC || this.messageHeader.sender,
    };
  }

  addTransaction({
    returnmentId,
    originalMsgId,
    originalMsgNmId = "pacs.004.001.02",
    originalInstrId,
    originalEndToEndId,
    originalTxId,
    originalIntrBkSttlmAmt,
    rtrdIntrBkSttlmAmt,
    rtrdInstdAmt,
    chargeBearer = "SLEV",
    сhrgsInfAmt,
    originatorClient,
    originatorBank,
    reasonForReturn,  // allowed values: AC01, AC04, AC06, AG01, AG02, AM05, BE04, FOCR, MD07, MS02, MS03, RC01, RR01, RR02, RR03, RR04.
    rtrRsnInfAddtInf, //if reason: FOCR
    originalSettlementDate,
    originalSettlementMethod = "CLRG",
    originalServiceLevel = "SEPA",
    originalDebtorName,
    originalDebtorAccount,
    originalDebtorBic,
    originalCreditorName,
    originalCreditorAccount,
    originalCreditorBic,
  }) {
    super.addTransaction({
      returnmentId,
      originalMsgId,
      originalMsgNmId,
      originalInstrId,
      originalEndToEndId,
      originalTxId,
      originalIntrBkSttlmAmt,
      rtrdIntrBkSttlmAmt,
      rtrdInstdAmt,
      chargeBearer,
      сhrgsInfAmt,
      originatorClient,
      originatorBank,
      reasonForReturn, 
      rtrRsnInfAddtInf,
      originalSettlementDate,
      originalSettlementMethod,
      originalServiceLevel,
      originalDebtorName,
      originalDebtorAccount,
      originalDebtorBic,
      originalCreditorName,
      originalCreditorAccount,
      originalCreditorBic,
    });
    return this;
  }

  parseTransactions(xmlTransactions) {
    if (Array.isArray(xmlTransactions))
      xmlTransactions.forEach(transaction => this.parseTransaction(transaction));
    else
      this.parseTransaction(xmlTransactions);
  }

  parseTransaction(xmlTransaction) {
    this.addTransaction({
      returnmentId: xmlTransaction.RtrId,
      originalMsgId: xmlTransaction.OrgnlGrpInf.OrgnlMsgId,
      originalMsgNmId: xmlTransaction.OrgnlGrpInf.OrgnlMsgNmId,
      originalInstrId: xmlTransaction.OrgnlInstrId,
      originalEndToEndId: xmlTransaction.OrgnlEndToEndId,
      originalTxId: xmlTransaction.OrgnlTxId,
      originalIntrBkSttlmAmt: xmlTransaction.OrgnlIntrBkSttlmAmt._,
      rtrdIntrBkSttlmAmt: xmlTransaction.RtrdIntrBkSttlmAmt._,
      rtrdInstdAmt: xmlTransaction.RtrdInstdAmt ? xmlTransaction.RtrdInstdAmt._ : xmlTransaction.OrgnlIntrBkSttlmAmt._,
      chargeBearer: xmlTransaction.ChrgBr,
      сhrgsInfAmt: xmlTransaction.RtrRsnInf.Rsn.Cd === "FOCR" && xmlTransaction.ChrgsInf ? xmlTransaction.ChrgsInf.Amt._ : undefined , //IF reasor is FOCR
      originatorClient: xmlTransaction.RtrRsnInf.Orgtr.Nm,
      originatorBank: xmlTransaction.RtrRsnInf.Orgtr.Id ? xmlTransaction.RtrRsnInf.Orgtr.Id.OrgId.BICOrBEI : undefined,
      reasonForReturn: xmlTransaction.RtrRsnInf.Rsn.Cd, 
      rtrRsnInfAddtInf: xmlTransaction.RtrRsnInf.AddtlInf,
      originalSettlementDate: xmlTransaction.OrgnlTxRef.IntrBkSttlmDt,
      originalSettlementMethod: xmlTransaction.OrgnlTxRef.SttlmInf.SttlmMtd,
      originalServiceLevel: xmlTransaction.OrgnlTxRef.PmtTpInf.SvcLvl.Cd,
      originalDebtorName: xmlTransaction.OrgnlTxRef.Dbtr.Nm,
      originalDebtorAccount: xmlTransaction.OrgnlTxRef.DbtrAcct.Id.IBAN,
      originalDebtorBic: xmlTransaction.OrgnlTxRef.DbtrAgt.FinInstnId.BIC,
      originalCreditorName: xmlTransaction.OrgnlTxRef.Cdtr.Nm,
      originalCreditorAccount: xmlTransaction.OrgnlTxRef.CdtrAcct.Id.IBAN,
      originalCreditorBic: xmlTransaction.OrgnlTxRef.CdtrAgt.FinInstnId.BIC,
    });
  }

  toXml() {
    // this._calculateGroupHeader();

    const messageHeader = {
      Sender: this.messageHeader.sender,
      Receiver: this.messageHeader.receiver,
      Priority: this.messageHeader.priority,
      MsgId: this.messageHeader.msgId,
      Date: this.messageHeader.date,
      System: this.messageHeader.system
    };

    const docHeader = {
      DocId: this.messageHeader.msgId,
      Dates: {
        Type: "FormDoc",
        Date: this.messageHeader.date
      },
      Type: this.messageHeader.type,
      Priority: this.messageHeader.priority,
      BusinessArea: this.messageHeader.businessArea
    };

    const groupHeader = {
      MsgId: this.groupHeader.MsgId,
      CreDtTm: this.groupHeader.CreDtTm,
      NbOfTxs: this.groupHeader.NbOfTxs,
      TtlRtrdIntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: this.groupHeader.TtlRtrdIntrBkSttlmAmt / 100 },
      IntrBkSttlmDt: this.groupHeader.IntrBkSttlmDt,
      SttlmInf: {
        SttlmMtd: this.groupHeader.SttlmMtd,
        ClrSys: { Prtry: this.groupHeader.ClrSysPrtry }
      },
      InstgAgt: { FinInstnId: { BIC: this.groupHeader.FinInstnIdBIC } }
    };

    const body = this.transactions.map(transaction => {
      let transactionSchema = {
        RtrId: transaction.returnmentId,
        OrgnlGrpInf: {
          OrgnlMsgId: transaction.originalMsgId,
          OrgnlMsgNmId: transaction.originalMsgNmId,
        },
        OrgnlInstrId: transaction.originalInstrId,
        OrgnlEndToEndId: transaction.originalEndToEndId,
        OrgnlTxId: transaction.originalTxId,
        OrgnlIntrBkSttlmAmt: { $: { Ccy: "EUR" }, _: transaction.originalIntrBkSttlmAmt / 100 },
        RtrdIntrBkSttlmAmt: { $: { Ccy: "EUR" }, _:  transaction.rtrdIntrBkSttlmAmt / 100 },
        ChrgBr: transaction.chargeBearer,
        // RtrRsnInf/Orgtr/Nm or RtrRsnInf/Orgtr/Id/OrgId/BICOrBEI 
        RtrRsnInf: { 
          Orgtr: {  },
          Rsn: { Cd: transaction.reasonForReturn }
        },
        OrgnlTxRef: {
          IntrBkSttlmDt: transaction.originalSettlementDate,
          SttlmInf: { SttlmMtd: transaction.originalSettlementMethod } ,
          PmtTpInf: { SvcLvl: { Cd: transaction.originalServiceLevel } },
          Dbtr: { Nm: transaction.originalDebtorName },
          DbtrAcct: { Id: { IBAN: transaction.originalDebtorAccount } },
          DbtrAgt: { FinInstnId: { BIC: transaction.originalDebtorBic } },
          CdtrAgt: { FinInstnId: { BIC: transaction.originalCreditorBic } },
          Cdtr: { Nm: transaction.originalCreditorName },
          CdtrAcct: { Id: { IBAN: transaction.originalCreditorAccount } },
        }
      };

      if (transactionSchema.RtrRsnInf.Rsn.Cd === "FOCR" && transaction.сhrgsInfAmt) {
        // transactionSchema.ChrgsInf = { Amt: { $: { Ccy: "EUR" }, _: transaction.сhrgsInfAmt / 100 } };
        transactionSchema.RtrdInstdAmt = { $: { Ccy: "EUR" }, _: transaction.rtrdInstdAmt / 100 };
      }
      if (transaction.originatorClient)
        transactionSchema.RtrRsnInf.Orgtr = { Nm: transaction.originatorClient };
      else if (transaction.originatorBank)
        transactionSchema.RtrRsnInf.Orgtr = { Id: { OrgId: { BICOrBEI: transaction.originatorBank } } };
        
      if(transaction.rtrRsnInfAddtInf)
        transactionSchema.RtrRsnInf.AddtlInf = transaction.rtrRsnInfAddtInf;
      return transactionSchema;
    });

    const document = {
      Msg: {
        $: { ID: "Edoc", Type: this.messageHeader.type, Version: this.messageHeader.version + ".2021" },
        Header: messageHeader,
        Docs: {
          Doc: [ {  
            Header: docHeader,
            Prtrn: {
              Document: {
                PmtRtr: {
                  GrpHdr: groupHeader,
                  TxInf: body
                }
              }
            }
          } ]
        }
      }
    };
    return this.buildXml(JSON.parse(JSON.stringify(document)));
  }
}

module.exports = PRTRN;

// const message = new PRTRN({ msgId: "D119070801827981", sender: "TBFULT21XXX", receiver: "LIABLT2XMSD" });

// const xml = message.addTransaction({
//   paymentInstructionId: "D119070801829878",
//   paymentTransactionId: "SU569",
//   amount: 100,
//   debtorName: "Voisin Dupuis SAS",
//   debtorAccount: "LT033180000050100232",
//   debtorBic: "TBFULT21XXX",
//   creditorName: "Bandomasis bankas Nr.1",
//   creditorAccount: "LT950100100000123456",
//   creditorBic: "BANDLT21XXX",
// }).addTransaction({
//   returnmentId: Date.now(),
//   originalMsgId,
//   originalMsgNmId: "pacs.004.001.02",
//   originalInstrId,
//   originalEndToEndId,
//   originalTxId,
//   originalIntrBkSttlmAmt,
//   rtrdIntrBkSttlmAmt,
//   rtrdInstdAmt,
//   chargeBearer: "SLEV",
//   сhrgsInfAmt,
//   originatorClient,
//   originatorBank,
//   reasonForReturn,  // allowed values: AC01, AC04, AC06, AG01, AG02, AM05, BE04, FOCR, MD07, MS02, MS03, RC01, RR01, RR02, RR03, RR04.
//   rtrRsnInfAddtInf, //if reason: FOCR
//   originalSettlementDate,
//   originalSettlementMethod = "CLRG",
//   originalServiceLevel = "SEPA",
//   originalDebtorName,
//   originalDebtorAccount,
//   originalDebtorBic,
//   originalCreditorName,
//   originalCreditorAccount,
//   originalCreditorBic,
// }).toXml()

// require("fs").writeFileSync("./aa.xml", xml)
