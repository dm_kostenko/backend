const {XmlMessageParser} = require("./");

const data = `<?xml version='1.0' encoding='utf-8'?>
<?xml-stylesheet type='text/xsl' href='https://dstestlitas.lb.lt/msg/DICTIONR/2015-1-0/DICTIONR_view.xsl'?>
<Msg ID="Edoc" Type="DICTIONR" Version="2015-1-0">
	<Header>
		<Sender>LIABLT2XMSD</Sender>
		<Receiver>TBFULT21XXX</Receiver>
		<Priority>90</Priority>
		<MsgId>ZN20060407054744</MsgId>
		<Date>2020-06-04T15:18:43</Date>
		<System>LITAS-MMS</System>
	</Header>
	<Docs>
		<Doc>
			<Header>
				<DocId>ZN20060401969301</DocId>
				<Dates>
					<Type>FormDoc</Type>
					<Date>2020-06-04T15:18:14</Date>
				</Dates>
				<Type>PARTS</Type>
				<Priority>90</Priority>
				<BusinessArea>Reference</BusinessArea>
			</Header>
			<Parts>
				<Participant>
					<BIC>CBSBLT26XXX</BIC>
					<Name>AB Šiaulių bankas</Name>
					<Branch>
						<Code>71800</Code>
						<Name>AB Šiaulių bankas</Name>
					</Branch>
					<Branch>
						<Code>70130</Code>
						<Name>Testinis bankas</Name>
					</Branch>
					<Branch>
						<Code>70500</Code>
						<Name>AB bankas "Finasta"</Name>
					</Branch>
					<Branch>
						<Code>71802</Code>
						<Name>AB Šiaulių banko Kelmės KAC</Name>
					</Branch>
					<Branch>
						<Code>71803</Code>
						<Name>AB Šiaulių banko Vilniaus filialas</Name>
					</Branch>
					<Branch>
						<Code>71804</Code>
						<Name>AB Šiaulių banko Kuršėnų KAC</Name>
					</Branch>
					<Branch>
						<Code>71805</Code>
						<Name>AB Šiaulių banko Klaipėdos filialas</Name>
					</Branch>
					<Branch>
						<Code>71806</Code>
						<Name>AB Šiaulių banko Palangos KAC</Name>
					</Branch>
					<Branch>
						<Code>71807</Code>
						<Name>AB Šiaulių banko Šilutės KAC</Name>
					</Branch>
					<Branch>
						<Code>71808</Code>
						<Name>AB Šiaulių banko Mažeikių KAC</Name>
					</Branch>
					<Branch>
						<Code>71809</Code>
						<Name>AB Šiaulių banko Kauno filialas</Name>
					</Branch>
					<Branch>
						<Code>71810</Code>
						<Name>AB Šiaulių banko Vilkaviškio KAS</Name>
					</Branch>
					<Branch>
						<Code>71811</Code>
						<Name>AB Šiaulių banko Utenos KAC</Name>
					</Branch>
					<Branch>
						<Code>71812</Code>
						<Name>AB Šiaulių banko Alytaus KAC</Name>
					</Branch>
					<Branch>
						<Code>71813</Code>
						<Name>AB Šiaulių banko Tauragės KAC</Name>
					</Branch>
					<Branch>
						<Code>71814</Code>
						<Name>AB Šiaulių banko Radviliškio KAC</Name>
					</Branch>
					<Branch>
						<Code>71815</Code>
						<Name>AB Šiaulių banko Panevėžio KAC</Name>
					</Branch>
					<Branch>
						<Code>71816</Code>
						<Name>AB Šiaulių banko Birštono KAC</Name>
					</Branch>
					<Branch>
						<Code>71817</Code>
						<Name>AB Šiaulių banko Telšių KAC</Name>
					</Branch>
					<Branch>
						<Code>71818</Code>
						<Name>AB Šiaulių banko Marijampolės KAC</Name>
					</Branch>
					<Branch>
						<Code>71819</Code>
						<Name>AB Šiaulių banko Druskininkų KAC</Name>
					</Branch>
					<Branch>
						<Code>71820</Code>
						<Name>AB Šiaulių banko Plungės KAC</Name>
					</Branch>
					<Branch>
						<Code>71821</Code>
						<Name>AB Šiaulių banko Anykščių KAS</Name>
					</Branch>
					<Branch>
						<Code>71822</Code>
						<Name>AB Šiaulių banko Visagino KAC</Name>
					</Branch>
					<Branch>
						<Code>71823</Code>
						<Name>AB Šiaulių banko Kedainių KAC</Name>
					</Branch>
					<Branch>
						<Code>71824</Code>
						<Name>AB Šiaulių banko Ukmergės KAC</Name>
					</Branch>
					<Branch>
						<Code>71825</Code>
						<Name>AB Šiaulių banko Biržų KAC</Name>
					</Branch>
					<Branch>
						<Code>71826</Code>
						<Name>AB Šiaulių banko Jonavos KAS</Name>
					</Branch>
					<Branch>
						<Code>71827</Code>
						<Name>AB Šiaulių banko Zarasų KAS</Name>
					</Branch>
					<Branch>
						<Code>71828</Code>
						<Name>AB Šiaulių banko Rokiškio KAS</Name>
					</Branch>
					<Branch>
						<Code>71829</Code>
						<Name>AB Šiaulių banko Kupiškio KAS</Name>
					</Branch>
					<Branch>
						<Code>71830</Code>
						<Name>AB Šiaulių banko Raseinių KAS</Name>
					</Branch>
					<Branch>
						<Code>71899</Code>
						<Name>AB Šiaulių banko centrinė būstinė</Name>
					</Branch>
					<CD>
						<Code>0930</Code>
						<Name>AB Šiaulių banko VP apskaitos skyrius</Name>
					</CD>
					<Date>2017-12-15T08:33:41</Date>
				</Participant>
				<Participant>
					<BIC>BANDLT21XXX</BIC>
					<Name>Bandomasis bankas Nr.1</Name>
					<Branch>
						<Code>01001</Code>
						<Name>Bandomasis bankas Nr.1</Name>
					</Branch>
					<Date>2018-03-15T15:00:11</Date>
				</Participant>
				<Participant>
					<BIC>ABLPLT21XXX</BIC>
					<Name>AB Lietuvos paštas</Name>
					<Branch>
						<Code>30300</Code>
						<Name>AB Lietuvos paštas</Name>
					</Branch>
					<Date>2016-12-15T17:38:57</Date>
				</Participant>
				<Participant>
					<BIC>EVIULT2VXXX</BIC>
					<Name>"Paysera LT", UAB</Name>
					<Branch>
						<Code>35000</Code>
						<Name>"Paysera LT", UAB</Name>
					</Branch>
					<Date>2020-01-30T18:35:03</Date>
				</Participant>
				<Participant>
					<BIC>XYTSLT21XXX</BIC>
					<Name>Transactive test</Name>
					<Branch>
						<Code>01002</Code>
						<Name>Transactive test</Name>
					</Branch>
					<Date>2017-04-11T14:40:14</Date>
				</Participant>
				<Participant>
					<BIC>VEPALT21XXX</BIC>
					<Name>Verified Payments, UAB</Name>
					<Branch>
						<Code>37500</Code>
						<Name>Verified Payments, UAB</Name>
					</Branch>
					<Date>2019-01-21T10:28:21</Date>
				</Participant>
				<Participant>
					<BIC>PAPYMTMTXXX</BIC>
					<Name>Papaya Ltd</Name>
					<Branch>
						<Code>38200</Code>
						<Name>Papaya Ltd</Name>
					</Branch>
					<Date>2018-05-22T13:28:15</Date>
				</Participant>
				<Participant>
					<BIC>TRFBLT21XXX</BIC>
					<Name>Trustcom Financial UAB</Name>
					<Branch>
						<Code>36900</Code>
						<Name>Trustcom Financial UAB</Name>
					</Branch>
					<Date>2017-10-17T08:07:24</Date>
				</Participant>
				<Participant>
					<BIC>UFPOLT21XXX</BIC>
					<Name>Contis Financial Services Limited</Name>
					<Branch>
						<Code>39600</Code>
						<Name>Contis Financial Services Limited</Name>
					</Branch>
					<Date>2017-12-19T08:45:38</Date>
				</Participant>
				<Participant>
					<BIC>ARPYGB21XXX</BIC>
					<Name>ARCAPAY LTD</Name>
					<Branch>
						<Code>30039</Code>
						<Name>ARCAPAY LTD</Name>
					</Branch>
					<Date>2018-06-29T19:23:36</Date>
				</Participant>
				<Participant>
					<BIC>BEFKCZP1XXX</BIC>
					<Name>B-Efekt a.s.</Name>
					<Branch>
						<Code>30040</Code>
						<Name>B-Efekt a.s.</Name>
					</Branch>
					<Date>2018-07-05T18:19:14</Date>
				</Participant>
				<Participant>
					<BIC>IGTRPLP1XXX</BIC>
					<Name>Igoria Trade S.A.</Name>
					<Branch>
						<Code>30050</Code>
						<Name>Igoria Trade S.A.</Name>
					</Branch>
					<Date>2018-10-08T16:17:13</Date>
				</Participant>
				<Participant>
					<BIC>CFZZGB21XXX</BIC>
					<Name>CFS-ZIPP LIMITED</Name>
					<Branch>
						<Code>30043</Code>
						<Name>CFS-ZIPP LIMITED</Name>
					</Branch>
					<Date>2019-05-03T14:20:06</Date>
				</Participant>
				<Participant>
					<BIC>UNNIGB21XXX</BIC>
					<Name>UNIFIED FINANCE LTD</Name>
					<Branch>
						<Code>30061</Code>
						<Name>UNIFIED FINANCE LTD</Name>
					</Branch>
					<Date>2019-05-03T14:16:03</Date>
				</Participant>
				<Participant>
					<BIC>PAUUMTM1XXX</BIC>
					<Name>Paymentworld Europe Limited</Name>
					<Branch>
						<Code>30055</Code>
						<Name>Paymentworld Europe Limited</Name>
					</Branch>
					<Date>2019-12-06T14:54:32</Date>
				</Participant>
				<Participant>
					<BIC>FEMAMTMAXXX</BIC>
					<Name>Ferratum Bank PLC</Name>
					<Branch>
						<Code>30066</Code>
						<Name>Ferratum Bank PLC</Name>
					</Branch>
					<Date>2019-07-12T16:49:36</Date>
				</Participant>
				<Participant>
					<BIC>UAPPLT21XXX</BIC>
					<Name>UAB "PHOENIX PAYMENTS"</Name>
					<Branch>
						<Code>32200</Code>
						<Name>UAB "PHOENIX PAYMENTS"</Name>
					</Branch>
					<Date>2019-05-03T13:58:51</Date>
				</Participant>
				<Participant>
					<BIC>MAEHEE21XXX</BIC>
					<Name>Maaelu Edendamise Hoiu-laenuuhistu</Name>
					<Branch>
						<Code>30071</Code>
						<Name>Maaelu Edendamise Hoiu-laenuuhistu</Name>
					</Branch>
					<Date>2020-04-02T11:13:46</Date>
				</Participant>
				<Participant>
					<BIC>TRLYSESSXXX</BIC>
					<Name>Trustly Group AB</Name>
					<Branch>
						<Code>30072</Code>
						<Name>Trustly Group AB</Name>
					</Branch>
					<Date>2019-03-18T09:05:18</Date>
				</Participant>
				<Participant>
					<BIC>DSFELT21XXX</BIC>
					<Name>DSBC Financial Europe, UAB</Name>
					<Branch>
						<Code>32700</Code>
						<Name>DSBC Financial Europe, UAB</Name>
					</Branch>
					<Date>2019-10-03T13:46:47</Date>
				</Participant>
				<Participant>
					<BIC>ADWSGB22XXX</BIC>
					<Name>Advanced Wallet Solutions Limited</Name>
					<Branch>
						<Code>30086</Code>
						<Name>Advanced Wallet Solutions Limited</Name>
					</Branch>
					<Date>2020-02-18T19:25:24</Date>
				</Participant>
				<Participant>
					<BIC>PYYPGB21XXX</BIC>
					<Name>MY EU PAY LTD</Name>
					<Branch>
						<Code>30083</Code>
						<Name>MY EU PAY LTD</Name>
					</Branch>
					<Date>2020-03-20T10:21:19</Date>
				</Participant>
				<Participant>
					<BIC>PYYTGB21XXX</BIC>
					<Name>PAYSTREE Limited</Name>
					<Branch>
						<Code>30077</Code>
						<Name>PAYSTREE Limited</Name>
					</Branch>
					<Date>2019-05-20T17:27:55</Date>
				</Participant>
				<Participant>
					<BIC>PAYBGB21XXX</BIC>
					<Name>Paybase Limited</Name>
					<Branch>
						<Code>30078</Code>
						<Name>Paybase Limited</Name>
					</Branch>
					<Date>2019-05-30T09:46:02</Date>
				</Participant>
				<Participant>
					<BIC>EPUALT22XXX</BIC>
					<Name>UAB "epayblock"</Name>
					<Branch>
						<Code>33500</Code>
						<Name>UAB "epayblock"</Name>
					</Branch>
					<Date>2020-02-18T19:48:37</Date>
				</Participant>
				<Participant>
					<BIC>USPELT22XXX</BIC>
					<Name>UAB "Simplex Payment Services"</Name>
					<Branch>
						<Code>33900</Code>
						<Name>UAB "Simplex Payment Services"</Name>
					</Branch>
					<Date>2020-04-06T16:47:11</Date>
				</Participant>
				<Participant>
					<BIC>MTCCMTMTXXX</BIC>
					<Name>MTACC Ltd.</Name>
					<Branch>
						<Code>30095</Code>
						<Name>MTACC Ltd.</Name>
					</Branch>
					<Date>2020-03-04T19:44:05</Date>
				</Participant>
				<Participant>
					<BIC>BAXXLT22XXX</BIC>
					<Name>Banxe, UAB</Name>
					<Branch>
						<Code>34200</Code>
						<Name>Banxe, UAB</Name>
					</Branch>
					<Date>2020-04-20T13:07:24</Date>
				</Participant>
				<Participant>
					<BIC>LIABLT2XXXX</BIC>
					<Name>Lietuvos bankas</Name>
					<Branch>
						<Code>10100</Code>
						<Name>Lietuvos bankas</Name>
					</Branch>
					<Branch>
						<Code>10101</Code>
						<Name>Lietuvos banko Kauno skyrius</Name>
					</Branch>
					<Branch>
						<Code>10900</Code>
						<Name>Mokėjimo sistemų departamentas</Name>
					</Branch>
					<CD>
						<Code>8100</Code>
						<Name>Lietuvos bankas</Name>
					</CD>
					<Date>2009-10-20T22:50:25</Date>
				</Participant>
				<Participant>
					<BIC>VRKULT21XXX</BIC>
					<Name>Rato kredito unija</Name>
					<Branch>
						<Code>51900</Code>
						<Name>Rato kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50160</Code>
						<Name>Rato kredito unijos padalinys</Name>
					</Branch>
					<Date>2020-05-28T18:10:42</Date>
				</Participant>
				<Participant>
					<BIC>UAINLT21XXX</BIC>
					<Name>UAB "NIUM EU"</Name>
					<Branch>
						<Code>35900</Code>
						<Name>UAB "NIUM EU"</Name>
					</Branch>
					<Date>2020-04-09T18:16:18</Date>
				</Participant>
				<Participant>
					<BIC>CNUALT21XXX</BIC>
					<Name>UAB "ConnectPay"</Name>
					<Branch>
						<Code>37400</Code>
						<Name>UAB "ConnectPay"</Name>
					</Branch>
					<Date>2019-02-06T14:28:10</Date>
				</Participant>
				<Participant>
					<BIC>UAPLLT21XXX</BIC>
					<Name>UAB "Pyrros Lithuania"</Name>
					<Branch>
						<Code>36700</Code>
						<Name>UAB "Pyrros Lithuania"</Name>
					</Branch>
					<Date>2019-06-03T14:48:15</Date>
				</Participant>
				<Participant>
					<BIC>OKPYCY2LXXX</BIC>
					<Name>OKPAY CY LTD</Name>
					<Branch>
						<Code>30033</Code>
						<Name>OKPAY CY LTD</Name>
					</Branch>
					<Date>2018-03-23T10:35:17</Date>
				</Participant>
				<Participant>
					<BIC>DUPYLV21XXX</BIC>
					<Name>Dukascopy Payments SIA</Name>
					<Branch>
						<Code>30037</Code>
						<Name>Dukascopy Payments SIA</Name>
					</Branch>
					<Date>2018-06-28T17:05:32</Date>
				</Participant>
				<Participant>
					<BIC>BITSNL2AXXX</BIC>
					<Name>Verotel Merchant Services B.V.</Name>
					<Branch>
						<Code>30042</Code>
						<Name>Verotel Merchant Services B.V.</Name>
					</Branch>
					<Date>2018-07-27T20:03:40</Date>
				</Participant>
				<Participant>
					<BIC>NEUALT21XXX</BIC>
					<Name>UAB "NexPay"</Name>
					<Branch>
						<Code>30800</Code>
						<Name>UAB "NexPay"</Name>
					</Branch>
					<Date>2019-01-07T10:52:19</Date>
				</Participant>
				<Participant>
					<BIC>BIYSGB21XXX</BIC>
					<Name>Bilderlings Pay Limited</Name>
					<Branch>
						<Code>30135</Code>
						<Name>Bilderlings Pay Limited</Name>
					</Branch>
					<Date>2018-08-13T18:48:27</Date>
				</Participant>
				<Participant>
					<BIC>INTFBGSFXXX</BIC>
					<Name>iCARD AD</Name>
					<Branch>
						<Code>30053</Code>
						<Name>iCARD AD</Name>
					</Branch>
					<Date>2018-11-28T15:10:55</Date>
				</Participant>
				<Participant>
					<BIC>FAPOCZP1XXX</BIC>
					<Name>Fairplay Pay s.r.o.</Name>
					<Branch>
						<Code>30046</Code>
						<Name>Fairplay Pay s.r.o.</Name>
					</Branch>
					<Date>2018-09-05T19:15:36</Date>
				</Participant>
				<Participant>
					<BIC>UFUNLT22XXX</BIC>
					<Name>Finolita Unio UAB</Name>
					<Branch>
						<Code>35600</Code>
						<Name>Finolita Unio UAB</Name>
					</Branch>
					<Date>2019-10-24T11:35:06</Date>
				</Participant>
				<Participant>
					<BIC>ORRRCZP1XXX</BIC>
					<Name>OrangeTrust s.r.o.</Name>
					<Branch>
						<Code>30051</Code>
						<Name>OrangeTrust s.r.o.</Name>
					</Branch>
					<Date>2019-03-04T18:04:13</Date>
				</Participant>
				<Participant>
					<BIC>MCPIMTM1XXX</BIC>
					<Name>Money+Card Payment Institution Ltd</Name>
					<Branch>
						<Code>30052</Code>
						<Name>Money+Card Payment Institution Ltd</Name>
					</Branch>
					<Date>2018-11-28T15:21:15</Date>
				</Participant>
				<Participant>
					<BIC>SHFRLT21XXX</BIC>
					<Name>SHIFT Financial Services LT, UAB</Name>
					<Branch>
						<Code>31700</Code>
						<Name>SHIFT Financial Services LT, UAB</Name>
					</Branch>
					<Date>2019-03-04T18:02:25</Date>
				</Participant>
				<Participant>
					<BIC>PYMXMTMTXXX</BIC>
					<Name>PAYMIX LTD</Name>
					<Branch>
						<Code>30065</Code>
						<Name>PAYMIX LTD</Name>
					</Branch>
					<Date>2019-02-13T15:53:04</Date>
				</Participant>
				<Participant>
					<BIC>TBFULT31XXX</BIC>
					<Name>TBF Finance, UAB</Name>
					<Branch>
						<Code>31801</Code>
						<Name>TBF Finance, UAB</Name>
					</Branch>
					<Date>2019-02-07T09:47:11</Date>
				</Participant>
				<Participant>
					<BIC>TEUALT22XXX</BIC>
					<Name>UAB TeslaPay</Name>
					<Branch>
						<Code>31900</Code>
						<Name>UAB TeslaPay</Name>
					</Branch>
					<Date>2019-03-05T12:20:45</Date>
				</Participant>
				<Participant>
					<BIC>PHPYMTM1XXX</BIC>
					<Name>Phoenix Payments Ltd</Name>
					<Branch>
						<Code>30070</Code>
						<Name>Phoenix Payments Ltd</Name>
					</Branch>
					<Date>2019-02-21T14:43:22</Date>
				</Participant>
				<Participant>
					<BIC>PAERCZPPXXX</BIC>
					<Name>Payment execution s.r.o.</Name>
					<Branch>
						<Code>30141</Code>
						<Name>Payment execution s.r.o.</Name>
					</Branch>
					<Date>2019-02-22T11:01:20</Date>
				</Participant>
				<Participant>
					<BIC>CNENPLP1XXX</BIC>
					<Name>CURRENCY ONE S.A.</Name>
					<Branch>
						<Code>30079</Code>
						<Name>CURRENCY ONE S.A.</Name>
					</Branch>
					<Date>2019-06-17T13:29:40</Date>
				</Participant>
				<Participant>
					<BIC>UAUPLT22XXX</BIC>
					<Name>UAB "UPLATA EU"</Name>
					<Branch>
						<Code>33400</Code>
						<Name>UAB "UPLATA EU"</Name>
					</Branch>
					<Date>2020-02-18T19:33:39</Date>
				</Participant>
				<Participant>
					<BIC>FOUNGB21XXX</BIC>
					<Name>MONETLEY LTD</Name>
					<Branch>
						<Code>30091</Code>
						<Name>MONETLEY LTD</Name>
					</Branch>
					<Date>2019-11-27T16:39:33</Date>
				</Participant>
				<Participant>
					<BIC>GUPULT22XXX</BIC>
					<Name>UAB Guru Pay</Name>
					<Branch>
						<Code>34000</Code>
						<Name>UAB Guru Pay</Name>
					</Branch>
					<Date>2020-04-06T17:03:50</Date>
				</Participant>
				<Participant>
					<BIC>MDBALT22XXX</BIC>
					<Name>UAB Medicinos bankas</Name>
					<Branch>
						<Code>72300</Code>
						<Name>UAB Medicinos bankas</Name>
					</Branch>
					<CD>
						<Code>0954</Code>
						<Name>UAB 'MEDICINOS BANKAS' FMS</Name>
					</CD>
					<Date>2007-01-08T14:52:40</Date>
				</Participant>
				<Participant>
					<BIC>FIBALT22XXX</BIC>
					<Name>AB bankas "Finasta"</Name>
					<CD>
						<Code>0966</Code>
						<Name>AB Šiaulių banko VP apskaitos skyrius (buvęs AB bankas "FINASTA")</Name>
					</CD>
					<Date>2016-10-17T11:20:09</Date>
				</Participant>
				<Participant>
					<BIC>KUSRLT21XXX</BIC>
					<Name>AB "Mano bankas"</Name>
					<Branch>
						<Code>50300</Code>
						<Name>AB "Mano bankas"</Name>
					</Branch>
					<Date>2019-01-04T10:29:23</Date>
				</Participant>
				<Participant>
					<BIC>VIKULT21XXX</BIC>
					<Name>Vilniaus kredito unija</Name>
					<Branch>
						<Code>50800</Code>
						<Name>Vilniaus kredito unija</Name>
					</Branch>
					<Date>2016-09-20T00:58:12</Date>
				</Participant>
				<Participant>
					<BIC>KUPRLT21XXX</BIC>
					<Name>Kredito unija "Magnus"</Name>
					<Branch>
						<Code>51400</Code>
						<Name>Kredito unija "Magnus"</Name>
					</Branch>
					<Date>2018-12-12T14:04:33</Date>
				</Participant>
				<Participant>
					<BIC>TRWIGB22XXX</BIC>
					<Name>TransferWise Ltd.</Name>
					<Branch>
						<Code>39000</Code>
						<Name>TransferWise Ltd.</Name>
					</Branch>
					<Date>2018-02-27T14:04:32</Date>
				</Participant>
				<Participant>
					<BIC>UAPYLT21XXX</BIC>
					<Name>UAB "Alternative Payments"</Name>
					<Branch>
						<Code>35300</Code>
						<Name>UAB "Alternative Payments"</Name>
					</Branch>
					<Date>2016-12-16T12:31:56</Date>
				</Participant>
				<Participant>
					<BIC>EVIUPLP1XXX</BIC>
					<Name>UAB PAYSERA PL</Name>
					<Branch>
						<Code>35002</Code>
						<Name>UAB PAYSERA PL</Name>
					</Branch>
					<Date>2019-04-29T12:48:29</Date>
				</Participant>
				<Participant>
					<BIC>XZYWLT21XXX</BIC>
					<Name>UAB "Baltic Banking Service"</Name>
					<Branch>
						<Code>30002</Code>
						<Name>UAB "Baltic Banking Service"</Name>
					</Branch>
					<Date>2017-11-22T09:43:16</Date>
				</Participant>
				<Participant>
					<BIC>MPABSE22XXX</BIC>
					<Name>Monark Finans AB</Name>
					<Branch>
						<Code>38000</Code>
						<Name>Monark Finans AB</Name>
					</Branch>
					<Date>2017-11-08T16:10:43</Date>
				</Participant>
				<Participant>
					<BIC>ECBXCY2NXXX</BIC>
					<Name>ECOMMBX LIMITED</Name>
					<Branch>
						<Code>37900</Code>
						<Name>ECOMMBX LIMITED</Name>
					</Branch>
					<Date>2018-05-28T11:02:08</Date>
				</Participant>
				<Participant>
					<BIC>PAOMGB21XXX</BIC>
					<Name>Payoma Limited</Name>
					<Branch>
						<Code>38600</Code>
						<Name>Payoma Limited</Name>
					</Branch>
					<Date>2018-10-06T14:10:17</Date>
				</Participant>
				<Participant>
					<BIC>PAEOLT21XXX</BIC>
					<Name>UAB PanPay Europe</Name>
					<Branch>
						<Code>38700</Code>
						<Name>UAB PanPay Europe</Name>
					</Branch>
					<Date>2018-06-19T14:12:41</Date>
				</Participant>
				<Participant>
					<BIC>UAPELT22XXX</BIC>
					<Name>UAB "Pervesk"</Name>
					<Branch>
						<Code>35500</Code>
						<Name>UAB "Pervesk"</Name>
					</Branch>
					<Date>2020-03-18T16:08:11</Date>
				</Participant>
				<Participant>
					<BIC>CARDCY2LXXX</BIC>
					<Name>CardPay Ltd</Name>
					<Branch>
						<Code>30034</Code>
						<Name>CardPay Ltd</Name>
					</Branch>
					<Date>2018-05-28T11:05:53</Date>
				</Participant>
				<Participant>
					<BIC>PFSRIE21XXX</BIC>
					<Name>Prepaid Financial Services Limited</Name>
					<Branch>
						<Code>30032</Code>
						<Name>Prepaid Financial Services Limited</Name>
					</Branch>
					<Date>2020-06-03T12:13:26</Date>
				</Participant>
				<Participant>
					<BIC>ISEMCY22XXX</BIC>
					<Name>iSignthis eMoney Ltd</Name>
					<Branch>
						<Code>30038</Code>
						<Name>iSignthis eMoney Ltd</Name>
					</Branch>
					<Date>2018-11-28T15:12:38</Date>
				</Participant>
				<Participant>
					<BIC>PRNSFRP1XXX</BIC>
					<Name>Prepaid Financial Services Limited (FR branch)</Name>
					<Branch>
						<Code>30132</Code>
						<Name>Prepaid Financial Services Limited (FR branch)</Name>
					</Branch>
					<Date>2020-06-04T13:18:05</Date>
				</Participant>
				<Participant>
					<BIC>PAEDGB21XXX</BIC>
					<Name>PAYSEND PLC</Name>
					<Branch>
						<Code>30047</Code>
						<Name>PAYSEND PLC</Name>
					</Branch>
					<Date>2019-01-21T10:25:57</Date>
				</Participant>
				<Participant>
					<BIC>TRPEMTM1XXX</BIC>
					<Name>TRUEVO PAYMENTS LTD</Name>
					<Branch>
						<Code>30054</Code>
						<Name>TRUEVO PAYMENTS LTD</Name>
					</Branch>
					<Date>2019-04-05T14:55:57</Date>
				</Participant>
				<Participant>
					<BIC>PATCBGSFXXX</BIC>
					<Name>Paynetics AD</Name>
					<Branch>
						<Code>30064</Code>
						<Name>Paynetics AD</Name>
					</Branch>
					<Date>2019-01-25T16:56:49</Date>
				</Participant>
				<Participant>
					<BIC>SEOUGB21XXX</BIC>
					<Name>SETTLEGO SOLUTIONS LIMITED</Name>
					<Branch>
						<Code>30068</Code>
						<Name>SETTLEGO SOLUTIONS LIMITED</Name>
					</Branch>
					<Date>2020-02-27T14:40:03</Date>
				</Participant>
				<Participant>
					<BIC>EUEBLT22XXX</BIC>
					<Name>European Merchant Bank UAB</Name>
					<Branch>
						<Code>71100</Code>
						<Name>European Merchant Bank UAB</Name>
					</Branch>
					<Date>2019-11-19T12:50:00</Date>
				</Participant>
				<Participant>
					<BIC>TPAYSK21XXX</BIC>
					<Name>Trust Pay, a.s.</Name>
					<Branch>
						<Code>30075</Code>
						<Name>Trust Pay, a.s.</Name>
					</Branch>
					<Date>2019-04-29T10:54:39</Date>
				</Participant>
				<Participant>
					<BIC>UHISEE21XXX</BIC>
					<Name>Uhisarveldused AS</Name>
					<Branch>
						<Code>30074</Code>
						<Name>Uhisarveldused AS</Name>
					</Branch>
					<Date>2020-04-02T10:58:14</Date>
				</Participant>
				<Participant>
					<BIC>PFSSESM1XXX</BIC>
					<Name>Prepaid Financial Services Limited Sucursal en Espana</Name>
					<Branch>
						<Code>30232</Code>
						<Name>Prepaid Financial Services Limited Sucursal en Espana</Name>
					</Branch>
					<Date>2019-06-27T11:50:04</Date>
				</Participant>
				<Participant>
					<BIC>MOOWGB22XXX</BIC>
					<Name>Moorwand LTD</Name>
					<Branch>
						<Code>30084</Code>
						<Name>Moorwand LTD</Name>
					</Branch>
					<Date>2019-10-03T14:01:08</Date>
				</Participant>
				<Participant>
					<BIC>BUEEGB22XXX</BIC>
					<Name>Bureau Buttercrane Ltd</Name>
					<Branch>
						<Code>30081</Code>
						<Name>Bureau Buttercrane Ltd</Name>
					</Branch>
					<Date>2019-10-03T13:43:09</Date>
				</Participant>
				<Participant>
					<BIC>FNHOGB21XXX</BIC>
					<Name>FINANCIAL HOUSE LIMITED</Name>
					<Branch>
						<Code>30090</Code>
						<Name>FINANCIAL HOUSE LIMITED</Name>
					</Branch>
					<Date>2020-01-10T14:35:51</Date>
				</Participant>
				<Participant>
					<BIC>CSDLLT22577</BIC>
					<Name>Nasdaq CSD SE Lietuvos filialas</Name>
					<Branch>
						<Code>90577</Code>
						<Name>Nasdaq CSD SE Lietuvos filialas</Name>
					</Branch>
					<CD>
						<Code>0577</Code>
						<Name>Nasdaq CSD SE Lietuvos filialas</Name>
					</CD>
					<CD>
						<Code>0600</Code>
						<Name>LCVPD (Lietuvos banko įkeitimo operacijos)</Name>
					</CD>
					<Date>2017-09-25T17:06:02</Date>
				</Participant>
				<Participant>
					<BIC>TESTLT21XXX</BIC>
					<Name>TESTAS</Name>
					<Branch>
						<Code>79999</Code>
						<Name>TESTAS</Name>
					</Branch>
					<Date>2015-04-08T09:55:23</Date>
				</Participant>
				<Participant>
					<BIC>KRUALT21XXX</BIC>
					<Name>Kredito unija "Taupa"</Name>
					<Branch>
						<Code>50500</Code>
						<Name>Kredito unija "Taupa"</Name>
					</Branch>
					<Date>2019-02-20T12:51:05</Date>
				</Participant>
				<Participant>
					<BIC>LTKULT21XXX</BIC>
					<Name>LTL kredito unija</Name>
					<Branch>
						<Code>51500</Code>
						<Name>LTL kredito unija</Name>
					</Branch>
					<Date>2017-06-06T14:01:20</Date>
				</Participant>
				<Participant>
					<BIC>PEPGLT21XXX</BIC>
					<Name>UAB "Perlas Finance"</Name>
					<Branch>
						<Code>30200</Code>
						<Name>UAB "Perlas Finance"</Name>
					</Branch>
					<Date>2019-08-20T11:17:27</Date>
				</Participant>
				<Participant>
					<BIC>IBIULT21XXX</BIC>
					<Name>UAB "IBS Lithuania"</Name>
					<Branch>
						<Code>39100</Code>
						<Name>UAB "IBS Lithuania"</Name>
					</Branch>
					<Date>2019-02-06T14:25:50</Date>
				</Participant>
				<Participant>
					<BIC>GFCBEE22XXX</BIC>
					<Name>GFC Good Finance Company AS</Name>
					<Branch>
						<Code>39400</Code>
						<Name>GFC Good Finance Company AS</Name>
					</Branch>
					<Date>2017-08-08T11:55:28</Date>
				</Participant>
				<Participant>
					<BIC>MAABLT21XXX</BIC>
					<Name>UAB "Majestic Financial“</Name>
					<Branch>
						<Code>37300</Code>
						<Name>UAB "Majestic Financial“</Name>
					</Branch>
					<Date>2017-12-29T08:45:37</Date>
				</Participant>
				<Participant>
					<BIC>GLUALT22XXX</BIC>
					<Name>GlobalNetint UAB</Name>
					<Branch>
						<Code>38800</Code>
						<Name>GlobalNetint UAB</Name>
					</Branch>
					<Date>2019-04-10T17:28:56</Date>
				</Participant>
				<Participant>
					<BIC>CNNNGB21XXX</BIC>
					<Name>Connectum Limited</Name>
					<Branch>
						<Code>30030</Code>
						<Name>Connectum Limited</Name>
					</Branch>
					<Date>2018-08-07T13:35:23</Date>
				</Participant>
				<Participant>
					<BIC>RNUALT21XXX</BIC>
					<Name>Rention UAB</Name>
					<Branch>
						<Code>36500</Code>
						<Name>Rention UAB</Name>
					</Branch>
					<Date>2017-10-19T07:59:26</Date>
				</Participant>
				<Participant>
					<BIC>VPAYBEB1XXX</BIC>
					<Name>VIVA PAYMENT SERVICES S.A. Belgian</Name>
					<Branch>
						<Code>30049</Code>
						<Name>VIVA PAYMENT SERVICES S.A. Belgian</Name>
					</Branch>
					<Date>2018-09-18T14:16:46</Date>
				</Participant>
				<Participant>
					<BIC>WEERGB21XXX</BIC>
					<Name>Webmoney Europe Ltd</Name>
					<Branch>
						<Code>30062</Code>
						<Name>Webmoney Europe Ltd</Name>
					</Branch>
					<Date>2019-01-25T13:50:09</Date>
				</Participant>
				<Participant>
					<BIC>SEPMCY2NXXX</BIC>
					<Name>SEPAGA E.M.I. LIMITED</Name>
					<Branch>
						<Code>30063</Code>
						<Name>SEPAGA E.M.I. LIMITED</Name>
					</Branch>
					<Date>2019-01-31T09:32:38</Date>
				</Participant>
				<Participant>
					<BIC>OPEXGB2LXXX</BIC>
					<Name>OPENAX LIMITED</Name>
					<Branch>
						<Code>30085</Code>
						<Name>OPENAX LIMITED</Name>
					</Branch>
					<Date>2020-03-17T13:45:05</Date>
				</Participant>
				<Participant>
					<BIC>TBIBBGSFXXX</BIC>
					<Name>TBI Bank EAD</Name>
					<Branch>
						<Code>30069</Code>
						<Name>TBI Bank EAD</Name>
					</Branch>
					<Date>2019-07-19T11:46:22</Date>
				</Participant>
				<Participant>
					<BIC>SUPULT22XXX</BIC>
					<Name>SumUp EU Payments, UAB</Name>
					<Branch>
						<Code>33700</Code>
						<Name>SumUp EU Payments, UAB</Name>
					</Branch>
					<Date>2020-02-18T19:19:26</Date>
				</Participant>
				<Participant>
					<BIC>PAYELV21XXX</BIC>
					<Name>Paysera LV, UAB</Name>
					<Branch>
						<Code>35001</Code>
						<Name>Paysera LV, UAB</Name>
					</Branch>
					<Date>2017-02-10T12:38:26</Date>
				</Participant>
				<Participant>
					<BIC>DIPOGB22XXX</BIC>
					<Name>DiPocket Limited</Name>
					<Branch>
						<Code>30088</Code>
						<Name>DiPocket Limited</Name>
					</Branch>
					<Date>2019-09-20T14:33:40</Date>
				</Participant>
				<Participant>
					<BIC>XZYALT21XXX</BIC>
					<Name>Alna Software, UAB</Name>
					<Branch>
						<Code>30005</Code>
						<Name>Alna Software, UAB</Name>
					</Branch>
					<Date>2019-09-26T11:48:20</Date>
				</Participant>
				<Participant>
					<BIC>SUPAGB21XXX</BIC>
					<Name>SumUp Payments Limited</Name>
					<Branch>
						<Code>30089</Code>
						<Name>SumUp Payments Limited</Name>
					</Branch>
					<Date>2020-02-18T19:22:47</Date>
				</Participant>
				<Participant>
					<BIC>INDULT2XXXX</BIC>
					<Name>Akciju sabiedriba "Citadele banka" Lietuvos filialas</Name>
					<Branch>
						<Code>72900</Code>
						<Name>Akciju sabiedriba "Citadele banka" Lietuvos filialas</Name>
					</Branch>
					<CD>
						<Code>0957</Code>
						<Name>AB "Citadele" bankas Iždo apskaitos skyrius</Name>
					</CD>
					<Date>2019-01-02T08:17:48</Date>
				</Participant>
				<Participant>
					<BIC>UADELT21XXX</BIC>
					<Name>UAB "deVere E-Money"</Name>
					<Branch>
						<Code>36600</Code>
						<Name>UAB "deVere E-Money"</Name>
					</Branch>
					<Date>2017-12-11T11:26:37</Date>
				</Participant>
				<Participant>
					<BIC>MAFVGB21XXX</BIC>
					<Name>Mayzus Financial Services Limited</Name>
					<Branch>
						<Code>39900</Code>
						<Name>Mayzus Financial Services Limited</Name>
					</Branch>
					<Date>2017-08-21T08:38:21</Date>
				</Participant>
				<Participant>
					<BIC>CIURGB21XXX</BIC>
					<Name>Cauri Ltd</Name>
					<Branch>
						<Code>39800</Code>
						<Name>Cauri Ltd</Name>
					</Branch>
					<Date>2017-06-21T09:53:36</Date>
				</Participant>
				<Participant>
					<BIC>TRYULT21XXX</BIC>
					<Name>Transactive Systems UAB</Name>
					<Branch>
						<Code>37800</Code>
						<Name>Transactive Systems UAB</Name>
					</Branch>
					<Date>2019-01-07T10:57:16</Date>
				</Participant>
				<Participant>
					<BIC>VIPULT22XXX</BIC>
					<Name>UAB Via Payments</Name>
					<Branch>
						<Code>35700</Code>
						<Name>UAB Via Payments</Name>
					</Branch>
					<Date>2019-04-10T17:28:36</Date>
				</Participant>
				<Participant>
					<BIC>PAYUGB21XXX</BIC>
					<Name>Paysolut Ltd</Name>
					<Branch>
						<Code>30003</Code>
						<Name>Paysolut Ltd</Name>
					</Branch>
					<Date>2018-11-20T10:35:27</Date>
				</Participant>
				<Participant>
					<BIC>PVZZLT21XXX</BIC>
					<Name>Test momentiniai mokėjimai</Name>
					<Branch>
						<Code>30099</Code>
						<Name>Test momentiniai mokėjimai</Name>
					</Branch>
					<Date>2017-11-08T10:49:41</Date>
				</Participant>
				<Participant>
					<BIC>SESOLT21XXX</BIC>
					<Name>UAB "Seven Seas Europe"</Name>
					<Branch>
						<Code>37100</Code>
						<Name>UAB "Seven Seas Europe"</Name>
					</Branch>
					<Date>2017-11-27T16:16:11</Date>
				</Participant>
				<Participant>
					<BIC>GLPELT21XXX</BIC>
					<Name>UAB Glocash Payment</Name>
					<Branch>
						<Code>37200</Code>
						<Name>UAB Glocash Payment</Name>
					</Branch>
					<Date>2018-05-23T09:45:19</Date>
				</Participant>
				<Participant>
					<BIC>MNNELT21XXX</BIC>
					<Name>UAB Maneuver LT</Name>
					<Branch>
						<Code>31100</Code>
						<Name>UAB Maneuver LT</Name>
					</Branch>
					<Date>2019-10-08T14:06:30</Date>
				</Participant>
				<Participant>
					<BIC>VPAYGRAAXXX</BIC>
					<Name>VIVA PAYMENT SERVICES S.A. Greek</Name>
					<Branch>
						<Code>30048</Code>
						<Name>VIVA PAYMENT SERVICES S.A. Greek</Name>
					</Branch>
					<Date>2018-09-18T13:54:10</Date>
				</Participant>
				<Participant>
					<BIC>UAPELT21BKO</BIC>
					<Name>UAB "Pervesk" nuosava</Name>
					<Branch>
						<Code>35501</Code>
						<Name>UAB "Pervesk" nuosava</Name>
					</Branch>
					<Date>2018-09-06T09:13:47</Date>
				</Participant>
				<Participant>
					<BIC>SEEOLT21XXX</BIC>
					<Name>UAB Seven Seas Europe N</Name>
					<Branch>
						<Code>37101</Code>
						<Name>UAB Seven Seas Europe N</Name>
					</Branch>
					<Date>2019-02-06T14:25:03</Date>
				</Participant>
				<Participant>
					<BIC>MRRMGB21XXX</BIC>
					<Name>MIR LIMITED UK LTD</Name>
					<Branch>
						<Code>30044</Code>
						<Name>MIR LIMITED UK LTD</Name>
					</Branch>
					<Date>2018-11-23T18:28:15</Date>
				</Participant>
				<Participant>
					<BIC>MSBBGB2LXXX</BIC>
					<Name>MSBB MONEY LTD</Name>
					<Branch>
						<Code>30067</Code>
						<Name>MSBB MONEY LTD</Name>
					</Branch>
					<Date>2019-02-12T08:54:29</Date>
				</Participant>
				<Participant>
					<BIC>REVOLT21XXX</BIC>
					<Name>Revolut Payments UAB</Name>
					<Branch>
						<Code>32500</Code>
						<Name>Revolut Payments UAB</Name>
					</Branch>
					<Date>2019-03-27T11:41:31</Date>
				</Participant>
				<Participant>
					<BIC>EPSULT22XXX</BIC>
					<Name>Earthport Payment Services UAB</Name>
					<Branch>
						<Code>32800</Code>
						<Name>Earthport Payment Services UAB</Name>
					</Branch>
					<Date>2019-08-02T09:16:49</Date>
				</Participant>
				<Participant>
					<BIC>WIUALT22XXX</BIC>
					<Name>Wittix, UAB</Name>
					<Branch>
						<Code>33000</Code>
						<Name>Wittix, UAB</Name>
					</Branch>
					<Date>2019-10-03T14:10:24</Date>
				</Participant>
				<Participant>
					<BIC>VAAALT21XXX</BIC>
					<Name>ValorPay UAB</Name>
					<Branch>
						<Code>33100</Code>
						<Name>ValorPay UAB</Name>
					</Branch>
					<Date>2020-03-26T14:13:03</Date>
				</Participant>
				<Participant>
					<BIC>UABULT21XXX</BIC>
					<Name>UAB "WB21"</Name>
					<Branch>
						<Code>39300</Code>
						<Name>UAB "WB21"</Name>
					</Branch>
					<Date>2017-01-16T13:02:29</Date>
				</Participant>
				<Participant>
					<BIC>FJORLT21XXX</BIC>
					<Name>AB Fjord Bank</Name>
					<Branch>
						<Code>22500</Code>
						<Name>AB Fjord Bank</Name>
					</Branch>
					<Date>2020-04-02T11:00:44</Date>
				</Participant>
				<Participant>
					<BIC>UAMMLT21XXX</BIC>
					<Name>UAB "Mobilieji mokėjimai“</Name>
					<Branch>
						<Code>36000</Code>
						<Name>UAB "Mobilieji mokėjimai“</Name>
					</Branch>
					<Date>2018-04-18T11:40:59</Date>
				</Participant>
				<Participant>
					<BIC>PAYDCY21XXX</BIC>
					<Name>Paydelta Limited</Name>
					<Branch>
						<Code>34001</Code>
						<Name>Paydelta Limited</Name>
					</Branch>
					<Date>2017-09-15T12:45:59</Date>
				</Participant>
				<Participant>
					<BIC>VAUALT21XXX</BIC>
					<Name>Valyuz, UAB</Name>
					<Branch>
						<Code>30700</Code>
						<Name>Valyuz, UAB</Name>
					</Branch>
					<Date>2018-03-28T16:36:52</Date>
				</Participant>
				<Participant>
					<BIC>BIYSGB2LXXX</BIC>
					<Name>Bilderlings Pay Limited</Name>
					<Branch>
						<Code>30035</Code>
						<Name>Bilderlings Pay Limited</Name>
					</Branch>
					<Date>2018-05-02T15:22:24</Date>
				</Participant>
				<Participant>
					<BIC>ZEGOLT21XXX</BIC>
					<Name>UAB ZEN.COM</Name>
					<Branch>
						<Code>31300</Code>
						<Name>UAB ZEN.COM</Name>
					</Branch>
					<Date>2019-06-07T11:42:12</Date>
				</Participant>
				<Participant>
					<BIC>UAPULT21XXX</BIC>
					<Name>UAB PAYTEND EUROPE</Name>
					<Branch>
						<Code>31200</Code>
						<Name>UAB PAYTEND EUROPE</Name>
					</Branch>
					<Date>2018-07-27T15:16:35</Date>
				</Participant>
				<Participant>
					<BIC>MOTOLT21XXX</BIC>
					<Name>UAB Foxpay</Name>
					<Branch>
						<Code>30500</Code>
						<Name>UAB Foxpay</Name>
					</Branch>
					<Date>2020-03-26T12:26:28</Date>
				</Participant>
				<Participant>
					<BIC>TRUDBG21XXX</BIC>
					<Name>Transact Europe EAD</Name>
					<Branch>
						<Code>30036</Code>
						<Name>Transact Europe EAD</Name>
					</Branch>
					<Date>2018-09-03T14:06:48</Date>
				</Participant>
				<Participant>
					<BIC>KUSRLT24XXX</BIC>
					<Name>AB "Mano bankas" - 2</Name>
					<Branch>
						<Code>50301</Code>
						<Name>AB "Mano bankas" - 2</Name>
					</Branch>
					<Date>2019-05-06T17:14:14</Date>
				</Participant>
				<Participant>
					<BIC>CLJUGB31XXX</BIC>
					<Name>Clear Junction LTD</Name>
					<Branch>
						<Code>39501</Code>
						<Name>Clear Junction LTD</Name>
					</Branch>
					<Date>2018-08-03T18:20:43</Date>
				</Participant>
				<Participant>
					<BIC>TPAYSKBXXXX</BIC>
					<Name>Trust Pay, a.s.</Name>
					<Branch>
						<Code>30056</Code>
						<Name>Trust Pay, a.s.</Name>
					</Branch>
					<Date>2019-04-11T17:54:57</Date>
				</Participant>
				<Participant>
					<BIC>TRLULT21XXX</BIC>
					<Name>TRANSFERGO LITHUANIA UAB</Name>
					<Branch>
						<Code>32400</Code>
						<Name>TRANSFERGO LITHUANIA UAB</Name>
					</Branch>
					<Date>2019-02-06T15:03:04</Date>
				</Participant>
				<Participant>
					<BIC>MSFVMTM1XXX</BIC>
					<Name>CEEVO Financial Services (Malta) Limited</Name>
					<Branch>
						<Code>30082</Code>
						<Name>CEEVO Financial Services (Malta) Limited</Name>
					</Branch>
					<Date>2019-11-13T10:50:16</Date>
				</Participant>
				<Participant>
					<BIC>REVOGB21XXX</BIC>
					<Name>Revolut LTD</Name>
					<Branch>
						<Code>39200</Code>
						<Name>Revolut LTD</Name>
					</Branch>
					<Date>2017-07-07T16:37:15</Date>
				</Participant>
				<Participant>
					<BIC>GRFNLT21XXX</BIC>
					<Name>UAB "General Financing"</Name>
					<Branch>
						<Code>60024</Code>
						<Name>UAB "General Financing"</Name>
					</Branch>
					<Date>2020-04-02T11:11:03</Date>
				</Participant>
				<Participant>
					<BIC>EAPFESM2XXX</BIC>
					<Name>EASY PAYMENT AND FINANCE, E.P., S.A</Name>
					<Branch>
						<Code>30093</Code>
						<Name>EASY PAYMENT AND FINANCE, E.P., S.A</Name>
					</Branch>
					<Date>2020-02-03T18:59:49</Date>
				</Participant>
				<Participant>
					<BIC>TPAYLT21XXX</BIC>
					<Name>TESTAS LT LT</Name>
					<Branch>
						<Code>70199</Code>
						<Name>TESTAS LT LT</Name>
					</Branch>
					<Date>2018-01-09T17:31:09</Date>
				</Participant>
				<Participant>
					<BIC>LCKULT22XXX</BIC>
					<Name>Lietuvos centrinė kredito unija</Name>
					<Branch>
						<Code>50100</Code>
						<Name>Lietuvos centrinė kredito unija</Name>
					</Branch>
					<Branch>
						<Code>30513</Code>
						<Name>testas</Name>
					</Branch>
					<Branch>
						<Code>50101</Code>
						<Name>Kredito unija "Sūduvos parama"</Name>
					</Branch>
					<Branch>
						<Code>50102</Code>
						<Name>Šilutės kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50103</Code>
						<Name>Radviliškio kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50104</Code>
						<Name>Vilkaviškio kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50105</Code>
						<Name>Plungės kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50106</Code>
						<Name>Kredito unija "Žemdirbio gerovė"</Name>
					</Branch>
					<Branch>
						<Code>50108</Code>
						<Name>Kauno arkivyskupijos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50110</Code>
						<Name>Kredito unija "Ūkininkų viltis"</Name>
					</Branch>
					<Branch>
						<Code>50111</Code>
						<Name>Centro kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50112</Code>
						<Name>Kredito unija "Tikroji viltis"</Name>
					</Branch>
					<Branch>
						<Code>50113</Code>
						<Name>Kredito unija "Germanto lobis"</Name>
					</Branch>
					<Branch>
						<Code>50115</Code>
						<Name>Širvintų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50116</Code>
						<Name>Kredito unija Skuodo bankelis</Name>
					</Branch>
					<Branch>
						<Code>50118</Code>
						<Name>Kaišiadorių kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50119</Code>
						<Name>Pakruojo ūkininkų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50120</Code>
						<Name>Kredito unija "Vievio taupa"</Name>
					</Branch>
					<Branch>
						<Code>50122</Code>
						<Name>Kelmės kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50123</Code>
						<Name>Kredito unija "Prienų taupa"</Name>
					</Branch>
					<Branch>
						<Code>50124</Code>
						<Name>Pasvalio kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50125</Code>
						<Name>Kredito unija "Moterų taupa"</Name>
					</Branch>
					<Branch>
						<Code>50127</Code>
						<Name>Kauno kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50128</Code>
						<Name>Tauragės kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50130</Code>
						<Name>Anykščių kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50131</Code>
						<Name>"Achemos" kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50132</Code>
						<Name>Kredito unija "Jonavos žemė"</Name>
					</Branch>
					<Branch>
						<Code>50133</Code>
						<Name>Jurbarko kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50134</Code>
						<Name>Ukmergės ūkininkų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50135</Code>
						<Name>Klausučių kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50136</Code>
						<Name>Palangos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50138</Code>
						<Name>Kretingos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50140</Code>
						<Name>Pagėgių kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50142</Code>
						<Name>Grinkiškio kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50143</Code>
						<Name>Sedos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50145</Code>
						<Name>Utenos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50146</Code>
						<Name>Mažeikių kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50148</Code>
						<Name>Naftininkų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50149</Code>
						<Name>Druskininkų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50151</Code>
						<Name>Trakų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50152</Code>
						<Name>Alytaus kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50153</Code>
						<Name>Kredito unija "Gargždų taupa"</Name>
					</Branch>
					<Branch>
						<Code>50154</Code>
						<Name>Kredito unija "Mėmelio taupomoji kasa"</Name>
					</Branch>
					<Branch>
						<Code>50156</Code>
						<Name>Grigiškių kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50159</Code>
						<Name>Kėdainių krašto kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50163</Code>
						<Name>Klaipėdos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50165</Code>
						<Name>Šeimos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50166</Code>
						<Name>Kredito unija "Neris"</Name>
					</Branch>
					<Branch>
						<Code>50168</Code>
						<Name>Kredito unija "Magnus"</Name>
					</Branch>
					<CD>
						<Code>0900</Code>
						<Name>Lietuvos centrinė kredito unija</Name>
					</CD>
					<Date>2018-12-05T18:41:49</Date>
				</Participant>
				<Participant>
					<BIC>MIEGLT21XXX</BIC>
					<Name>"Secure Nordic Payments", UAB</Name>
					<Branch>
						<Code>35100</Code>
						<Name>"Secure Nordic Payments", UAB</Name>
					</Branch>
					<Date>2020-02-18T17:50:42</Date>
				</Participant>
				<Participant>
					<BIC>JCKULT21XXX</BIC>
					<Name>Jungtinė centrinė kredito unija</Name>
					<Branch>
						<Code>50200</Code>
						<Name>Jungtinė centrinė kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50109</Code>
						<Name>Akademinė kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50114</Code>
						<Name>Kredito unija "Kupiškėnų taupa"</Name>
					</Branch>
					<Branch>
						<Code>50117</Code>
						<Name>Kredito unija Zanavykų bankelis</Name>
					</Branch>
					<Branch>
						<Code>50121</Code>
						<Name>Aukštaitijos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50126</Code>
						<Name>Biržų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50129</Code>
						<Name>Šilalės kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50139</Code>
						<Name>Raseinių kredito unija </Name>
					</Branch>
					<Branch>
						<Code>50144</Code>
						<Name>Ignalinos kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50147</Code>
						<Name>Joniškio kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50162</Code>
						<Name>Pareigūnų kredito unija</Name>
					</Branch>
					<Branch>
						<Code>50164</Code>
						<Name>Gyventojų ir smulkaus verslo kredito unija</Name>
					</Branch>
					<Date>2019-08-30T17:53:50</Date>
				</Participant>
				<Participant>
					<BIC>MBSULT21XXX</BIC>
					<Name>UAB Metasite Business Solutions</Name>
					<Branch>
						<Code>30001</Code>
						<Name>UAB Metasite Business Solutions</Name>
					</Branch>
					<Date>2019-08-16T15:02:25</Date>
				</Participant>
				<Participant>
					<BIC>UADRLT21XXX</BIC>
					<Name>UAB Drauskaita</Name>
					<Branch>
						<Code>31500</Code>
						<Name>UAB Drauskaita</Name>
					</Branch>
					<Date>2018-08-20T10:32:34</Date>
				</Participant>
				<Participant>
					<BIC>EPMTGB2LLTH</BIC>
					<Name>Epayments Systems Limited</Name>
					<Branch>
						<Code>30045</Code>
						<Name>Epayments Systems Limited</Name>
					</Branch>
					<Date>2019-01-21T10:50:36</Date>
				</Participant>
				<Participant>
					<BIC>EVIULV21XXX</BIC>
					<Name>Paysera LV</Name>
					<Date>2016-08-12T09:59:07</Date>
				</Participant>
				<Participant>
					<BIC>UIPULT21XXX</BIC>
					<Name>UAB International Payment Union</Name>
					<Branch>
						<Code>32300</Code>
						<Name>UAB International Payment Union</Name>
					</Branch>
					<Date>2019-05-03T14:06:21</Date>
				</Participant>
				<Participant>
					<BIC>TRWIBEB1XXX</BIC>
					<Name>TransferWise Europe SA NV</Name>
					<Branch>
						<Code>30073</Code>
						<Name>TransferWise Europe SA NV</Name>
					</Branch>
					<Date>2019-06-03T15:43:40</Date>
				</Participant>
				<Participant>
					<BIC>FINDMTMTXXX</BIC>
					<Name>Finductive Ltd</Name>
					<Branch>
						<Code>30080</Code>
						<Name>Finductive Ltd</Name>
					</Branch>
					<Date>2019-06-28T17:13:31</Date>
				</Participant>
				<Participant>
					<BIC>VPLALT21XXX</BIC>
					<Name>Verse Payments Lithuania, UAB</Name>
					<Branch>
						<Code>31600</Code>
						<Name>Verse Payments Lithuania, UAB</Name>
					</Branch>
					<Date>2019-10-03T14:00:10</Date>
				</Participant>
				<Participant>
					<BIC>UASILT22XXX</BIC>
					<Name>UAB Silvergate LT</Name>
					<Branch>
						<Code>33300</Code>
						<Name>UAB Silvergate LT</Name>
					</Branch>
					<Date>2019-12-18T09:42:49</Date>
				</Participant>
				<Participant>
					<BIC>UAAMLT21XXX</BIC>
					<Name>UAB "Argentum mobile"</Name>
					<Branch>
						<Code>35400</Code>
						<Name>UAB "Argentum mobile"</Name>
					</Branch>
					<Date>2019-02-06T14:26:21</Date>
				</Participant>
				<Participant>
					<BIC>TONIGB22XXX</BIC>
					<Name>Tonio Limited</Name>
					<Branch>
						<Code>30094</Code>
						<Name>Tonio Limited</Name>
					</Branch>
					<Date>2020-03-03T13:52:33</Date>
				</Participant>
				<Participant>
					<BIC>CBNULT2VXXX</BIC>
					<Name>CBI Money UAB</Name>
					<Branch>
						<Code>34100</Code>
						<Name>CBI Money UAB</Name>
					</Branch>
					<Date>2020-04-02T11:27:25</Date>
				</Participant>
				<Participant>
					<BIC>CPAYIE2DXXX</BIC>
					<Name>Fire Financial Services Limited EU</Name>
					<Branch>
						<Code>30096</Code>
						<Name>Fire Financial Services Limited EU</Name>
					</Branch>
					<Date>2020-05-19T10:52:19</Date>
				</Participant>
				<Participant>
					<BIC>CPAYIE31XXX</BIC>
					<Name>Fire Financial Services Limited UK</Name>
					<Branch>
						<Code>30097</Code>
						<Name>Fire Financial Services Limited UK</Name>
					</Branch>
					<Date>2020-06-03T13:09:01</Date>
				</Participant>
				<Participant>
					<BIC>FIHHLT21XXX</BIC>
					<Name>UAB FMĮ "Finhill"</Name>
					<Branch>
						<Code>90123</Code>
						<Name>UAB FMĮ "Finhill"</Name>
					</Branch>
					<CD>
						<Code>0935</Code>
						<Name>UAB 'VIVUM'</Name>
					</CD>
					<Date>2018-11-21T12:47:29</Date>
				</Participant>
				<Participant>
					<BIC>CRUGLT21XXX</BIC>
					<Name>Kredito unija "Saulėgrąža"</Name>
					<Branch>
						<Code>51700</Code>
						<Name>Kredito unija "Saulėgrąža"</Name>
					</Branch>
					<Date>2018-08-30T08:41:14</Date>
				</Participant>
				<Participant>
					<BIC>CLJUGB21XXX</BIC>
					<Name>Clear Junction Ltd</Name>
					<Branch>
						<Code>39500</Code>
						<Name>Clear Junction Ltd</Name>
					</Branch>
					<Date>2017-04-28T14:09:23</Date>
				</Participant>
				<Participant>
					<BIC>AKELEE21XXX</BIC>
					<Name>AS Pocopay</Name>
					<Branch>
						<Code>38100</Code>
						<Name>AS Pocopay</Name>
					</Branch>
					<Date>2017-10-18T12:45:43</Date>
				</Participant>
				<Participant>
					<BIC>INHULT21XXX</BIC>
					<Name>International Fintech, UAB</Name>
					<Branch>
						<Code>36400</Code>
						<Name>International Fintech, UAB</Name>
					</Branch>
					<Date>2017-11-03T10:07:27</Date>
				</Participant>
				<Participant>
					<BIC>UAWALT21XXX</BIC>
					<Name>Walletto UAB</Name>
					<Branch>
						<Code>30900</Code>
						<Name>Walletto UAB</Name>
					</Branch>
					<Date>2019-05-03T14:04:08</Date>
				</Participant>
				<Participant>
					<BIC>PAERCZP1XXX</BIC>
					<Name>Payment Execution s.r.o.</Name>
					<Branch>
						<Code>30041</Code>
						<Name>Payment Execution s.r.o.</Name>
					</Branch>
					<Date>2018-08-27T10:27:26</Date>
				</Participant>
				<Participant>
					<BIC>EMONMTM2XXX</BIC>
					<Name>Emoney PLC</Name>
					<Branch>
						<Code>30031</Code>
						<Name>Emoney PLC</Name>
					</Branch>
					<Date>2018-02-13T17:39:49</Date>
				</Participant>
				<Participant>
					<BIC>STUALT21XXX</BIC>
					<Name>SatchelPay, UAB</Name>
					<Branch>
						<Code>30600</Code>
						<Name>SatchelPay, UAB</Name>
					</Branch>
					<Date>2019-02-06T14:25:26</Date>
				</Participant>
				<Participant>
					<BIC>PCTULT21XXX</BIC>
					<Name>PCS Transfer, UAB</Name>
					<Branch>
						<Code>31400</Code>
						<Name>PCS Transfer, UAB</Name>
					</Branch>
					<Date>2018-08-16T16:59:36</Date>
				</Participant>
				<Participant>
					<BIC>TBFULT21XXX</BIC>
					<Name>TBF Finance, UAB</Name>
					<Branch>
						<Code>31800</Code>
						<Name>TBF Finance, UAB</Name>
					</Branch>
					<Date>2019-01-11T07:57:54</Date>
				</Participant>
				<Participant>
					<BIC>WAUALT21XXX</BIC>
					<Name>Wallter UAB</Name>
					<Branch>
						<Code>32000</Code>
						<Name>Wallter UAB</Name>
					</Branch>
					<Date>2019-04-05T14:45:42</Date>
				</Participant>
				<Participant>
					<BIC>UANFLT21XXX</BIC>
					<Name>UAB "NEO Finance"</Name>
					<Branch>
						<Code>35200</Code>
						<Name>UAB "NEO Finance"</Name>
					</Branch>
					<Date>2020-01-24T10:51:19</Date>
				</Participant>
				<Participant>
					<BIC>FFCSCZP1XXX</BIC>
					<Name>FAS finance company s.r.o.</Name>
					<Branch>
						<Code>30076</Code>
						<Name>FAS finance company s.r.o.</Name>
					</Branch>
					<Date>2019-05-17T10:08:52</Date>
				</Participant>
				<Participant>
					<BIC>URFSLT21XXX</BIC>
					<Name>Rebellion Fintech Services UAB</Name>
					<Branch>
						<Code>32600</Code>
						<Name>Rebellion Fintech Services UAB</Name>
					</Branch>
					<Date>2019-04-26T15:24:11</Date>
				</Participant>
				<Participant>
					<BIC>SONCLT21XXX</BIC>
					<Name>Sonect Europe UAB</Name>
					<Branch>
						<Code>33200</Code>
						<Name>Sonect Europe UAB</Name>
					</Branch>
					<Date>2019-10-24T11:32:06</Date>
				</Participant>
				<Participant>
					<BIC>CAHDGB21XXX</BIC>
					<Name>CASHDASH UK LIMITED</Name>
					<Branch>
						<Code>30087</Code>
						<Name>CASHDASH UK LIMITED</Name>
					</Branch>
					<Date>2019-09-20T13:47:11</Date>
				</Participant>
				<Participant>
					<BIC>KUFULT22XXX</BIC>
					<Name>Kuan Financial UAB</Name>
					<Branch>
						<Code>32900</Code>
						<Name>Kuan Financial UAB</Name>
					</Branch>
					<Date>2019-07-24T09:45:29</Date>
				</Participant>
				<Participant>
					<BIC>GUAVGB22XXX</BIC>
					<Name>Guavapay Limited</Name>
					<Branch>
						<Code>30092</Code>
						<Name>Guavapay Limited</Name>
					</Branch>
					<Date>2020-04-06T15:51:35</Date>
				</Participant>
			</Parts>
		</Doc>
	</Docs>
</Msg>`

const f = async() => {
  try {
    const messageParser = new XmlMessageParser(unescape(data));
    const message = await messageParser._xml2json(unescape(data));
    console.log(message.Msg.$.Type)
  }
  catch(err) {
    throw(err)
  }
}

f()

