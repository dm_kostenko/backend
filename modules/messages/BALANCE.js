const Message = require("./Message");
const type = "BALANCE";
const version = "2007-1-0";

class BALANCE extends Message {
  constructor({ parsedXml }){
    const { Sender, Receiver, Priority, MsgId, Date, System } = parsedXml.Msg.Header;
    super({ sender: Sender, receiver: Receiver, priority: Priority, msgId: MsgId, date: Date, system: System, type, version });    

    this.parseDocs(parsedXml.Msg.Docs.Doc);
  }

  parseDocs (xmlDocs) {
    if (Array.isArray(xmlDocs))
      xmlDocs.forEach(doc => this.parseDoc(doc));
    else
      this.parseDoc(xmlDocs);
  } 

  parseDoc(xmlDoc) {
    this.addDoc({
      docId: xmlDoc.Header.DocId,
      dateType: xmlDoc.Header.Dates.Type,
      date: xmlDoc.Header.Dates.Date,
      type: xmlDoc.Header.Type,
      priority: xmlDoc.Header.Priority,
      businessArea: xmlDoc.Header.BusinessArea,
      bic: xmlDoc.CBalPat ? xmlDoc.CBalPat.BIC : xmlDoc.CBalPri.BIC,
      iban: xmlDoc.CBalPat ? xmlDoc.CBalPat.Account : xmlDoc.CBalPri.Account,
      currency: xmlDoc.CBalPat ? xmlDoc.CBalPat.Currency : xmlDoc.CBalPri.Currency,
      openingBalance: xmlDoc.CBalPat ? xmlDoc.CBalPat.Opening.Balance : xmlDoc.CBalPri.Opening.Balance,
      openingAvailableBalance: xmlDoc.CBalPat ? xmlDoc.CBalPat.Opening.AvailableBalance : xmlDoc.CBalPri.Opening.AvailableBalance,
      closingBalance: xmlDoc.CBalPat ? xmlDoc.CBalPat.Closing.Balance : xmlDoc.CBalPri.Closing.Balance,
      closingAvailableBalance: xmlDoc.CBalPat ? xmlDoc.CBalPat.Closing.AvailableBalance : xmlDoc.CBalPri.Closing.AvailableBalance
    });
  }

  addDoc({
    docId,
    dateType,
    date,
    type,
    priority,
    businessArea,
    bic,
    iban,
    currency,
    openingBalance,
    openingAvailableBalance,
    closingBalance,
    closingAvailableBalance
  }) {
    super.addTransaction({
      docId,
      dateType,
      date,
      type,
      priority,
      businessArea,
      bic,
      iban,
      currency,
      openingBalance,
      openingAvailableBalance,
      closingBalance,
      closingAvailableBalance
    })
  }
}

module.exports = BALANCE;