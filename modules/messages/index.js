module.exports = {
  FFCCTRNS: require("./FFCCTRNS"),
  FFPCRQST: require("./FFPCRQST"),
  FFPSRPRT: require("./FFPSRPRT"),
  PRTRN: require("./PRTRN"),
  ROINVSTG: require("./ROINVSTG"),
  BALANCE: require("./BALANCE"),
  BTCDCNTF: require("./BTCDCNTF"),
  DEBIT: require("./DEBIT"),
  BTCSTMT: require("./BTCSTMT"),
  DICTIONR: require("./DICTIONR"),
  XmlMessageParser: require("./XmlMessageParser")
};