const db = require("../../helpers/db/bankApi");
const { iban, bic } = require("../../config/")
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";
const { getCETDate } = require("../../helpers/target2Timing");
const messageId = require("../messageIdGenerator/")
const { 
  EXTERNAL_TRANSACTION_APPROVED
} = require("../../helpers/constants/statuses").externalTransaction;
const { creditTransfer } = require("../../helpers/transactions");


const transferCommission = async() => {
  let connection;

  try {

    connection = await db.getConnection();
    await db._transaction.begin(connection);

    const [ balanceRecord ] = await db.currency_balance.get({ 
      input_account_number: iban.client_system, 
    });


    if(balanceRecord.balance > 0) {
      await db.currency_balance.upsert({ 
        input_account_number: iban.client_system, 
        input_currency_code: "EUR",
        input_balance: 0,
        input_author_guid: author_guid
      }, connection);

      const msgId = await messageId.generate();

      const [ newTransaction ] = await db.ffcctrns_transaction.createTransaction({
        input_login_guid: author_guid,
        input_sender_iban: iban.customer.internal,
        input_receiver_iban: iban.settlement.internal,
        input_currency: "EUR",
        input_value: balanceRecord.balance,
        input_commission: 0,
        input_cdttrftxinfpmtidinstrid: msgId,
        input_cdttrftxinfpmtidendtoendid: "NOTPROVIDED",
        input_cdttrftxinfpmtidtxid: msgId,
        input_cdttrftxinfpmtidpmttpinfsvclvlcd: "SEPA",
        // input_cdttrftxinfpmttpinflclinstrm: ,
        input_cdttrftxinfintrbksttlmamt: balanceRecord.balance,
        input_cdttrftxinfchrgbr: "SLEV",
        // input_cdttrftxinfinstgagtfininstnidbic: ,
        input_cdttrftxinfdbtrnm: "TBF Finance, UAB",
        input_cdttrftxinfdbtracctidiban: iban.customer.internal,   // or iban.client_system
        input_cdttrftxinfdbtragtfininstnidbic: bic,
        input_cdttrftxinfcdtragtfininstnidbic: bic,
        input_cdttrftxinfcdtrnm: "TBF Finance, UAB",
        input_cdttrftxinfcdtracctidiban: iban.settlement.internal,
        // input_cdttrftxinfnnnid: ,
        // input_cdttrftxinfpurpcd: ,
        // input_cdttrftxinfrmtinfustrd: ,
        // input_cdttrftxinfrmtinfstrd: ,
        // input_cdttrftxinfrmtinfstrdcdtrrefinftpcdorprtrycd: ,
        // input_cdttrftxinfrmtinfstrdcdtrrefinftpissr: ,
        input_cdttrftxinfrmtinfstrdcdtrrefinfref: "Commission transfer",
        input_status: EXTERNAL_TRANSACTION_APPROVED,
        input_author_guid: author_guid
      }, connection);

      await creditTransfer.prepareXml({ 
        guid: newTransaction.guid,
        status: EXTERNAL_TRANSACTION_APPROVED,
        system: "LITAS-MIG",
        author_guid: author_guid
      }, connection);

    }

    await db._transaction.commit(connection);

  }
  catch(err) {
    if(connection)
      await db._transaction.rollback(connection);
    throw(err)
  }
  finally {
    if(connection)
      connection.release()
  }
};
setInterval(() => {
  const date = getCETDate();
  const hours = date.getUTCHours();
  const minutes = date.getUTCMinutes();
  const seconds = date.getUTCSeconds();

  const isTime = hours === 18 && minutes === 4 && seconds === 0;
  
  if(isTime)
    transferCommission();
}, 1000);