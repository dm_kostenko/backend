const db = require("../../helpers/db/bankApi")
const firstTwo = "EZ";

module.exports.generate = async() => {
  const [ { msgid } ] = await db.messageId.generate({});
  return (firstTwo + JSON.stringify(parseInt(msgid)).padStart(14, "0"));
};