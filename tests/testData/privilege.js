const guid = "8d3956ff5dd643fa8d335d4ce19140c7";
const name = "read";
const type = "system";
const description = "read all documents";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  type: type,
  description: description,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    type: type,
    description: description
  },

  update:{
    guid: guid,
    name: name,
    type: type,
    description: description
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "dfecd4",
    name: "",
    type: "",
    description: ""
  }
};
