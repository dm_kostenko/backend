const merchant = require("./merchant");

const guid = "6e35455d708c4a62b689071dc51d9182";
const merchant_guid = merchant.record.guid;
const merchant_name = merchant.record.name;
const name = "Test shop";
const url = "http://test.shop.ru";
const email = "shop@test.ru";
const phone = "88006009900";
const enabled = 1;
const secret = "some_secret";
const note = "some note about our shop";
const hash_key = "Hardcode";
const enable_checkout = 1;
const checkout_method = "checkout method";
const antiFraudMonitor = 1;
const antiFraudMonitorValue = 123;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  merchant_guid: merchant_guid,
  merchant_name: merchant_name,
  name: name,
  url: url,
  email: email,
  phone: phone,
  enabled: enabled,
  //secret: secret,
  note: note,
  //hash_key: hash_key,
  enable_checkout: enable_checkout,
  checkout_method: checkout_method,
  antiFraudMonitor: antiFraudMonitor,
  antiFraudMonitorValue: antiFraudMonitorValue,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    merchant_guid: merchant_guid,
    name: name,
    url: url,
    email: email,
    phone: phone,
    enabled: enabled,
    secret: secret,
    note : note,
    enable_checkout: enable_checkout,
    checkout_method: checkout_method,
    antiFraudMonitor: antiFraudMonitor,
    antiFraudMonitorValue: antiFraudMonitorValue
  },

  update:{
    guid: guid,
    merchant_guid: merchant_guid,
    name: name,
    url: url,
    email: email,
    phone: phone,
    enabled: enabled,
    secret: secret,
    note : note,
    enable_checkout: enable_checkout,
    checkout_method: checkout_method,
    antiFraudMonitor: antiFraudMonitor,
    antiFraudMonitorValue: antiFraudMonitorValue
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    merchant_guid: "d3%#sdv45s4dv35dsv",
    name: true,
    url: "hehe:ru",
    email: "@.ne.verno",
    phone: "nomer telephona",
    enabled: 9,
    note : false,
    enable_checkout: undefined,
    checkout_method: "//",
    antiFraudMonitor: "no",
    antiFraudMonitorValue: NaN
  }
};
