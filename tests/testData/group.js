const partner = require("./partner");

const guid = "a4cce2fc67d04cd7b948740136295bcc";
const name = "Michael";
const type = "administartor";
const partner_guid = partner.record.guid;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  type: type,
  partner_guid: partner_guid,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    type: type,
    partner_guid: partner_guid
  },

  update:{
    guid: guid,
    name: name,
    type: type,
    partner_guid: partner_guid
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "dfecd4",
    name: "",
    type: "",
    partner_guid: "wreg46"
  }
};
