const shop = require("./shop");
const gateway = require("./gateway");

const shop_guid = shop.record.guid;
const gateway_guid = gateway.record.guid;
const billing_descriptor = "billing_descriptor";
const routing_string = "routing_string";
const payment_amount_limit = 123456789;
const monthly_amount_limit = 987654321;
const supported_brands = "we are support this brands: ...";
const supported_currencies = "RUB, EUR, USD";
const generate_statement = 1;
const enabled = 1;
const created_at = "2020-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  shop_guid: shop_guid,
  gateway_guid: gateway_guid,
  billing_descriptor: billing_descriptor,
  routing_string: routing_string,
  payment_amount_limit: payment_amount_limit,
  monthly_amount_limit: monthly_amount_limit,
  supported_brands: supported_brands,
  supported_currencies: supported_currencies,
  generate_statement: generate_statement,
  enabled: enabled,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by,
};

module.exports = {
  create:[
    {
      guid: gateway_guid,
      billing_descriptor: billing_descriptor,
      routing_string: routing_string,
      payment_amount_limit: payment_amount_limit,
      monthly_amount_limit: monthly_amount_limit,
      supported_brands: supported_brands,
      supported_currencies: supported_currencies,
      generate_statement: generate_statement,
      enabled: enabled,
    }
  ],

  delete:[
    {
      guid: gateway_guid,
      delete: true
    }
  ],

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:[
    {
      shop_guid: "newerNO",
      gateway_guid: "15 d8"
    }
  ]
};
