const shop = require("./shop");
const account = require("./account");

const shop_guid = shop.record.guid;
const account_guid = account.record.guid;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  shop_guid: shop_guid,
  account_guid: account_guid,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:[
    {
      guid: account_guid
    }
  ],

  delete:[
    {
      guid: account_guid,
      delete: true
    }
  ],

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:[
    {
      guid: "dfecd4"
    }
  ]
};
