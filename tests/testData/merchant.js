const group = require("./group");

const guid = "3aa2e26231e54497a8f8586555c27d59";
const name = "Imxo";
const type = "None";
const group_guid = group.record.guid;
const group_name = group.record.name;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  type: type,
  group_guid: group_guid,
  group_name: group_name,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    type: type,
    group_guid: group_guid
  },

  update:{
    guid: guid,
    name: name,
    type: type,
    group_guid: group_guid
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "dfecd4",
    name: "",
    type: "",
    group_guid: "wreg46"
  }
};
