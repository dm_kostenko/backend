const login = require("./login");
const role = require("./role");

const login_guid = login.record.guid;
const role_guid = role.record.guid;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  login_guid: login_guid,
  role_guid: role_guid,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    guid: role_guid
  },

  delete:{
    guid: role_guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "dfecd4"
  }
};
