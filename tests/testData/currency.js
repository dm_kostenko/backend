const guid = "c93e77e0aa014bf6b870e7f874e11216";
const name = "rubles";
const code = "RUB";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  code: code,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    code: code
  },

  update:{
    guid: guid,
    name: name,
    code: code
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "dfecd4",
    name: "",
    code: 1
  }
};
