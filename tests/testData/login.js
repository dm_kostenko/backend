const userprofile = require("./userprofile");

const guid = "14b66f4589b24ca0a1012d955851a846";
const user_profile_guid = userprofile.record.guid;
const username = "userName";
const username_canonical = "userNameCanonical";
const password = "qwerty";
const email = "test@data.ru";
const email_canonical = "test@data.co";
const enabled = 1;
const last_login = "9999-12-31 16:59:59.000";
const locked = 0;
const expired = true;
const expires_at = "9999-12-31T16:59:59.000Z";
const credentials_expired = false;
const credentials_expire_at = "9999-12-31T16:59:59.000Z";
const phone = "896485256";
const enabled_api = 1;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  user_profile_guid: user_profile_guid,
  user_profile_name : userprofile.record.name,
  user_profile_surname : userprofile.record.surname,
  user_profile_email : userprofile.record.email,
  user_profile_phone : userprofile.record.phone,
  user_profile_company_name : userprofile.record.company_name,
  user_profile_note : userprofile.record.note,
  user_profile_created_at: userprofile.record.created_at,
  user_profile_created_by: userprofile.record.created_by,
  user_profile_updated_at: userprofile.record.updated_at,
  user_profile_updated_by: userprofile.record.updated_by,
  username: username,
  username_canonical: username_canonical,
  email: email,
  email_canonical: email_canonical,
  enabled: enabled,
  last_login: last_login,
  locked: locked,
  expired: expired,
  expires_at: expires_at,
  credentials_expired: credentials_expired,
  credentials_expire_at: credentials_expire_at,
  phone: phone,
  enabled_api: enabled_api,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    username: username,
    email: email,
    password: password,
    enabled : enabled,
    phone : phone,
    user_profile : {
      name : userprofile.record.name,
      surname : userprofile.record.surname,
      email : userprofile.record.email,
      phone : userprofile.record.phone,
      company_name : userprofile.record.company_name,
      note : userprofile.record.note,
    },
  },

  update:{
    guid: guid,
    username: username,
    email: email,
    password: password,
    enabled : enabled,
    phone : phone,
    user_profile : {
      name : userprofile.record.name,
      surname : userprofile.record.surname,
      email : userprofile.record.email,
      phone : userprofile.record.phone,
      company_name : userprofile.record.company_name,
      note : userprofile.record.note,
    },
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "segvk 5",
    role_guid: "dv $",
    username: 4,
    email: "d)(",
    password: "",
    enabled : undefined,
    phone : "phone",
    user_profile : {
      name : "000@#4",
      surname : true,
      email : "email",
      phone : "phone",
      company_name : 0,
      note : "~",
    },
  }
};
