const login = require("./login");
const merchant = require("./merchant");
const role = require("./role");

const login_guid = login.record.guid;
const merchant_guid = merchant.record.guid;
const role_guid = role.record.guid;
const created_at = "2020-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  login_guid: login_guid,
  merchant_guid: merchant_guid,
  role_guid: role_guid,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    login_guid: login_guid,
    role_guid: role_guid
  },

  delete:{
    login_guid: login_guid,
    role_guid: role_guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    login_guid: "newerNO",
    role_guid: "15 d8"
  }
};
