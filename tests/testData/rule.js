const blacklistRule = require("./blacklistRule");

const blacklist_rule_guid = "9a888b9f677046f88a6305510316f398"; //TODOs
const value = "2.5.6.7";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  blacklist_rule_guid: blacklist_rule_guid,
  value: value,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    blacklist_rule_guid: blacklist_rule_guid,
    value: value,
  },
  
  delete:{
    blacklist_rule_guid: blacklist_rule_guid,
    value: value,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:{
    blacklist_rule_guid: "dfecd4",
    value: 5
  }
};
