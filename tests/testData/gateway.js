const guid = "a26d5866939a47ffb99e9bf18229c7a5";
const name = "MasterVisa";
const description = "gate for MasterVisa";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  description: description,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    description: description
  },

  update:{
    guid: guid,
    name: name,
    description: description
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:{
    guid: "dfecd4",
    name: 5,
    description: false
  }
};
