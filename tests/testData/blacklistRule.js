const rule = require("./rule");

const guid = "9a888b9f677046f88a6305510316f398";
const name = "China";
const type = "ip";
const description = "block Chineese ip :)";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  type: type,
  description: description,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    type: type,
    description: description,
    values: [ rule.record.value ]
  },

  update:{
    guid: guid,
    type: type,
    name: name,
    description: description
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:{
    guid: "dfecd4",
    name: 5,
    type: undefined,
    description: true,
    values: { undefined }
  }
};
