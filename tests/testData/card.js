const account = require("./account");
const currency = require("./currency");

const guid = "cd351552459d4526a32055284c66f60b";
const type = "credit";
const account_guid = account.record.guid;
const account_number = account.record.number;
const card_number = "1111222233334444";
const currency_guid = currency.record.guid;
const currency_name = currency.record.name;
const name_on_card = "Ivanov Ivan Batkovich";
const expired = 1;
const expires_at = "2025-03-01T03:04:59.000Z";
const balance = 123456;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  type: type,
  account_guid: account_guid,
  account_number: account_number,
  card_number: card_number,
  currency_guid: currency_guid,
  currency_name: currency_name,
  name_on_card: name_on_card,
  expired: expired,
  expires_at: expires_at,
  balance: balance,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    account_guid: account_guid,
    type: type,
    card_number: card_number,
    currency_guid: currency_guid,
    name_on_card: name_on_card,
    expired: expired,
    expires_at: expires_at,
    balance: balance
  },

  update:{
    guid: guid,
    account_guid: account_guid,
    type: type,
    card_number: card_number,
    currency_guid: currency_guid,
    name_on_card: name_on_card,
    expired: expired,
    expires_at: expires_at,
    balance: balance
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "guid",
    account_guid: true,
    type: 12,
    card_number: "-",
    currency_guid: undefined,
    name_on_card: "ups...",
    expired: "1",
    expires_at: "2012-20-9",
    balance: "55"
  }
};
