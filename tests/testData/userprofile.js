const guid = "5eeb7b1e71c1403780153be59b11309f";
const name = "Anton";
const surname = "Chekhov";
const email = "admin@mail.com";
const phone = "7008005454";
const company_name = "Corporation";
const note = "some notes about live ...";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  surname: surname,
  email: email,
  phone: phone,
  company_name: company_name,
  note: note,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    surname: surname,
    email: email,
    phone: phone,
    company_name: company_name,
    note: note
  },

  update:{
    guid: guid,
    name: name,
    surname: surname,
    email: email,
    phone: phone,
    company_name: company_name,
    note: note
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    name: "",
    surname: "",
    email: "adm@.",
    phone: "70aas",
    company_name: "",
    note: ""
  }
};
