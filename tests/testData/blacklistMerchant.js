const merchant = require("./merchant");
const blacklistRule = require("./blacklistRule");

const merchant_guid = merchant.guid;
const blacklist_rule_guid = blacklistRule.guid;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  merchant_guid: merchant_guid,
  blacklist_rule_guid: blacklistRule,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    blacklist_rule_guid: blacklist_rule_guid,
  },

  delete:{
    blacklist_rule_guid: blacklist_rule_guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:{
    merchant_guid: "dfecd4",
    blacklist_rule_guid: 5
  }
};
