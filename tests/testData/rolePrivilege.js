const role = require("./role");
const privilege = require("./privilege");

const role_guid = role.record.guid;
const privilege_guid = privilege.record.guid;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  role_guid: role_guid,
  privilege_guid: privilege_guid,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:[
    {
      guid: privilege_guid
    }
  ],

  delete:[
    {
      guid: privilege_guid,
      delete: true
    }
  ],

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:[
    {
      guid: "dfecd4"
    }
  ]
};
