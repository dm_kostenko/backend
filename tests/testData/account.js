const currency = require("./currency");

const guid = "e624afbf51944ae8974b30f184d20270";
const currency_guid = currency.record.guid;
const currency_name = currency.record.name;
const number = "1111222233334444";
const balance = 123456;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  currency_guid: currency_guid,
  currency_name: currency_name,
  number: number,
  balance: balance,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    currency_guid: currency_guid,
    number: number,
    balance: balance,
  },

  update:{
    guid: guid,
    currency_guid: currency_guid,
    number: number,
    balance: balance,
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:{
    guid: guid,
    currency_guid: "cr_g",
    number: "486",
    balance: false,
  }
};
