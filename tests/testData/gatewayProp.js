const gateway = require("./gateway");

const gateway_guid = gateway.record.guid;
const name = "some_prop_name";
const label = "all-powerful";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  gateway_guid: gateway_guid,
  name: name,
  label: label,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    label: label
  },

  update:{
    name: name,
    label: label
  },

  delete:{
    name: name,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:{
    name: undefined,
    label: false
  }
};
