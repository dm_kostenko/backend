const guid = "a496e6e9e6d54c6199711d87552a2cb3";
const name = "Michael";
const type = "administartor";
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  name: name,
  type: type,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    name: name,
    type: type
  },

  update:{
    guid: guid,
    name: name,
    type: type
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]],

  invalid:{
    guid: "dfecd4",
    name: "",
    type: ""
  }
};
