const currency = require("./currency");
const gateway = require("./gateway");

const guid = "620947d27b3c47c3a51dd0e28420c9f0";
const currency_guid = currency.record.guid;
const currency_name = currency.record.name;
const gateway_guid = gateway.record.guid;
const gateway_name = gateway.record.name;
const amount = 20.52;
const hold_rate = 0.5;
const hold_period = 123;
const apply_from = "2017-03-01T03:04:59.000Z";
const apply_to = "2050-03-01T03:04:59.000Z";
const payment_success = 0.1;
const payment_failed = 0.2;
const authorization_success = 0.3;
const authorization_failed = 0.4;
const capture_success = 0.5;
const capture_failed = 0.6;
const refund = "refund";
const chargeback = "charge back";
const rebill = 1.2;
const rate = 1.3;
const created_at = "2019-03-01T03:04:59.000Z";
const created_by = "admin";
const updated_at = "null";
const updated_by  ="null";

const obj =
{
  guid: guid,
  currency_guid: currency_guid,
  currency_name: currency_name,
  gateway_guid: gateway_guid,
  gateway_name: gateway_name,
  amount: amount,
  hold_rate: hold_rate,
  hold_period: hold_period,
  apply_from: apply_from,
  apply_to: apply_to,
  payment_success: payment_success,
  payment_failed: payment_failed,
  authorization_success: authorization_success,
  authorization_failed: authorization_failed,
  capture_success: capture_success,
  capture_failed: capture_failed,
  refund: refund,
  chargeback: chargeback,
  rebill: rebill,
  rate: rate,
  created_at: created_at,
  created_by: created_by,
  updated_at: updated_at,
  updated_by: updated_by
};

module.exports = {
  create:{
    currency_guid: currency_guid,
    gateway_guid: gateway_guid,
    amount: amount,
    hold_rate: hold_rate,
    hold_period: hold_period,
    apply_from: apply_from,
    apply_to: apply_to,
    payment_success: payment_success,
    payment_failed: payment_failed,
    authorization_success: authorization_success,
    authorization_failed: authorization_failed,
    capture_success: capture_success,
    capture_failed: capture_failed,
    refund: refund,
    chargeback: chargeback,
    rebill: rebill,
    rate: rate,
  },

  update:{
    guid: guid,
    currency_guid: currency_guid,
    gateway_guid: gateway_guid,
    amount: amount,
    hold_rate: hold_rate,
    hold_period: hold_period,
    apply_from: apply_from,
    apply_to: apply_to,
    payment_success: payment_success,
    payment_failed: payment_failed,
    authorization_success: authorization_success,
    authorization_failed: authorization_failed,
    capture_success: capture_success,
    capture_failed: capture_failed,
    refund: refund,
    chargeback: chargeback,
    rebill: rebill,
    rate: rate,
  },

  delete:{
    guid: guid,
    delete: true
  },

  record: obj,

  records:[[ obj, obj ],[ { count: 2 } ]], 

  invalid:{
    guid: "afihfijpd",
    currency_guid: "  ",
    gateway_guid: "44",
    amount: 45.015,
    hold_rate: "dfg",
    hold_period: true,
    apply_from: undefined,
    apply_to: false,
    payment_success: NaN,
    payment_failed: "ds",
    authorization_success: 1,
    authorization_failed: "*",
    capture_success: 54.2,
    capture_failed: 45.501,
    refund: true,
    chargeback: "1",
    rebill: "1.05",
    rate: "",
  }
};
