/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { rolePrivilege, privilege, role } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /roles/{guid}/privileges", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.privilege, "byRoleInfo").withArgs({ input_role_guid: rolePrivilege.record.role_guid,
      input_page_number: undefined,
      input_items_count: undefined })
      .resolves(clone(privilege.records));

    chai.request(app)
      .get(`/api/v1/management/roles/${rolePrivilege.record.role_guid}/privileges`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(rolePrivilege.records[0].length);
          res.body.data[0].should.to.eql(privilege.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.rolePrivilege, "upsert").withArgs({
      input_role_guid: role.record.guid,
      input_privilege_guid: rolePrivilege.create[0].guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(rolePrivilege.record) ]]);

    chai.request(app)
      .post(`/api/v1/management/roles/${role.record.guid}/privileges`)
      .set("content-type", "application/json")
      .send(rolePrivilege.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("array");
          res.body.should.to.eql([ rolePrivilege.record ]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.rolePrivilege, "delete")
      .resolves([[ {
        role_guid: role.record.guid,
        privilege_guid: privilege.record.guid
      } ]]);

    const resp = clone( DELETED );
    resp.role_guid = rolePrivilege.record.role_guid;
    resp.privilege_guid = rolePrivilege.record.privilege_guid;

    chai.request(app)
      .post(`/api/v1/management/roles/${rolePrivilege.record.role_guid}/privileges`)
      .set("content-type", "application/json")
      .send(rolePrivilege.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("array");
          res.body.should.to.eql([ resp ]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/roles/${rolePrivilege.record.role_guid}/privileges`)
      .set("content-type", "application/json")
      .send(rolePrivilege.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
