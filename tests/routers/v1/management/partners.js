/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { partner, group } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /partners", () => {

  afterEach(() => {
    sinon.restore();
  });


  it("GET", (done) => {
    sinon.stub(db.partner, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(partner.records));

    chai.request(app)
      .get("/api/v1/management/partners")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(partner.records[0].length);
          res.body.data[0].should.to.eql(partner.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.partner, "info").withArgs({ input_guid: partner.record.guid })
      .resolves([[ clone(partner.record) ]]);

    sinon.stub(db.group, "info").withArgs({ input_partner_guid: partner.record.guid })
      .resolves([[ (group.record) ]]);

    const resp = clone(partner.record);

    chai.request(app)
      .get(`/api/v1/management/partners/${partner.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.partner, "upsert").withArgs({
      input_guid: partner.create.guid,
      input_name: partner.create.name,
      input_type: partner.create.type,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(partner.record) ]]);

    chai.request(app)
      .post("/api/v1/management/partners")
      .set("content-type", "application/json")
      .send(partner.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(partner.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.partner, "upsert").withArgs({
      input_guid: partner.update.guid,
      input_name: partner.update.name,
      input_type: partner.update.type,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(partner.record) ]]);

    chai.request(app)
      .post("/api/v1/management/partners")
      .set("content-type", "application/json")
      .send(partner.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(partner.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.partner, "delete").withArgs({ input_guid: partner.record.guid })
      .resolves([[ { guid: partner.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/partners")
      .set("content-type", "application/json")
      .send(partner.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/partners")
      .set("content-type", "application/json")
      .send(partner.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
