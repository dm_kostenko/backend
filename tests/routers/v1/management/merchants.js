/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { group, merchant } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /merchants", () => {

  beforeEach(() => {
    sinon.restore();
  });


  it("GET", (done) => {
    sinon.stub(db.merchant, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_group_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(merchant.records));

    const resp = clone(merchant.records[0][0]);

    chai.request(app)
      .get("/api/v1/management/merchants")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(merchant.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.merchant, "info").withArgs({ input_guid: merchant.record.guid })
      .resolves([[ clone(merchant.record) ]]);

    const resp = clone(merchant.record);

    chai.request(app)
      .get(`/api/v1/management/merchants/${merchant.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.merchant, "upsert").withArgs({
      input_guid: merchant.create.guid,
      input_name: merchant.create.name,
      input_type: merchant.create.type,
      input_group_guid: merchant.create.group_guid,
      input_monthly_amount_limit: null,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(merchant.record) ]]);

    sinon.stub(db.group, "get").withArgs({ input_guid: merchant.record.group_guid })
      .resolves([[ clone(group.record) ]]);

    const resp = clone(merchant.record);
    resp.group_name = group.record.name;

    chai.request(app)
      .post("/api/v1/management/merchants")
      .set("content-type", "application/json")
      .send(merchant.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.merchant, "upsert").withArgs({
      input_guid: merchant.update.guid,
      input_name: merchant.update.name,
      input_type: merchant.update.type,
      input_monthly_amount_limit: null,
      input_group_guid: merchant.update.group_guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(merchant.record) ]]);

    sinon.stub(db.group, "get").withArgs({ input_guid: merchant.record.group_guid })
      .resolves([[ clone(group.record) ]]);

    const resp = clone(merchant.record);
    resp.group_name = group.record.name;

    chai.request(app)
      .post("/api/v1/management/merchants")
      .set("content-type", "application/json")
      .send(merchant.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.merchant, "delete").withArgs({ input_guid: merchant.record.guid })
      .resolves([[ { guid: merchant.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/merchants")
      .set("content-type", "application/json")
      .send(merchant.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/merchants")
      .set("content-type", "application/json")
      .send(merchant.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
