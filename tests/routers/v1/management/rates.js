/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { rate, gateway, currency } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /rates", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.rate, "info").withArgs({
      input_page_number: undefined,
      input_items_count: undefined,
      input_guid: undefined,
      input_amount: undefined,
      input_apply_to: undefined,
      input_apply_from: undefined,
      input_currency_name: undefined,
      input_hold_rate: undefined,
      input_hold_period: undefined,
      input_rate: undefined,
    })
      .resolves(clone(rate.records));

    const resp = clone(rate.record);

    chai.request(app)
      .get("/api/v1/management/rates")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(rate.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET /rates?currency_guid={guid}&gateway_guid={guid}", (done) => {
    sinon.stub(db.rate, "info").withArgs({
      input_page_number: undefined,
      input_items_count: undefined,
      input_guid: undefined,
      input_amount: undefined,
      input_apply_to: undefined,
      input_apply_from: undefined,
      input_currency_name: undefined,
      input_hold_rate: undefined,
      input_hold_period: undefined,
      input_rate: undefined,
    })
      .resolves(clone(rate.records));
    const resp = clone(rate.record);

    chai.request(app)
      .get(`/api/v1/management/rates?currency_guid=${currency.record.guid}&gateway_guid=${gateway.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(rate.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.rate, "info").withArgs({ input_guid: rate.record.guid })
      .resolves([[ clone(rate.record) ]]);
    const resp = clone(rate.record);

    chai.request(app)
      .get(`/api/v1/management/rates/${rate.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.rate, "upsert").withArgs({
      input_guid: rate.create.guid,
      input_currency_guid: rate.create.currency_guid,
      input_gateway_guid: rate.create.gateway_guid,
      input_amount: rate.create.amount,
      input_hold_rate: rate.create.hold_rate,
      input_hold_period: rate.create.hold_period,
      input_apply_from: rate.create.apply_from,
      input_apply_to: rate.create.apply_to,
      input_payment_success: rate.create.payment_success,
      input_payment_failed: rate.create.payment_failed,
      input_authorization_success: rate.create.authorization_success,
      input_authorization_failed: rate.create.authorization_failed,
      input_capture_success: rate.create.capture_success,
      input_capture_failed: rate.create.capture_failed,
      input_refund: rate.create.refund,
      input_chargeback: rate.create.chargeback,
      input_rebill: rate.create.rebill,
      input_rate: rate.create.rate,
      input_active: rate.create.active,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(rate.record) ]]);

    sinon.stub(db.gateway, "get").withArgs({ input_guid: rate.record.gateway_guid })
      .resolves([[ clone(gateway.record) ]]);

    sinon.stub(db.currency, "get").withArgs({ input_guid: rate.record.currency_guid })
      .resolves([[ clone(currency.record) ]]);

    const resp = clone(rate.record);
    resp.gateway_name = gateway.record.name;
    resp.currency_name = currency.record.name;

    chai.request(app)
      .post("/api/v1/management/rates")
      .set("content-type", "application/json")
      .send(rate.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.rate, "delete").withArgs({ input_guid: rate.record.guid })
      .resolves([[ { guid: rate.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/rates")
      .set("content-type", "application/json")
      .send(rate.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/rates")
      .set("content-type", "application/json")
      .send(rate.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
