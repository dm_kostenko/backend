/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { privilege } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /privileges", () => {

  afterEach(() => {
    sinon.restore();
  });


  it("GET", (done) => {
    sinon.stub(db.privilege, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_type: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(privilege.records));

    chai.request(app)
      .get("/api/v1/management/privileges")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(privilege.records[0].length);
          res.body.data[0].should.to.eql(privilege.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.privilege, "info").withArgs({ input_guid: privilege.record.guid })
      .resolves([[ clone(privilege.record) ]]);

    chai.request(app)
      .get(`/api/v1/management/privileges/${privilege.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(privilege.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.privilege, "upsert").withArgs({
      input_guid: privilege.create.guid,
      input_name: privilege.create.name,
      input_type: privilege.create.type,
      input_description: privilege.create.description,
      input_author_guid: "emptyname",
    })
      .resolves([[ clone(privilege.record) ]]);

    chai.request(app)
      .post("/api/v1/management/privileges")
      .set("content-type", "application/json")
      .send(privilege.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(privilege.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.privilege, "upsert").withArgs({
      input_guid: privilege.update.guid,
      input_name: privilege.update.name,
      input_type: privilege.update.type,
      input_description: privilege.update.description,
      input_author_guid: "emptyname",
    })
      .resolves([[ clone(privilege.record) ]]);

    chai.request(app)
      .post("/api/v1/management/privileges")
      .set("content-type", "application/json")
      .send(privilege.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(privilege.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.privilege, "delete").withArgs({ input_guid: privilege.record.guid })
      .resolves([[ { guid: privilege.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/privileges")
      .set("content-type", "application/json")
      .send(privilege.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/privileges")
      .set("content-type", "application/json")
      .send(privilege.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
