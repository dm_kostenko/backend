/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { partner, group } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /groups", () => {

  afterEach(() => {
    sinon.restore();
  });


  it("GET", (done) => {
    sinon.stub(db.group, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_partner_guid: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(group.records));

    const resp = clone(group.records[0][0]);

    chai.request(app)
      .get("/api/v1/management/groups")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(group.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.group, "info").withArgs({ input_guid: group.record.guid })
      .resolves([[ clone(group.record) ]]);

    const resp = clone(group.record);

    chai.request(app)
      .get(`/api/v1/management/groups/${group.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.group, "upsert").withArgs({
      input_guid: group.create.guid,
      input_name: group.create.name,
      input_type: group.create.type,
      input_partner_guid: group.create.partner_guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(group.record) ]]);

    sinon.stub(db.partner, "get").withArgs({ input_guid: group.record.partner_guid })
      .resolves([[ clone(partner.record) ]]);

    const resp = clone(group.record);
    resp.partner_name = partner.record.name;

    chai.request(app)
      .post("/api/v1/management/groups")
      .set("content-type", "application/json")
      .send(group.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.group, "upsert").withArgs({
      input_guid: group.update.guid,
      input_name: group.update.name,
      input_type: group.update.type,
      input_partner_guid: group.update.partner_guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(group.record) ]]);

    sinon.stub(db.partner, "get").withArgs({ input_guid: group.record.partner_guid })
      .resolves([[ clone(partner.record) ]]);

    const resp = clone(group.record);
    resp.partner_name = partner.record.name;

    chai.request(app)
      .post("/api/v1/management/groups")
      .set("content-type", "application/json")
      .send(group.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.group, "delete").withArgs({ input_guid: group.record.guid })
      .resolves([[ { guid: group.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/groups")
      .set("content-type", "application/json")
      .send(group.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/groups")
      .set("content-type", "application/json")
      .send(group.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
