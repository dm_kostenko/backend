/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { userprofile, login } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /logins", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.login, "info").withArgs({
      input_guid: undefined,
      input_username: undefined,
      input_email: undefined,
      input_enabled: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(login.records));

    const resp = {
      guid: login.record.guid,
      username : login.record.username,
      username_canonical : login.record.username_canonical,
      email : login.record.email,
      email_canonical : login.record.email_canonical,
      enabled : login.record.enabled,
      last_login : login.record.last_login,
      locked : login.record.locked,
      expired : login.record.expired,
      expires_at : login.record.expires_at,
      credentials_expired : login.record.credentials_expired,
      credentials_expire_at : login.record.credentials_expire_at,
      phone : login.record.phone,
      enabled_api : login.record.enabled_api,
      created_at: login.record.created_at,
      created_by: login.record.created_by,
      updated_at: login.record.updated_at,
      updated_by: login.record.updated_by,
      user_profile: userprofile.record,
    };

    chai.request(app)
      .get("/api/v1/management/logins")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(login.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.login, "info").withArgs({ input_guid: login.record.guid })
      .resolves([[ clone(login.record) ]]);

    const resp = {
      guid: login.record.guid,
      username : login.record.username,
      username_canonical : login.record.username_canonical,
      email : login.record.email,
      email_canonical : login.record.email_canonical,
      enabled : login.record.enabled,
      last_login : login.record.last_login,
      locked : login.record.locked,
      expired : login.record.expired,
      expires_at : login.record.expires_at,
      credentials_expired : login.record.credentials_expired,
      credentials_expire_at : login.record.credentials_expire_at,
      phone : login.record.phone,
      enabled_api : login.record.enabled_api,
      created_at: login.record.created_at,
      created_by: login.record.created_by,
      updated_at: login.record.updated_at,
      updated_by: login.record.updated_by,
      user_profile: userprofile.record,
    };

    chai.request(app)
      .get(`/api/v1/management/logins/${login.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.login, "get").withArgs({ input_guid: login.record.guid })
      .resolves([[ clone(login.record) ]]);

    sinon.stub(db.userProfile, "upsert").withArgs({
      input_guid: login.create.user_profile.guid,
      input_name: login.create.user_profile.name,
      input_surname: login.create.user_profile.surname,
      input_email: login.create.user_profile.email,
      input_phone: login.create.user_profile.phone,
      input_company_name: login.create.user_profile.company_name,
      input_note: login.create.user_profile.note,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(userprofile.record) ]]);

    sinon.stub(db.login, "upsert")
      .resolves([[ clone(login.record) ]]);

    sinon.stub(db.userProfile, "get").withArgs({ input_guid: login.record.user_profile_guid })
      .resolves([[ clone(userprofile.record) ]]);

    const resp = {
      guid: login.record.guid,
      username : login.record.username,
      username_canonical : login.record.username_canonical,
      email : login.record.email,
      email_canonical : login.record.email_canonical,
      enabled : login.record.enabled,
      last_login : login.record.last_login,
      locked : login.record.locked,
      expired : login.record.expired,
      expires_at : login.record.expires_at,
      credentials_expired : login.record.credentials_expired,
      credentials_expire_at : login.record.credentials_expire_at,
      phone : login.record.phone,
      enabled_api : login.record.enabled_api,
      created_at: login.record.created_at,
      created_by: login.record.created_by,
      updated_at: login.record.updated_at,
      updated_by: login.record.updated_by,
      user_profile: userprofile.record,
    };

    chai.request(app)
      .post("/api/v1/management/logins")
      .set("content-type", "application/json")
      .send(login.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.login, "get").withArgs({ input_guid: login.record.guid })
      .resolves([[ clone(login.record) ]]);

    sinon.stub(db.userProfile, "upsert")
      .resolves([[ clone(userprofile.record) ]]);

    sinon.stub(db.login, "upsert")
      .resolves([[ clone(login.record) ]]);

    sinon.stub(db.userProfile, "get").withArgs({ input_guid: login.record.user_profile_guid })
      .resolves([[ clone(userprofile.record) ]]);

    const resp = {
      guid: login.record.guid,
      username : login.record.username,
      username_canonical : login.record.username_canonical,
      email : login.record.email,
      email_canonical : login.record.email_canonical,
      enabled : login.record.enabled,
      last_login : login.record.last_login,
      locked : login.record.locked,
      expired : login.record.expired,
      expires_at : login.record.expires_at,
      credentials_expired : login.record.credentials_expired,
      credentials_expire_at : login.record.credentials_expire_at,
      phone : login.record.phone,
      enabled_api : login.record.enabled_api,
      created_at: login.record.created_at,
      created_by: login.record.created_by,
      updated_at: login.record.updated_at,
      updated_by: login.record.updated_by,
      user_profile: userprofile.record,
    };

    chai.request(app)
      .post("/api/v1/management/logins")
      .set("content-type", "application/json")
      .send(login.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.login, "delete").withArgs({ input_guid: login.record.guid })
      .resolves([[ { guid: login.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/logins")
      .set("content-type", "application/json")
      .send(login.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    sinon.stub(db.login, "upsert")
      .resolves([[ clone(login.record) ]]);

    chai.request(app)
      .post("/api/v1/management/logins")
      .set("content-type", "application/json")
      .send(login.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
