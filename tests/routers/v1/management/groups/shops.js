/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { group, merchant, shop } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /groups/{guid}/shops", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.shop, "byGroupInfo").withArgs({
      input_group_guid: group.record.guid,
      input_guid: undefined,
      input_name: undefined,
      input_merchant_name: undefined,
      input_enabled: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(shop.records));

    const resp = {
      guid: shop.record.guid,
      merchant_guid: shop.record.merchant_guid,
      merchant_name: merchant.record.name,
      name: shop.record.name,
      url: shop.record.url,
      email: shop.record.email,
      phone: shop.record.phone,
      enabled: shop.record.enabled,
      note: shop.record.note,
      enable_checkout: shop.record.enable_checkout,
      checkout_method: shop.record.checkout_method,
      antiFraudMonitor: shop.record.antiFraudMonitor,
      antiFraudMonitorValue: shop.record.antiFraudMonitorValue,
      created_at: shop.record.created_at,
      created_by: shop.record.created_by,
      updated_at: shop.record.updated_at,
      updated_by: shop.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/groups/${group.record.guid}/shops`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(shop.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});