/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { loginGroup, login, userprofile, group } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /groups/{guid}/logins", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.login, "byGroupInfo").withArgs({
      input_group_guid: group.record.guid,
      input_guid: undefined,
      input_username: undefined,
      input_email: undefined,
      input_enabled: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(login.records));

    const resp = {
      guid: login.record.guid,
      username: login.record.username,
      email: login.record.email,
      role: {
        //guid: role.record.guid,
        //name: role.record.name,
        //description: role.record.description,
        //created_at: role.record.created_at,
        //created_by: role.record.created_by,
        //updated_at: role.record.updated_at,
        //updated_by: role.record.updated_by,
      },
      user_profile_guid: login.record.user_profile_guid,
      user_profile: {
        guid: userprofile.record.guid,
        name: userprofile.record.name,
        surname: userprofile.record.surname,
        email: userprofile.record.email,
        note: userprofile.record.note,
        phone: userprofile.record.phone,
        company_name: userprofile.record.company_name,
        created_at: userprofile.record.created_at,
        created_by: userprofile.record.created_by,
        updated_at: userprofile.record.updated_at,
        updated_by: userprofile.record.updated_by,
      },
      created_at: login.record.created_at,
      created_by: login.record.created_by,
      updated_at: login.record.updated_at,
      updated_by: login.record.updated_by,
    };

    chai.request(app)
      .get(`/api/v1/management/groups/${group.record.guid}/logins`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(loginGroup.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.loginGroup, "upsert").withArgs({
      input_login_guid: loginGroup.create.login_guid,
      input_group_guid: group.record.guid,
      input_role_guid: loginGroup.create.role_guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(loginGroup.record) ]]);

    let resp = clone(loginGroup.record);
    resp.guid = resp.login_guid;
    delete resp.login_guid;

    chai.request(app)
      .post(`/api/v1/management/groups/${group.record.guid}/logins`)
      .set("content-type", "application/json")
      .send(loginGroup.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.loginGroup, "delete").withArgs({
      input_login_guid: loginGroup.create.login_guid,
      input_group_guid: group.record.guid
    })
      .resolves([[ {
        login_guid: loginGroup.create.login_guid,
        group_guid: group.record.guid
      } ]]);

    chai.request(app)
      .post(`/api/v1/management/groups/${group.record.guid}/logins`)
      .set("content-type", "application/json")
      .send(loginGroup.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/groups/${group.record.guid}/logins`)
      .set("content-type", "application/json")
      .send(loginGroup.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
