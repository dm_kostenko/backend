/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { gateway } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /gateways", () => {
  
  afterEach(() => {
    sinon.restore();
  });


  it("GET", (done) => {
    sinon.stub(db.gateway, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(gateway.records));

    chai.request(app)
      .get("/api/v1/management/gateways")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(gateway.records[0].length);
          res.body.data[0].should.to.eql(gateway.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.gateway, "info").withArgs({ input_guid: gateway.record.guid })
      .resolves([[ clone(gateway.record) ]]);

    chai.request(app)
      .get(`/api/v1/management/gateways/${gateway.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(gateway.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.gateway, "upsert").withArgs({
      input_guid: gateway.create.guid,
      input_name: gateway.create.name,
      input_description: gateway.create.description,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(gateway.record) ]]);

    chai.request(app)
      .post("/api/v1/management/gateways")
      .set("content-type", "application/json")
      .send(gateway.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(gateway.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.gateway, "upsert").withArgs({
      input_guid: gateway.update.guid,
      input_name: gateway.update.name,
      input_description: gateway.update.description,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(gateway.record) ]]);

    chai.request(app)
      .post("/api/v1/management/gateways")
      .set("content-type", "application/json")
      .send(gateway.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(gateway.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.gateway, "delete").withArgs({ input_guid: gateway.record.guid })
      .resolves([[ { guid: gateway.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/gateways")
      .set("content-type", "application/json")
      .send(gateway.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/gateways")
      .set("content-type", "application/json")
      .send(gateway.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});