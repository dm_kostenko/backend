/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { blacklistRule, rule } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /blacklist", () => {
  
  afterEach(() => {
    sinon.restore();
  });

  it("GET /blacklist", (done) => {
    sinon.stub(db.blacklistRule, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_type: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(blacklistRule.records));

    chai.request(app)
      .get("/api/v1/management/blacklist")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(blacklistRule.records[0].length);
          res.body.data[0].should.to.eql(blacklistRule.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.blacklistRule, "get").withArgs({ input_guid: blacklistRule.record.guid })
      .resolves([[ clone(blacklistRule.record) ]]);

    sinon.stub(db.rule, "get").withArgs({ input_blacklist_rule_guid: blacklistRule.record.guid })
      .resolves([[ clone(rule.record) ]]);

    chai.request(app)
      .get(`/api/v1/management/blacklist/${blacklistRule.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.values[0].should.to.eql(rule.record.value);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.blacklistRule, "upsert").withArgs({
      input_guid: undefined,
      input_name: blacklistRule.create.name,
      input_type: blacklistRule.create.type,
      input_description: blacklistRule.create.description,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(blacklistRule.record) ]]);

    sinon.stub(db.rule, "upsert").withArgs({
      input_blacklist_rule_guid: blacklistRule.record.guid,
      input_value: rule.record.value,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(rule.record) ]]);

    chai.request(app)
      .post("/api/v1/management/blacklist")
      .set("content-type", "application/json")
      .send(blacklistRule.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(blacklistRule.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.blacklistRule, "upsert").withArgs({
      input_guid: blacklistRule.update.guid,
      input_name: blacklistRule.update.name,
      input_type: blacklistRule.update.type,
      input_description: blacklistRule.update.description,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(blacklistRule.record) ]]);

    chai.request(app)
      .post("/api/v1/management/blacklist")
      .set("content-type", "application/json")
      .send(blacklistRule.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(blacklistRule.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.blacklistRule, "delete").withArgs({ input_guid: blacklistRule.record.guid })
      .resolves([[ { guid: blacklistRule.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/blacklist")
      .set("content-type", "application/json")
      .send(blacklistRule.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/blacklist")
      .set("content-type", "application/json")
      .send(blacklistRule.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});