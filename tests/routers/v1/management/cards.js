/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { card, currency, account } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /cards", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.card, "info").withArgs({
      input_guid: undefined,
      input_type: undefined,
      input_account_guid: undefined,
      input_account_number: undefined,
      input_card_number: undefined,
      input_currency_name: undefined,
      input_name_on_card: undefined,
      input_balance: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(card.records));

    const resp = {
      guid: card.record.guid,
      type: card.record.type,
      account_guid: card.record.account_guid,
      account_number: account.record.number,
      card_number: card.record.card_number,
      currency_guid: card.record.currency_guid,
      currency_name: currency.record.name,
      name_on_card: card.record.name_on_card,
      expired: card.record.expired,
      expires_at: card.record.expires_at,
      balance: card.record.balance,
      created_at: card.record.created_at,
      created_by: card.record.created_by,
      updated_at: card.record.updated_at,
      updated_by: card.record.updated_by
    };

    chai.request(app)
      .get("/api/v1/management/cards")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(card.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.card, "info").withArgs({ input_guid: card.record.guid })
      .resolves([[ clone(card.record) ]]);

    const resp = {
      guid: card.record.guid,
      type: card.record.type,
      account_guid: card.record.account_guid,
      account_number: account.record.number,
      card_number: card.record.card_number,
      currency_guid: card.record.currency_guid,
      currency_name: currency.record.name,
      name_on_card: card.record.name_on_card,
      expired: card.record.expired,
      expires_at: card.record.expires_at,
      balance: card.record.balance,
      created_at: card.record.created_at,
      created_by: card.record.created_by,
      updated_at: card.record.updated_at,
      updated_by: card.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/cards/${card.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.card, "upsert").withArgs({
      input_guid: card.create.guid,
      input_type: card.create.type,
      input_account_guid: card.create.account_guid,
      input_card_number: card.create.card_number,
      input_currency_guid: card.create.currency_guid,
      input_name_on_card: card.create.name_on_card,
      input_expired: card.create.expired,
      input_expires_at: card.create.expires_at,
      input_balance: card.create.balance,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(card.record) ]]);

    sinon.stub(db.currency, "get").withArgs({ input_guid: card.record.currency_guid })
      .resolves([[ clone(currency.record) ]]);

    sinon.stub(db.account, "get").withArgs({ input_guid: card.record.account_guid })
      .resolves([[ clone(account.record) ]]);

    const resp = {
      guid: card.record.guid,
      type: card.record.type,
      account_guid: card.record.account_guid,
      account_number: account.record.number,
      card_number: card.record.card_number,
      currency_guid: card.record.currency_guid,
      currency_name: currency.record.name,
      name_on_card: card.record.name_on_card,
      expired: card.record.expired,
      expires_at: card.record.expires_at,
      balance: card.record.balance,
      created_at: card.record.created_at,
      created_by: card.record.created_by,
      updated_at: card.record.updated_at,
      updated_by: card.record.updated_by
    };

    chai.request(app)
      .post("/api/v1/management/cards")
      .set("content-type", "application/json")
      .send(card.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.card, "delete").withArgs({ input_guid: card.record.guid })
      .resolves([[ { guid: card.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/cards")
      .set("content-type", "application/json")
      .send(card.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/cards")
      .set("content-type", "application/json")
      .send(card.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});