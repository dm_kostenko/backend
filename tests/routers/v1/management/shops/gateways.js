/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { shopGateway, shop, gateway } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /shops/{guid}/gateways", () => {
  
  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.gateway, "byShopInfo").withArgs({
      input_shop_guid: shop.record.guid,
      input_guid: undefined,
      input_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(shopGateway.records));

    const resp = {
      //shop gateway info
      billing_descriptor: shopGateway.record.billing_descriptor,
      routing_string: shopGateway.record.routing_string,
      payment_amount_limit: shopGateway.record.payment_amount_limit,
      monthly_amount_limit: shopGateway.record.monthly_amount_limit,
      supported_brands: shopGateway.record.supported_brands,
      supported_currencies: shopGateway.record.supported_currencies,
      generate_statement: shopGateway.record.generate_statement,
      enabled: shopGateway.record.enabled,
      created_at: shopGateway.record.created_at,
      created_by: shopGateway.record.created_by,
      updated_at: shopGateway.record.updated_at,
      updated_by: shopGateway.record.updated_by,
      //gateway info
      guid: gateway.record.guid,
      //name: gateway.record.name,
      //description: gateway.record.description
    };

    chai.request(app)
      .get(`/api/v1/management/shops/${shop.record.guid}/gateways`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(shopGateway.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.shopGateway, "upsert").withArgs({
      input_shop_guid: shop.record.guid,
      input_gateway_guid: shopGateway.create[0].guid,
      input_billing_descriptor: shopGateway.create[0].billing_descriptor,
      input_routing_string: shopGateway.create[0].routing_string,
      input_payment_amount_limit: shopGateway.create[0].payment_amount_limit,
      input_monthly_amount_limit: shopGateway.create[0].monthly_amount_limit,
      input_supported_brands: shopGateway.create[0].supported_brands,
      input_supported_currencies: shopGateway.create[0].supported_currencies,
      input_generate_statement: shopGateway.create[0].generate_statement,
      input_enabled: shopGateway.create[0].enabled,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(shopGateway.record) ]]);

    chai.request(app)
      .post(`/api/v1/management/shops/${shop.record.guid}/gateways`)
      .set("content-type", "application/json")
      .send(shopGateway.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("array");
          res.body.should.to.eql([ shopGateway.record ]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.shopGateway, "delete")
      .resolves([[ {
        shop_guid: shopGateway.record.shop_guid,
        gateway_guid: shopGateway.record.gateway_guid
      } ]]);

    const resp = clone( DELETED );
    resp.gateway_guid = shopGateway.record.gateway_guid;
    resp.shop_guid = shopGateway.record.shop_guid;

    chai.request(app)
      .post(`/api/v1/management/shops/${shop.record.guid}/gateways`)
      .set("content-type", "application/json")
      .send(shopGateway.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("array");
          res.body.should.to.eql([ resp ]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/shops/${shop.record.guid}/gateways`)
      .set("content-type", "application/json")
      .send(shopGateway.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});