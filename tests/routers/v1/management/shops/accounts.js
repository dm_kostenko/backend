/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { shopAccount, account, shop } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /shops/{guid}/accounts", () => {
  
  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.account, "byShopInfo").withArgs({
      input_shop_guid: shop.record.guid,
      input_guid: undefined,
      input_currency_name: undefined,
      input_number: undefined,
      input_balance: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(account.records));

    const resp = {
      guid: account.record.guid,
      currency_guid: account.record.currency_guid,
      currency_name: account.record.currency_name,
      number: account.record.number,
      balance: account.record.balance,
      created_at: account.record.created_at,
      created_by: account.record.created_by,
      updated_at: account.record.updated_at,
      updated_by: account.record.updated_by,
    };

    chai.request(app)
      .get(`/api/v1/management/shops/${shop.record.guid}/accounts`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(shopAccount.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.shopAccount, "upsert").withArgs({
      input_shop_guid: shop.record.guid,
      input_account_guid: shopAccount.create[0].guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(shopAccount.record) ]]);

    const resp = {
      shop_guid: shopAccount.record.shop_guid,
      account_guid: shopAccount.record.account_guid,
      created_at: shopAccount.record.created_at,
      created_by: shopAccount.record.created_by,
      updated_at: shopAccount.record.updated_at,
      updated_by: shopAccount.record.updated_by,
    };

    chai.request(app)
      .post(`/api/v1/management/shops/${shop.record.guid}/accounts`)
      .set("content-type", "application/json")
      .send(shopAccount.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("array");
          res.body[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.shopAccount, "delete")
      .resolves([[ { guid: shopAccount.record.guid } ]]);

    const resp = clone(DELETED);
    resp.shop_guid = shopAccount.record.shop_guid;
    resp.account_guid = shopAccount.record.account_guid;

    chai.request(app)
      .post(`/api/v1/management/shops/${shop.record.guid}/accounts`)
      .set("content-type", "application/json")
      .send(shopAccount.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("array");
          res.body[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/shops/${shop.record.guid}/accounts`)
      .set("content-type", "application/json")
      .send(shopAccount.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});