/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { merchant, shop } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /merchants/{guid}/shops", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.shop, "byMerchantInfo").withArgs({
      input_merchant_guid: merchant.record.guid,
      input_guid: undefined,
      input_name: undefined,
      input_merchant_name: undefined,
      input_enabled: undefined,
      input_page_number: undefined,
      input_items_count: undefined

    })
      .resolves(clone(shop.records));

    chai.request(app)
      .get(`/api/v1/management/merchants/${merchant.record.guid}/shops`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(shop.records[0].length);
          res.body.data[0].should.to.eql(shop.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});