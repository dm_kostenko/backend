/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { currency } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /currencies", () => {
  
  afterEach(() => {
    sinon.restore();
  });


  it("GET", (done) => {
    sinon.stub(db.currency, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_code: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(currency.records));

    chai.request(app)
      .get("/api/v1/management/currencies")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(currency.records[0].length);
          res.body.data[0].should.to.eql(currency.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.currency, "info").withArgs({ input_guid: currency.record.guid })
      .resolves([[ clone(currency.record) ]]);

    chai.request(app)
      .get(`/api/v1/management/currencies/${currency.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(currency.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.currency, "upsert").withArgs({
      input_guid: currency.create.guid,
      input_name: currency.create.name,
      input_code: currency.create.code,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(currency.record) ]]);

    chai.request(app)
      .post("/api/v1/management/currencies")
      .set("content-type", "application/json")
      .send(currency.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(currency.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.currency, "upsert").withArgs({
      input_guid: currency.update.guid,
      input_name: currency.update.name,
      input_code: currency.update.code,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(currency.record) ]]);

    chai.request(app)
      .post("/api/v1/management/currencies")
      .set("content-type", "application/json")
      .send(currency.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(currency.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.currency, "delete").withArgs({ input_guid: currency.record.guid })
      .resolves([[ { guid: currency.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/currencies")
      .set("content-type", "application/json")
      .send(currency.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/currencies")
      .set("content-type", "application/json")
      .send(currency.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});