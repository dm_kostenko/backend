/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { role } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /roles", () => {

  afterEach(() => {
    sinon.restore();
  });


  it("GET", (done) => {
    sinon.stub(db.role, "info").withArgs({
      input_guid: undefined,
      input_name: undefined,
      input_type: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(role.records));

    chai.request(app)
      .get("/api/v1/management/roles")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(role.records[0].length);
          res.body.data[0].should.to.eql(role.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.role, "info").withArgs({ input_guid: role.record.guid })
      .resolves([[ clone(role.record) ]]);

    chai.request(app)
      .get(`/api/v1/management/roles/${role.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(role.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.role, "upsert").withArgs({
      input_guid: role.create.guid,
      input_name: role.create.name,
      input_type: role.create.type,
      input_description: role.create.description,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(role.record) ]]);

    chai.request(app)
      .post("/api/v1/management/roles")
      .set("content-type", "application/json")
      .send(role.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(role.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record", (done) => {
    sinon.stub(db.role, "upsert").withArgs({
      input_guid: role.update.guid,
      input_name: role.update.name,
      input_type: role.update.type,
      input_description: role.update.description,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(role.record) ]]);

    chai.request(app)
      .post("/api/v1/management/roles")
      .set("content-type", "application/json")
      .send(role.update)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(role.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.role, "delete").withArgs({ input_guid: role.record.guid })
      .resolves([[ { guid: role.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/roles")
      .set("content-type", "application/json")
      .send(role.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/roles")
      .set("content-type", "application/json")
      .send(role.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
