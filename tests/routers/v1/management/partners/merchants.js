/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { partner, merchant, group } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /partners/{guid}/merchants", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.merchant, "byPartnerInfo").withArgs({
      input_partner_guid: partner.record.guid,
      input_guid: undefined,
      input_name: undefined,
      input_group_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(merchant.records));

    const resp = {
      guid: merchant.record.guid,
      name: merchant.record.name,
      type: merchant.record.type,
      group_guid: merchant.record.group_guid,
      group_name: group.record.name,
      created_at: merchant.record.created_at,
      created_by: merchant.record.created_by,
      updated_at: merchant.record.updated_at,
      updated_by: merchant.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/partners/${partner.record.guid}/merchants`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(merchant.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});