/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { partner, group } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /partners/{guid}/groups", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.group, "byPartnerInfo").withArgs({
      input_partner_guid: partner.record.guid,
      input_guid: undefined,
      input_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(group.records));

    const resp = {
      guid: group.record.guid,
      name: group.record.name,
      type: group.record.type,
      created_at: group.record.created_at,
      created_by: group.record.created_by,
      updated_at: group.record.updated_at,
      updated_by: group.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/partners/${partner.record.guid}/groups`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(partner.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});