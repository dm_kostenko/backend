/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { loginPartner, login, userprofile, partner } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /partners/{guid}/logins", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.login, "byPartnerInfo").withArgs({
      input_partner_guid: partner.record.guid,
      input_guid: undefined,
      input_username: undefined,
      input_email: undefined,
      input_enabled: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(login.records));

    const resp = {
      guid: login.record.guid,
      username: login.record.username,
      email: login.record.email,
      role: {
        //guid: role.record.guid,
        //name: role.record.name,
        //description: role.record.description,
        //created_at: role.record.created_at,
        //created_by: role.record.created_by,
        //updated_at: role.record.updated_at,
        //updated_by: role.record.updated_by,
      },
      user_profile_guid: login.record.user_profile_guid,
      user_profile: {
        guid: userprofile.record.guid,
        name: userprofile.record.name,
        surname: userprofile.record.surname,
        email: userprofile.record.email,
        note: userprofile.record.note,
        phone: userprofile.record.phone,
        company_name: userprofile.record.company_name,
        created_at: userprofile.record.created_at,
        created_by: userprofile.record.created_by,
        updated_at: userprofile.record.updated_at,
        updated_by: userprofile.record.updated_by,
      },
      created_at: login.record.created_at,
      created_by: login.record.created_by,
      updated_at: login.record.updated_at,
      updated_by: login.record.updated_by,
    };

    chai.request(app)
      .get(`/api/v1/management/partners/${partner.record.guid}/logins`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(loginPartner.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.loginPartner, "upsert").withArgs({
      input_login_guid: loginPartner.create.login_guid,
      input_partner_guid: partner.record.guid,
      input_role_guid: loginPartner.create.role_guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(loginPartner.record) ]]);

    let resp = clone(loginPartner.record);
    resp.guid = resp.login_guid;
    delete resp.login_guid;

    chai.request(app)
      .post(`/api/v1/management/partners/${partner.record.guid}/logins`)
      .set("content-type", "application/json")
      .send(loginPartner.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.loginPartner, "delete").withArgs({
      input_login_guid: loginPartner.delete.login_guid,
      input_partner_guid: partner.record.guid
    })
      .resolves([[ clone(loginPartner.record) ]]);

    chai.request(app)
      .post(`/api/v1/management/partners/${partner.record.guid}/logins`)
      .set("content-type", "application/json")
      .send(loginPartner.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/partners/${partner.record.guid}/logins`)
      .set("content-type", "application/json")
      .send(loginPartner.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});
