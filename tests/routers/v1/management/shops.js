/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { shop, merchant, shopGateway } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /shops", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET /shops", (done) => {
    sinon.stub(db.shop, "info").withArgs({
      input_guid:undefined,
      input_name:undefined,
      input_merchant_name:undefined,
      input_enabled:undefined,
      input_page_number:undefined,
      input_items_count:undefined
    })
      .resolves(clone(shop.records));

    const resp = {
      guid: shop.record.guid,
      merchant_guid: shop.record.merchant_guid,
      merchant_name: merchant.record.name,
      name: shop.record.name,
      url: shop.record.url,
      email: shop.record.email,
      phone: shop.record.phone,
      enabled: shop.record.enabled,
      note: shop.record.note,
      enable_checkout: shop.record.enable_checkout,
      checkout_method: shop.record.checkout_method,
      antiFraudMonitor: shop.record.antiFraudMonitor,
      antiFraudMonitorValue: shop.record.antiFraudMonitorValue,
      created_at: shop.record.created_at,
      created_by: shop.record.created_by,
      updated_at: shop.record.updated_at,
      updated_by: shop.record.updated_by
    };

    chai.request(app)
      .get("/api/v1/management/shops")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(shop.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET /shops?gateway_guid={guid}", (done) => {
    sinon.stub(db.shop, "byGatewayInfo").withArgs({
      input_gateway_guid: shopGateway.record.gateway_guid,
      input_guid:undefined,
      input_name:undefined,
      input_merchant_name:undefined,
      input_enabled:undefined,
      input_page_number:undefined,
      input_items_count:undefined
    })
      .resolves(clone(shop.records));

    const resp = {
      guid: shop.record.guid,
      merchant_guid: shop.record.merchant_guid,
      merchant_name: merchant.record.name,
      name: shop.record.name,
      url: shop.record.url,
      email: shop.record.email,
      phone: shop.record.phone,
      enabled: shop.record.enabled,
      note: shop.record.note,
      enable_checkout: shop.record.enable_checkout,
      checkout_method: shop.record.checkout_method,
      antiFraudMonitor: shop.record.antiFraudMonitor,
      antiFraudMonitorValue: shop.record.antiFraudMonitorValue,
      created_at: shop.record.created_at,
      created_by: shop.record.created_by,
      updated_at: shop.record.updated_at,
      updated_by: shop.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/shops?gateway_guid=${shopGateway.record.gateway_guid}&merchant_guid=${shop.record.merchant_guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(shop.records.length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.shop, "info").withArgs({ input_guid: shop.record.guid })
      .resolves([[ clone(shop.record) ]]);

    const resp = {
      guid: shop.record.guid,
      merchant_guid: shop.record.merchant_guid,
      merchant_name: merchant.record.name,
      name: shop.record.name,
      url: shop.record.url,
      email: shop.record.email,
      phone: shop.record.phone,
      enabled: shop.record.enabled,
      note: shop.record.note,
      enable_checkout: shop.record.enable_checkout,
      checkout_method: shop.record.checkout_method,
      antiFraudMonitor: shop.record.antiFraudMonitor,
      antiFraudMonitorValue: shop.record.antiFraudMonitorValue,
      created_at: shop.record.created_at,
      created_by: shop.record.created_by,
      updated_at: shop.record.updated_at,
      updated_by: shop.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/shops/${shop.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.shop, "upsert").withArgs({
      input_guid: shop.create.guid,
      input_merchant_guid: shop.create.merchant_guid,
      input_name: shop.create.name,
      input_url: shop.create.url,
      input_email: shop.create.email,
      input_phone: shop.create.phone,
      input_enabled: shop.create.enabled,
      input_secret: sinon.match.defined,
      input_note: shop.create.note,
      input_hash_key: sinon.match.defined,
      input_enable_checkout: shop.create.enable_checkout,
      input_checkout_method: shop.create.checkout_method,
      input_antiFraudMonitor: shop.create.antiFraudMonitor,
      input_antiFraudMonitorValue: shop.create.antiFraudMonitorValue,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(shop.record) ]]);

    sinon.stub(db.merchant, "get").withArgs({ input_guid: shop.record.merchant_guid })
      .resolves([[ clone(merchant.record) ]]);

    const resp = {
      guid: shop.record.guid,
      merchant_guid: shop.record.merchant_guid,
      merchant_name: merchant.record.name,
      name: shop.record.name,
      url: shop.record.url,
      email: shop.record.email,
      phone: shop.record.phone,
      enabled: shop.record.enabled,
      note: shop.record.note,
      enable_checkout: shop.record.enable_checkout,
      checkout_method: shop.record.checkout_method,
      antiFraudMonitor: shop.record.antiFraudMonitor,
      antiFraudMonitorValue: shop.record.antiFraudMonitorValue,
      created_at: shop.record.created_at,
      created_by: shop.record.created_by,
      updated_at: shop.record.updated_at,
      updated_by: shop.record.updated_by
    };

    chai.request(app)
      .post("/api/v1/management/shops")
      .set("content-type", "application/json")
      .send(shop.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.shop, "delete").withArgs({ input_guid: shop.record.guid })
      .resolves([[ { guid: shop.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/shops")
      .set("content-type", "application/json")
      .send(shop.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/shops")
      .set("content-type", "application/json")
      .send(shop.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});