/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { rule } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /blacklist/values", () => {
  
  afterEach(() => {
    sinon.restore();
  });

  it("POST for add record", (done) => {
    sinon.stub(db.rule, "upsert").withArgs({
      input_blacklist_rule_guid: rule.record.blacklist_rule_guid,
      input_value: rule.record.value,
      input_author_guid: "emptyname"
    })
      .resolves(clone([[ rule.record ]]));

    chai.request(app)
      .post("/api/v1/management/blacklist/values")
      .set("content-type", "application/json")
      .send(rule.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(rule.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.rule, "delete").withArgs({
      input_blacklist_rule_guid: rule.record.blacklist_rule_guid,
      input_value: rule.record.value
    })
      .resolves([[ { guid: rule.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/blacklist/values")
      .set("content-type", "application/json")
      .send(rule.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/blacklist/values")
      .set("content-type", "application/json")
      .send(rule.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});