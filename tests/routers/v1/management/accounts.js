/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../helpers/db/api/");
const app = require("../../../..");
const { DELETED } = require("../../../replyMessages");
const { account, currency } = require("../../../testData");
const { clone } = require("../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /accounts", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.account, "info").withArgs({
      input_guid: undefined,
      input_currency_name: undefined,
      input_number: undefined,
      input_balance: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(account.records));

    sinon.stub(db.currency, "info").withArgs({ input_guid: account.record.currency_guid })
      .resolves([[ clone(currency.record) ]]);

    const resp = {
      guid: account.record.guid,
      currency_guid: account.record.currency_guid,
      currency_name: currency.record.name,
      number: account.record.number,
      balance: account.record.balance,
      created_at: account.record.created_at,
      created_by: account.record.created_by,
      updated_at: account.record.updated_at,
      updated_by: account.record.updated_by
    };

    chai.request(app)
      .get("/api/v1/management/accounts")
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(account.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET by {guid}", (done) => {
    sinon.stub(db.account, "info").withArgs({ input_guid: account.record.guid })
      .resolves([[ clone(account.record) ]]);

    const resp = {
      guid: account.record.guid,
      currency_guid: account.record.currency_guid,
      currency_name: currency.record.name,
      number: account.record.number,
      balance: account.record.balance,
      created_at: account.record.created_at,
      created_by: account.record.created_by,
      updated_at: account.record.updated_at,
      updated_by: account.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/accounts/${account.record.guid}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.account, "upsert").withArgs({
      input_guid: account.create.guid,
      input_currency_guid: account.create.currency_guid,
      input_number: account.create.number,
      input_balance: account.create.balance,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(account.record) ]]);

    sinon.stub(db.currency, "get").withArgs({ input_guid: account.record.currency_guid })
      .resolves([[ clone(currency.record) ]]);

    const resp = {
      guid: account.record.guid,
      currency_guid: account.record.currency_guid,
      currency_name: currency.record.name,
      number: account.record.number,
      balance: account.record.balance,
      created_at: account.record.created_at,
      created_by: account.record.created_by,
      updated_at: account.record.updated_at,
      updated_by: account.record.updated_by
    };

    chai.request(app)
      .post("/api/v1/management/accounts")
      .set("content-type", "application/json")
      .send(account.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.account, "delete").withArgs({ input_guid: account.record.guid })
      .resolves([[ { guid: account.record.guid } ]]);

    chai.request(app)
      .post("/api/v1/management/accounts")
      .set("content-type", "application/json")
      .send(account.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post("/api/v1/management/accounts")
      .set("content-type", "application/json")
      .send(account.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});