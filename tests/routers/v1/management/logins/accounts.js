/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { loginAccount, account, login } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /logins/{guid}/accounts", () => {
  
  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.account, "byLoginInfo").withArgs({
      input_login_guid: login.record.guid,
      input_guid: undefined,
      input_currency_name: undefined,
      input_number: undefined,
      input_balance: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(account.records));

    const resp = {
      guid: account.record.guid,
      currency_guid: account.record.currency_guid,
      currency_name: account.record.currency_name,
      number: account.record.number,
      balance: account.record.balance,
      created_at: account.record.created_at,
      created_by: account.record.created_by,
      updated_at: account.record.updated_at,
      updated_by: account.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/logins/${login.record.guid}/accounts`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(loginAccount.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.loginAccount, "upsert").withArgs({
      input_login_guid: login.record.guid,
      input_account_guid: loginAccount.create.guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(loginAccount.record) ]]);

    sinon.stub(db.account, "get").withArgs({ input_guid: loginAccount.record.account_guid })
      .resolves([[ clone(account.record) ]]);

    const resp = {
      login_guid: login.record.guid,
      guid: account.record.guid,
      currency_guid: account.record.currency_guid,
      number: account.record.number,
      balance: account.record.balance,
      created_at: loginAccount.record.created_at,
      created_by: loginAccount.record.created_by,
      updated_at: loginAccount.record.updated_at,
      updated_by: loginAccount.record.updated_by
    };

    chai.request(app)
      .post(`/api/v1/management/logins/${login.record.guid}/accounts`)
      .set("content-type", "application/json")
      .send(loginAccount.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.loginAccount, "delete").withArgs({
      input_login_guid: login.record.guid,
      input_account_guid: loginAccount.create.guid,
    })
      .resolves([[ {
        login_guid: login.record.guid,
        account_guid: loginAccount.create.guid
      } ]]);

    chai.request(app)
      .post(`/api/v1/management/logins/${login.record.guid}/accounts`)
      .set("content-type", "application/json")
      .send(loginAccount.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/logins/${login.record.guid}/accounts`)
      .set("content-type", "application/json")
      .send(loginAccount.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});