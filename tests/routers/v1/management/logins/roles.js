/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { login, defaultLoginRole, role } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /logins/{guid}/roles", () => {
  
  afterEach(() => {
    sinon.restore();
  });

  it("GET", (done) => {
    sinon.stub(db.defaultRole, "byLoginInfo").withArgs({
      input_login_guid: login.record.guid,
      input_guid: undefined,
      input_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(role.records));

    const resp = {
      guid: role.record.guid,
      name: role.record.name,
      description: role.record.description,
      created_at: role.record.created_at,
      created_by: role.record.created_by,
      updated_at: role.record.updated_at,
      updated_by: role.record.updated_by
    };

    chai.request(app)
      .get(`/api/v1/management/logins/${login.record.guid}/roles`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(role.records[0].length);
          res.body.data[0].should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.defaultLoginRole, "upsert").withArgs({
      input_login_guid: login.record.guid,
      input_role_guid: defaultLoginRole.create.guid,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(defaultLoginRole.record) ]]);

    sinon.stub(db.role, "get").withArgs({ input_guid: defaultLoginRole.record.role_guid })
      .resolves([[ clone(role.record) ]]);

    const resp = {
      login_guid: login.record.guid,
      guid: role.record.guid,
      name: role.record.name,
      type: role.record.type,
      description: role.record.description,
      created_at: defaultLoginRole.record.created_at,
      created_by: defaultLoginRole.record.created_by,
      updated_at: defaultLoginRole.record.updated_at,
      updated_by: defaultLoginRole.record.updated_by
    };

    chai.request(app)
      .post(`/api/v1/management/logins/${login.record.guid}/roles`)
      .set("content-type", "application/json")
      .send(defaultLoginRole.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(resp);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.defaultLoginRole, "delete").withArgs({
      input_login_guid: login.record.guid,
      input_role_guid: defaultLoginRole.delete.guid
    })
      .resolves([[ {
        login_guid: defaultLoginRole.record.login_guid,
        role_guid: defaultLoginRole.record.role_guid
      } ]]);

    chai.request(app)
      .post(`/api/v1/management/logins/${login.record.guid}/roles`)
      .set("content-type", "application/json")
      .send(defaultLoginRole.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/logins/${login.record.guid}/roles`)
      .set("content-type", "application/json")
      .send(defaultLoginRole.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});