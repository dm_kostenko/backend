/* eslint-disable no-undef */
process.env.NODE_ENV = "test";

const sinon = require("sinon").createSandbox();
const chai = require("chai");
const chaiHttp = require("chai-http");
const db = require("../../../../../helpers/db/api/");
const app = require("../../../../..");
const { DELETED } = require("../../../../replyMessages");
const { gatewayProp } = require("../../../../testData");
const { clone } = require("../../../../functions");

chai.should();
chai.use(chaiHttp);

describe("Test /gateways/{guid}/props", () => {
  
  afterEach(() => {
    sinon.restore();
  });

  it("GET /gateways/{guid}/props", (done) => {
    sinon.stub(db.gatewayProp, "info").withArgs({
      input_gateway_guid: gatewayProp.record.gateway_guid,
      input_name: undefined,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves(clone(gatewayProp.records));

    chai.request(app)
      .get(`/api/v1/management/gateways/${gatewayProp.record.gateway_guid}/props`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(gatewayProp.records[0].length);
          res.body.data[0].should.to.eql(gatewayProp.records[0][0]);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("GET /gateways/{guid}/props/{name}", (done) => {
    sinon.stub(db.gatewayProp, "info").withArgs({
      input_gateway_guid: gatewayProp.record.gateway_guid,
      input_name: gatewayProp.record.name,
      input_page_number: undefined,
      input_items_count: undefined
    })
      .resolves([[ clone(gatewayProp.record) ], [ { count: 1 } ]]);

    chai.request(app)
      .get(`/api/v1/management/gateways/${gatewayProp.record.gateway_guid}/props/${gatewayProp.record.name}`)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(gatewayProp.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for add new record", (done) => {
    sinon.stub(db.gatewayProp, "upsert").withArgs({
      input_gateway_guid: gatewayProp.record.gateway_guid,
      input_name: gatewayProp.create.name,
      input_label: gatewayProp.create.label,
      input_author_guid: "emptyname"
    })
      .resolves([[ clone(gatewayProp.record) ]]);

    chai.request(app)
      .post(`/api/v1/management/gateways/${gatewayProp.record.gateway_guid}/props`)
      .set("content-type", "application/json")
      .send(gatewayProp.create)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql(gatewayProp.record);
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for delete record", (done) => {
    sinon.stub(db.gatewayProp, "delete").withArgs({
      input_gateway_guid: gatewayProp.record.gateway_guid,
      input_name: gatewayProp.create.name,
    })
      .resolves([[ { gateway_guid: gatewayProp.record.gateway_guid } ]]);

    chai.request(app)
      .post(`/api/v1/management/gateways/${gatewayProp.record.gateway_guid}/props`)
      .set("content-type", "application/json")
      .send(gatewayProp.delete)
      .end((err, res) => {
        try{
          res.should.have.status(200);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.should.to.eql( DELETED );
          done();
        }
        catch(err){
          done(err);
        }
      });
  });

  it("POST for update record with invalid data", (done) => {
    chai.request(app)
      .post(`/api/v1/management/gateways/${gatewayProp.record.gateway_guid}/props`)
      .set("content-type", "application/json")
      .send(gatewayProp.invalid)
      .end((err, res) => {
        try{
          res.should.have.status(400);
          res.header["content-type"].should.be.eql("application/json; charset=utf-8");
          res.body.should.be.a("object");
          res.body.error.should.to.eql("ValidationError");
          done();
        }
        catch(err){
          done(err);
        }
      });
  });
});