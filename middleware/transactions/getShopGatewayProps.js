const db = require("../../helpers/db/api/");
const { InternalError } = require("../../errorTypes");

module.exports = async (req, res, next) => {
  try {
    const { shopGuid } = req.auth;
    const { gatewayGuid } = req.body;
    const [ props ] = await db.shopGatewayProp.info({ input_shop_guid: shopGuid, input_gateway_guid: gatewayGuid });
    const obj = props.reduce((acc, item) => ({ ...acc, [item.name]: item.value ? item.value: undefined }), {});
    for (let key in obj)
      if (obj[key] === undefined)
        throw new InternalError(`Failed starting transaction: ${key} is undefined`);

    req.body = { 
      ...req.body, 
      ...obj
    };
    //set additional props
    
    if (req.body.credit_card) 
      req.body.credit_card = { ...req.body.credit_card, exp_year_last2Digits: req.body.credit_card.exp_year.substr(-2) };

    next();
  } catch (err) {
    next(err);
  }
};