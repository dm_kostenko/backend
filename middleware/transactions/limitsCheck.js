const db = require("../../helpers/db/api/");
const { LimitReachedError, InvalidDataError } = require("../../errorTypes");

module.exports = async (amount, shopGuid, gatewayGuid, accountGuid) => {
  const [[ shopGateway ]] = await db.shopGateway.get({input_shop_guid: shopGuid, input_gateway_guid: gatewayGuid });
  if (!shopGateway) 
    throw new InvalidDataError("Any shop gateway not found");
  if (amount > shopGateway.payment_amount_limit) 
    throw  new LimitReachedError("Payment limit reached");

  const [[ gatewayProcessedAmount ]] = await db.gatewayProcessedAmount.info({input_account_guid: accountGuid});
  if (!gatewayProcessedAmount) 
    throw new InvalidDataError("Any gateway processed amount not found");
  if (gatewayProcessedAmount.amount > shopGateway.monthly_amount_limit) 
    throw  new LimitReachedError("Shop monthly payment limit reached");

  const [[ merchantLimit ]] = await db.merchantLimit.info({input_shop_guid: shopGuid });
  if (!merchantLimit) 
    throw new InvalidDataError("Any merchant limit not found");

  if (merchantLimit.monthly_amount_limit != null) {
    const [[ merchantProcessedAmount ]] = await db.merchantProcessedAmount.info({input_merchant_guid: merchantLimit.guid });
    if (!merchantProcessedAmount) 
      throw new InvalidDataError("Any merchant processed amount limit not found");
      
    if (merchantProcessedAmount.amount > merchantLimit.monthly_amount_limit) 
      throw  new LimitReachedError("Merchant monthly payment limit reached");
  }
}