const db = require("../../helpers/db/api/");
const crypto = require("crypto");    
const { ShopAuthorizationError } = require("../../errorTypes");
const encodeBasicAuth = require("../../helpers/encodeBasicAuth");
const aes = require("../../helpers/aes");

module.exports = async (req, res, next) => {
  try {
    if (req.paymentForm)
      return next();

    if (!req.headers.authorization || req.headers.authorization.split(" ")[0] !== "Basic") 
      if ((process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev") && req.body.shopGuid){
        req.auth = { shopGuid: req.body.shopGuid };
        return next(); 
      }
      else new ShopAuthorizationError();
    
    const [ shopGuid, shopSecret ] = encodeBasicAuth(req.headers.authorization);
    if (!shopGuid || !shopSecret)
      throw new ShopAuthorizationError();
 
    const [[ shop ]] = await db.shop.verify({ input_guid: shopGuid, input_secret: aes.encode(shopSecret) });
    if (!shop)
      throw new ShopAuthorizationError();

    const shopHashKey = aes.decode(shop.hash_key);
    if (!shopHashKey)
      throw new ShopAuthorizationError("Shops hashKey doesn't generated");
  
    const { signature } = req.body;
    if (!signature)
      throw new ShopAuthorizationError();

    req.body.signature = undefined;
    
    const signatureGenerated = crypto.createHash("sha256").update(JSON.stringify(req.body) + aes.decode(shop.hash_key)).digest("base64");
    if (signature !== signatureGenerated)
      throw new ShopAuthorizationError();
      
    req.auth = { ...req.auth, shopGuid };
    next();
  } catch (error) {
    next(error);
  }
};
