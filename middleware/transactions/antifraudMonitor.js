const AntifraudMonitor = require("../../helpers/AntifraudMonitor");
const { bins } = require("../../config").antifraud; 

module.exports = async (req, res, next) => {
  try {
    const { auth: { shopGuid }, body } = req; 
    const antifraudMonitor = new AntifraudMonitor(bins, shopGuid);
    await antifraudMonitor.checkData(body);
    
    next();
  } catch (err) {
    next(err);
  }
};