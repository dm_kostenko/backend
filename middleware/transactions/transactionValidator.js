const db = require("../../helpers/db/api/");
const JoiSchema = require("joi-json").builder(require("@hapi/joi"));
const uuidv1 = require("uuid/v1");
const emitLog = require("../../modules/logStorage/emitLog");

const generateTransactionTemplate = require("../../helpers/generateTransactionTemplate");
const validate = require("../../helpers/validate");
const limitsCheck = require("./limitsCheck"); 
const { testCard } = require("../../config");
const { InternalError, InvalidDataError } = require("../../errorTypes");

module.exports = async (req, res, next) => {
  try {
    const { shopGuid } = req.auth;
    const { type, credit_card, transactionId, currency, amount } = req.body;
    
    if (!type || !shopGuid)
      throw new InvalidDataError("Type, shopguid doesn't presented");
    
    let gateway, isTestGateway;
    if (JSON.stringify(credit_card) === JSON.stringify(testCard)) {
      [[ gateway ]] = await db.gateway.get({ input_name: "TestGateway" });
      isTestGateway = true;
    }
    else 
      [[ gateway ]] = await db.shopGateway.get({ input_shop_guid: shopGuid });
    if (!gateway) 
      throw new InvalidDataError("Any gateway not found");
    const gatewayGuid = gateway.gateway_guid || gateway.guid ;  
    
    const [[ account ]] = await db.shopAccount.get({ input_shop_guid: shopGuid });
    if (!account)
      throw new InvalidDataError("Any account not found");
    const accountGuid = account.account_guid;

    let currencyGuid;
    if (currency){
      const [[ currencyData ]] = await db.currency.get({ input_code: currency });
      if (!currencyData)
        throw new InvalidDataError(`Currency=${currency} doesn't find in database`);
      currencyGuid = currencyData.guid;
    }

    const template = await generateTransactionTemplate(gatewayGuid, type);
    if (!template || !template.steps)
      throw new InternalError("Transaction isn't have template");
    
    const validationStep = template.steps.shift();
    
    if (!validationStep.params.validation)
      throw new InternalError("Validation in step params doesn't presented");
    if (!validationStep.params.options)
      throw new InternalError("Options in step params doesn't presented");
      
    validate(req.body, JoiSchema.build(JSON.parse(validationStep.params.validation)));
    
    if (JSON.parse(validationStep.params.options).isCardRequired && !req.body.credit_card){
      const [[ securePayment ]] = await db.securePayment.upsert({ input_auth_key: uuidv1(), input_data: JSON.stringify(req.body), input_shop_guid: shopGuid, input_author_guid: shopGuid });
      
      const message = { ...req.body, redirect_to: { url: "/secure/payment", params: { auth_key: securePayment.auth_key } } };
      emitLog.emit("transactionLog", {
        step_info: "Middleware: Redirect",
        request_id: req.id,
        shop_guid: shopGuid,
        message
      });

      return res.json(message);
    }

    if (!isTestGateway)
      await limitsCheck(req.body.amount, shopGuid, gatewayGuid, accountGuid);
    
    req.template = { ...template, accountGuid, gatewayGuid, shopGuid, currencyGuid, amount };
    if (validationStep.params.getSavedParams && transactionId) {
      const { step_name, values } = JSON.parse(validationStep.params.getSavedParams);

      const [[ savedStep ]] = await db.stepProcessing.get({ input_transaction_processing_guid: transactionId, input_name: step_name });
      if (!savedStep)
        throw `Step ${step_name} not found in db.stepProcessing for ${transactionId.transactionId}`;
        
      const [ savedParams ] = await db.stepParamProcessing.get({ input_step_processing_guid: savedStep.guid  });
      if (!savedParams || !savedParams.length)
        throw `Params for ${savedStep.step_processing_guid} not found in db.stepParamProcessing`;
        
      const savedParamsAsObject = savedParams.reduce((acc, savedParam) => ({ ...acc, [savedParam.name]: savedParam.value }), {});
      values
        .forEach(value => {
          if (savedParamsAsObject[value] === undefined) throw `Part of params for ${savedStep.guid} not found in db.stepParamProcessing`;
        });
      values.forEach(value => { req.template[value] = savedParamsAsObject[value]; });
      req.template = { ...req.template, ...savedParamsAsObject };
    }

    if (!req.template.currencyGuid || !req.template.amount)
      throw new InternalError("Uncorrect data - currency or amount is undefined ");

    req.body = { ...req.body,
      requestId: req.id.split("-")[0],
      gatewayGuid,
      currencyGuid
    };
    next();
  } catch (error) {
    next(error);
  }
};