const db = require("../helpers/db/api/");
const { PrivilegesError } = require("../errorTypes");
const config = require("../config").auth;

module.exports = (privilegesByType = {}, rule = "every") => async (req, res, next) => {
  if (!req.headers[config.session.header.toLowerCase()] && (process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev"))
    return next();

  const { privileges, type } = req.auth;
  const requiredPrivileges = privilegesByType[type];

  if (!requiredPrivileges)
    return next();

  try {
    const privilegesNames = privileges.map(privilege => privilege.name);
    let isAllowed = false;

    switch (rule) {
    case "every":
      isAllowed = requiredPrivileges.every(requiredPrivelege => privilegesNames.includes(requiredPrivelege));
      break;
    case "some":  
      isAllowed = requiredPrivileges.some(requiredPrivelege => privilegesNames.includes(requiredPrivelege));
      break;
    default:
      throw new PrivilegesError("Doesn't have enough privileges");
    }

    if (isAllowed)
      return next();

    throw new PrivilegesError("Doesn't have enough privileges");
  } catch (error) {
    next(error);
  }
};