const validate = require("../helpers/validate");
module.exports = component => (req, res, next) => {
  try {
    validate(req.body, component(req.body));
    next();
  } catch (err) {
    next(err);
  }
};