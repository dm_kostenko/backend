const { PrivilegesError } = require("../errorTypes");

module.exports = types => async (req, res, next) => {
  try {
    if (!req.headers.authorization)
      if (process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev")
        return next();
      else
        throw new PrivilegesError("Must be authenticated");
  
    const { type: loginType } = req.auth;
    if (types.includes(loginType))
      return next();
      
    throw new PrivilegesError(`Login type must be ${type}`);
  } catch (err) {
    next(err);
  }
};