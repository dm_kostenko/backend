const log = require("../helpers/logger");
const { InternalError, MysqlError, InvalidDataError, ValidationError, AntifraudError, LimitReachedError, UNAUTHORIZED, OtpError } = require("../errorTypes");

module.exports.errorHandler = async (err, req, res, next) => {
  try{    
    if (err.name === "PersonalDataStorageError")
      err.message = err.message || "Internal error";

    else if ((err.name === "AuthorizationError") || (err.type === UNAUTHORIZED) || (err.type === "ShopAuthorizationError"))
      err.message = err.status === 403 ? "Unauthorized" : err.message;

    else if (err instanceof MysqlError) 
      err.message = `${err.message || " "} ${err.error.sqlMessage || " "} ${err.error.errno || " "}`;
    
    else if (err instanceof InvalidDataError)
      err.message = err.message || "Invalid Data";

    else if (err instanceof InternalError)
      err.message = err.message || "Internal error";

    else if (err instanceof ValidationError)
      err.message = err.message || "Validation error";  

    else if (err instanceof AntifraudError)
      err.message = err.message || "Antifraud error";

    else if (err instanceof LimitReachedError)
      err.message = err.message || "Limit reached error";
   
    else if (err instanceof OtpError)
      err.message = err.message || "One-time-password error";
    
    if (process.env.NODE_ENV !== "test")
      log.error(`'${err.name}' statusCode:'${err.status}' message:'${err.message}'${err.details ? ` details ${err.details}`:""}'${err.stack ? ` stack:'${err.stack}'` : ""}`);
    console.log(err)
    res.status(err.status || 500).json({ error: err.message, details: err.details, stack: process.env.NODE_ENV === "dev" ? err.stack : undefined });
    
    //TODOs emitLog.emit("errorLog", err.type, message, statusCode, req);
  }catch(err){
    console.log(err);
  }
};