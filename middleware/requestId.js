const uuid = require("uuid");

module.exports = ({ uuidVersion, headerName, attributeName }) => {
  return function (req, res, next) {
    req[attributeName] = req.headers[headerName.toLowerCase()] || uuid[uuidVersion]();
    if (headerName) 
      res.setHeader(headerName, req[attributeName]);
    next();
  };
};