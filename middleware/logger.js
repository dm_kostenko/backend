const log = require("../helpers/logger");
const emitLog = require("../modules/logStorage/emitLog");

module.exports.reqResLog = (req, res, next) => {
  log.info (`${req.method} ${req.url} ip=${req.hostname} user-agent=${req.headers["user-agent"]} ${res.statusCode}`);
  if (process.env.NODE_ENV !== "test") emitLog.emit("writeReqResLog", req, res, next);
  next();
};