module.exports = (req, res, next) => {
  res.set("Access-Control-Expose-Headers", "Authorization");
  next();
};