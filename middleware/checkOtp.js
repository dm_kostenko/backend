const authenticator = require("otplib/authenticator");
const db = require("../helpers/db/bankApi");
const { OtpError } = require("../errorTypes");
const crypto = require("crypto");

authenticator.options = { crypto, step: 30, window: 1 };
const isOtpValid = (otp, secret) => authenticator.check(otp, secret);

module.exports = async (req, res, next) => {
  try {
    if (!req.headers.authorization)
      // if (process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev")
      //   next();
      // else
        throw new OtpError("Must be authorized");
      
    const { loginGuid, auth_type } = req.auth;
    if (auth_type !== "login-password-otp")
      return next();
    // throw new OtpError("Two-factor-authentication required. Please, activate 2FA and relogin.", 409);
    if(req.body._disableOtp)
      return next();
    const { otp } = req.body;
    if (!otp)
      if ((process.env.NODE_ENV === "test" || process.env.NODE_ENV === "dev") && req.headers["one-time-password-checking-ignore"] === "true")
        return next();
      else
        throw new OtpError("Otp required", 409);
    
    const [ otpData ] = await db.login.otp_get({ input_guid: loginGuid });
    if (!otpData)
      throw new OtpError("User not found");

    const { otpsalt, otpsaltstatus } = otpData;
    if (otpsaltstatus === "confirmed" && isOtpValid(otp, otpsalt))
      return next();

    throw new OtpError("Correct otp required");
  } catch (err) {
    next(err);
  }
};