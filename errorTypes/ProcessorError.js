class ProcessorError extends Error {
  constructor(message, status = 403){
    super(message);
    this.type = "ProcessorError";
    this.status = status;
  }
}

module.exports = ProcessorError;