class ValidationError extends Error {
  constructor(message, details, status = 400){
    super(message);
    this.type = "ValidationError";
    this.status = status;
    this.details = details;
  }
}

module.exports = ValidationError;