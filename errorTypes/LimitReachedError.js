class LimitReachedError extends Error {
  constructor(message, status = 403){
    super(message);
    this.type = "LimitReachedError";
    this.status = status;
  }
}

module.exports = LimitReachedError;