class AntifraudError extends Error {
  constructor(message, status = 403){
    super(message);
    this.type = "AntifraudError";
    this.status = status;
  }
}

module.exports = AntifraudError;