class PrivilegesError extends Error {
  constructor(message, status = 418){
    super(message);
    this.type = "PrivilegesError";
    this.status = status;
  }
}

module.exports = PrivilegesError;