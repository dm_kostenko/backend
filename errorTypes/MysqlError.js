class MysqlError extends Error {
  constructor(message, error, status = 500){
    super(message);
    this.type = "MysqlError";
    this.error = error;
    this.status = status;
  }
}

module.exports = MysqlError;