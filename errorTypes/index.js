module.exports = {
  AntifraudError: require("./AntifraudError"),
  ProcessorError: require("./ProcessorError"),
  InternalError: require("./InternalError"),
  InvalidDataError: require("./InvalidDataError"),
  MysqlError: require("./MysqlError"),
  MONGO_ERROR: "MongoError", //legacy
  OtpError: require("./OtpError"),
  PrivilegesError: require("./PrivilegesError"),
  ShopAuthorizationError: require("./ShopAuthorizationError"),
  ValidationError: require("./ValidationError"),
  LimitReachedError: require("./LimitReachedError"),
  UNAUTHORIZED: "Unauthorized"
};