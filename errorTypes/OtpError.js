class OtpError extends Error {
  constructor(message, status = 403){
    super(message);
    this.type = "OtpError";
    this.status = status;
  }
}

module.exports = OtpError;