class ShopAuthorizationError extends Error {
  constructor(message, status = 403){
    super(message);
    this.type = "ShopAuthorizationError";
    this.status = status;
  }
}

module.exports = ShopAuthorizationError;