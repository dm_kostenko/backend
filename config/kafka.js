module.exports = {
  hosts: [
    // `${process.env.KAFKA_HOST}:${process.env.KAFKA_PORT }`
    'localhost:9092'
  ],
  services: {
    currencyExchange: true,
    checkRules: true,
    blockAccounts: true
  }
}