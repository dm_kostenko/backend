module.exports = {
  ...require("./app"),
  auth: require("./authorizationModule"),
  // personalDataStorage: require("./personalDataStorage"),
  // loggerModule: require("./loggerModule")
};
