const bcrypt = require("bcrypt");
const fs = require("fs");
const { guiUrl } = require("./app").app;
const AuthorizationError = require("../modules/auth/lib/helpers/AuthorizationError"); 

module.exports.authType = "login-password";

module.exports.routes = {
  login: "/api/bank/auth/login",
  logout: "/api/bank/auth/logout",
  refreshToken: "/api/bank/auth/token",
  validateToken: "/api/bank/auth/validate",
  changeLogin: "/api/bank/auth/changeLogin",
  otp: "/api/bank/auth/otp",
  forgotPassword: "/api/bank/auth/forgot",
  checkRecoveryToken: "/api/bank/auth/check/recovery",
  recoveryPassword: "/api/bank/auth/recovery",
  changeExpiredPassword: [ "/api/bank/user/logins", "/api/bank/admin/logins" ]
};

module.exports.keys = {
  private: fs.readFileSync(__dirname + "/keys/private.key", "utf-8"),
  public: fs.readFileSync(__dirname + "/keys/public.pem", "utf-8")
};

module.exports.user = {
  baseRecoveryLink: `${guiUrl}/#/recovery_password/`,

  checkExpiredPassword: async (payload) => {
    const db = require("../helpers/db/bankApi/");
    const [ loginInfo ] = await db.login.get({ input_guid: payload.loginGuid });
    return loginInfo ? loginInfo.credentials_expired : true;
  },
  getLoginData: async ({ username, email, phone }) => {
    const db = require("../helpers/db/bankApi");
    const [ login ] = await db.login.get({ 
      input_username: username,
      input_email: email,
      input_phone: phone
    });
    if (!login) throw new AuthorizationError("Incorrect login or password", 400);

    const [ loginInfo ] = await db.showPassword.get({
      input_username: username,
      input_email: email,
      input_phone: phone
    });
    if(loginInfo) {
      // if (!loginInfo.enabled)
      //   throw new Error("User blocked");
      return {
        ...loginInfo,
        id: loginInfo.guid,
        mail: loginInfo.email
      };
    }
  },
  setRecoveryToken: async (id) => {
    const { setRecoveryToken: setRecoveryTokenFunc } = require("../helpers/password");
    const recoveryData = await setRecoveryTokenFunc(id);
    return {
      token: recoveryData.token,
      email: recoveryData.email
    };
  },
  checkRecoveryToken: async (token) => {
    const { checkRecoveryToken } = require("../helpers/password");
    const recoveryData = await checkRecoveryToken(token);
    return {
      id: recoveryData.guid,
      status: recoveryData.status
    };
  },
  setNewPassword: async (id, password) => {
    const { recovery } = require("../helpers/password");
    return recovery(id, password);
  },
  // checkPassword: (password, dbPassword) => {
  //   return bcrypt.compareSync(password, dbPassword);
  // },
  checkPassword: async (id, password) => {
    const { check } = require("../helpers/password");
    const checkData = await check(id, password);
    return {
      status: checkData.status,
      credentials_expired: checkData.credentials_expired,
      credentials_expire_after: checkData.credentials_expire_after,
      first_time_login: checkData.first_time_login
    };
  },
  getAccessTokenPayload: async (loginId, auth) => {    
    const db = require("../helpers/db/bankApi");

    const [ login ] = await db.login.get({ input_guid: loginId });
    const { type, email, username, auth_type, enabled } = login;
    let logins = [ loginId ];
    let privileges;

    // auth.loginGuid - last level (1)
    // loginId        - current level (2)
    // entities       - next level (3)

    if (type !== "admin") {
      privileges = await db.privilege.info({ input_login_guid: loginId });
      privileges = privileges.map(privilegeRow => ({ name: privilegeRow.name, guid: privilegeRow.guid }));
      if(!enabled)
        privileges = privileges.filter(privilege => privilege.name !== "CREATE_TRANSACTIONS")
    }
    const authGuid = auth && auth.loginGuid ? auth.loginGuid : null;
    const authLogins = auth && auth.logins ? auth.logins : null;
    let isCheckPrivs = true;
    if(!authGuid && type !== "admin")
      isCheckPrivs = privileges.map(privilege => privilege.name).includes("RELOGIN");
    if(type !== "admin" && isCheckPrivs) {
      if (authLogins) 
        logins.push(...authLogins);
      const entities = await db.login_entities.get({ input_guid: loginId });
      logins.push(...entities.map(entity => entity.guid));
    }
    logins = [ ...new Set(logins) ];
    return { type, email, username, logins, privileges, auth_type, loginGuid: loginId };
  },

  getRefreshTokenPayload: async (loginId, auth) => {    
    const db = require("../helpers/db/bankApi");

    const [ login ] = await db.login.get({ input_guid: loginId });
    const { type, email, username, auth_type, enabled } = login;
    let logins = [ loginId ];
    let privileges;

    if (type !== "admin") {
      privileges = await db.privilege.info({ input_login_guid: loginId });
      privileges = privileges.map(privilegeRow => ({ name: privilegeRow.name, guid: privilegeRow.guid }));
      if(!enabled)
        privileges = privileges.filter(privilege => privilege.name !== "CREATE_TRANSACTIONS")
    }
    const authGuid = auth && auth.loginGuid ? auth.loginGuid : null;
    const authLogins = auth && auth.logins ? auth.logins : null;
    let isCheckPrivs = true;
    if(!authGuid && type !== "admin")
      isCheckPrivs = privileges.map(privilege => privilege.name).includes("RELOGIN");
    if(type !== "admin" && isCheckPrivs) {
      if (authLogins) 
        logins.push(...authLogins);
      const entities = await db.login_entities.get({ input_guid: loginId });
      logins.push(...entities.map(entity => entity.guid));
    }
    logins = [ ...new Set(logins) ];
    return { type, email, username, logins, privileges, auth_type, loginGuid: loginId };
  },
};

module.exports.session = {
  serviceName: "Internet Bank",
  header: "Authorization",
  accessTokenTTL: "20m",
  refreshTokenTTL: "7d",
  timesToCheck: 3,
  get: async (id) => {
    const db = require("../helpers/db/bankApi");
    const [ session ] = await db.session.get({ input_login_guid: id });
    
    return session ? session : false;
  },
  upsert: async (session) => {
    const db = require("../helpers/db/bankApi");
    await db.session.upsert({
      input_login_guid: session.id,
      input_ip: session.ip,
      input_user_agent: session.user_agent,
      input_token: session.token,
      input_author_guid: session.id
    });
    await db.login.upsert({
      input_guid: session.id,
      input_last_login: new Date().toISOString().slice(0, 19).replace("T", " "),
      input_author: session.id
    })
  },
  delete: async (id) => {
    const db = require("../helpers/db/bankApi");
    await db.session.delete({
      input_login_guid: id
    });
    // return true;
  },
  getOtpSalt: async (id) => {
    const db = require("../helpers/db/bankApi");
    const [ login ] = await db.login.otp_get({ input_guid: id });
    return login ? { otpsalt: login.otpsalt, otpsaltstatus: login.otpsaltstatus, lastUpdate: login.updated_at } : {};
  },
  setOtpSalt: async (id, otpSalt, otpSaltStatus = "unconfirmed") => {
    const db = require("../helpers/db/bankApi");
    await db.login.upsert({
      input_guid: id, 
      input_otpsalt: otpSalt,
      input_otpsaltstatus: otpSaltStatus,
      input_auth_type: otpSaltStatus === "confirmed" ? "login-password-otp" : undefined,
      input_author_guid: id
    });
  },
  disableOtp: async (id) => {
    const db = require("../helpers/db/bankApi");
    await db.login.upsert({
      input_guid: id,
      input_auth_type: "login-password",
      input_otpsaltstatus: "unconfirmed",
      input_author_guid: id
    });

    return true;
  }
};

module.exports.mail = {
  send: async (otp, to, subject) => {
    const mailer = require("../helpers/mailer");

    try {

      const res = await mailer.send({
        text: `Enter the code: ${otp}`,
        subject: subject || "Code for authorization", 
        to
      })
      // const transporter = nodemailer.createTransport({
      //   service: "gmail",
      //   auth: {
      //     user: "validatormaila@gmail.com",
      //     pass: "trumpdonald"
      //   }
      // });
      // const mailOptions = {
      //   from: "Auth policy",
      //   to,
      //   subject: subject || "Code for authorization", 
      //   text: `Enter the code: ${otp}`
      // };
  
      // const res = await transporter.sendMail(mailOptions);
      if(res.response) {
        return {
          status: res.response
        };
      }
      else
        return {
          error: res
        };
    }
    catch(err) {
      return { error: "Two-factor authorization failed: mail cannot be send" };
    }
  }
};

module.exports.recoveryPasswordMail = {
  send: async (url, to, lang) => {
    const mailer = require("../helpers/mailer");

    try {
      const mailerResponse = await mailer.send({
        text: "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
        "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
         `${url} \n\nIf you did not request this, please ignore this email and your password will remain unchanged.\n`,
        subject: "Recovery Password",
        to
      });

      if (mailerResponse.status) return { status: mailerResponse.status };
      else return { error: mailerResponse.error };
    }
    catch(err) {
      console.log(err);
      return { error: "Recovery password  failed: mail cannot be send" };
    }
  }
};

module.exports.logger = async (action, req, auth) => {
  return true;
  // if (action === "login"){
  //   const db = require("../helpers/db/bankApi");

  //   await db.login.upsert({
  //     input_guid: auth.loginGuid,
  //     input_last_login: new Date().toISOString().slice(0, 19).replace("T", " ")
  //   }).catch(err => {
  //     console.error(err);
  //   });
  // }

  // const emitLog = require("../modules/logStorage/emitLog");
  // emitLog.emit("loginLogoutLog", req, auth);
};