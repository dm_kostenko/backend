const host = process.env.MONGO_LOG_HOST;
const port = parseInt(process.env.MONGO_LOG_PORT);
const dbName = process.env.MONGO_LOG_NAME;
const user = process.env.MONGO_LOG_USER;
const password = process.env.MONGO_LOG_PASSWORD;

module.exports.url = `mongodb://${host}:${port}/${dbName}`;
module.exports.dbOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  poolSize: 4
};