const fs = require("fs");

const appConfig = {
  // aes: {
  //   algorithm: "aes-256-cbc",
  //   password: fs.readFileSync(__dirname + "/keys/private.key", "utf8"),
  // },
  // antifraud: {
  //   bins: fs.readFileSync(__dirname + "/bins_iso_demo.csv", "utf8").split("\r\n")
  // },
  app: {
    port: parseInt(process.env.APP_PORT),
    guiUrl: process.env.APP_GUI_URL
  },
  db: {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD || "",
    name: process.env.DB_NAME
  },
  iban: {
    settlement: process.env.NODE_ENV === "prod" ? {
      internal: "LT813180027116000001",
      external: "LT031020105000031800"
    }
    : {
      internal: "LT283180110201010000",
      external: "LT731020105000031801"
    },
    customer: process.env.NODE_ENV === "prod" ? {
      internal: "LT223180027123000001",
      external: "LT931020103000031800"
    } 
    : {
      internal: "LT273180110201030000",
      external: "LT661020103000031801"
    },
    client_system: "00000000001"
  },
  bic: process.env.NODE_ENV === "prod" ? "TBFULT21XXX" : "TBFULT31XXX",
  companyCode: process.env.NODE_ENV === "prod" ? "31800" : "31801",
  // mongodb: {
  //   host: process.env.MONGO_HOST,
  //   port: parseInt(process.env.MONGO_PORT),
  //   dbName: process.env.MONGO_NAME,
  //   user: process.env.MONGO_USER,
  //   password: process.env.MONGO_PASSWORD
  // },
  // mongologdb: {
  //   host: process.env.MONGO_LOG_HOST,
  //   port: parseInt(process.env.MONGO_LOG_PORT),
  //   dbName: process.env.MONGO_LOG_NAME,
  //   user: process.env.MONGO_LOG_USER,
  //   password: process.env.MONGO_LOG_PASSWORD
  // },
  rsa: {
    privateKey: fs.readFileSync(__dirname + "/keys/private.key", "utf8"),
    publicKey: fs.readFileSync(__dirname + "/keys/public.pem", "utf8")
  },
  certs: {
    cert: fs.readFileSync(__dirname + "/certs/server.cert", "utf8"),
    key: fs.readFileSync(__dirname + "/certs/server.key", "utf8")
  },
  aml: {
    url: "https://api.complyadvantage.com/searches",
    apiKey: process.env.AML_API_KEY
  },
  mailer: {
    username: process.env.MAILER_USERNAME,
    password: process.env.MAILER_PASSWORD,
    awsRegion: process.env.AWS_REGION,
    senderMail: process.env.SENDER_MAIL
  },
  // testCard: {
  //   number: "4242424242424242",
  //   verification_value: "123",
  //   holder: "DAN BALAN",
  //   exp_month: "12",
  //   exp_year: "2020"
  // },
  // transactionTimeoutChecker: {
  //   maxPendingMinutes: 500,
  //   intervalMinutes: 30
  // }
};

if (process.env.NODE_ENV !== "test" )
  for (let type in appConfig)
    for (let prop in appConfig[type])
      if (appConfig[type][prop] === undefined) throw new Error(`Environment variable error: ${type} -> ${prop} undefined`);

module.exports = appConfig;