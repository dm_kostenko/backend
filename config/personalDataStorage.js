const fs = require("fs");
const mongoose = require("mongoose");
const crypto = require("crypto");

const host = process.env.MONGO_HOST;
const port = parseInt(process.env.MONGO_PORT);
const dbName = process.env.MONGO_NAME;
const user = process.env.MONGO_USER;
const password = process.env.MONGO_PASSWORD;
const pwd = fs.readFileSync(__dirname + "/keys/private.key", "utf-8");

module.exports.schema = new mongoose.Schema({
  company_name: {
    type: String,
    required: true
  },
  note: { type: String },
  security: {
    first_name: {
      type: String,
      required: true
    },
    last_name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    }
  }
});

module.exports.url = `mongodb://${host}:${port}/${dbName}`;

module.exports.dbOptions = {
  useNewUrlParser: true,
  poolSize: 4
};

module.exports.modelName = "user_profile";

module.exports.key = crypto.scryptSync(pwd, pwd.substr(0,32), 32);