const { schema, dbOptions, url, modelName, key } = require("../config").personalDataStorage;
const userProfileStorage = require("../modules/personalDataStorage");

if (process.env.NODE_ENV !== "test"){
  userProfileStorage.connect(url, schema, modelName, key, dbOptions)
    .catch(err => console.log(`\x1b[31m[personal-data-storage]: ${err}`));
}

module.exports = userProfileStorage;