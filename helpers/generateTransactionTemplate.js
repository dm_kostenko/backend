const db = require("./db/api/");
const { InvalidDataError } = require("../errorTypes");

module.exports = async (gatewayGuid, type) => {
  const [ templateRaw ] = await db.transaction.byTypeAndGatewayInfo({ input_gateway_guid: gatewayGuid, input_type: type });
  if (!templateRaw.length)
    throw new InvalidDataError(`Invalid settings for ${type} with gateway guid ${gatewayGuid}`);    
  const steps = templateRaw.map(step => {
    const { step_name, step_guid, step_type, step_number } = step;

    let params;
    if (step.step_param_name){
      const stepParamNames = step.step_param_name.split("---");
      const stepParamValues = step.step_param_value.split("---");
      
      params = stepParamNames.reduce((acc, name, index) => ({ ...acc, [name]: stepParamValues[index] }), {});
    }
    return { step_name, step_guid, step_type, step_number, params };
  });
  return { transaction_guid: templateRaw[0].transaction_guid, transaction_name: templateRaw[0].transaction_name, steps };
};