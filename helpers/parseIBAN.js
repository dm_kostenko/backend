const { companyCode, iban } = require("../config")

module.exports.checkIBANs = (creditorIBAN, debtorIBAN) => {
  let response = {}
  if(!creditorIBAN || !debtorIBAN)
    response.error = "Empty iban"
  else {
    // TBF account -> ...
    if([ iban.settlement.internal, iban.customer.internal ].includes(debtorIBAN))
      response.test = true
    // internal -> internal
    else if(creditorIBAN.slice(4, 9) === companyCode && ![ iban.settlement.internal, iban.customer.internal ].includes(creditorIBAN) && ![ iban.settlement.internal, iban.customer.internal ].includes(debtorIBAN))
      response.internal = true
  }
  return response
  
}