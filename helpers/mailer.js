const { InternalError } = require("../errorTypes/");

const nodemailer = require("nodemailer");
const { mailer } = require("../config");

// const AWS = require("aws-sdk");
// AWS.config.update({ region: mailer.awsRegion });  

// const awsSendMail = async (text, to, subject) => {  
//   const params = {
//     Destination: {
//       // CcAddresses: [ 'EMAIL_ADDRESS' ],
//       ToAddresses: [ to ]
//     },
//     Message: {
//       Body: {
//         Text: {
//           Charset: "UTF-8",
//           Data: text
//         }
//       },
//       Subject: {
//         Charset: "UTF-8",
//         Data: subject
//       }
//     },
//     Source: mailer.senderMail,
//   };

//   return new AWS.SES({ apiVersion: "2010-12-01" }).sendEmail(params).promise();
// }

// module.exports.awsSendMail = awsSendMail

const mailerSendMail = async (text, to, subject) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    requireTLS: true,
    auth: {
      user: mailer.username,
      pass: mailer.password
    },
    // tls: {
    //   rejectUnauthorized: false
    //   }
  });

  const mailOptions = {
    from: "Internet Bank",
    to,
    subject,
    text
  };

  return transporter.sendMail(mailOptions);
}

module.exports.mailerSendMail = mailerSendMail

  
module.exports.send = async ({ text, to, subject }) => {
  try {

    let response;
    if (process.env.NODE_ENV === "prod")
      // response = await awsSendMail(text, to, subject);
      response = await mailerSendMail(text, to, subject);
    else if (process.env.NODE_ENV === "dev"){
      response = await mailerSendMail(text, to, subject);
      if (!response.response) throw new Error(response);
    }
    else
      throw new InternalError("Ooops... Mail sending is disabled :(");
    return { status: JSON.stringify(response) };
  }
  catch(err) {
    throw new InternalError("Sending failed: mail cannot be send");
  }
};