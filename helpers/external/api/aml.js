const axios = require("axios")
const { aml } = require("../../../config")


module.exports.get = async ({ page, items, ...params }) => {
  // const from = items * (page - 1);
  // const to = parseInt(from) + parseInt(items);

  let { data: { content: { data: _data } } } = await axios({
    method: "GET",
    url: aml.url,
    params: {
      api_key: aml.apiKey,
      ...params
    }
  })

  // const count = _data.length;
  // _data = _data.slice(from, to);

  // for(let item of _data) {
  //   let { data: { content: { is_monitored } } } = await axios({
  //     method: "GET",
  //     url: `${aml.url}/${item.id}/monitors`,
  //     params: {
  //       api_key: aml.apiKey
  //     }
  //   })
  //   item.is_monitored = is_monitored
  // }
  return {
    data: _data
  }
}

module.exports.getById = async (id) => {
  try {
    let { data: { content: { data: _data } } } = await axios({
      method: "GET",
      url: `${aml.url}/${id}/details`,
      params: {
        api_key: aml.apiKey
      }
    })

    let { data: { content: { is_monitored } } } = await axios({
      method: "GET",
      url: `${aml.url}/${id}/monitors`,
      params: {
        api_key: aml.apiKey
      }
    })
    _data.is_monitored = is_monitored;
    return _data
  }
  catch(err) {
    throw(err)
  }
}

module.exports.post = async obj => {
  let responses = [];
  for(let i = 0; i < obj.terms.length; i++) {
    let request = {
      filters: {
        entity_type: obj.entityType.toLowerCase(),
      }
    }
    if(request.filters.entity_type === "person") {
      request.search_term = {
        last_name: obj.terms[i].lastName,
        first_name: obj.terms[i].name
      }
    }
    else {
      request.search_term = obj.terms[i].name
    }
    if(obj.terms[i].reference)
      request.client_ref = obj.terms[i].reference
    if(obj.fuzziness === "exact")
      request.exact_match = true
    else
      request.fuzziness = obj.fuzziness
    if(obj.terms[i].year)
      request.filters.birth_year = obj.terms[i].year
    if(obj.terms[i].selectedCountries && obj.terms[i].selectedCountries.length > 0)
      request.filters.country_codes = obj.terms[i].selectedCountries
    const { data: { content: { data: _data } } } = await axios({
      method: "POST",
      url: aml.url,
      params: {
        api_key: aml.apiKey
      },
      data: request
    })
    
    responses.push(_data)

    if(obj.terms[i].willMonitored) {
      const res = await axios({
        method: "PATCH",
        url: `${aml.url}/${_data.id}/monitors`,
        params: {
          api_key: aml.apiKey
        },
        data: { is_monitored: true }
      })
    }
    
  };
  return responses;
}

module.exports.patchMonitor = async ({ id, is_monitored }) => {
  const { data: { code, status } } = await axios({
    method: "PATCH",
    url: `${aml.url}/${id}/monitors`,
    params: {
      api_key: aml.apiKey
    },
    data: { is_monitored }
  })
  return { code, status };
}

module.exports.patchSearch = async ({ id, ...props }) => {
  let url = `${aml.url}/${id}`
  if(props.entities)
    url += "/entities"
  const { data: { code, status } } = await axios({
    method: "PATCH",
    url,
    params: {
      api_key: aml.apiKey
    },
    data: { ...props }
  })
  return { code, status };
}

module.exports.getCertificate = async (id) => {
  let url = `${aml.url}/${id}/certificate`
  const { data } = await axios({
    method: "GET",
    url,
    params: {
      api_key: aml.apiKey
    }
  })
  return data;
}