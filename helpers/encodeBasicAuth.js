module.exports = authorizationHeader => {
  if (authorizationHeader) {
    const authData = authorizationHeader.replace(/^Basic/, "");
    const [ username, password ] = Buffer.from(authData, "base64").toString("utf8").split(":");
    return [ username, password ];
  } else return [];
};