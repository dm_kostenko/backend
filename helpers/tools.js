module.exports.decodeBasicAuth = authorizationHeader => {
  const authData = authorizationHeader.replace(/^Basic/, "");
  const [ username, password ] = Buffer.from(authData, "base64").toString("utf8").split(":");
  return [ username, password ];
};

module.exports.generateSecret = (length = 64) => require("crypto").randomBytes(length).toString("hex");

const getDescendantProp = (obj, desc) => {
  let arr = desc.split(".");
  while (arr.length && (obj = obj[arr.shift()]));
  return typeof(obj) === "object"? JSON.stringify(obj) : obj;
};

module.exports.fillHTMLTemplate = (template, data) => {
  const matches = template.match(/[\\]?["]{1}[{]{2}[^{}]+[}]{2}[\\]?["]{1}/g)
    .map(variable => {
      let value = variable[0] === "\\" ? getDescendantProp(data, variable.slice(4,-4)) : getDescendantProp(data, variable.slice(3,-3));
      if (typeof(value) !== "number" && variable[0] !== "\\") 
        value = `"${value}"`;
      return [ variable, value ]; //if string - quotes, if number or with backslash - without quotes
    }).reduce((accumulator, pair) => accumulator.replace(pair[0], pair[1]), template);
  return matches;
};
module.exports.fillForm = (map, data) => {
  const newObj = JSON.parse(JSON.stringify(map));
  const func = obj => Object.keys(obj).forEach(prop => typeof(obj[prop]) === "object" ? func(obj[prop], data) : obj[prop] = getDescendantProp(data, obj[prop]));
  func(newObj);
  return newObj;
};

module.exports.findDeepNestedProperty = (data, targetProp, targetValue, results) => {
  const getObject = obj => {
    if (obj instanceof Array) 
      obj.forEach(obj => getObject(obj));
    else 
      for (let prop in obj)
        if (prop === targetProp && obj[prop] === targetValue) 
          results.push(obj);
        else if (obj[prop] instanceof Object || obj[prop] instanceof Array)
          getObject(obj[prop]);
  };
  getObject(data);
};

module.exports.getDescendantProp = getDescendantProp;