/* 
  TARGET2 is open every day, with the exception of: 
    Saturdays; 
    Sundays;
    New Year’s Day;
    Good Friday and Easter Monday;
    1 May (Labour Day);
    Christmas Day;
    and 26 December. 
*/
const moment = require("moment");
const momentHoliday = require("moment-holiday");

moment.modifyHolidays.set({
  "Saturday": { date: "(6)" },
  "Sunday": { date: "(0)" },
  "New Year’s Day": { date: "1/1" },
  "Good Friday": { date: "easter-2" },
  "Easter Monday": { date: "easter+1" },
  "1 May (Labour Day)": { date: "5/1" },
  "Christmas Day": { date: "12/25" },
  "26 December": { date: "12/26" },
});

const calcTime = (date = new Date()) => {
  const offset = date.getTimezoneOffset();
  return new Date(date.getTime() - offset * 60000);
};


const getCETDate = (d = calcTime()) => {
  return new Date(d.toLocaleString('en-US', {
    timeZone: 'Europe/Amsterdam',
    hour12: false
  }).replace(",",""));
}

const isWorkingTime = (d = calcTime()) => {
  const CETDate = getCETDate(d);
  const CETHours = CETDate.getUTCHours();
  return CETHours >= 7 && CETHours < 15;
};

const isHoliday = (d = calcTime()) => {
  const CETTime = getCETDate(d);
  return momentHoliday(CETTime).isHoliday();
};

const getNextWorkingDay = () => {
  let CETDate = getCETDate();
  const CETHours = CETDate.getHours();
  // if(CETHours >= 7 && CETHours < 15 && !momentHoliday(CETDate).isHoliday())
  //   return CETDate.toISOString().slice(0,10);
  // else {
    CETDate.setDate(CETDate.getDate() + 1);
    CETDate.setHours(7);
  // }
  while(true) {    
    if(!momentHoliday(CETDate).isHoliday())
      return CETDate.toISOString().slice(0,10);
    else
      CETDate.setDate(CETDate.getDate() + 1);
  }
};

module.exports = { 
  getCETDate,
  getDateTime: () => getCETDate().toISOString().slice(0, 19),
  isHoliday, 
  isWorkingTime, 
  getNextWorkingDay,
  calcTime
};