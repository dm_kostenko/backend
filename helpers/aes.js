const crypto = require("crypto");

const { algorithm, password } = require("../config").aes;
const key = crypto.scryptSync(password, password.substr(0,32), 32);
const iv = Buffer.alloc(16, 0);

module.exports.encode = (decrypted) => {
  const cipher = crypto.createCipheriv(algorithm, key, iv);
  let encrypted = cipher.update(decrypted, "utf8", "hex");
  encrypted += cipher.final("hex");
  return encrypted;
};

module.exports.decode = (encrypted) => {
  try {
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    let decrypted = decipher.update(encrypted, "hex", "utf8");
    decrypted += decipher.final("utf8");
    return decrypted;    
  } catch (error) {
    return undefined; 
  }
};