const { Pool } = require("pg");
const { host, port, user, password, name } = require("../../config").db;

const config = {
  host,
  port,
  user,
  password,
  database: name
};

const pool = new Pool(config);
// const pool = database.createPool(config);
// const getConnection = () => database.createConnection(config);
const getConnection = () => pool.connect();

module.exports = { pool, getConnection };