const { dbquery } = require("../api");

module.exports.info = ({
  input_gateway_guid,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "gateway_prop_info";
  const args = [
    input_gateway_guid,
    input_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_gateway_guid,
  input_name,
  input_label,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "gateway_prop_get";
  const args = [
    input_gateway_guid,
    input_name,
    input_label,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_gateway_guid,
  input_name,
  input_label,
  input_author_guid
}, connection) => {
  const type = "gateway_prop_upsert";
  const args = [
    input_gateway_guid,
    input_name,
    input_label,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_gateway_guid,
  input_name
}, connection) => {
  const type = "gateway_prop_delete";
  const args = [
    input_gateway_guid,
    input_name
  ];
  return dbquery(type, args, connection);
};
