const { dbquery } = require("../api");

module.exports.get = ({
  input_shop_guid,
  input_gateway_guid,
  input_billing_descriptor,
  input_routing_string,
  input_payment_amount_limit,
  input_monthly_amount_limit,
  input_supported_brands,
  input_supported_currencies,
  input_generate_statement,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_gateway_get";
  const args = [
    input_shop_guid,
    input_gateway_guid,
    input_billing_descriptor,
    input_routing_string,
    input_payment_amount_limit,
    input_monthly_amount_limit,
    input_supported_brands,
    input_supported_currencies,
    input_generate_statement,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_shop_guid,
  input_gateway_guid,
  input_billing_descriptor,
  input_routing_string,
  input_payment_amount_limit,
  input_monthly_amount_limit,
  input_supported_brands,
  input_supported_currencies,
  input_generate_statement,
  input_enabled,
  input_author_guid
}, connection) => {
  const type = "shop_gateway_upsert";
  const args = [
    input_shop_guid,
    input_gateway_guid,
    input_billing_descriptor,
    input_routing_string,
    input_payment_amount_limit,
    input_monthly_amount_limit,
    input_supported_brands,
    input_supported_currencies,
    input_generate_statement,
    input_enabled,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_shop_guid,
  input_gateway_guid,
}, connection) => {
  const type = "shop_gateway_delete";
  const args = [
    input_shop_guid,
    input_gateway_guid
  ];
  return dbquery(type, args, connection);
};
