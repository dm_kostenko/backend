const { dbquery } = require("../api");

module.exports.byLoginInfo = ({
  input_login_guid,
  input_guid,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "default_role_by_login_info";
  const args = [
    input_login_guid,
    input_guid,
    input_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};