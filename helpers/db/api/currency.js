const { dbquery } = require("../api");

module.exports.info = ({
  input_guid,
  input_name,
  input_code,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "currency_info";
  const args = [
    input_guid,
    input_name,
    input_code,
    input_page_number,
    input_items_count
  ];
  return dbquery(type,args, connection);
};

module.exports.get = ({
  input_guid,
  input_name,
  input_code,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "currency_get";
  const args = [
    input_guid,
    input_name,
    input_code,
    input_page_number,
    input_items_count
  ];
  return dbquery(type,args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_name,
  input_code,
  input_author_guid
}, connection) => {
  const type = "currency_upsert";
  const args = [
    input_guid,
    input_name,
    input_code,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "currency_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
