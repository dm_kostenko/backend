const { dbquery } = require("../api");

module.exports.get = ({
  input_login_guid,
  input_group_guid,
  input_role_guid,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "login_group_get";
  const args = [
    input_login_guid,
    input_group_guid,
    input_role_guid,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_login_guid,
  input_group_guid,
  input_role_guid,
  input_author_guid
}, connection) => {
  const type = "login_group_upsert";
  const args = [
    input_login_guid,
    input_group_guid,
    input_role_guid,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_login_guid,
  input_group_guid
}, connection) => {
  const type = "login_group_delete";
  const args = [
    input_login_guid,
    input_group_guid
  ];
  return dbquery(type, args, connection);
};
