const { dbquery } = require("../api");

module.exports.byShopInfo = ({
  input_shop_guid,
  input_guid,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "gateway_by_shop_info";
  const args = [
    input_shop_guid,
    input_guid,
    input_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "gateway_info";
  const args = [
    input_guid,
    input_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_name,
  input_description,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "gateway_get";
  const args = [
    input_guid,
    input_name,
    input_description,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_name,
  input_description,
  input_author_guid
}, connection) => {
  const type = "gateway_upsert";
  const args = [
    input_guid,
    input_name,
    input_description,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "gateway_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
