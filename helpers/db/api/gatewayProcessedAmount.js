const { dbquery } = require("../api");

module.exports.info = ({
  input_account_guid
}, connection) => {
  const type = "gateway_processed_amount";
  const args = [
    input_account_guid
  ];
  return dbquery(type, args, connection);
};