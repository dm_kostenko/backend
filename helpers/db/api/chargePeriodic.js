const { dbquery } = require("../api");

module.exports.info = ({
  input_shop_guid,
  input_shop_name,
  input_currency_guid,
  input_currency_name,
  input_gateway_guid,
  input_gateway_name,
  input_name,
  input_flat_rate,
  input_percent,
  input_period,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "charge_periodic_info";
  const args = [
    input_shop_guid,
    input_shop_name,
    input_currency_guid,
    input_currency_name,
    input_gateway_guid,
    input_gateway_name,
    input_name,
    input_flat_rate,
    input_percent,
    input_period,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid,
  input_name,
  input_flat_rate,
  input_percent,
  input_period,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "charge_periodic_get";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid,
    input_name,
    input_flat_rate,
    input_percent,
    input_period,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid,
  input_name,
  input_flat_rate,
  input_percent,
  input_period,
  input_author_guid
}, connection) => {
  const type = "charge_periodic_upsert";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid,
    input_name,
    input_flat_rate,
    input_percent,
    input_period,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid
}, connection) => {
  const type = "charge_periodic_delete";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid
  ];
  return dbquery(type, args, connection);
};
