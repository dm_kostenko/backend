const { dbquery } = require("../api");

module.exports.info = ({
  input_entity,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "free_logins";
  const args = [
    input_entity,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};