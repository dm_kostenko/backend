const { dbquery } = require("../api");

module.exports.verify = ({
  input_guid,
  input_secret
}, connection) => {
  const type = "shop_verify";
  const args = [
    input_guid,
    input_secret
  ];
  return dbquery(type, args, connection);
};

module.exports.byGatewayInfo = ({
  input_gateway_guid,
  input_guid,
  input_name,
  input_merchant_name,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_by_gateway_info";
  const args = [
    input_gateway_guid,
    input_guid,
    input_name,
    input_merchant_name,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.byGroupInfo = ({
  input_group_guid,
  input_guid,
  input_name,
  input_merchant_name,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_by_group_info";
  const args = [
    input_group_guid,
    input_guid,
    input_name,
    input_merchant_name,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.byMerchantInfo = ({
  input_merchant_guid,
  input_guid,
  input_name,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_by_merchant_info";
  const args = [
    input_merchant_guid,
    input_guid,
    input_name,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.byPartnerInfo = ({
  input_partner_guid,
  input_guid,
  input_name,
  input_merchant_name,
  input_group_name,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_by_partner_info";
  const args = [
    input_partner_guid,
    input_guid,
    input_name,
    input_merchant_name,
    input_group_name,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid,
  input_merchant_name,
  input_name,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_info";
  const args = [
    input_guid,
    input_merchant_name,
    input_name,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_merchant_guid,
  input_name,
  input_url,
  input_email,
  input_phone,
  input_enabled,
  input_secret,
  input_note,
  input_hash_key,
  input_enable_checkout,
  input_checkout_method,
  input_antiFraudMonitor,
  input_antiFraudMonitorValue,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_get";
  const args = [
    input_guid,
    input_merchant_guid,
    input_name,
    input_url,
    input_email,
    input_phone,
    input_enabled,
    input_secret,
    input_note,
    input_hash_key,
    input_enable_checkout,
    input_checkout_method,
    input_antiFraudMonitor,
    input_antiFraudMonitorValue,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = (
  {
    input_guid,
    input_merchant_guid,
    input_name,
    input_url,
    input_email,
    input_phone,
    input_enabled,
    input_secret,
    input_note,
    input_hash_key,
    input_enable_checkout,
    input_checkout_method,
    input_antiFraudMonitor,
    input_antiFraudMonitorValue,
    input_author_guid
  }, connection) => {
  const type = "shop_upsert";
  const args = [
    input_guid,
    input_merchant_guid,
    input_name,
    input_url,
    input_email,
    input_phone,
    input_enabled,
    input_secret,
    input_note,
    input_hash_key,
    input_enable_checkout,
    input_checkout_method,
    input_antiFraudMonitor,
    input_antiFraudMonitorValue,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = (
  {
    input_guid
  }, connection) => {
  const type = "shop_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
