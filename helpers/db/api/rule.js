const { dbquery } = require("../api");

module.exports.byBlacklistRuleInfo = ({
  input_blacklist_rule_guid,
  input_name,
  input_type,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "rule_by_blacklist_rule_info";
  const args = [
    input_blacklist_rule_guid,
    input_name,
    input_type,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_blacklist_rule_guid,
  input_value,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "rule_get";
  const args = [
    input_blacklist_rule_guid,
    input_value,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_blacklist_rule_guid,
  input_value,
  input_author_guid
}, connection) => {
  const type = "rule_upsert";
  const args = [
    input_blacklist_rule_guid,
    input_value,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_blacklist_rule_guid,
  input_value
}, connection) => {
  const type = "rule_delete";
  const args = [
    input_blacklist_rule_guid,
    input_value
  ];
  return dbquery(type, args, connection);
};
