const { dbquery } = require("../api");

module.exports.info = ({
  input_merchant_guid
}, connection) => {
  const type = "merchant_processed_amount";
  const args = [
    input_merchant_guid
  ];
  return dbquery(type, args, connection);
};