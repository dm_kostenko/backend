const { dbquery } = require("../../api");

module.exports.get = (connection) => {
  const type = "report_amount_of_partners";
  const args = [];
  return dbquery(type, args, connection);
};