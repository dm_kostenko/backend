const { dbquery } = require("../../api");

module.exports.get = ({
  input_partner_guid
}, connection) => {
  const type = "report_amount_of_groups";
  const args = [
    input_partner_guid
  ];
  return dbquery(type, args, connection);
};