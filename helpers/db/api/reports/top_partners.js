const { dbquery } = require("../../api");

module.exports.get = ({
  input_from_date,
  input_to_date,
  input_days,
  input_count
}, connection) => {
  const type = "report_top_partners";
  const args = [
    input_from_date,
    input_to_date,
    input_days,
    input_count
  ];
  return dbquery(type, args, connection);
};