const { dbquery } = require("../../api");

module.exports.get = ({
  input_partner_guid,
  input_group_guid,
  input_merchant_guid,
  input_from_date,
  input_to_date,
  input_days,
  input_count
}, connection) => {
  const type = "report_top_shops";
  const args = [
    input_partner_guid,
    input_group_guid,
    input_merchant_guid,
    input_from_date,
    input_to_date,
    input_days,
    input_count
  ];
  return dbquery(type, args, connection);
};