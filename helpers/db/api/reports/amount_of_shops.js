const { dbquery } = require("../../api");

module.exports.get = ({
  input_partner_guid,
  input_group_guid,
  input_merchant_guid
}, connection) => {
  const type = "report_amount_of_shops";
  const args = [
    input_partner_guid,
    input_group_guid,
    input_merchant_guid
  ];
  return dbquery(type, args, connection);
};