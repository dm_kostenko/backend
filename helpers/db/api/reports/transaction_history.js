const { dbquery } = require("../../api");

module.exports.get = ({
  input_guid,
  input_status,
  input_type,
  input_partner_guid,
  input_partner_name,
  input_group_guid,
  input_group_name,
  input_merchant_guid,
  input_merchant_name,
  input_shop_guid,
  input_shop_name,
  input_account_guid,
  input_account_number,
  input_from_date,
  input_to_date,
  input_days,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "report_transaction_history";
  const args = [
    input_guid,
    input_status,
    input_type,
    input_partner_guid,
    input_partner_name,
    input_group_guid,
    input_group_name,
    input_merchant_guid,
    input_merchant_name,
    input_shop_guid,
    input_shop_name,
    input_account_guid,
    input_account_number,
    input_from_date,
    input_to_date,
    input_days,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};