const { dbquery } = require("../../api");

module.exports.get = ({
  input_partner_guid,
  input_group_guid,
  input_merchant_guid,
  input_shop_guid,
  input_from_date,
  input_to_date,
  input_days
}, connection) => {
  const type = "report_total_to_client";
  const args = [
    input_partner_guid,
    input_group_guid,
    input_merchant_guid,
    input_shop_guid,
    input_from_date,
    input_to_date,
    input_days
  ];
  return dbquery(type, args, connection);
};