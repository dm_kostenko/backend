const { dbquery } = require("../../api");

module.exports.get = ({
  input_from_date,
  input_to_date,
  input_days
}, connection) => {
  const type = "report_new_clients";
  const args = [
    input_from_date,
    input_to_date,
    input_days
  ];
  return dbquery(type, args, connection);
};