const { dbquery } = require("../../api");

module.exports.get = ({
  input_status,
  input_from_date,
  input_to_date,
  input_days,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "report_orders";
  const args = [
    input_status,
    input_from_date,
    input_to_date,
    input_days,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};