const { dbquery } = require("../../api");

module.exports.get = ({
  input_partner_guid,
  input_group_guid,
  input_merchant_guid,
  input_shop_guid,
  input_from_date,
  input_to_date,
  input_days,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "report_totals";
  const args = [
    input_partner_guid,
    input_group_guid,
    input_merchant_guid,
    input_shop_guid,
    input_from_date,
    input_to_date,
    input_days,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};