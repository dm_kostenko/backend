const { dbquery } = require("../../api");

module.exports.get = ({
  input_partner_guid,
  input_group_guid,
  input_merchant_guid,
  input_shop_guid
}, connection) => {
  const type = "report_daily_transactions";
  const args = [
    input_partner_guid,
    input_group_guid,
    input_merchant_guid,
    input_shop_guid
  ];
  return dbquery(type, args, connection);
};