const { dbquery } = require("../api");

module.exports.byGroupInfo = ({
  input_group_guid,
  input_guid,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "merchant_by_group_info";
  const args = [
    input_group_guid,
    input_guid,
    input_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.byPartnerInfo = ({
  input_partner_guid,
  input_guid,
  input_name,
  input_group_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "merchant_by_partner_info";
  const args = [
    input_partner_guid,
    input_guid,
    input_name,
    input_group_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid,
  input_name,
  input_group_name,
  input_partner_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "merchant_info";
  const args = [
    input_guid,
    input_name,
    input_group_name,
    input_partner_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_name,
  input_type,
  input_group_guid,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "merchant_get";
  const args = [
    input_guid,
    input_name,
    input_type,
    input_group_guid,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_name,
  input_type,
  input_group_guid,
  input_monthly_amount_limit,
  input_author_guid
}, connection) => {
  const type = "merchant_upsert";
  const args = [
    input_guid,
    input_name,
    input_type,
    input_group_guid,
    input_monthly_amount_limit,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "merchant_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
