const { dbquery } = require("../api");

module.exports.get = ({
  input_step_processing_guid,
  input_name,
  input_value,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "step_param_processing_get";
  const args = [
    input_step_processing_guid,
    input_name,
    input_value,
    input_page_number,
    input_items_count    
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_step_processing_guid,
  input_name,
  input_value,
  input_author_guid
}, connection) => {
  const type = "step_param_processing_upsert";
  const args = [
    input_step_processing_guid,
    input_name,
    input_value,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_step_processing_guid,
  input_name 
}, connection) => {
  const type = "step_param_processing_delete";
  const args = [
    input_step_processing_guid,
    input_name 
  ];
  return dbquery(type, args, connection);
};
