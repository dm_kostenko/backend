const { dbquery } = require("../api");

module.exports.get = ({
  input_transaction_processing_guid,
  input_base_amount,
  input_to_processor,
  input_to_bank,
  input_to_client,
  input_hold,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_overview_get";
  const args = [
    input_transaction_processing_guid,
    input_base_amount,
    input_to_processor,
    input_to_bank,
    input_to_client,
    input_hold,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_transaction_processing_guid,
  input_base_amount,
  input_to_processor,
  input_to_bank,
  input_to_client,
  input_hold,
  input_author_guid
}, connection) => {
  const type = "transaction_overview_upsert";
  const args = [
    input_transaction_processing_guid,
    input_base_amount,
    input_to_processor,
    input_to_bank,
    input_to_client,
    input_hold,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_transaction_processing_guid
}, connection) => {
  const type = "transaction_overview_delete";
  const args = [
    input_transaction_processing_guid
  ];
  return dbquery(type, args, connection);
};
