module.exports = {
  getConnection: require("../dbconnection").getConnection,
  account: require("./account"),
  card: require("./card"),
  currency: require("./currency"),
  defaultLoginRole: require("./defaultLoginRole"),
  defaultRole: require("./defaultRole"),
  blacklistGlobal: require("./blacklistGlobal"),
  blacklistMerchant: require("./blacklistMerchant"),
  blacklistRule: require("./blacklistRule"),
  gateway: require("./gateway"),
  gatewayProp: require("./gatewayProp"),
  group: require("./group"),
  login: require("./login"),
  free_logins: require("./free_logins"),
  loginAccount: require("./loginAccount"),
  loginGroup: require("./loginGroup"),
  loginMerchant: require("./loginMerchant"),
  loginPartner: require("./loginPartner"),
  merchant: require("./merchant"),
  partner: require("./partner"),
  privilege: require("./privilege"),
  rate: require("./rate"),
  chargeOneTime: require("./chargeOneTime"),
  chargePeriodic: require("./chargePeriodic"),
  chargeConditional: require("./chargeConditional"),
  role: require("./role"),
  rolePrivilege: require("./rolePrivilege"),
  securePayment: require("./securePayment"),
  rule: require("./rule"),
  session: require("./session"),
  shop: require("./shop"),
  shopAccount: require("./shopAccount"),
  shopGateway: require("./shopGateway"),
  shopGatewayProp: require("./shopGatewayProp"),
  step: require("./step"),
  stepParam: require("./stepParam"),
  stepParamProcessing: require("./stepParamProcessing"),
  stepProcessing: require("./stepProcessing"),
  transaction: require("./transaction"),
  transactionProcessing: require("./transactionProcessing"),
  transactionStep: require("./transactionStep"),
  transactionRate: require("./transactionRate"),
  transactionOverview: require("./transactionOverview"),
  gatewayProcessedAmount: require("./gatewayProcessedAmount"),
  merchantLimit: require("./merchantLimit"),
  merchantProcessedAmount: require("./merchantProcessedAmount"),
  
  report_amount_of_groups: require ("./reports/amount_of_groups"),
  report_amount_of_merchants: require ("./reports/amount_of_merchants"),
  report_amount_of_partners: require ("./reports/amount_of_partners"),
  report_amount_of_shops: require ("./reports/amount_of_shops"),
  report_balance: require ("./reports/balance"),
  report_currencies_of_transactions: require ("./reports/currencies_of_transactions"),
  report_daily_transactions: require ("./reports/daily_transactions"),
  report_new_clients: require("./reports/new_clients"),
  report_orders: require ("./reports/orders"),
  report_shop_totals: require ("./reports/shop_totals"),
  report_step_logs: require ("./reports/step_logs"),
  report_top_groups: require ("./reports/top_groups"),
  report_top_merchants: require ("./reports/top_merchants"),
  report_top_partners: require ("./reports/top_partners"),
  report_top_shops: require ("./reports/top_shops"),
  report_totals: require ("./reports/totals"),
  report_total_processed: require ("./reports/total_processed"),
  report_total_to_client: require ("./reports/total_to_client"),
  report_total_to_processor: require ("./reports/total_to_processor"),
  report_transaction_history: require ("./reports/transaction_history"),
  report_transaction_logs: require ("./reports/transaction_logs"),
  report_transaction_types: require ("./reports/transaction_types")
};