const { dbquery } = require("../api");

module.exports.info = ({
  input_guid,
  input_transaction_guid,
  input_transaction_type,
  input_account_guid,
  input_amount,
  input_currency,
  input_status,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_processing_info";
  const args = [
    input_guid,
    input_transaction_guid,
    input_transaction_type,
    input_account_guid,
    input_amount,
    input_currency,
    input_status,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_transaction_guid,
  input_account_guid,
  input_status,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_processing_get";
  const args = [
    input_guid,
    input_transaction_guid,
    input_account_guid,
    input_status,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_transaction_guid,
  input_account_guid,
  input_status,
  input_author_guid,
}, connection) => {
  const type = "transaction_processing_upsert";
  const args = [
    input_guid,
    input_transaction_guid,
    input_account_guid,
    input_status,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "transaction_processing_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};
