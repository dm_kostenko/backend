const { dbquery } = require("../api");

module.exports.info = ({
  input_shop_guid,
  input_shop_name,
  input_currency_guid,
  input_currency_name,
  input_gateway_guid,
  input_gateway_name,
  input_hold_rate,
  input_hold_period,
  input_apply_from,
  input_apply_to,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "rate_info";
  const args = [
    input_shop_guid,
    input_shop_name,
    input_currency_guid,
    input_currency_name,
    input_gateway_guid,
    input_gateway_name,
    input_hold_rate,
    input_hold_period,
    input_apply_from,
    input_apply_to,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid,
  input_hold_rate,
  input_hold_period,
  input_apply_from,
  input_apply_to,
  input_note,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "rate_get";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid,
    input_hold_rate,
    input_hold_period,
    input_apply_from,
    input_apply_to,
    input_note,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid,
  input_hold_rate,
  input_hold_period,
  input_apply_from,
  input_apply_to,
  input_note,
  input_author_guid
}, connection) => {
  const type = "rate_upsert";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid,
    input_hold_rate,
    input_hold_period,
    input_apply_from,
    input_apply_to,
    input_note,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid
}, connection) => {
  const type = "rate_delete";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid
  ];
  return dbquery(type, args, connection);
};
