const { dbquery } = require("../api");

module.exports.byLoginInfo = ({
  input_login_guid,
  input_guid,
  input_currency_name,
  input_number,
  input_balance,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "account_by_login_info";
  const args = [
    input_login_guid,
    input_guid,
    input_currency_name,
    input_number,
    input_balance,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.byShopInfo = ({
  input_shop_guid,
  input_guid,
  input_currency_name,
  input_number,
  input_balance,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "account_by_shop_info";
  const args = [
    input_shop_guid,
    input_guid,
    input_currency_name,
    input_number,
    input_balance,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid,
  input_currency_name,
  input_number,
  input_balance,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "account_info";
  const args = [
    input_guid,
    input_currency_name,
    input_number,
    input_balance,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_currency_guid,
  input_number,
  input_balance,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "account_get";
  const args = [
    input_guid,
    input_currency_guid,
    input_number,
    input_balance,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_currency_guid,
  input_number,
  input_balance,
  input_author_guid
}, connection) => {
  const type = "account_upsert";
  const args = [
    input_guid,
    input_currency_guid,
    input_number,
    input_balance,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "account_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
