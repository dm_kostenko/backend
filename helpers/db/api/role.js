const { dbquery } = require("../api");

module.exports.byLoginInfo = ({
  input_login_guid
}, connection) => {
  const type = "role_by_login_info";
  const args = [
    input_login_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid,
  input_name,
  input_type,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "role_info";
  const args = [
    input_guid,
    input_name,
    input_type,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_name,
  input_type,
  input_description,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "role_get";
  const args = [
    input_guid,
    input_name,
    input_type,
    input_description,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_name,
  input_type,
  input_description,
  input_author_guid
}, connection) => {
  const type = "role_upsert";
  const args = [
    input_guid,
    input_name,
    input_type,
    input_description,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "role_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
