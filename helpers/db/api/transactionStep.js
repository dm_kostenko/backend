const { dbquery } = require("../api");

module.exports.get = ({
  input_transaction_guid,
  input_step_guid,
  input_number,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_step_get";
  const args = [
    input_transaction_guid,
    input_step_guid,
    input_number,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_transaction_guid,
  input_step_guid,
  input_number,
  input_author_guid
}, connection) => {
  const type = "transaction_step_upsert";
  const args = [
    input_transaction_guid,
    input_step_guid,
    input_number,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_transaction_guid,
  input_step_guid
}, connection) => {
  const type = "transaction_step_delete";
  const args = [
    input_transaction_guid,
    input_step_guid
  ];
  return dbquery(type, args, connection);
};
