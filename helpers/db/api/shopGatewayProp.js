const { dbquery } = require("../api");

module.exports.info = ({
  input_shop_guid,
  input_gateway_guid
}, connection) => {
  const type = "shop_gateway_prop_info";
  const args = [
    input_shop_guid,
    input_gateway_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_shop_guid,
  input_gateway_guid,
  input_name,
  input_value,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_gateway_prop_get";
  const args = [
    input_shop_guid,
    input_gateway_guid,
    input_name,
    input_value,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_shop_guid,
  input_gateway_guid,
  input_name,
  input_value,
  input_author_guid
}, connection) => {
  const type = "shop_gateway_prop_upsert";
  const args = [
    input_shop_guid,
    input_gateway_guid,
    input_name,
    input_value,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_shop_guid,
  input_gateway_guid,
  input_name
}, connection) => {
  const type = "shop_gateway_prop_delete";
  const args = [
    input_shop_guid,
    input_gateway_guid,
    input_name
  ];
  return dbquery(type, args, connection);
};
