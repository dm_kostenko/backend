const { dbquery } = require("../api");

module.exports.info = ({
  input_blacklist_rule_guid,
  input_type,
  input_blacklist_rule_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "blacklist_global_info";
  const args = [
    input_blacklist_rule_guid,
    input_type,
    input_blacklist_rule_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_blacklist_rule_guid,
  input_type,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "blacklist_global_get";
  const args = [
    input_blacklist_rule_guid,
    input_type,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_blacklist_rule_guid,
  input_type,
  input_author_guid
}, connection) => {
  const type = "blacklist_global_upsert";
  const args = [
    input_blacklist_rule_guid,
    input_type,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_blacklist_rule_guid
}, connection) => {
  const type = "blacklist_global_delete";
  const args = [
    input_blacklist_rule_guid
  ];
  return dbquery(type, args, connection);
};
