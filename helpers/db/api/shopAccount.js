const { dbquery } = require("../api");

module.exports.get = ({
  input_shop_guid,
  input_account_guid,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "shop_account_get";
  const args = [
    input_shop_guid,
    input_account_guid,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_shop_guid,
  input_account_guid,
  input_author_guid
}, connection) => {
  const type = "shop_account_upsert";
  const args = [
    input_shop_guid,
    input_account_guid,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_shop_guid,
  input_account_guid
}, connection) => {
  const type = "shop_account_delete";
  const args = [
    input_shop_guid,
    input_account_guid
  ];
  return dbquery(type, args, connection);
};
