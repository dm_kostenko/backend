const { dbquery } = require("../api");

module.exports.byTransactionInfo = ({
  input_transaction_guid,
  input_number,
  input_step_guid,
  input_step_name,
  input_step_type,
  input_gateway_guid,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "step_by_transaction_info";
  const args = [
    input_transaction_guid,
    input_number,
    input_step_guid,
    input_step_name,
    input_step_type,
    input_gateway_guid,
    input_page_number,
    input_items_count    
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid, 
  input_name, 
  input_type, 
  input_gateway_guid, 
  input_page_number, 
  input_items_count
}, connection) => {
  const type = "step_info";
  const args = [
    input_guid, 
    input_name, 
    input_type, 
    input_gateway_guid, 
    input_page_number, 
    input_items_count 
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_name,
  input_type,
  input_gateway_guid,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "step_get";
  const args = [
    input_guid,
    input_name,
    input_type,
    input_gateway_guid,
    input_page_number, 
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_name,
  input_type,
  input_gateway_guid,
  input_author_guid
}, connection) => {
  const type = "step_upsert";
  const args = [
    input_guid,
    input_name,
    input_type,
    input_gateway_guid,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid 
}, connection) => {
  const type = "step_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};
