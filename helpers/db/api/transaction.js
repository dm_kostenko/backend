const { dbquery } = require("../api");

module.exports.byTypeAndGatewayInfo = ({
  input_type, 
  input_gateway_guid 
}, connection) => {
  const type = "transaction_by_type_and_gateway_info";
  const args = [
    input_type, 
    input_gateway_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.byGatewayGuid = ({
  input_gateway_guid 
}, connection) => {
  const type = "transaction_type_by_gateway";
  const args = [
    input_gateway_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid,
  input_type,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_info";
  const args = [
    input_guid,
    input_type,
    input_name,
    input_page_number,
    input_items_count  
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_type,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_get";
  const args = [
    input_guid,
    input_type,
    input_name,
    input_page_number,
    input_items_count  
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_type,
  input_name,
  input_author_guid
}, connection) => {
  const type = "transaction_upsert";
  const args = [
    input_guid,
    input_type,
    input_name,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "transaction_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};
