const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_shop_guid,
  input_auth_key,
  input_data,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "secure_payment_get";
  const args = [
    input_guid,
    input_shop_guid,
    input_auth_key,
    input_data,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_shop_guid,
  input_auth_key,
  input_data,
  input_author_guid
}, connection) => {
  const type = "secure_payment_upsert";
  const args = [
    input_guid,
    input_shop_guid,
    input_auth_key,
    input_data,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_auth_key
}, connection) => {
  const type = "secure_payment_delete";
  const args = [ input_auth_key ];
  return dbquery(type, args, connection);
};
