const { dbquery } = require("../api");

module.exports.byGroupInfo = ({
  input_group_guid,
  input_guid,
  input_username,
  input_email,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "login_by_group_info";
  const args = [
    input_group_guid,
    input_guid,
    input_username,
    input_email,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.byMerchantInfo = ({
  input_merchant_guid,
  input_guid,
  input_username,
  input_email,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "login_by_merchant_info";
  const args = [
    input_merchant_guid,
    input_guid,
    input_username,
    input_email,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.byPartnerInfo = ({
  input_partner_guid,
  input_guid,
  input_username,
  input_email,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "login_by_partner_info";
  const args = [
    input_partner_guid,
    input_guid,
    input_username,
    input_email,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.info = ({
  input_guid,
  input_username,
  input_email,
  input_enabled,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "login_info";
  const args = [
    input_guid,
    input_username,
    input_email,
    input_enabled,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_user_profile_id,
  input_username,
  input_username_canonical,
  input_email,
  input_email_canonical,
  input_password,
  input_salt,
  input_otp,
  input_auth_type,
  input_enabled,
  input_last_login,
  input_locked,
  input_expired,
  input_expires_at,
  input_confirmation_token,
  input_password_requested_at,
  input_credentials_expired,
  input_credentials_expire_at,
  input_phone,
  input_enabled_api,
  input_allowed_ips,
  input_hash_key,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "login_get";
  const args = [
    input_guid,
    input_user_profile_id,
    input_username,
    input_username_canonical,
    input_email,
    input_email_canonical,
    input_password,
    input_salt,
    input_otp,
    input_auth_type,
    input_enabled,
    input_last_login,
    input_locked,
    input_expired,
    input_expires_at,
    input_confirmation_token,
    input_password_requested_at,
    input_credentials_expired,
    input_credentials_expire_at,
    input_phone,
    input_enabled_api,
    input_allowed_ips,
    input_hash_key,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_user_profile_id,
  input_username,
  input_email,
  input_password,
  input_salt,
  input_otp,
  input_auth_type,
  input_enabled,
  input_last_login,
  input_locked,
  input_expired,
  input_expires_at,
  input_confirmation_token,
  input_password_requested_at,
  input_credentials_expired,
  input_credentials_expire_at,
  input_phone,
  input_enabled_api,
  input_allowed_ips,
  input_hash_key,
  input_author_guid
}, connection) => {
  const type = "login_upsert";
  const args = [
    input_guid,
    input_user_profile_id,
    input_username,
    input_email,
    input_password,
    input_salt,
    input_otp,
    input_auth_type,
    input_enabled,
    input_last_login,
    input_locked,
    input_expired,
    input_expires_at,
    input_confirmation_token,
    input_password_requested_at,
    input_credentials_expired,
    input_credentials_expire_at,
    input_phone,
    input_enabled_api,
    input_allowed_ips,
    input_hash_key,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "login_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
