const { dbquery } = require("../api");

module.exports.info = ({
  input_guid,
  input_type,
  input_account_number,
  input_card_number,
  input_currency_name,
  input_name_on_card,
  input_balance,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "card_info";
  const args = [
    input_guid,
    input_type,
    input_account_number,
    input_card_number,
    input_currency_name,
    input_name_on_card,
    input_balance,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_type,
  input_account_guid,
  input_card_number,
  input_currency_guid,
  input_name_on_card,
  input_expired,
  input_expires_at,
  input_balance,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "card_get";
  const args = [
    input_guid,
    input_type,
    input_account_guid,
    input_card_number,
    input_currency_guid,
    input_name_on_card,
    input_expired,
    input_expires_at,
    input_balance,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_type,
  input_account_guid,
  input_card_number,
  input_currency_guid,
  input_name_on_card,
  input_expired,
  input_expires_at,
  input_balance,
  input_author_guid
}, connection) => {
  const type = "card_upsert";
  const args = [
    input_guid,
    input_type,
    input_account_guid,
    input_card_number,
    input_currency_guid,
    input_name_on_card,
    input_expired,
    input_expires_at,
    input_balance,
    input_author_guid ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "card_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
