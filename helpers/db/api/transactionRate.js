const { dbquery } = require("../api");

module.exports.get = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid,
  input_transaction_guid,
  input_type,
  input_success,
  input_failure,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_rate_get";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid,
    input_transaction_guid,
    input_type,
    input_success,
    input_failure,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid,
  input_transaction_guid,
  input_type,
  input_success,
  input_failure,
  input_author_guid
}, connection) => {
  const type = "transaction_rate_upsert";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid,
    input_transaction_guid,
    input_type,
    input_success,
    input_failure,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_shop_guid,
  input_currency_guid,
  input_gateway_guid
}, connection) => {
  const type = "transaction_rate_delete";
  const args = [
    input_shop_guid,
    input_currency_guid,
    input_gateway_guid
  ];
  return dbquery(type, args, connection);
};
