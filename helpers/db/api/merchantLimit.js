const { dbquery } = require("../api");

module.exports.info = ({
  input_shop_guid
}, connection) => {
  const type = "merchant_payment_limit";
  const args = [
    input_shop_guid
  ];
  return dbquery(type, args, connection);
};