const { pool } = require("./dbconnection");
const log = require("../logger");
const { MysqlError } = require("../../errorTypes");
//  const bcrypt = require("bcrypt")
//returns promise-object with request to database
const dbquery = async (type, args, connection) => {
  // const query = `CALL ${type}${argsToString(args)};`;
  const query = args ? `select * from ${type}${argsToString(args)};` : type;
  log.info(`DATABASE QUERY:'${query}'`);
  // console.log(bcrypt.hashSync("passwordhash", bcrypt.genSaltSync(10)));
  const request = connection ? connection.query(query) : pool.query(query);
  const response = await request.catch(error => { throw new MysqlError(error.message, error); });
  log.info(`DATABASE RESPONSE:'${JSON.stringify([ response ])} '`); // to file
  return response.rows;
};

pool.on("error", err => console.error("Unexpected error on idle client", err));

//tranforms array ["Banana", "Apple", "Orange"] into string "('Banana','Apple','Orange')" for sql request. null stays null
const argsToString = (args) => {
  const addQuotes = args.map(value => 
    value || value === "" || value === 0 ? `'${value}'` : "null"
  );
  return `(${addQuotes.join()})`;
};

module.exports = { dbquery };