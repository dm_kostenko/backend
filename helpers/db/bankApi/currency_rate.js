const { dbquery } = require("../api");

module.exports.get = ({
  input_currency_code,
  input_date,
  input_rate
}, connection) => {
  const type = "currency_rate_get";
  const args = [
    input_currency_code,
    input_date,
    input_rate
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_currency_code,
  input_date,
  input_rate,
  input_author_guid
}, connection) => {
  const type = "currency_rate_upsert";
  const args = [
    input_currency_code,
    input_date,
    input_rate,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};