const { dbquery } = require("../api");


module.exports.begin = connection => dbquery("BEGIN", undefined, connection);
module.exports.rollback = connection => dbquery("ROLLBACK", undefined, connection);
module.exports.commit = connection => dbquery("COMMIT", undefined, connection);

