const { dbquery } = require("../api");

module.exports.get = ({
  input_version,
  input_sender,
  input_receiver,
  input_priority,
  input_msgid,
  input_system,
  input_docid,
  input_datestype,
  input_businessarea,
  input_reportmsgid,
  input_reportdttm,
  input_grsts,
  input_rsnprty,
  input_status,
  input_assgnmtid,
  input_assgnmtcredttm,
  input_ctrldatanboftxs,
}, connection) => {
  const type = "ffpcrqst_document_get";
  const args = [
    input_version,
    input_sender,
    input_receiver,
    input_priority,
    input_msgid,
    input_system,
    input_docid,
    input_datestype,
    input_businessarea,
    input_reportmsgid,
    input_reportdttm,
    input_grsts,
    input_rsnprty,
    input_status,
    input_assgnmtid,
    input_assgnmtcredttm,
    input_ctrldatanboftxs,
  ];
  return dbquery(type, args, connection);
};


module.exports.upsert = ({  
  input_version,
  input_sender,
  input_receiver,
  input_priority,
  input_msgid,
  input_system,
  input_docid,
  input_datestype,
  input_businessarea,
  input_reportmsgid,
  input_reportdttm,
  input_grsts,
  input_rsnprty,
  input_status,
  input_assgnmtid,
  input_assgnmtcredttm,
  input_ctrldatanboftxs,
  input_data,
  input_author_guid,
}, connection) => {
  const type = "ffpcrqst_document_upsert";
  const args = [
    input_version,
    input_sender,
    input_receiver,
    input_priority,
    input_msgid,
    input_system,
    input_docid,
    input_datestype,
    input_businessarea,
    input_reportmsgid,
    input_reportdttm,
    input_grsts,
    input_rsnprty,
    input_status,
    input_assgnmtid,
    input_assgnmtcredttm,
    input_ctrldatanboftxs,
    input_data,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_msgid
}, connection) => {
  const type = "ffpcrqst_document_delete";
  const args = [ input_msgid ];
  return dbquery(type, args, connection);
};
