const { dbquery } = require("../api");

module.exports.info = ({
  input_token
}, connection) => {
  const type = "session_info";
  const args = [
    input_token
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_login_guid,
  input_ip,
  input_user_agent,
  input_token,
  input_expired_at,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "session_get";
  const args = [
    input_login_guid,
    input_ip,
    input_user_agent,
    input_token,
    input_expired_at,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_login_guid,
  input_ip,
  input_user_agent,
  input_token,
  input_expired_at,
  input_author_guid,
}, connection) => {
  const type = "session_upsert";
  const args = [
    input_login_guid,
    input_ip,
    input_user_agent,
    input_token,
    input_expired_at,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_login_guid
}, connection) => {
  const type = "session_delete";
  const args = [ input_login_guid ];
  return dbquery(type, args, connection);
};
