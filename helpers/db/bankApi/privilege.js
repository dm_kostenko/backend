const { dbquery } = require("../api");

module.exports.info = ({
  input_login_guid, 
  input_privilege_guid
}, connection) => {
  const type = "privilege_info";
  const args = [
    input_login_guid, 
    input_privilege_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_guid,
  input_type, 
  input_name 
}, connection) => {
  const type = "privilege_get";
  const args = [
    input_guid,
    input_type, 
    input_name 
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_type, 
  input_name,
  input_description
}, connection) => {
  const type = "privilege_upsert";
  const args = [
    input_guid,
    input_type, 
    input_name,
    input_description
  ];
  return dbquery(type, args, connection);
};
