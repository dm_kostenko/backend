const { dbquery } = require("../api");

module.exports.get = ({
  input_bic,
  input_name,
  input_code
}, connection) => {
  const type = "institute_get";
  const args = [   
    input_bic,
    input_name,
    input_code
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_bic,
  input_name,
  input_code,
  input_author_guid 
}, connection) => {
  const type = "institute_upsert";
  const args = [
    input_bic,
    input_name,
    input_code,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_code
}, connection) => {
  const type = "institute_delete";
  const args = [
    input_code
  ];
  return dbquery(type, args, connection);
};