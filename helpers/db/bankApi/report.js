const { dbquery } = require("../api");

module.exports.transaction_history = ({
  input_guid, 
  input_login_guid, 
  input_currency, 
  input_status, 
  input_type, 
  input_from_date, 
  input_to_date, 
  input_days, 
  input_page_number, 
  input_items_count
}, connection) => {
  const type = "report_transaction_history";
  const args = [
    input_guid, 
    input_login_guid, 
    input_currency, 
    input_status, 
    input_type, 
    input_from_date, 
    input_to_date, 
    input_days, 
    input_page_number, 
    input_items_count  
  ];
  return dbquery(type, args, connection);
};

module.exports.amount_of_transactions = ({
  input_report_type,
  input_login_guid,
  input_currency,
  input_status,
  input_type,
  input_from_date,
  input_to_date,
  input_days,
}, connection) => {
  const type = "report_amount_of_transactions";
  const args = [
    input_report_type,
    input_login_guid,
    input_currency,
    input_status,
    input_type,
    input_from_date,
    input_to_date,
    input_days,
  ];
  return dbquery(type, args, connection);
};

module.exports.currency = ({
  input_login_guid,
  input_number,
  input_iban,
  input_currency
}, connection) => {
  const type = "report_currency";
  const args = [
    input_login_guid,
    input_number,
    input_iban,
    input_currency
  ];
  return dbquery(type, args, connection);
};

module.exports.transaction_types = ({}, connection) => {
  const type = "report_transaction_types";
  const args = [];
  return dbquery(type, args, connection);
};