const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_type,
  input_status,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "inbox_get";
  const args = [
    input_guid,
    input_type,
    input_status,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_type,
  input_data,
  input_status,
  input_details,
  input_author_guid,
}, connection) => {
  const type = "inbox_upsert";
  const args = [
    input_guid,
    input_type,
    input_data,
    input_status,
    input_details,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};
