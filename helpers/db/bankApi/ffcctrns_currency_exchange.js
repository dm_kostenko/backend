const { dbquery } = require("../api");

module.exports.upsert = ({
  input_transaction_history_guid,
  input_from_currency_code,
  input_to_currency_code,
  input_rate,
  input_date,
  input_from_amount,
  input_to_amount,
  input_author_guid
}, connection) => {
  const type = "ffcctrns_currency_exchange_upsert";
  const args = [
    input_transaction_history_guid,
    input_from_currency_code,
    input_to_currency_code,
    input_rate,
    input_date,
    input_from_amount,
    input_to_amount,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};