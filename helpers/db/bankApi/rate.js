const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_name,
  input_description,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "rate_get";
  const args = [
    input_guid,
    input_name,
    input_description,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_name,
  input_description,
  input_author_guid
}, connection) => {
  const type = "rate_upsert";
  const args = [
    input_guid,
    input_name,
    input_description,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "rate_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.getRules = ({
  input_guid,
  input_rule_type,
  input_type
}, connection) => {
  const type = "rate_rule_get_by_rate";
  const args = [    
    input_guid,
    input_rule_type,
    input_type
  ];
  return dbquery(type, args, connection);
};

module.exports.addRule = ({
  input_rate_rule_guid,
  input_rate_guid,
  input_author_guid
}, connection) => {
  const type = "rate_rate_rule_upsert";
  const args = [
    input_rate_rule_guid,
    input_rate_guid,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.deleteRule = ({
  input_rate_rule_guid,
  input_rate_guid
}, connection) => {
  const type = "rate_rate_rule_delete";
  const args = [
    input_rate_rule_guid,
    input_rate_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.setAllExchange = ({
  input_rate_guid,
  input_above,
  input_percent_rate_above,
  input_number_rate_above,
  input_below,
  input_percent_rate_below,
  input_number_rate_below,
  input_percent_rate_other,
  input_number_rate_other
}, connection) => {
  const type = "set_all_exchange_rates";
  const args = [
    input_rate_guid,
    input_above,
    input_percent_rate_above,
    input_number_rate_above,
    input_below,
    input_percent_rate_below,
    input_number_rate_below,
    input_percent_rate_other,
    input_number_rate_other
  ];
  return dbquery(type, args, connection);
};

module.exports.copy = ({
  input_rate_guid,
  input_name,
  input_description,
  input_rate_rules,
  input_author_guid
}, connection) => {
  const type = "copy_rate";
  const args = [
    input_rate_guid,
    input_name,
    input_description,
    input_rate_rules,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};
