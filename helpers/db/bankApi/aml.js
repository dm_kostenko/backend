const { dbquery } = require("../api");

module.exports.get = ({
  input_id,
  input_name,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "aml_get";
  const args = [
    input_id,
    input_name,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_id,
  input_name,
  input_score,
  input_data,
  input_author_guid 
}, connection) => {
  const type = "aml_upsert";
  const args = [
    input_id,
    input_name,
    input_score,
    input_data,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_id
}, connection) => {
  const type = "aml_delete";
  const args = [
    input_id
  ];
  return dbquery(type, args, connection);
};