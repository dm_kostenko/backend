const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_login_guid,
  input_name,
  input_from_account,
  input_to_account,
  input_once,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "template_get";
  const args = [
    input_guid,
    input_login_guid,
    input_name,
    input_from_account,
    input_to_account,
    input_once,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "template_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};