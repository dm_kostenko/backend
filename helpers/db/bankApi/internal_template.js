const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_login_guid,
  input_name,
  input_from_account,
  input_to_account,
  input_once,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "internal_template_get";
  const args = [
    input_guid,
    input_login_guid,
    input_name,
    input_from_account,
    input_to_account,
    input_once,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_login_guid,
  input_name,
  input_from_account,
  input_to_account,
  input_from_currency_code,
  input_to_currency_code,
  input_amount,
  input_once,
  input_author_guid
}, connection) => {
  const type = "internal_template_upsert";
  const args = [
    input_guid,
    input_login_guid,
    input_name,
    input_from_account,
    input_to_account,
    input_from_currency_code,
    input_to_currency_code,
    input_amount,
    input_once,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "internal_template_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};
