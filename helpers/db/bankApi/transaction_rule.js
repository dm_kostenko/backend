const { dbquery } = require("../api");

module.exports.get = ({
  input_direction, 
  input_transaction_type,
  input_rule_type,
  input_name,
  input_value
}, connection) => {
  const type = "transaction_rule_get";
  const args = [
    input_direction, 
    input_transaction_type,
    input_rule_type,
    input_name,
    input_value
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_direction, 
  input_transaction_type,
  input_rule_type,
  input_name,
  input_value,
  input_author_guid
}, connection) => {
  const type = "transaction_rule_upsert";
  const args = [
    input_direction, 
    input_transaction_type,
    input_rule_type,
    input_name,
    input_value,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};