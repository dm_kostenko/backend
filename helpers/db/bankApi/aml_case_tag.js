const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_case_id,
  input_tag_name,
  input_value
}, connection) => {
  const type = "aml_case_tag_get";
  const args = [
    input_guid,
    input_case_id,
    input_tag_name,
    input_value
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_case_id,
  input_tag_name,
  input_value,
  input_author_guid 
}, connection) => {
  const type = "aml_case_tag_upsert";
  const args = [
    input_guid,
    input_case_id,
    input_tag_name,
    input_value,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "aml_case_tag_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};