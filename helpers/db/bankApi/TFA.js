const { dbquery } = require("../api");

module.exports.get = ({
  input_transaction_guid,
  input_otp,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "TFA_get";
  const args = [
    input_transaction_guid,
    input_otp,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_transaction_guid,
  input_otp,
  input_author_guid,
}, connection) => {
  const type = "TFA_upsert";
  const args = [
    input_transaction_guid,
    input_otp,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_transaction_guid,
}, connection) => {
  const type = "TFA_delete";
  const args = [ input_transaction_guid ];
  return dbquery(type, args, connection);
};
