const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_sender_iban,
  input_receiver_iban,
  input_currency,
  input_status,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "internal_transaction_history_get";
  const args = [
    input_guid,
    input_sender_iban,
    input_receiver_iban,
    input_currency,
    input_status,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_sender_iban,
  input_receiver_iban,
  input_currency,
  input_value,
  input_commission,
  input_status,
  input_author_guid,
}, connection) => {
  const type = "internal_transaction_history_upsert";
  const args = [
    input_guid,
    input_sender_iban,
    input_receiver_iban,
    input_currency,
    input_value,
    input_commission,
    input_status,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};
