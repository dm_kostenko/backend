const { dbquery } = require("../api");

module.exports.get = ({
  input_name,
  input_type,
  input_color
}, connection) => {
  const type = "aml_tag_get";
  const args = [
    input_name,
    input_type,
    input_color
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_name,
  input_type,
  input_color,
  input_author_guid 
}, connection) => {
  const type = "aml_tag_upsert";
  const args = [
    input_name,
    input_type,
    input_color,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_name
}, connection) => {
  const type = "aml_tag_delete";
  const args = [
    input_name
  ];
  return dbquery(type, args, connection);
};