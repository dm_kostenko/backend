const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_sender_iban,
  input_receiver_iban,
  input_currency,
  input_status,
  input_type,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "transaction_history_get";
  const args = [
    input_guid,
    input_sender_iban,
    input_receiver_iban,
    input_currency,
    input_status,
    input_type,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.getByLogin = ({
  input_login_guid ,
  input_guid,
  input_status,
  input_direction,
  input_type,
  input_from,
  input_to,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_history_by_login";
  const args = [
    input_login_guid ,
    input_guid,
    input_status,
    input_direction,
    input_type,
    input_from,
    input_to,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.getByAdmin = ({
  input_login_guid ,
  input_guid,
  input_status,
  input_direction,
  input_type,
  input_from,
  input_to,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "transaction_history_by_login_admin";
  const args = [
    input_login_guid ,
    input_guid,
    input_status,
    input_direction,
    input_type,
    input_from,
    input_to,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};