const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_case_id,
  input_name
}, connection) => {
  const type = "aml_case_file_get";
  const args = [
    input_guid,
    input_case_id,
    input_name
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_case_id,
  input_name,
  input_data,
  input_author_guid 
}, connection) => {
  const type = "aml_case_file_upsert";
  const args = [
    input_guid,
    input_case_id,
    input_name,
    input_data,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "aml_case_file_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};