const { dbquery } = require("../api");

module.exports.get = ({
  input_guid
}, connection) => {
  const type = "login_tree";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};
