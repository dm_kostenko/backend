const { dbquery } = require("../api");

module.exports.caseGet = ({
  input_guid,
  input_case_id,
  input_date,
  input_username,
  input_text,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "aml_case_comments_get";
  const args = [
    input_guid,
    input_case_id,
    input_date,
    input_username,
    input_text,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.caseUpsert = ({
  input_guid,
  input_case_id,
  input_date,
  input_username,
  input_text,
  input_author_guid 
}, connection) => {
  const type = "aml_case_comments_upsert";
  const args = [
    input_guid,
    input_case_id,
    input_date,
    input_username,
    input_text,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.entityGet = ({
  input_guid,
  input_entity_id,
  input_case_id,
  input_date,
  input_username,
  input_text,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "aml_entity_comments_get";
  const args = [
    input_guid,
    input_entity_id,
    input_case_id,
    input_date,
    input_username,
    input_text,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.entityUpsert = ({
  input_guid,
  input_entity_id,
  input_case_id,
  input_date,
  input_username,
  input_text,
  input_author_guid 
}, connection) => {
  const type = "aml_entity_comments_upsert";
  const args = [
    input_guid,
    input_entity_id,
    input_case_id,
    input_date,
    input_username,
    input_text,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};