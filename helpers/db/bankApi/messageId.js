const { dbquery } = require("../api");

module.exports.generate = ({}, connection) => dbquery("generate_msgid", [], connection);