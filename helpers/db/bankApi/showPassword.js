const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_username,
  input_email,
  input_phone
}, connection) => {
  const type = "show_password_get";
  const args = [
    input_guid,
    input_username,
    input_email,
    input_phone
  ];
  return dbquery(type, args, connection);
};

