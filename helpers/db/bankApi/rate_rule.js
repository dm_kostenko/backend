const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_rule_type,
  input_type,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "rate_rule_get";
  const args = [
    input_guid,
    input_rule_type,
    input_type,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_rule_type,
  input_type,
  input_author_guid
}, connection) => {
  const type = "rate_rule_upsert";
  const args = [
    input_guid,
    input_rule_type,
    input_type,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "rate_rule_delete";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};