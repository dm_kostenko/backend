const { dbquery } = require("../api");

module.exports.get = ({
  input_rate_rule_guid,
  input_name,
  input_value_type,
  input_value,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "rate_rule_param_get";
  const args = [
    input_rate_rule_guid,
    input_name,
    input_value_type,
    input_value,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.getByRate = ({
  input_rate_guid,
  input_rule_type,
  input_type
}, connection) => {
  const type = "rate_rule_param_get_by_rate_rule";
  const args = [
    input_rate_guid,
    input_rule_type,
    input_type
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_rate_rule_guid,
  input_name,
  input_value_type,
  input_value,
  input_author_guid
}, connection) => {
  const type = "rate_rule_param_upsert";
  const args = [
    input_rate_rule_guid,
    input_name,
    input_value_type,
    input_value,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

// module.exports.delete = ({
//   input_guid
// }, connection) => {
//   const type = "rate_rule_param_delete";
//   const args = [
//     input_guid
//   ];
//   return dbquery(type, args, connection);
// };

