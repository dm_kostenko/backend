const { dbquery } = require("../api");

module.exports.tokenUpsert = ({
  input_guid,
  input_recovery_token,
  input_author_guid
}, connection) => {
  const type = "recovery_login_upsert";
  const args = [
    input_guid,
    input_recovery_token,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.tokenCheck = ({
  input_recovery_token
}, connection) => {
  const type = "recovery_login_check";
  const args = [
    input_recovery_token
  ];
  return dbquery(type, args, connection);
};

