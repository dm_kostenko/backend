const { dbquery } = require("../api");

module.exports.get = ({
  input_transaction_history_guid,
  input_from_currency_code,
  input_to_currency_code,
  input_rate,
  input_date,
  input_from_amount,
  input_to_amount
}, connection) => {
  const type = "currency_exchange_get";
  const args = [
    input_transaction_history_guid,
    input_from_currency_code,
    input_to_currency_code,
    input_rate,
    input_date,
    input_from_amount,
    input_to_amount
  ];
  return dbquery(type, args, connection);
};