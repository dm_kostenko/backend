const { dbquery } = require("../api");

module.exports.get = ({
  input_guid,
  input_login_guid,
  input_name,
  input_address,
  input_profile_id,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "entity_get";
  const args = [
    input_guid,
    input_login_guid,
    input_name,
    input_address,
    input_profile_id,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_login_guid,
  input_name,
  input_address,
  input_profile_id,
  input_author_guid,
}, connection) => {
  const type = "entity_upsert";
  const args = [
    input_guid,
    input_login_guid,
    input_name,
    input_address,
    input_profile_id,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_username,
  input_email,
  input_password,
  input_otpsalt,
  input_otpsaltstatus,
  input_auth_type,
  input_enabled,
  input_last_login,
  input_phone,
  input_name,
  input_address,
  input_profile_id,
  input_author_guid,
}, connection) => {
  const type = "create_entity";
  const args = [
    input_guid,
    input_username,
    input_email,
    input_password,
    input_otpsalt,
    input_otpsaltstatus,
    input_auth_type,
    input_enabled,
    input_last_login,
    input_phone,
    input_name,
    input_address,
    input_profile_id,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};