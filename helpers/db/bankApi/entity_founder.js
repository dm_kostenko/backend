const { dbquery } = require("../api");

module.exports.get = ({
  input_entity_guid,
  input_founder_guid,
  input_founder_type,
}, connection) => {
  const type = "entity_founder_get";
  const args = [
    input_entity_guid,
    input_founder_guid,
    input_founder_type,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_entity_guid, 
  input_founder_guid,
  input_author_guid,
}, connection) => {
  const type = "entity_founder_upsert";
  const args = [
    input_entity_guid, 
    input_founder_guid,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_entity_guid,
  input_founder_guid, 
}, connection) => {
  const type = "entity_founder_delete";
  const args = [
    input_entity_guid,
    input_founder_guid, 
  ];
  return dbquery(type, args, connection);
};
