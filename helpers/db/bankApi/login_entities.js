const { dbquery } = require("../api");

module.exports.get = ({
  input_guid
}, connection) => {
  const type = "login_entities_get";
  const args = [
    input_guid
  ];
  return dbquery(type, args, connection);
};
