const { dbquery } = require("../api");

module.exports.get = ({
  input_sender,
  input_receiver,
  input_msgid,
  input_accountid,
  input_accountiban,
  input_accountcurrency,
  input_instrid,
  input_endtoendid,
  input_transactionid,
  input_creditdebitindex,
  input_initgptybic,
  input_debtoriban,
  input_creditoriban,
  input_debtorbic,
  input_creditorbic,
  input_banksettlementdate,
  input_pmtinfid,
  input_from,
  input_to,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "btcdcntf_get";
  const args = [    
    input_sender,
    input_receiver,
    input_msgid,
    input_accountid,
    input_accountiban,
    input_accountcurrency,
    input_instrid,
    input_endtoendid,
    input_transactionid,
    input_creditdebitindex,
    input_initgptybic,
    input_debtoriban,
    input_creditoriban,
    input_debtorbic,
    input_creditorbic,
    input_banksettlementdate,
    input_pmtinfid,
    input_from,
    input_to,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_sender,
  input_receiver,
  input_priority,
  input_msgid,
  input_date,
  input_system,
  input_businessarea,
  input_accountid,
  input_accountiban,
  input_accountcurrency,
  input_instrid,
  input_endtoendid,
  input_transactionid,
  input_amount,
  input_creditdebitindex,
  input_domaincode,
  input_domainfamilycode,
  input_domainsubfamilycode,
  input_initgptybic,
  input_debtoriban,
  input_creditoriban,
  input_debtorbic,
  input_creditorbic,
  input_acceptancedatetime,
  input_banksettlementdate,
  input_transactiondatetime,  
  input_pmtinfid,
  input_author_guid 
}, connection) => {
  const type = "btcdcntf_upsert";
  const args = [
    input_sender,
    input_receiver,
    input_priority,
    input_msgid,
    input_date,
    input_system,
    input_businessarea,
    input_accountid,
    input_accountiban,
    input_accountcurrency,
    input_instrid,
    input_endtoendid,
    input_transactionid,
    input_amount,
    input_creditdebitindex,
    input_domaincode,
    input_domainfamilycode,
    input_domainsubfamilycode,
    input_initgptybic,
    input_debtoriban,
    input_creditoriban,
    input_debtorbic,
    input_creditorbic,
    input_acceptancedatetime,
    input_banksettlementdate,
    input_transactiondatetime,
    input_pmtinfid,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_msgid,
  input_instrid,
  input_transactionid,
  input_pmtinfid
}, connection) => {
  const type = "btcdcntf_delete";
  const args = [
    input_msgid,
    input_instrid,
    input_transactionid,
    input_pmtinfid
  ];
  return dbquery(type, args, connection);
};