const { dbquery } = require("../api");

module.exports.getByLogin = ({
  input_login_guid,
  input_number,
  input_iban,
  input_type,
  input_currency_code,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "account_get_by_login";
  const args = [
    input_login_guid,
    input_number,
    input_iban,
    input_type,
    input_currency_code,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.get = ({
  input_number,
  input_iban,
  input_type,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "account_get";
  const args = [
    input_number,
    input_iban,
    input_type,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_number,
  input_iban,
  input_type,
  input_author_guid,
}, connection) => {
  const type = "account_upsert";
  const args = [
    input_number,
    input_iban,
    input_type,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_number
}, connection) => {
  const type = "account_delete";
  const args = [ input_number ];
  return dbquery(type, args, connection);
};
