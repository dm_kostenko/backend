const { dbquery } = require("../api");

module.exports.get_outbox = (connection) => {
  const type = "demon_ready_to_send_get";
  return dbquery(type, [], connection);
};

module.exports.add_inbox = ({
  input_data,
  input_details
}, connection) => {
  const type = "demon_add_inbox";
  const args = [
    input_data,
    input_details
  ];
  return dbquery(type, args, connection);
};

module.exports.update_status = ({
  input_guid,
  input_status
}, connection) => {
  const type = "demon_update_status";
  const args = [
    input_guid,
    input_status
  ];
  return dbquery(type, args, connection);
};