const { dbquery } = require("../api");

module.exports.get = ({
  version,
  sender,
  receiver,
  priority,
  msgid,
  system,
  docid,
  datestype,
  businessarea,
  status,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "message_get";
  const args = [
    version,
    sender,
    receiver,
    priority,
    msgid,
    system,
    docid,
    datestype,
    businessarea,
    status,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  version,
  sender,
  receiver,
  priority,
  msgid,
  system,
  docid,
  datestype,
  businessarea,
  status,
  input_author_guid,
}, connection) => {
  const type = "message_upsert";
  const args = [
    version,
    sender,
    receiver,
    priority,
    msgid,
    system,
    docid,
    datestype,
    businessarea,
    status,
    input_author_guid
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_login_guid
}, connection) => {
  const type = "message_delete";
  const args = [ input_login_guid ];
  return dbquery(type, args, connection);
};
