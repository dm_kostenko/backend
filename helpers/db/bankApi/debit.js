const { dbquery } = require("../api");

module.exports.get = ({
  input_sender,
  input_receiver,
  input_msgid,
  input_docid,
  input_referencetype,
  input_referencesender,
  input_referencedocid,
  input_payerbic,
  input_payeraccount,
  input_payerccodeicode,
  input_payername,
  input_payeebic,
  input_payeeaccount,
  input_payeeccodeicode,
  input_payeename,
  input_remittanceinformationid,
  input_from,
  input_to,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "debit_get";
  const args = [   
    input_sender,
    input_receiver,
    input_msgid,
    input_docid,
    input_referencetype,
    input_referencesender,
    input_referencedocid,
    input_payerbic,
    input_payeraccount,
    input_payerccodeicode,
    input_payername,
    input_payeebic,
    input_payeeaccount,
    input_payeeccodeicode,
    input_payeename,
    input_remittanceinformationid,
    input_from,
    input_to,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_sender,
  input_receiver,
  input_priority,
  input_msgid,
  input_date,
  input_system,
  input_businessarea,
  input_docid,
  input_transactionbusinessarea,
  input_referencetype,
  input_referencesender,
  input_referencedocid,
  input_payerbic,
  input_payeraccount,
  input_payerccodeicode,
  input_payername,
  input_payeebic,
  input_payeeaccount,
  input_payeeccodeicode,
  input_payeename,
  input_currency,
  input_amount,
  input_remittanceinformationdate,
  input_remittanceinformationid,
  input_remittanceinformationpaymentreason,
  input_data,
  input_author_guid 
}, connection) => {
  const type = "debit_upsert";
  const args = [
    input_sender,
    input_receiver,
    input_priority,
    input_msgid,
    input_date,
    input_system,
    input_businessarea,
    input_docid,
    input_transactionbusinessarea,
    input_referencetype,
    input_referencesender,
    input_referencedocid,
    input_payerbic,
    input_payeraccount,
    input_payerccodeicode,
    input_payername,
    input_payeebic,
    input_payeeaccount,
    input_payeeccodeicode,
    input_payeename,
    input_currency,
    input_amount,
    input_remittanceinformationdate,
    input_remittanceinformationid,
    input_remittanceinformationpaymentreason,
    input_data,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_msgid
}, connection) => {
  const type = "debit_delete";
  const args = [
    input_msgid
  ];
  return dbquery(type, args, connection);
};