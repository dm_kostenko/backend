const { dbquery } = require("../api");

module.exports.get = ({
  input_sender,
  input_receiver,
  input_msgid,
  input_docid,
  input_bic,
  input_account,
  input_currency,
  input_from,
  input_to,
  input_page_number,
  input_items_count
}, connection) => {
  const type = "balance_get";
  const args = [
    input_sender,
    input_receiver,
    input_msgid,
    input_docid,
    input_bic,
    input_account,
    input_currency,
    input_from,
    input_to,
    input_page_number,
    input_items_count
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_sender,
  input_receiver,
  input_priority,
  input_msgid,
  input_date,
  input_system,
  input_docid,
  input_datestype,
  input_type,
  input_businessarea,
  input_bic,
  input_account,
  input_currency,
  input_openingbalance,
  input_openingavailablebalance,
  input_closingbalance,
  input_closingavailablebalance,
  input_data,
  input_author_guid 
}, connection) => {
  const type = "balance_upsert";
  const args = [
    input_sender,
    input_receiver,
    input_priority,
    input_msgid,
    input_date,
    input_system,
    input_docid,
    input_datestype,
    input_type,
    input_businessarea,
    input_bic,
    input_account,
    input_currency,
    input_openingbalance,
    input_openingavailablebalance,
    input_closingbalance,
    input_closingavailablebalance,
    input_data,
    input_author_guid 
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_msgid,
  input_account
}, connection) => {
  const type = "balance_delete";
  const args = [
    input_msgid,
    input_account
  ];
  return dbquery(type, args, connection);
};