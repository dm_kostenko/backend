const { dbquery } = require("../api");

module.exports.get = ({
  input_login_guid,
  input_case_id
}, connection) => {
  const type = "login_aml_case_get";
  const args = [
    input_login_guid,
    input_case_id
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_login_guid,
  input_case_id,
  input_author_guid,
}, connection) => {
  const type = "login_aml_case_upsert";
  const args = [
    input_login_guid,
    input_case_id,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};
