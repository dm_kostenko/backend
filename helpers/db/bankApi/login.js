const { dbquery } = require("../api");

module.exports.otp_get = ({
  input_guid,
}, connection) => {
  const type = "login_otp_get";
  const args = [
    input_guid,
  ];
  return dbquery(type, args, connection);
};


module.exports.get = ({
  input_guid,
  input_username,
  input_type,
  input_rate_guid,
  input_email,
  input_password,
  input_auth_type,
  input_enabled,
  input_last_login,
  input_phone,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "login_get";
  const args = [
    input_guid,
    input_username,
    input_type,
    input_rate_guid,
    input_email,
    input_password,
    input_auth_type,
    input_enabled,
    input_last_login,
    input_phone,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_guid,
  input_username,
  input_type,
  input_rate_guid,
  input_email,
  input_password,
  input_otpsalt,
  input_otpsaltstatus,
  input_auth_type,
  input_enabled,
  input_last_login,
  input_phone,
  input_credentials_expired = false,
  input_credentials_expire_at,
  input_recovery_token,
  input_recovery_token_expires_at,
  input_author_guid,
}, connection) => {
  const type = "login_upsert";
  const args = [
    input_guid,
    input_username,
    input_type,
    input_rate_guid,
    input_email,
    input_password,
    input_otpsalt,
    input_otpsaltstatus,
    input_auth_type,
    input_enabled,
    input_last_login,
    input_phone,
    input_credentials_expired,
    input_credentials_expire_at,
    input_recovery_token,
    input_recovery_token_expires_at,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_guid
}, connection) => {
  const type = "login_delete";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};

module.exports.block = ({
  input_guid
}, connection) => {
  const type = "login_block";
  const args = [ input_guid ];
  return dbquery(type, args, connection);
};
