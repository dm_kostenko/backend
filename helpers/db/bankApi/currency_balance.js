const { dbquery } = require("../api");

module.exports.get = ({
  input_account_number,
  input_currency_code,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "currencybalance_get";
  const args = [
    input_account_number,
    input_currency_code,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_account_number,
  input_currency_code,
  input_balance,
  input_reserved,
  input_monthly_limit,
  input_daily_limit,
  input_author_guid,
}, connection) => {
  const type = "currencybalance_upsert";
  const args = [
    input_account_number,
    input_currency_code,
    input_balance,
    input_reserved,
    input_monthly_limit,
    input_daily_limit,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_cdttrftxinfpmtidtxid
}, connection) => {
  const type = "currencybalance_delete";
  const args = [ input_cdttrftxinfpmtidtxid ];
  return dbquery(type, args, connection);
};
