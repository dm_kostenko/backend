const { dbquery } = require("../api");

module.exports.get = ({
  input_login_guid,
  input_account_number,
  input_page_number,
  input_items_count,
}, connection) => {
  const type = "login_account_get";
  const args = [
    input_login_guid,
    input_account_number,
    input_page_number,
    input_items_count,
  ];
  return dbquery(type, args, connection);
};

module.exports.upsert = ({
  input_login_guid,
  input_account_number,
  input_author_guid,
}, connection) => {
  const type = "login_account_upsert";
  const args = [
    input_login_guid,
    input_account_number,
    input_author_guid,
  ];
  return dbquery(type, args, connection);
};

module.exports.delete = ({
  input_login_guid,
  input_account_number,
}, connection) => {
  const type = "login_account_delete";
  const args = [
    input_login_guid,
    input_account_number,
  ];
  return dbquery(type, args, connection);
};
