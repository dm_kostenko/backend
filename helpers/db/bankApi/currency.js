const { dbquery } = require("../api");

module.exports.get = ({
  input_code,
  input_name
}, connection) => {
  const type = "currency_get";
  const args = [
    input_code,
    input_name
  ];
  return dbquery(type, args, connection);
};