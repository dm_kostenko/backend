const Consumer = require("../Consumer");

module.exports = class MessageReceiverConsumer extends Consumer {
  constructor({ name, topics }) {
    super({ name, topics });
  }  
}