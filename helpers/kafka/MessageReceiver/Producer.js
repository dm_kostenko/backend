const Producer = require("../Producer");

module.exports = class MessageReceiverProducer extends Producer {
  constructor() {
    super({ name: "Message receiver producer"});
  }  
}