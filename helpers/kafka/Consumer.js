const kafka = require("kafka-node");
const { hosts } = require("../../config/kafka");


module.exports = class Consumer {
  constructor({ name, topics }) {
    this.name = name || "Unnamed consumer";
    this.topics = topics || [];
    this.consumer = new kafka.ConsumerGroup(
      {
        kafkaHost: hosts.join(","),
        groupId: name
      },
      topics
    );
    console.log("\x1b[32m%s\x1b[0m",`\n${this.name} started\n`);


  }

  onMessage(callback) {
    this.consumer.on("message", callback)
  }
}