const Consumer = require("../Consumer");

module.exports = class CurrencyExchangeConsumer extends Consumer {
  constructor({ name, topics }) {
    super({ name, topics });
  }  
}