const Producer = require("../Producer");

module.exports = class TransactionProcessingProducer extends Producer {
  constructor() {
    super({ name: "Transaction processing producer"});
  }  
}