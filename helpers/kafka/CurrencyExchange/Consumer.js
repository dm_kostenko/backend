const Consumer = require("../Consumer");

module.exports = class DataLoaderConsumer extends Consumer {
  constructor({ name, topics }) {
    super({ name, topics });
  }  
}