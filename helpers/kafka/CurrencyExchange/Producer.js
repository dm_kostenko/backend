const Producer = require("../Producer");

module.exports = class CurrencyExchangeProducer extends Producer {
  constructor() {
    super({ name: "Currency exchange producer"});
  }  
}