const Producer = require("../Producer");

module.exports = class AccountBlockerProducer extends Producer {
  constructor() {
    super({ name: "Account blocker producer"});
  }  
}