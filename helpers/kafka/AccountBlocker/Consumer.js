const Consumer = require("../Consumer");

module.exports = class AccountBlockerConsumer extends Consumer {
  constructor({ name, topics }) {
    super({ name, topics });
  }  
}