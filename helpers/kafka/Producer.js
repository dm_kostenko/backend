const kafka = require("kafka-node");
const { hosts } = require("../../config/kafka");

module.exports = class Producer {
  constructor({ name }) {
    this.name = name || "Unnamed producer";
    this.producer = new kafka.Producer(new kafka.KafkaClient({ kafkaHost: hosts.join(",") }));
  }

  send({ topic, event, payload }) {
      this.producer.send([
        { 
          topic, 
          key: event, 
          messages: JSON.stringify(payload), 
          partition: 0 
        }
      ], (err, data) => {
      if(err) {
        // console.log(`${this.name} send data failed`, err)
        throw(err)
      }
      // console.log(`${this.name} send data success`, JSON.stringify(payload))
    })
  }

  onError() {
    this.producer.on("error", (err) => {
      console.log(`${this.name} error`, err)
    })
  }
}