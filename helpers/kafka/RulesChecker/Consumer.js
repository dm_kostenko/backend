const Consumer = require("../Consumer");

module.exports = class RulesCheckerConsumer extends Consumer {
  constructor({ name, topics }) {
    super({ name, topics });
  }  
}