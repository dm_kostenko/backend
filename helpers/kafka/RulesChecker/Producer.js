const Producer = require("../Producer");

module.exports = class RulesCheckerProducer extends Producer {
  constructor() {
    super({ name: "Rules checker producer"});
  }  
}