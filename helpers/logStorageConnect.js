const { url, dbOptions } = require("../config").loggerModule;
const logStorage = require("../modules/logStorage");

if (process.env.NODE_ENV !== "test"){
  logStorage.connect(url, dbOptions)
    .catch(err => console.log(`\x1b[31m[logger-module]: ${err}`));
}

module.exports = logStorage;