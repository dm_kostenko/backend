const db = require("../db/bankApi");
const { iban, bic } = require("../../config")
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";
const messageId = require("../../modules/messageIdGenerator/")
const { 
  EXTERNAL_TRANSACTION_APPROVED
} = require("../constants/statuses").externalTransaction;
const { generateXml } = require("./generateXml");


module.exports.returnCommission = async({ msgId }, connection) => {
  try {

    const [ ffcctrnsTransaction ] = await db.ffcctrns_transaction.get({ 
      input_msgid: msgId
    });

    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: ffcctrnsTransaction.guid
    })

    if(!transaction)
      throw {
        message: `Transaction with message id ${msgId} didn't found`
      }

    if(Math.round(transaction.commission) > 0) {
      const newMsgId = await messageId.generate();

      let eurCommission = transaction.commission;
      if(transaction.currency !== "EUR") {
        const [{ rate }] = await db.currency_exchange.get({
          input_transaction_history_guid: transaction.guid
        });
        eurCommission = Math.round(transaction.commission * rate);
      }

      const [ newTransaction ] = await db.ffcctrns_transaction.createTransaction({
        input_login_guid: author_guid,
        input_sender_iban: iban.settlement.internal,
        input_receiver_iban: iban.customer.internal,
        input_currency: "EUR",
        input_value: eurCommission,
        input_commission: "0",
        input_cdttrftxinfpmtidinstrid: newMsgId,
        input_cdttrftxinfpmtidendtoendid: "NOTPROVIDED",
        input_cdttrftxinfpmtidtxid: newMsgId,
        input_cdttrftxinfpmtidpmttpinfsvclvlcd: "SEPA",
        // input_cdttrftxinfpmttpinflclinstrm: ,
        input_cdttrftxinfintrbksttlmamt: transaction.commission,
        input_cdttrftxinfchrgbr: "SLEV",
        // input_cdttrftxinfinstgagtfininstnidbic: ,
        input_cdttrftxinfdbtrnm: "TBF Finance, UAB",
        input_cdttrftxinfdbtracctidiban: iban.settlement.internal,   // or iban.client_system
        input_cdttrftxinfdbtragtfininstnidbic: bic,
        input_cdttrftxinfcdtragtfininstnidbic: bic,
        input_cdttrftxinfcdtrnm: "TBF Finance, UAB",
        input_cdttrftxinfcdtracctidiban: iban.customer.internal,
        // input_cdttrftxinfnnnid: ,
        // input_cdttrftxinfpurpcd: ,
        // input_cdttrftxinfrmtinfustrd: ,
        // input_cdttrftxinfrmtinfstrd: ,
        // input_cdttrftxinfrmtinfstrdcdtrrefinftpcdorprtrycd: ,
        // input_cdttrftxinfrmtinfstrdcdtrrefinftpissr: ,
        input_cdttrftxinfrmtinfstrdcdtrrefinfref: `Commission transfer returning. Transaction № ${msgId}`,
        input_status: EXTERNAL_TRANSACTION_APPROVED,
        input_author_guid: author_guid
      }, connection);

      await generateXml({ 
        guid: newTransaction.guid,
        status: EXTERNAL_TRANSACTION_APPROVED,
        system: "LITAS-MIG",
        author_guid: author_guid
      }, connection);

      const isOutgoing = ffcctrnsTransaction.sender === bic && ffcctrnsTransaction.receiver !== bic;
      const isIncoming = ffcctrnsTransaction.sender !== bic && ffcctrnsTransaction.receiver === bic;
      if(isOutgoing) {
        const [ clientBalance ] = await db.currency_balance.get({ 
          input_account_number: transaction.sender_iban.slice(9), 
          input_currency_code: transaction.currency
        });

        await db.currency_balance.upsert({ 
          input_account_number: transaction.sender_iban.slice(9), 
          input_currency_code: transaction.currency,
          input_balance: Math.round(clientBalance.balance) + Math.round(transaction.commission),
          input_author_guid: author_guid
        }, connection);    
      }
      else if(isIncoming) {
        const [ clientBalance ] = await db.currency_balance.get({ 
          input_account_number: transaction.receiver_iban.slice(9), 
          input_currency_code: transaction.currency
        });

        await db.currency_balance.upsert({ 
          input_account_number: transaction.receiver_iban.slice(9), 
          input_currency_code: transaction.currency,
          input_balance: Math.round(clientBalance.balance) + Math.round(transaction.commission),
          input_author_guid: author_guid
        }, connection);    
      }
      else 
        throw {
          message: "Incorrect data"
        }

    }
  }
  catch(err) {
    throw(err)
  }
};