const db = require("../db/bankApi");

module.exports.process = async ({ sender_number, receiver_number, currency, value, author_guid }) => {
  let connection;

  try {
    if (sender_number === receiver_number)
      throw { message: "Accounts cannot be equal" };

    const [ senderBalance ] = await db.currency_balance.get({ input_account_number: sender_number, input_currency_code: currency });
    if (!senderBalance)
      throw { message: "Sender account not found or didnt have current currency" };

    const [ senderAccount ] = await db.account.get({ input_number: sender_number, input_currency_code: currency });
    if (!senderAccount)
      throw { message: "Sender account not found" };

    const [ receiverBalance ] = await db.currency_balance.get({ input_account_number: receiver_number, input_currency_code: currency });
    if (!receiverBalance)
      throw { message: "Receiver account not found or didnt have current currency" };

    const [ receiverAccount ] = await db.account.get({ input_number: receiver_number, input_currency_code: currency });
    if (!receiverAccount)
      throw { message: "Receiver account not found" };

    if (parseFloat(senderBalance.balance) - parseFloat(senderBalance.reserved) < parseFloat(value)) {
      await db.internal_transaction_history.upsert({
        input_sender_iban: senderAccount.iban,
        input_receiver_iban: receiverAccount.iban,
        input_currency: currency,
        input_value: value,
        input_status: "Failed",
        input_author_guid: author_guid,
      });
    
      throw { message: "Not enough money" };
    }

    connection = await db.getConnection();
    await db._transaction.begin(connection);

    const [ upsertedTransaction ] = await db.internal_transaction_history.upsert({
      input_sender_iban: senderAccount.iban,
      input_receiver_iban: receiverAccount.iban,
      input_currency: currency,
      input_value: value,
      input_status: "Success",
      input_author_guid: author_guid,
    });

    await db.currency_balance.upsert({ 
      input_account_number: sender_number, 
      input_currency_code: currency, 
      input_balance: (parseFloat(senderBalance.balance) - parseFloat(value)).toFixed(2),
      input_author_guid: author_guid
    });

    await db.currency_balance.upsert({ 
      input_account_number: receiver_number, 
      input_currency_code: currency, 
      input_balance: (parseFloat(receiverBalance.balance) + parseFloat(value)).toFixed(2),
      input_author_guid: author_guid
    });
    await db._transaction.commit(connection);

    return upsertedTransaction; 
  } catch (err) {
    if (connection)
      await db._transaction.rollback(connection);

    throw err;
  } finally {
    if (connection)
      connection.release();
  }
};