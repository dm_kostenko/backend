const db = require("../db/bankApi");
const { iban } = require("../../config/")
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs

module.exports.sendCommission = async ({ commission }, connection) => {
  try {
    let [ commissionBalance ] = await db.currency_balance.get({ 
      input_account_number: iban.client_system, 
      input_currency_code: "EUR" 
    }, connection);

    if(!commissionBalance)
      throw {
        message: `System account ${iban.client_system} with EUR didn't found`
      }

    return await db.currency_balance.upsert({
      input_account_number: iban.client_system, 
      input_currency_code: "EUR", 
      input_balance: Math.round(commission) + Math.round(commissionBalance.balance),
      input_author_guid: author_guid
    }, connection);
  }
  catch(err) {
    throw(err)
  }
}

module.exports.reserve = async ({ senderAccountNumber, currency, amount }, connection) => {
  try {
    const [ senderBalance ] = await db.currency_balance.get({ input_account_number: senderAccountNumber, input_currency_code: currency }, connection);
    
    if (!senderBalance)
      throw { 
        message: `Account ${senderAccountNumber} with currency ${currency} didn't found`,
        code: "AC04"    // The account is closed (PRTRN)
      };
    
    if (Math.round(senderBalance.balance) - Math.round(senderBalance.reserved) < Math.round(amount))
      throw { 
        message: "Not enough money",
        code: "AM04"    // There are insufficient funds on the account
      };    
      
    return await db.currency_balance.upsert({
      input_account_number: senderAccountNumber, 
      input_currency_code: currency, 
      input_reserved: Math.round(amount) + Math.round(senderBalance.reserved),
      input_author_guid: author_guid,
    }, connection);
  }
  catch(err) {
    throw(err)
  }
};

module.exports.closeReserved = async ({ senderAccountNumber, currency, amount }, connection) => {
  try {
    const [ senderBalance ] = await db.currency_balance.get({ input_account_number: senderAccountNumber, input_currency_code: currency }, connection);
    const { balance, reserved } = senderBalance;

    if (!senderBalance)
      throw { message: `Account ${senderAccountNumber} with currency ${currency} didn't found` };
    
    if ((Math.round(senderBalance.reserved) - Math.round(amount) < 0) 
      ||(Math.round(senderBalance.balance) - Math.round(amount) < 0))
      throw { message: "Not enough money" };

    return await db.currency_balance.upsert({
      input_account_number: senderAccountNumber, 
      input_currency_code: currency, 
      input_balance: Math.round(balance) - Math.round(amount),
      input_reserved: Math.round(reserved) - Math.round(amount),
      input_author_guid: author_guid,
    }, connection);
  }
  catch(err) {
    throw(err)
  }
};

module.exports.cancelReserved = async ({ senderAccountNumber, currency, amount }, connection) => {
  try {
    const [ senderBalance ] = await db.currency_balance.get({ input_account_number: senderAccountNumber, input_currency_code: currency }, connection);
    const { reserved } = senderBalance;

    if (!senderBalance)
      throw { message: `Account ${senderAccountNumber} with currency ${currency} didn't found` };

    if (Math.round(senderBalance.reserved) - Math.round(amount) < 0)
      throw { message: "Not enough money" };

    return await db.currency_balance.upsert({
      input_account_number: senderAccountNumber, 
      input_currency_code: currency, 
      input_reserved: Math.round(reserved) - Math.round(amount),
      input_author_guid: author_guid,
    }, connection);
  }
  catch(err) {
    throw(err)
  }
};

module.exports.returnAmount = async ({ senderAccountNumber, currency, amount }, connection) => {
  try {
    const [ senderBalance ] = await db.currency_balance.get({ input_account_number: senderAccountNumber, input_currency_code: currency }, connection);
    if (!senderBalance)
      throw { message: `Account ${senderAccountNumber} with currency ${currency} didn't found` };

    const { balance } = senderBalance;

    const [ updatedBalance ] = await db.currency_balance.upsert({
      input_account_number: senderAccountNumber, 
      input_currency_code: currency, 
      input_balance: Math.round(balance) + Math.round(amount),
      input_author_guid: author_guid,
    }, connection);

    return updatedBalance;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.receiveAmount = async ({ receiverAccountNumber, currency, amount }, connection) => {
  try {
    const [ receiverBalance ] = await db.currency_balance.get({ input_account_number: receiverAccountNumber, input_currency_code: currency }, connection);
    if (!receiverBalance)
      throw { message: `Account ${receiverAccountNumber} with currency ${currency} didn't found` };

    const { balance } = receiverBalance;

    const [ updatedBalance ] = await db.currency_balance.upsert({
      input_account_number: receiverAccountNumber, 
      input_currency_code: currency, 
      input_balance: Math.round(balance) + Math.round(amount),
      input_author_guid: author_guid,
    }, connection);

    return updatedBalance;
  }
  catch(err) {
    throw(err)
  }
};