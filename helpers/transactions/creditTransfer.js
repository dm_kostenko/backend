const db = require("../db/bankApi");
const { generateXml } = require("./generateXml");
const { 
  EXTERNAL_TRANSACTION_SENT,
  EXTERNAL_TRANSACTION_SETTLED,
  EXTERNAL_TRANSACTION_ACCEPTED, 
  EXTERNAL_TRANSACTION_REJECTED,
  EXTERNAL_TRANSACTION_CANCELLED
} = require("../constants/statuses").externalTransaction;
const { 
  cancelReserved, 
  closeReserved, 
  returnAmount,
  receiveAmount,
  sendCommission
} = require("./account");

const { returnCommission } = require("./commission");

module.exports.prepareXml = async ({
  guid,
  system,
  status,
  author_guid,
}, connection) => {

  try {
    await generateXml({
      guid,
      system,
      status,
      author_guid
    }, connection)
  }
  catch(err) {
    throw(err)
  }
};

module.exports.accept = async ({ transactionId }, connection) => {
  try {
    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });
    if(!transaction || transaction.status !== EXTERNAL_TRANSACTION_SENT)
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SENT}`}
    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_ACCEPTED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.set = async ({ transactionId, senderIban, amount, currency, isCommissionTransfer }, connection) => {
  try {
    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || transaction.status !== EXTERNAL_TRANSACTION_ACCEPTED)
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_ACCEPTED}`}

    if(!isCommissionTransfer) {
      const [ senderAccount ] = await db.account.getByLogin({ input_iban: senderIban, input_currency_code: currency });
      if (!senderAccount)
        throw { message: "Sender account didn't found" };

      await closeReserved({ senderAccountNumber: senderAccount.number, currency, amount }, connection);
    }
    
    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_SETTLED
    }, connection);

    // add record in system transfer to client money account table

    if(Math.round(transaction.commission) > 0 && !isCommissionTransfer) {
      let commission = transaction.commission
      if(transaction.currency !== "EUR") {
        const [{ rate }] = await db.currency_exchange.get({
          input_transaction_history_guid: transaction.guid
        })
  
        commission = Math.round(transaction.commission / rate);
      }
      await sendCommission({ commission }, connection);
    }

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.reject = async ({ transactionId, senderIban, amount, currency, isCommissionTransfer }, connection) => {
  try {

    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || ![ EXTERNAL_TRANSACTION_SENT, EXTERNAL_TRANSACTION_ACCEPTED].includes(transaction.status))
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SENT} or ${EXTERNAL_TRANSACTION_ACCEPTED}`}

    if(!isCommissionTransfer) {
      const [ senderAccount ] = await db.account.getByLogin({ input_iban: senderIban,  input_currency_code: currency }, connection);
      if (!senderAccount)
        throw { message: "Sender account didn't found" };

      await cancelReserved({ senderAccountNumber: senderAccount.number, currency, amount }, connection);
    }
      
    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_REJECTED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.cancel = async ({ transactionId, senderIban, amount, currency, isCommissionTransfer }, connection) => {
  try {

    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || ![ EXTERNAL_TRANSACTION_SENT, EXTERNAL_TRANSACTION_ACCEPTED].includes(transaction.status))
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SENT} or ${EXTERNAL_TRANSACTION_ACCEPTED}`}

    if(!isCommissionTransfer) {
      const [ senderAccount ] = await db.account.getByLogin({ input_iban: senderIban,  input_currency_code: currency }, connection);
      if (!senderAccount)
        throw { message: "Sender account didn't found" };
      await cancelReserved({ senderAccountNumber: senderAccount.number, currency, amount }, connection);
    }

    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_CANCELLED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.return = async ({ transactionId, senderIban, amount, currency, isCommissionTransfer }, connection) => {
  try {
  
    const [{ guid }] = await db.ffcctrns_transaction.get({ 
      input_msgid: transactionId
    });

    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: guid
    });

    if(!transaction || transaction.status !== EXTERNAL_TRANSACTION_SETTLED)
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SETTLED}`}

    if(!isCommissionTransfer) {
      const [ senderAccount ] = await db.account.getByLogin({ input_iban: senderIban,  input_currency_code: currency }, connection);
      if (!senderAccount)
        throw { message: "Sender account didn't found" };

      await returnAmount({ senderAccountNumber: senderAccount.number, currency, amount }, connection);

      await returnCommission({ msgId: transactionId }, connection);

    }
      
    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: guid,
      input_status: EXTERNAL_TRANSACTION_CANCELLED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.receive = async({ receiverIban, amount, commission, currency }, connection) => {
  try {
    const [ receiverAccount ] = await db.account.getByLogin({ input_iban: receiverIban,  input_currency_code: currency }, connection);
    if (!receiverAccount)
      throw { message: "Receiver account didn't found" };

    await receiveAmount({ receiverAccountNumber: receiverAccount.number, currency, amount }, connection);

    await sendCommission({ commission }, connection);
  }
  catch(err) {
    throw(err)
  }
}