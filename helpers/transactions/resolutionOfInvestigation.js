const db = require("../db/bankApi");
const { 
  EXTERNAL_TRANSACTION_SENT,
  EXTERNAL_TRANSACTION_INVESTIGATED,
  EXTERNAL_TRANSACTION_ACCEPTED, 
  EXTERNAL_TRANSACTION_REJECTED,
} = require("../constants/statuses").externalTransaction;

module.exports.accept = async ({ transactionId, investigatedMsgId }, connection) => {
  try {
    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || transaction.status !== EXTERNAL_TRANSACTION_SENT)
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SENT}`}

    await db.external_transaction_history.updateStatusByMsgId({
      input_msgid: investigatedMsgId,
      input_status: EXTERNAL_TRANSACTION_INVESTIGATED
    }, connection);

    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_ACCEPTED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.reject = async ({ transactionId }, connection) => {
  try {
    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || ![ EXTERNAL_TRANSACTION_SENT, EXTERNAL_TRANSACTION_ACCEPTED].includes(transaction.status))
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SENT} or ${EXTERNAL_TRANSACTION_ACCEPTED}`}
      
    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_REJECTED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};