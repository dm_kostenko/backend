const { FFCCTRNS, FFPCRQST, FFPSRPRT, PRTRN, XmlMessageParser } = require("../../../modules/messages");
const { XML_IMPORT_IMPORTED, XML_IMPORT_ERRORED } = require("../../constants/statuses").xmlImport;
const { 
  EXTERNAL_TRANSACTION_PROCESSED, 
  EXTERNAL_TRANSACTION_REJECTED, 
  EXTERNAL_TRANSACTION_CANCELLED,
  EXTERNAL_TRANSACTION_RETURNED
} = require("../../constants/statuses").externalTransaction;
const { directDebit, paymentCancellation, paymentReturn } = require("../");
const db = require("../../db/bankApi");
const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs

const subscribeOnNewDocuments = () => db.getConnection().then(client => {
  client.on("notification", ({ /* channel, */ payload }) => new_incoming_xml(payload));
  client.query("LISTEN new_inbox");
});

module.exports = () => subscribeOnNewDocuments().catch((/* err */) => setTimeout(subscribeOnNewDocuments, 5 * 60 * 1000));

const new_incoming_xml = async payload => {
  const guid = payload;
  let connection;
  
  try {
    const [ receivedDocument ] = await db.inbox.get({ input_guid: guid });
    const { data } = receivedDocument;

    const messageParser = new XmlMessageParser(unescape(data));
    const message = await messageParser.getMessage();

    connection = await db.getConnection();
    await db._transaction.begin(connection);

    switch (message.constructor) {
    case FFCCTRNS:
      await importFFCCTRNS(message, data, connection);
      break;
    case FFPCRQST:
      // await importFFPSRPRT(message);
      break;
    case PRTRN:
      await importPRTRN(message, data, connection);
      break;
    case FFPSRPRT:
      await importFFPSRPRT(message, connection);
      break;
    default:
      throw { message: "Message type unsupported" };
    }
  
    await db.inbox.upsert({ 
      input_guid: guid, 
      input_type: message.constructor.name || "undefined", 
      input_status: XML_IMPORT_IMPORTED, 
      input_details: "Imported successfully", 
      input_author_guid: author_guid 
    }, connection);

    await db._transaction.commit(connection);
  } catch (err) {

    if (connection)
      await db._transaction.rollback(connection);
    
    await db.inbox.upsert({ 
      input_guid: guid, 
      input_type: "undefined", 
      input_status: XML_IMPORT_ERRORED,
      input_details: escape(err.message), 
      input_author_guid: author_guid 
    }, connection);
  } finally {
    if (connection)
      connection.release();
  }
};

const importFFCCTRNS = async ({ messageHeader, groupHeader, transactions: [ transaction ] }, originalXml, connection) => {
  const status = XML_IMPORT_IMPORTED;
  await db.ffcctrns_transaction.upsert({
    input_version: messageHeader.version,
    input_sender: messageHeader.sender, 
    input_receiver: messageHeader.receiver,
    input_priority: messageHeader.priority,
    input_msgid: messageHeader.msgId, 
    input_system: messageHeader.system,
    input_docid: messageHeader.msgId, 
    input_datestype: messageHeader.date,
    input_businessarea: messageHeader.businessArea,
    input_grphdrmsgid: groupHeader.MsgId,
    input_grphdrcredttm: groupHeader.CreDtTm, 
    input_grphdrnboftxs: groupHeader.NbOfTxs,
    input_grphdrttlintrbksttlmamt: groupHeader.TtlIntrBkSttlmAmt, 
    input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
    input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
    input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
    input_grphdrinstgagfininstnidbic: groupHeader.FinInstnIdBIC,
    input_data: originalXml,
    // input_login_guid,
    input_cdttrftxinfpmtidinstrid: transaction.paymentInstructionId, 
    input_cdttrftxinfpmtidendtoendid: transaction.paymentEndToEndId, 
    input_cdttrftxinfpmtidtxid: transaction.paymentTransactionId, 
    input_cdttrftxinfpmtidpmttpinfsvclvlcd: transaction.serviceLevel.Cd, 
    // input_cdttrftxinfpmttpinflclinstrm, 
    // input_cdttrftxinfpmttpinfctgypurp, 
    input_cdttrftxinfintrbksttlmamt: transaction.amount, 
    input_cdttrftxinfchrgbr: transaction.chargeBearer, 
    // input_cdttrftxinfinstgagtfininstnidbic, 
    input_cdttrftxinfdbtrnm: transaction.debtorName,
    input_cdttrftxinfdbtracctidiban: transaction.debtorAccount, 
    input_cdttrftxinfdbtragtfininstnidbic: transaction.debtorBic, 
    input_cdttrftxinfcdtragtfininstnidbic: transaction.creditorBic, 
    input_cdttrftxinfcdtrnm: transaction.creditorName, 
    input_cdttrftxinfcdtracctidiban: transaction.creditorAccount, 
    // input_cdttrftxinfnnnid, 
    // input_cdttrftxinfpurpcd, 
    // input_cdttrftxinfrmtinfustrd, 
    // input_cdttrftxinfrmtinfstrd, 
    // input_cdttrftxinfrmtinfstrdcdtrrefinftpcdorprtrycd, 
    // input_cdttrftxinfrmtinfstrdcdtrrefinftpissr, 
    input_cdttrftxinfrmtinfstrdcdtrrefinfref: transaction.remittanceInformation,
    input_status: status,
    input_author_guid: author_guid,
  }, connection);
};

const importPRTRN = async (message, originalXml, connection) => {
  const { messageHeader, groupHeader, transactions: [ transaction ] } = message;

  const status = XML_IMPORT_IMPORTED;
  await db.prtrn_transaction.upsert({
    input_version: messageHeader.version,
    input_sender: messageHeader.sender, 
    input_receiver: messageHeader.receiver,
    input_priority: messageHeader.priority,
    input_msgid: messageHeader.msgId, 
    input_system: messageHeader.system,
    input_docid: messageHeader.msgId, 
    input_datestype: messageHeader.date,
    input_businessarea: messageHeader.businessArea,
    input_grphdrmsgid: groupHeader.MsgId,
    input_grphdrcredttm: groupHeader.CreDtTm, 
    input_grphdrnboftxs: groupHeader.NbOfTxs,
    input_grphdrttlintrbksttlmamt: groupHeader.TtlRtrdIntrBkSttlmAmt, 
    input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
    input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
    input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
    input_grphdrinstgagfininstnidbic: groupHeader.FinInstnIdBIC,
    input_data: originalXml,
    input_txinfrtrid: transaction.returnmentId,
    input_txinforgnlgrpinforgnlmsgid: transaction.originalMsgId,
    input_txinforgnlgrpinforgnlmsgnmid: transaction.originalMsgNmId,
    input_txinforgnlinstrid: transaction.originalInstrId,
    input_txinforgnlendtoendid: transaction.originalEndToEndId,
    input_txinforgnltxid: transaction.originalTxId,
    input_txinforgnlintrbksttlmamt: transaction.originalIntrBkSttlmAmt,
    input_txinfrtrdintrbksttlmamt: transaction.rtrdIntrBkSttlmAmt,
    input_txinfrtrdinstdamt: transaction.rtrdInstdAmt,
    input_txinfchrgbr: transaction.chargeBearer,
    input_txinfchrgsinfamt: transaction.сhrgsInfAmt,
    input_txinfrtrrsninforgtrnm: transaction.originatorClient,
    input_txinfrtrrsninforgtridorgidbicorbei: transaction.originatorBank,
    input_txinfrtrrsninfrsncd: transaction.reasonForReturn,
    input_txinfrtrrsninfaddtinf: transaction.rtrRsnInfAddtInf,
    input_txinforgnltxrefintrbksttlmdt: transaction.originalSettlementDate,
    input_txinforgnltxrefsttlminfsttlmmtd: transaction.originalSettlementMethod,
    input_txinforgnltxrefpmttpinfsvclvlcd: transaction.originalServiceLevel,
    input_txinforgnltxrefdbtrnm: transaction.originalDebtorName,
    input_txinforgnltxrefdbtracctidiban: transaction.originalDebtorAccount,
    input_txinforgnltxrefdbtragtfininstnidbic: transaction.originalDebtorBic,
    input_txinforgnltxrefcdtrnm: transaction.originalCreditorName,
    input_txinforgnltxrefcdtragtfininstnidbic: transaction.originalCreditorAccount,
    input_txinforgnltxrefcdtracctidiban: transaction.originalCreditorBic,
    // input_txsts,
    // input_rsnprty,
    // input_rsncd,
    input_status: status,
    input_author_guid: author_guid,
  }, connection);
};

const importFFPSRPRT = async ({ messageHeader, /* groupHeader, */ transactions, OrgnlGrpInfAndSts }, connection) => {
  const [ /* FFPSRPRT, */ FFCCTRNS, FFPCRQST, PRTRN, /* ROINVSTG */ ] = [ /* "pacs.002.001.03", */ "pacs.008.001.02", "camt.056.001.01", "pacs.004.001.02", /* "camt.029.001.03" */ ];
  let upsertedTransaction;
  if (OrgnlGrpInfAndSts.OrgnlMsgNmId === FFCCTRNS) {
    const storedTransactions = await db.ffcctrns_transaction.get({});
    const transaction = storedTransactions.find(tr => tr.msgid === OrgnlGrpInfAndSts.OrgnlMsgId);
    if (!transaction)
      throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} didn't found in database` };
    
    if (transaction.grsts)
      throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} is already has status ${transaction.grsts}` };
      
    const { guid, cdttrftxinfdbtracctidiban: sender_iban, cdttrftxinfintrbksttlmamt: value, currency = "EUR" } = transaction;

    [ upsertedTransaction ] = await db.ffcctrns_transaction.upsert({
      input_guid: transaction.guid,
      input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId, 
      input_grsts: OrgnlGrpInfAndSts.GrpSts,
      input_rsnprty: OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry,
      input_grphdrmsgid: OrgnlGrpInfAndSts.OrgnlMsgId,
      input_reportmsgid: messageHeader.msgId,
      input_reportdttm: messageHeader.date,
      // input_txsts: transactions[0] ? transactions[0].transactionStatus : undefined,
      // input_rsncd: transactions[0] ? transactions[0].reasonCode : undefined,
      input_author_guid: author_guid,
    }, connection);

    switch (OrgnlGrpInfAndSts.GrpSts) {
    case "ACCP":
      directDebit.accept({ transactionId: guid, senderIban: sender_iban, amount: value, currency: currency || "EUR" }, connection);
      break;
    case "RJCT":
      directDebit.reject({ transactionId: guid, senderIban: sender_iban, amount: value, currency: currency || "EUR" }, connection);
      break;
  
    default:
      break;
    }
  } else if (OrgnlGrpInfAndSts.OrgnlMsgNmId === FFPCRQST){
    const storedTransactions = await db.ffpcrqst_transaction.get({});
    const transaction = storedTransactions.find(tr => tr.msgid === OrgnlGrpInfAndSts.OrgnlMsgId);
    if (!transaction)
      throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} didn't found in database` };
    
    if (transaction.grsts)
      throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} has already status ${transaction.grsts}` };
    
    const { 
      guid, 
      undrlygtxinforgnltxrefdbtracctidiban: sender_iban, 
      undrlygtxinforgnlintrbksttlmamt: value, 
      undrlygtxinforgnlgrpinforgnlmsgid: cancelledMsgId,
      currency = "EUR" 
    } = transaction;

    [ upsertedTransaction ] = await db.ffpcrqst_transaction.upsert({
      input_guid: transaction.guid,
      input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId, 
      input_grsts: OrgnlGrpInfAndSts.GrpSts,
      input_rsnprty: OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry,
      // input_assgnmtid: OrgnlGrpInfAndSts.OrgnlMsgId,
      input_reportmsgid: messageHeader.msgId,
      // input_reportdttm: messageHeader.date,
      // input_undrlygtxinfcxlid: transactions[0] ? transactions[0].paymentTransactionId : undefined,
      // input_txsts: transactions[0] ? transactions[0].transactionStatus : undefined, 
      // input_rsncd: transactions[0] ? transactions[0].reasonCode : undefined,
      input_status: XML_IMPORT_IMPORTED,
      input_author_guid: author_guid,
    }, connection);

    switch (OrgnlGrpInfAndSts.GrpSts) {
    case "ACCP":
      await db.external_transaction_history.upsert({
        input_guid: cancelledMsgId,
        input_status: EXTERNAL_TRANSACTION_CANCELLED
      }, connection);
      await paymentCancellation.cancellationProcessed({ 
        transactionId: guid, 
        senderIban: sender_iban, 
        amount: value, 
        currency 
      }, connection);
      break;
    case "RJCT":
      //nothing to do
      break;
  
    default:
      break;
    }
  }
  else if (OrgnlGrpInfAndSts.OrgnlMsgNmId === PRTRN) {
    const storedTransactions = await db.prtrn_transaction.get({});
    const transaction = storedTransactions.find(tr => tr.msgid === OrgnlGrpInfAndSts.OrgnlMsgId);
    if (!transaction)
      throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} didn't found in database` };
    
    // if (transaction.grsts !== "ACCP")
    //   throw { message: `Transaction for msgId ${OrgnlGrpInfAndSts.OrgnlMsgId} has status ${transaction.grsts}, not ACCP` };

    const { 
      guid, 
      txinforgnltxrefdbtracctidiban: sender_iban, 
      grphdrttlintrbksttlmamt: value,
      txinforgnlgrpinforgnlmsgid: returnedMsgId,
      currency = "EUR" 
    } = transaction;

    
    [ upsertedTransaction ] = await db.prtrn_transaction.upsert({
      input_guid: guid,
      input_msgid: OrgnlGrpInfAndSts.OrgnlMsgId, 
      input_grsts: OrgnlGrpInfAndSts.GrpSts,
      input_rsnprty: OrgnlGrpInfAndSts.StsRsnInf.Rsn.Prtry,
      // input_assgnmtid: OrgnlGrpInfAndSts.OrgnlMsgId,
      input_reportmsgid: messageHeader.msgId,
      input_reportdttm: messageHeader.date,
      // input_txinfrtrid: transactions[0] ? transactions[0].paymentTransactionId : undefined,
      // input_txsts: transactions[0] ? transactions[0].transactionStatus : undefined, 
      // input_rsncd: transactions[0] ? transactions[0].reasonCode : undefined,
      input_status: XML_IMPORT_IMPORTED,
      input_author_guid: author_guid,
    }, connection);

    switch (OrgnlGrpInfAndSts.GrpSts) {
    case "ACCP":
      await db.external_transaction_history.upsert({
        input_guid: returnedMsgId,
        input_status: EXTERNAL_TRANSACTION_RETURNED
      }, connection)
      await paymentReturn.returnProcessed({ 
        senderIban: sender_iban, 
        amount: value, 
        currency 
      }, connection);
      break;
    case "RJCT":
      //nothing to do
      break;
  
    default:
      break;
    }

  } else 
    throw { message: `FFPSRPRT for ${OrgnlGrpInfAndSts.OrgnlMsgNmId} cannot be imported yet` };

  if (!upsertedTransaction)
    throw { message: "Transaction is undefined" };

  const { guid } = upsertedTransaction;
  await db.external_transaction_history.upsert({ 
    input_guid: guid,
    input_status: OrgnlGrpInfAndSts.GrpSts === "ACCP" ||  OrgnlGrpInfAndSts.GrpSts === "ACSC" ? EXTERNAL_TRANSACTION_PROCESSED : EXTERNAL_TRANSACTION_REJECTED,
    input_author_guid: author_guid 
  }, connection);
};