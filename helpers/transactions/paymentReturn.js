const db = require("../db/bankApi");
const { 
  EXTERNAL_TRANSACTION_SENT,
  EXTERNAL_TRANSACTION_SETTLED,
  EXTERNAL_TRANSACTION_ACCEPTED, 
  EXTERNAL_TRANSACTION_REJECTED,
  EXTERNAL_TRANSACTION_RETURNED
} = require("../constants/statuses").externalTransaction;

const { cancelReserved, closeReserved } = require("./account");
const { returnCommission } = require("./commission");

module.exports.accept = async ({ transactionId }, connection) => {
  try {
    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || transaction.status !== EXTERNAL_TRANSACTION_SENT)
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SENT}`}

    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_ACCEPTED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.set = async ({ transactionId, senderIban, amount, currency, returnedMsgId, reason, isCommissionTransfer }, connection) => {
  try {
    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || transaction.status !== EXTERNAL_TRANSACTION_ACCEPTED)
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_ACCEPTED}`}
    if(!isCommissionTransfer) {
      const [ senderAccount ] = await db.account.getByLogin({ input_iban: senderIban, input_currency_code: currency });
      if (!senderAccount)
        throw { message: "Sender account didn't found" };

      if(reason === "FOCR") {

        await returnCommission({ msgId: returnedMsgId });

        await closeReserved({ 
          senderAccountNumber: senderAccount.number, 
          currency, 
          amount 
        }, connection);
      }
    }

    await db.external_transaction_history.updateStatusByMsgId({
      input_msgid: returnedMsgId,
      input_status: EXTERNAL_TRANSACTION_RETURNED
    }, connection);

    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_SETTLED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};

module.exports.reject = async ({ transactionId, senderIban, amount, currency, reason, isCommissionTransfer }, connection) => {
  try {
    const [ transaction ] = await db.external_transaction_history.get({
      input_guid: transactionId
    });

    if(!transaction || ![ EXTERNAL_TRANSACTION_SENT, EXTERNAL_TRANSACTION_ACCEPTED].includes(transaction.status))
      throw { message: `Transaction ${transactionId} status is ${transaction.status} instead of ${EXTERNAL_TRANSACTION_SENT} or ${EXTERNAL_TRANSACTION_ACCEPTED}`}
    if(!isCommissionTransfer) {
      const [ senderAccount ] = await db.account.getByLogin({ input_iban: senderIban,  input_currency_code: currency }, connection);
      if (!senderAccount)
        throw { message: "Sender account didn't found" };

      if(reason === "FOCR")
        await cancelReserved({ 
          senderAccountNumber: senderAccount.number, 
          currency, 
          amount 
        }, connection);
    }
      
    const [ updatedTransaction ] = await db.external_transaction_history.upsert({ 
      input_guid: transactionId,
      input_status: EXTERNAL_TRANSACTION_REJECTED
    }, connection);

    return updatedTransaction;
  }
  catch(err) {
    throw(err)
  }
};