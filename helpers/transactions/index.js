module.exports = { 
  account: require("./account"),
  internalTransaction: require("./internalTransaction"),
  creditTransfer: require("./creditTransfer"),
  paymentCancellation: require("./paymentCancellation"),
  paymentReturn: require("./paymentReturn"),
  resolutionOfInvestigation: require("./resolutionOfInvestigation")
};