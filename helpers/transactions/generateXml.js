const db = require("../db/bankApi");
const { FFCCTRNS } = require("../../modules/messages");

module.exports.generateXml = async ({
  guid,
  system,
  status,
  author_guid,
}, connection) => {

  try {

    const [ transaction ] = await db.ffcctrns_transaction.get({ input_guid: guid }, connection);

    const message = new FFCCTRNS({ 
      sender: transaction.cdttrftxinfdbtragtfininstnidbic, 
      receiver: transaction.cdttrftxinfcdtragtfininstnidbic,
      msgId: transaction.cdttrftxinfpmtidtxid,
      system 
    });

    console.log('5: ', transaction.cdttrftxinfintrbksttlmamt)

    message.addTransaction({
      paymentInstructionId: transaction.cdttrftxinfpmtidinstrid,
      paymentEndToEndId: transaction.cdttrftxinfpmtidendtoendid,
      paymentTransactionId: transaction.cdttrftxinfpmtidtxid,
      serviceLevel: transaction.cdttrftxinfpmtidpmttpinfsvclvlcd,
      amount: transaction.cdttrftxinfintrbksttlmamt,
      chargeBearer: transaction.cdttrftxinfchrgbr,
      debtorName: transaction.cdttrftxinfdbtrnm,
      debtorAccount: transaction.cdttrftxinfdbtracctidiban,
      debtorBic: transaction.cdttrftxinfdbtragtfininstnidbic,
      creditorName: transaction.cdttrftxinfcdtrnm,
      creditorAccount: transaction.cdttrftxinfcdtracctidiban,
      creditorBic: transaction.cdttrftxinfcdtragtfininstnidbic,
      remittanceInformation: transaction.cdttrftxinfrmtinfstrdcdtrrefinfref
    });
    console.log('after add transaction')
    
    message._calculateGroupHeader({});
    console.log('after calculate header')

    const { messageHeader, groupHeader } = message;
    const [ upsertedMessage ] = await db.ffcctrns_transaction.upsert({
      input_guid: transaction.guid,
      input_cdttrftxinfpmtidtxid: transaction.cdttrftxinfpmtidtxid,
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: messageHeader.system,
      input_docid: messageHeader.msgId,
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      input_grphdrmsgid: groupHeader.MsgId,
      input_grphdrcredttm: groupHeader.CreDtTm,
      input_grphdrnboftxs: groupHeader.NbOfTxs,
      input_grphdrttlintrbksttlmamt: groupHeader.TtlIntrBkSttlmAmt,
      input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
      input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
      input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
      input_data: message.toXml(),
      input_author_guid: author_guid,
    }, connection);
    console.log('after upsert')

    const [ updatedStatus ] = await db.external_transaction_history.upsert({
      input_guid: guid,
      input_status: status,
      input_author_guid: author_guid,
    }, connection);
    console.log('after upsert2')

    return updatedStatus;
  }
  catch(err) {
    throw(err)
  }
};