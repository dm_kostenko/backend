const { createLogger, format, transports } = require("winston");
const { timestamp, printf } = format;

const logger = createLogger( {

  transports: [
    new transports.File({
      filename: "./logs/logs.txt",
      format: format.combine(
        timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
        printf ( info => `${info.level} ${info.timestamp} ${info.message}`),
      ),
    }),
  ],

  exceptionHandlers: [
    new transports.Console({ }),
    new transports.File({
      filename: "./logs/logs.txt",
    })
  ]
});

module.exports = logger;