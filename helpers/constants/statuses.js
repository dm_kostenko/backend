module.exports = {
  externalTransaction: {
    EXTERNAL_TRANSACTION_PENDING: "Pending",  // all
    EXTERNAL_TRANSACTION_WAITING: "Waiting",  // ffcctrns (outgoing)
    EXTERNAL_TRANSACTION_FAILED: "Failed",    // all
    EXTERNAL_TRANSACTION_UNCONFIRMED: "Unconfirmed", // all
    EXTERNAL_TRANSACTION_CANCELLED: "Cancelled",  // ffcctrns (incoming)
    EXTERNAL_TRANSACTION_UNAPPROVED: "Unapproved",  // all
    EXTERNAL_TRANSACTION_APPROVED: "Approved",  // all
    EXTERNAL_TRANSACTION_DECLINED: "Declined",  // all
    EXTERNAL_TRANSACTION_SENT: "Sent",          // all
    EXTERNAL_TRANSACTION_ACCEPTED: "Accepted",  // all
    EXTERNAL_TRANSACTION_SETTLED: "Settled",    // all
    EXTERNAL_TRANSACTION_REJECTED: "Rejected",  // all
    EXTERNAL_TRANSACTION_RETURNED: "Returned",  // ffcctrns (outgoing)
    EXTERNAL_TRANSACTION_INVESTIGATED: "Investigated" // ffcctrns (outgoing)
  },
  internalTransaction: {
    INTERNAL_TRANSACTION_PENDING: "Pending",
    INTERNAL_TRANSACTION_SUCCESS: "Success",
    INTERNAL_TRANSACTION_FAILED: "Failed",
  },
  xmlImport: {
    XML_IMPORT_RECEIVED: "Received",
    XML_IMPORT_IMPORTED: "Imported",
    XML_IMPORT_RETURNED : "Imported (Returned)",  // ffpcrqst (incoming)
    XML_IMPORT_INVESTIGATED : "Imported (Investigated)",  // ffpcrqst (incoming)
    XML_IMPORT_ERRORED: "Import errored",
    XML_IMPORT_DECLINED: "Import declined"
  }
};