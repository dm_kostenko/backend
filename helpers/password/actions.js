const db = require("../db/bankApi/");
const bcrypt = require("bcrypt");
const uuidv1 = require("uuid/v1");
const { InternalError, InvalidDataError } = require("../../errorTypes");
const saltRounds = 10;

const hideEmail = (email) => {
  let splitedEmail = email.split("@");
  if (splitedEmail.length !== 2) return email;

  let newEmail;
  if (splitedEmail[0].length > 8) newEmail = splitedEmail[0].substr(0,3) + "*".repeat(splitedEmail[0].length - 3);
  else newEmail = "*".repeat(splitedEmail[0].length);

  return newEmail + "@" + splitedEmail[1];
};

const checkPasswordRequirements = async (loginGuid, password) => {
  if (password.length < 8) throw new InvalidDataError("Minimum password length must be 8 characters!");
  if (!/[a-z]/.test(password)) throw new InvalidDataError("Password must contain at least 1 small letter!");
  if (!/[A-Z]/.test(password)) throw new InvalidDataError("Password must contain at least 1 uppercase letter!");
  if (!/[0-9]/.test(password)) throw new InvalidDataError("Password must contain at least 1 number!");
  if (!/[!#$%&()*+,-./:;<=>?@[\]^_`{|}~]/.test(password)) throw new InvalidDataError("Password must contain at least 1 special symbol!");
  
  if (loginGuid){
    const latestCredentials = await db.latestPasswords.get({ input_guid: loginGuid });
    if (latestCredentials){
      latestCredentials.forEach(credentials => {
        if (bcrypt.compareSync(password, credentials.password))
          throw new InvalidDataError("This password has already been used, create a new one, please!");
      });
    }
  }
};

module.exports.create = async (password) => {
  if (!password) throw new InternalError("password not passed to function create password");

  await checkPasswordRequirements(undefined, password);
  const newSalt = bcrypt.genSaltSync(saltRounds);

  return {
    password: bcrypt.hashSync(password, newSalt),
    salt: newSalt
  };
};


module.exports.change = async (loginGuid, oldPassword, newPassword, authorGuid) => {
  if (!loginGuid) throw new InternalError("loginGuid not passed to function change password");
  if (!oldPassword) throw new InternalError("oldPassword not passed to function change password");
  if (!newPassword) throw new InternalError("newPassword not passed to function change password");

  const [ login ] = await db.showPassword.get({
    input_guid: loginGuid,
    input_author: authorGuid
  });

  if (!login) throw new InternalError(`No response from database with login_guid = ${loginGuid}`);
  if (!bcrypt.compareSync(oldPassword, login.password)) throw new InvalidDataError("Invalid password");

  await checkPasswordRequirements(loginGuid, newPassword);

  const newSalt = bcrypt.genSaltSync(saltRounds);
  await db.login.upsert({
    input_guid: loginGuid,
    input_salt: newSalt,
    input_password: bcrypt.hashSync(newPassword, newSalt),
    input_author: authorGuid
  });

  await db.session.delete({
    input_login_guid: loginGuid
  });
};

module.exports.check = async (loginGuid, password) => {
  if (!loginGuid) throw new InternalError("loginGuid not passed to function check password");
  if (!password) throw new InternalError("password not passed to function check password");

  const [ login ] = await db.showPassword.get({
    input_guid: loginGuid,
    input_author: loginGuid
  });
  if (!login) throw new InternalError(`No response from database with login_guid = ${loginGuid}`);

  return {
    status: bcrypt.compareSync(password, login.password),
    credentials_expired: login.credentials_expired,
    credentials_expire_after: login.credentials_expire_after <= 3 ? login.credentials_expire_after : undefined,
    first_time_login: login.last_login ? undefined : true
  };
};

module.exports.recovery = async (loginGuid, password) => {
  if (!loginGuid) throw new InternalError("loginGuid not passed to function recovery password");
  if (!password) throw new InternalError("password not passed to function recovery password");

  await checkPasswordRequirements(loginGuid, password);

  const salt = bcrypt.genSaltSync(saltRounds);
  await db.login.upsert({
    input_guid: loginGuid,
    input_salt: salt,
    input_password: bcrypt.hashSync(password, salt),
    input_recovery_token: "",
    input_author: loginGuid
  });
};

module.exports.setRecoveryToken = async (loginGuid) => {
  const [ recoveryData ] = await db.recoveryPassword.tokenUpsert({ input_guid: loginGuid, input_recovery_token: uuidv1(), input_author_guid: loginGuid });
  if (!recoveryData || !recoveryData.email) throw new InternalError(`No response from database with login_guid = ${loginGuid}`);

  return {
    token: recoveryData.recovery_token,
    email: hideEmail(recoveryData.email)
  };
};

module.exports.checkRecoveryToken = async (token) => {
  const [ dbRecoveryToken ] = await db.recoveryPassword.tokenCheck({ input_recovery_token: token });

  if (!dbRecoveryToken) return { status: "Token does not exist" };
  if (dbRecoveryToken.recovery_expired) return { status: "Token expired" };
  await db.login.upsert({
    input_guid: dbRecoveryToken.guid,
    input_last_login: new Date().toISOString().slice(0, 19).replace("T", " "),
    input_author: dbRecoveryToken.guid
  })
  return {
    guid: dbRecoveryToken.guid,
    status: "true"
  };
};

