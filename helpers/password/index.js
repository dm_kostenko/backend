module.exports = {
  create: require("./actions").create,
  change: require("./actions").change,
  check: require("./actions").check,
  recovery: require("./actions").recovery,
  setRecoveryToken: require("./actions").setRecoveryToken,
  checkRecoveryToken: require("./actions").checkRecoveryToken,
  generate: require("./generate").generate
};
