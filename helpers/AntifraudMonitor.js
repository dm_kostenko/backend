const db = require("./db/api/"); 
const { AntifraudError } = require("../errorTypes");
const axios = require("axios");

class AntifraudMonitor{
  constructor(bins, shopid){
    this.bins = bins;
    this.shopid = shopid;
  }

  async checkData(data) {
    await this.initRules(this.shopid);
    await Promise.all(this.rules.map(async rule => await this.checkRule(rule, data)));
  }

  async checkRule(rule, data) {
    const { permissionType, type, values } = rule;
    let creditCard, creditCardBin, customerIp, billingAddressCountry;
    if (data.credit_card && data.credit_card.number){
      creditCard = data.credit_card.number;
      creditCardBin = data.credit_card.number.substr(0,6);
    }
    
    if (data.customer && data.customer.ip)
      customerIp = data.customer.ip;

    if (data.billing_address && data.billing_address.country)
      billingAddressCountry = data.billing_address.country;

    switch (type) {
    case "card":
      if (creditCard && (values.includes(creditCard) === (permissionType !== "allow")))
        throw new AntifraudError(`Blacklist blocked request cause of: card - ${rule.name}`);
      break;
    case "ip":
      if (customerIp && ((values.includes(customerIp) === (permissionType !== "allow"))))
        throw new AntifraudError(`Blacklist blocked request cause of: ip - ${rule.name}`);
      break;
    case "country":
      if (billingAddressCountry && ((values.includes(billingAddressCountry) === (permissionType !== "allow"))))
        throw new AntifraudError(`Blacklist blocked request cause of: country(ip) - ${rule.name}`);
      if (customerIp && ((values.includes(await this.getCountryByIp(customerIp)) === (permissionType !== "allow"))))
        throw new AntifraudError(`Blacklist blocked request cause of: country(ip) - ${rule.name}`);
      if (creditCardBin && ((values.includes(this.getCountryByBin(creditCardBin)) === (permissionType !== "allow"))))
        throw new AntifraudError(`Blacklist blocked request cause of: country(credit card)  - ${rule.name}`);
      break;
    }
    return true;
  }

  async initRules(shopid) {
    const [[ shop ]] = await db.shop.get({ input_guid: shopid });
    const [ globalRules ] = await db.blacklistGlobal.get({});
    const [ merchantRules ] = await db.blacklistMerchant.get({ input_merchant_guid: shop.merchant_guid });
    const rulesGuids = globalRules.concat(merchantRules);
  
    const rules = await Promise.all(rulesGuids.map(async ruleGuid => {
      const [[ rule ]] = await db.blacklistRule.get({ input_guid: ruleGuid.blacklist_rule_guid });
      const [ values ] = await db.rule.get({ input_blacklist_rule_guid: ruleGuid.blacklist_rule_guid });
      return {
        name: rule.name,
        permissionType: ruleGuid.type,
        type: rule.type,
        values: values.map(value => value.value)
      };
    }));
    this.rules = rules;
  }
  
  async getCountryByIp(ip) {
    try {
      const { data:{ countryCode } } = await axios.get(`http://ip-api.com/json/${ip}?fields=countryCode`);
      return countryCode;
    } catch (error) {
      return undefined;  
    }
  }

  getCountryByBin(bin) {
    const country = this.bins.find(str => str.startsWith(bin));
    return country ? country.split(";")[1] : undefined;
  }
}

module.exports = AntifraudMonitor;