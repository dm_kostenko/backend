const express = require("express");
const morgan = require("morgan");
const https = require("https");
const { constants } = require("crypto");
const cors = require("cors");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const { requestId, errorHandlers, exposingAuthHeader } = require("./middleware");
const auth = require("./modules/auth");
const fileUpload = require('express-fileupload');
// const messagesImporter = require("./helpers/transactions/messagesImporter");
// const transactionTimeoutChecker = require("./modules/transactionTimeoutChecker");
  
const authConfig = require("./config").auth;
const { port } = require("./config").app;
// const { maxPendingMinutes, intervalMinutes } = require("./config").transactionTimeoutChecker;
const router = require("./routes");
const app = express();

app.use(cors());
app.use(helmet());
app.use(exposingAuthHeader);
app.use(requestId({ uuidVersion: "v4", attributeName: "id", headerName: "X-Request-Id" }));
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.text());
app.use(express.urlencoded({ extended: false }));
app.use(fileUpload());

router.get("/", (req, res, next) => {
   res.json({ status: "UP" });
});
 
app.use("/health", router);


if (process.env.NODE_ENV !== "test") 
  app.use(morgan("common"));


app.use(auth(authConfig));
app.use("/api", router);
app.use(errorHandlers.errorHandler);

// if (process.env.NODE_ENV === "prod" || process.env.NODE_ENV === "uat"){
//   const httpsOptions = {
//     key: require("fs").readFileSync(__dirname + "/config/certs/server.key", "utf8"),
//     cert: require("fs").readFileSync(__dirname + "/config/certs/server.cert", "utf8"),
//     secureOptions: constants.SSL_OP_NO_TLSv1
//   };
//   https.createServer(httpsOptions, app).listen(port, () => console.log(`Https server listening port ${port}`));
// }
// else if (process.env.NODE_ENV !== "test") {
  app.listen(port, () => console.log(`Server listening port ${port}\n`));
  // transactionTimeoutChecker(maxPendingMinutes, intervalMinutes);
// }


// messagesImporter();

module.exports = app;