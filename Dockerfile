FROM node:11.14

# Environment variables
ARG APP_PORT
ARG AML_API_KEY
ARG KAFKA_HOST
ARG KAFKA_PORT
ARG DB_HOST
ARG DB_PORT
ARG DB_USER
ARG DB_PASSWORD
ARG DB_NAME
ARG NODE_ENV
ARG MAILER_USERNAME
ARG MAILER_PASSWORD
ARG SENDER_MAIL
ARG APP_GUI_URL
ARG AWS_REGION
# ARG MONGO_HOST
# ARG MONGO_PORT
# ARG MONGO_NAME
# ARG MONGO_USER
# ARG MONGO_PASSWORD
# ARG MONGO_LOG_HOST
# ARG MONGO_LOG_PORT
# ARG MONGO_LOG_NAME
# ARG MONGO_LOG_USER
# ARG MONGO_LOG_PASSWORD
    
ENV APP_PORT=${APP_PORT}
ENV AML_API_KEY=${AML_API_KEY}
ENV KAFKA_HOST=${KAFKA_HOST}
ENV KAFKA_PORT=${KAFKA_PORT}
ENV DB_HOST=${DB_HOST}
ENV DB_PORT=${DB_PORT}
ENV DB_USER=${DB_USER}
ENV DB_PASSWORD=${DB_PASSWORD}
ENV DB_NAME=${DB_NAME}
ENV NODE_ENV=${NODE_ENV}
ENV MAILER_USERNAME=${MAILER_USERNAME}
ENV MAILER_PASSWORD=${MAILER_PASSWORD}
ENV SENDER_MAIL=${SENDER_MAIL}
ENV APP_GUI_URL=${APP_GUI_URL}
ENV AWS_REGION=${AWS_REGION}
# ENV MONGO_HOST=${MONGO_HOST}
# ENV MONGO_PORT=${MONGO_PORT}
# ENV MONGO_NAME=${MONGO_NAME}
# ENV MONGO_USER=${MONGO_USER}
# ENV MONGO_PASSWORD=${MONGO_PASSWORD}
# ENV MONGO_LOG_HOST=${MONGO_LOG_HOST}
# ENV MONGO_LOG_PORT=${MONGO_LOG_PORT}
# ENV MONGO_LOG_NAME=${MONGO_LOG_NAME}
# ENV MONGO_LOG_USER=${MONGO_LOG_USER}
# ENV MONGO_LOG_PASSWORD=${MONGO_LOG_PASSWORD}

# Create directory app
WORKDIR /usr/src/app

# Install application dependencies
# The wildcard is used to copy both package.json and package-lock.json,
# works with npm @ 5+

COPY package*.json ./
 
RUN npm install --silent --only=production
# Used when assembling code in production
# RUN npm install --only=production

# Скопировать исходники приложения
COPY . .

EXPOSE ${APP_PORT}
CMD npm run start 
#&\
 #  npm run start:prod:kafka:txProcessing &\
  #  npm run start:prod:kafka:currencyExchange &\
   # npm run start:prod:kafka:messageReceiver &\
#    npm run start:prod:kafka:rulesCheck &\
 #   npm run start:prod:kafka:blockAccounts &\
  #  npm run start:prod:service:updateRates &\
   # npm run start:prod:service:sendReceive
