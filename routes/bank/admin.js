const router = require("express").Router();
const { 
  logins, 
  accounts, 
  transactions, 
  documents, 
  entities, 
  privileges, 
  reports, 
  inbox, 
  outbox,
  currencies, 
  rates, 
  rateRuleParams,
  aml
} = require("../../controllers/bank/admin");
const { validationHandler, otpValidationHandler, checkOtp } = require("../../middleware");

router.get("/logins", logins.get);
router.get("/logins/:guid", logins.getByGuid);
router.post("/logins", validationHandler(logins.schema), logins.post);

router.get("/accounts", accounts.get);
router.get("/accounts/:guid", accounts.getByGuid);
router.post("/accounts", validationHandler(accounts.schema), accounts.post);
router.post("/accounts/:guid/limits", validationHandler(accounts.limits.schema), accounts.limits.post);
router.post("/accounts/:guid/currencies", validationHandler(accounts.currencies.schema), accounts.currencies.post);
router.get("/accounts/:number/statement/xml", accounts.statement.xml.get);
router.get("/accounts/:number/statement", accounts.statement.get);

router.get("/accounts/:number/pays", accounts.pays.get);
router.get("/accounts/:number/balances", accounts.balances.get);



router.get("/reports/amountOfTransactions", reports.amountOfTransactions.get);
router.get("/reports/currency", reports.currency.get);
router.get("/reports/transactionHistory", reports.transactionHistory.get);
router.get("/reports/transactionTypes", reports.transactionTypes.get);

router.get("/transactions/history", transactions.history.get);

router.get("/transactions/internal", transactions.internal.get);
router.get("/transactions/internal/:guid", transactions.internal.getByGuid);
router.post("/transactions/internal", validationHandler(transactions.internal.schema), transactions.internal.post);

router.get("/transactions/ffcctrns", transactions.ffcctrns.get);
router.get("/transactions/ffcctrns/:guid/", transactions.ffcctrns.getByGuid);
router.post("/transactions/ffcctrns/:guid/status", validationHandler(transactions.ffcctrns.status.schema), transactions.ffcctrns.status.post);
router.post("/transactions/ffcctrns", validationHandler(transactions.ffcctrns.schema), otpValidationHandler(transactions.ffcctrns.otpSchema), checkOtp, transactions.ffcctrns.post);
router.post("/transactions/ffcctrns/fee", validationHandler(transactions.ffcctrns.fee.schema), transactions.ffcctrns.fee.post);
router.post("/transactions/ffcctrns/array", validationHandler(transactions.ffcctrns.array.schema), checkOtp, transactions.ffcctrns.array.post);


router.get("/transactions/ffpcrqst", transactions.ffpcrqst.get);
router.get("/transactions/ffpcrqst/:guid/", transactions.ffpcrqst.getByGuid);
router.post("/transactions/ffpcrqst/:guid/status", validationHandler(transactions.ffpcrqst.status.schema), transactions.ffpcrqst.status.post);
router.post("/transactions/ffpcrqst", validationHandler(transactions.ffpcrqst.schema), transactions.ffpcrqst.post);

router.get("/transactions/prtrn", transactions.prtrn.get);
router.get("/transactions/prtrn/:guid/", transactions.prtrn.getByGuid);
router.post("/transactions/prtrn/:guid/status", validationHandler(transactions.prtrn.status.schema), transactions.prtrn.status.post);
router.post("/transactions/prtrn", validationHandler(transactions.prtrn.schema), transactions.prtrn.post);

router.get("/transactions/roinvstg", transactions.roinvstg.get);
router.get("/transactions/roinvstg/:guid/", transactions.roinvstg.getByGuid);
router.post("/transactions/roinvstg/:guid/status", validationHandler(transactions.roinvstg.status.schema), transactions.roinvstg.status.post);
router.post("/transactions/roinvstg", validationHandler(transactions.roinvstg.schema), transactions.roinvstg.post);

router.get("/transactions/rules", transactions.rules.get);
router.post("/transactions/rules", validationHandler(transactions.rules.schema), transactions.rules.post);

router.get("/transactions/internalTemplates", transactions.internalTemplates.get);
router.post("/transactions/internalTemplates", validationHandler(transactions.internalTemplates.schema), transactions.internalTemplates.post);
router.get("/transactions/externalTemplates", transactions.externalTemplates.get);
router.post("/transactions/externalTemplates", validationHandler(transactions.externalTemplates.schema), transactions.externalTemplates.post);
router.get("/transactions/templates", transactions.templates.get);
router.post("/transactions/templates", transactions.templates.post);



router.get("/documents/ffcctrns", documents.ffcctrns.get);
router.get("/documents/ffcctrns/:guid/", documents.ffcctrns.getByGuid);

router.get("/documents/ffpcrqst", documents.ffpcrqst.get);
router.get("/documents/ffpcrqst/:guid/", documents.ffpcrqst.getByGuid);
router.get("/documents/ffpcrqst/:guid/xml", documents.ffpcrqst.xml.get);
router.post("/documents/ffpcrqst", validationHandler(documents.ffpcrqst.schema), documents.ffpcrqst.post);

router.get("/documents/prtrn", documents.prtrn.get);
router.get("/documents/prtrn/:guid/", documents.prtrn.getByGuid);
router.get("/documents/prtrn/:guid/xml", documents.prtrn.xml.get);
router.post("/documents/prtrn", validationHandler(documents.prtrn.schema), documents.prtrn.post);

router.post("/documents/incoming", documents.postIncomingXml);

router.get("/entities", entities.get);
router.post("/entities", validationHandler(entities.schema), entities.post);

router.get("/inbox", inbox.get);
router.post("/inbox", inbox.post);
router.get("/inbox/:guid", inbox.getByGuid);
router.post("/inbox/:guid/status", validationHandler(inbox.status.schema), inbox.status.post);
// router.get("/messages", messages.get);
// router.get("/messages/:guid", messages.getByGuid);
// router.post("/messages", messages.post);

router.get("/outbox", outbox.get);
router.post("/outbox", outbox.post);

router.get("/privileges", privileges.get);
router.get("/privileges/:guid", privileges.getByGuid);
router.post("/privileges", validationHandler(privileges.schema), privileges.post);

router.get("/currencies", currencies.get);
router.get("/currencies/rates", currencies.rates.get);

router.get("/rates", rates.get);
router.get("/rates/:guid", rates.getByGuid);
router.post("/rates"/* , validationHandler(rates.schema) */, rates.post);
router.get("/rates/:guid/rules", rates.rules.get);
router.get("/rates/:guid/params", rates.params.get);
router.get("/rates/:guid/rules/:ruleGuid/params", rates.rules.params.get);
router.post("/rateRuleParams", validationHandler(rateRuleParams.schema), rateRuleParams.post);

router.get("/aml/tags", aml.tags.get);
router.post("/aml/tags", aml.tags.post);
router.get("/aml/:caseId/files", aml.files.get);
router.post("/aml/:caseId/files", aml.files.post);
router.get("/aml/tags/:caseId/case", aml.tags.case.get);
router.post("/aml/tags/:caseId/case", aml.tags.case.post);
router.get("/aml", aml.get);
router.get("/aml/:id", aml.getById);
router.post("/aml", /*validationHandler(aml.schema),*/ aml.post);
router.post("/aml/:id", /*validationHandler(aml.schema),*/ aml.postById);
router.get("/aml/:id/certificate", aml.certificate.get);
router.get("/aml/comments/:caseId", aml.comments.get);
router.post("/aml/comments/:caseId", aml.comments.post);


module.exports = router;