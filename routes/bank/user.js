const router = require("express").Router();
const { accounts, transactions, reports, info, logins, currencies } = require("../../controllers/bank/user");
const { validationHandler, hasPrivilege, checkOtp } = require("../../middleware");

router.get("/accounts", hasPrivilege({ individual: [ "READ_ACCOUNTS" ] }), accounts.get);
router.get("/accounts/:guid", hasPrivilege({ individual: [ "READ_ACCOUNTS" ] }), accounts.getByGuid);

router.get("/transactions/history", hasPrivilege({ individual: [ "READ_TRANSACTIONS" ] }),  transactions.history.get);
router.get("/transactions/history/:guid", hasPrivilege({ individual: [ "READ_TRANSACTIONS" ] }),  transactions.history.getByGuid);
// router.get("/transactions/history/:guid/otp",  transactions.history.otp.getByGuid);

router.get("/transactions/internal", hasPrivilege({ individual: [ "READ_TRANSACTIONS" ] }), transactions.internal.get);
router.get("/transactions/internal/:guid", hasPrivilege({ individual: [ "READ_TRANSACTIONS" ] }), transactions.internal.getByGuid);
router.post("/transactions/internal", hasPrivilege({ individual: [ "CREATE_TRANSACTIONS" ] }), checkOtp, validationHandler(transactions.internal.schema), transactions.internal.post);

router.get("/transactions/ffcctrns", hasPrivilege({ individual: [ "READ_TRANSACTIONS" ] }), transactions.ffcctrns.get);
router.get("/transactions/ffcctrns/:guid", hasPrivilege({ individual: [ "READ_TRANSACTIONS" ] }), transactions.ffcctrns.getByGuid);
router.post("/transactions/ffcctrns/:guid/status", hasPrivilege({ individual: [ "APPROVE_CANCEL_TRANSACTIONS" ] }), checkOtp, validationHandler(transactions.ffcctrns.status.schema), transactions.ffcctrns.status.post);
router.post("/transactions/ffcctrns", hasPrivilege({ individual: [ "CREATE_TRANSACTIONS" ] }), checkOtp, validationHandler(transactions.ffcctrns.schema), transactions.ffcctrns.post);

router.get("/reports/amountOfTransactions", reports.amountOfTransactions.get);
router.get("/reports/currency", reports.currency.get);
router.get("/reports/transactionHistory", reports.transactionHistory.get);

router.get("/transactions/internalTemplates", transactions.internalTemplates.get);
router.post("/transactions/internalTemplates", validationHandler(transactions.internalTemplates.schema), transactions.internalTemplates.post);
router.get("/transactions/externalTemplates", transactions.externalTemplates.get);
router.post("/transactions/externalTemplates", validationHandler(transactions.externalTemplates.schema), transactions.externalTemplates.post);

router.get("/logins", logins.get);
router.post("/logins", checkOtp, validationHandler(logins.schema), logins.post);

router.get("/info", info.get);

// router.get("/messages", messages.get);
// router.get("/messages/:guid", messages.getByGuid);
// router.post("/messages", messages.post);

router.get("/currencies", currencies.get);
router.get("/currencies/rates", currencies.rates.get);

module.exports = router;