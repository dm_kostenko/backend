const router = require("express").Router();
const { checkType } = require("../../middleware");

router.use("/admin", checkType(["admin", "aml", "finance"]),  require("./admin"));
router.use("/user", require("./user"));

module.exports = router;