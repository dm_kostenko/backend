const router = require("express").Router();
const reports = require("../../controllers/v1/reports/");
const { hasPrivelege } = require("../../middleware");

router.get("/admin/logs/users", hasPrivelege([ "REPORT_ADMINLOGSUSERS" ], reports.logs.admin_users.canGet), reports.logs.admin_users.get);
router.get("/admin/logs/transactions", hasPrivelege([ "REPORT_ADMINLOGSTRANSACTIONS" ], reports.logs.admin_transactions.canGet), reports.logs.admin_transactions.get);

router.get("/merchant/logs/transactions", hasPrivelege([ "REPORT_MERCHANTLOGSTRANSACTIONS" ], reports.logs.merchant_transactions.canGet), reports.logs.merchant_transactions.get);
router.get("/group/logs/transactions", hasPrivelege([ "REPORT_GROUPLOGSTRANSACTIONS" ], reports.logs.group_transactions.canGet), reports.logs.group_transactions.get);
router.get("/partner/logs/transactions", hasPrivelege([ "REPORT_PARTNERLOGSTRANSACTIONS" ], reports.logs.partner_transactions.canGet), reports.logs.partner_transactions.get);

router.get("/amount_of_shops", hasPrivelege([ "REPORT_AMOUNTOFSHOPS" ]), reports.amount_of_shops.get);
router.get("/amount_of_merchants", hasPrivelege([ "REPORT_AMOUNTOFMERCHANTS" ], reports.amount_of_merchants.canGet), reports.amount_of_merchants.get);
router.get("/amount_of_groups", hasPrivelege([ "REPORT_AMOUNTOFGROUPS" ], reports.amount_of_groups.canGet), reports.amount_of_groups.get);
router.get("/amount_of_partners", hasPrivelege([ "REPORT_AMOUNTOFPARTNERS" ], reports.amount_of_partners.canGet), reports.amount_of_partners.get);

router.get("/balance", hasPrivelege([ "REPORT_BALANCE" ]), reports.balance.get);
router.get("/currencies_of_transactions", hasPrivelege([ "REPORT_CURRENCIESOFTRANSACTIONS" ]), reports.currencies_of_transactions.get);
router.get("/currencies", hasPrivelege([ "REPORT_CURRENCIES" ]), reports.currencies.get);
router.get("/daily_transactions", hasPrivelege([ "REPORT_DAILYTRANSACTIONS" ]), reports.daily_transactions.get);

router.get("/new_clients", hasPrivelege([ "REPORT_NEWCLIENTS" ], reports.new_clients.canGet), reports.new_clients.get);
router.get("/orders", hasPrivelege([ "REPORT_ORDERS" ], reports.orders.canGet), reports.orders.get);

router.get("/shop_totals", hasPrivelege([ "REPORT_SHOPTOTALS" ]), reports.shop_totals.get);
router.get("/step_logs", hasPrivelege([ "REPORT_STEPLOGS" ]), reports.step_logs.get);

router.get("/top_shops", hasPrivelege([ "REPORT_TOPSHOPS" ]), reports.top_shops.get);
router.get("/top_merchants", hasPrivelege([ "REPORT_TOPMERCHANTS" ], reports.top_merchants.canGet), reports.top_merchants.get);
router.get("/top_groups", hasPrivelege([ "REPORT_TOPGROUPS" ], reports.top_groups.canGet), reports.top_groups.get);
router.get("/top_partners", hasPrivelege([ "REPORT_TOPPARTNERS" ], reports.top_partners.canGet), reports.top_partners.get);

router.get("/totals", hasPrivelege([ "REPORT_TOTALS" ]), reports.totals.get);
router.get("/total_processed", hasPrivelege([ "REPORT_TOTALPROCESSED" ]), reports.total_processed.get);
router.get("/total_to_client", hasPrivelege([ "REPORT_TOTALTOCLIENT" ]), reports.total_to_client.get);
router.get("/total_to_processor", hasPrivelege([ "REPORT_TOTALTOPROCESSOR" ]), reports.total_to_processor.get);
router.get("/transaction_history", hasPrivelege([ "REPORT_TRANSACTIONHISTORY" ]), reports.transaction_history.get);
router.get("/transaction_logs", hasPrivelege([ "REPORT_TRANSACTIONLOGS" ]), reports.transaction_logs.get);
router.get("/transaction_types", hasPrivelege([ "REPORT_TRANSACTIONTYPES" ]), reports.transaction_types.get);

module.exports = router;