const router = require("express").Router();
const { payment } = require("../../controllers/v1/secure");
const { validationHandler } = require("../../middleware");

router.get("/payment", payment.get);
router.post("/payment", validationHandler(payment.schema), payment.post);

module.exports = router;