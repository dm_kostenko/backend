const router = require("express").Router();
const { user_profiles, roles, partners, groups, merchants, privileges, currencies, accounts, cards, gateways, shops, rates, logins, blacklist, transactions, steps, charges } = require("../../controllers/v1/management");
const { hasPrivelege, validationHandler } = require("../../middleware");

router.get("/logins", hasPrivelege([ "READ_LOGINS" ], logins.canGet), logins.get);
router.get("/logins/free", /*hasPrivelege([ "READ_LOGINS" ], logins.canGet),*/ logins.free.get);
router.get("/logins/:guid", hasPrivelege([ "READ_LOGINDETAIL" ], logins.canGetByGuid), logins.getByGuid);
router.post("/logins", hasPrivelege([ "EXECUTE_LOGINS" ], logins.canPost), validationHandler(logins.schema), logins.post);
router.get("/logins/:guid/accounts", hasPrivelege([ "READ_LOGINDETAIL" ], logins.canGetByGuid), logins.accounts.get);
router.post("/logins/:guid/accounts", hasPrivelege([ "EXECUTE_LOGINDETAIL" ]), validationHandler(logins.accounts.schema), logins.accounts.post);
router.get("/logins/:guid/roles", hasPrivelege([ "READ_LOGINDETAIL" ], logins.canGetByGuid), logins.roles.get);
router.post("/logins/:guid/roles", hasPrivelege([ "EXECUTE_LOGINDETAIL" ]), validationHandler(logins.roles.schema), logins.roles.post);
router.get("/user_profiles/:guid", user_profiles.getByGuid);

router.get("/roles", hasPrivelege([ "READ_ROLES" ], roles.canGet), roles.get);
router.get("/roles/:guid", hasPrivelege([ "READ_ROLESDETAIL" ], roles.canGetByGuid), roles.getByGuid);
router.post("/roles", hasPrivelege([ "EXECUTE_ROLES" ], roles.canPost), validationHandler(roles.schema), roles.post);

router.get("/privileges", privileges.get);
router.get("/privileges/:guid", privileges.getByGuid);
router.post("/privileges", validationHandler(privileges.schema), privileges.post);
router.get("/roles/:guid/privileges", roles.privileges.get);
router.post("/roles/:guid/privileges", validationHandler(roles.privileges.schema), roles.privileges.post);

router.get("/partners", hasPrivelege([ "READ_PARTNERS" ], partners.canGet), partners.get);
router.post("/partners", hasPrivelege([ "EXECUTE_PARTNERS" ], partners.canPost), validationHandler(partners.schema), partners.post);
router.get("/partners/:guid", hasPrivelege([ "READ_PARTNERDETAIL" ], partners.canGetByGuid), partners.getByGuid);
router.get("/partners/:guid/logins", hasPrivelege([ "READ_PARTNERDETAIL" ], partners.logins.canGet), partners.logins.get);
router.post("/partners/:guid/logins", hasPrivelege([ "EXECUTE_PARTNERDETAIL" ], partners.logins.canPost), validationHandler(partners.logins.schema), partners.logins.post);
router.get("/partners/:guid/groups", hasPrivelege([ "READ_PARTNERDETAIL" ], partners.groups.canGet), partners.groups.get);
router.get("/partners/:guid/merchants", hasPrivelege([ "READ_PARTNERDETAIL" ], partners.merchants.canGet), partners.merchants.get);
router.get("/partners/:guid/shops", hasPrivelege([ "READ_PARTNERDETAIL" ], partners.shops.canGet), partners.shops.get);

router.get("/groups", hasPrivelege([ "READ_GROUPS" ], groups.canGet), groups.get);
router.post("/groups", hasPrivelege([ "EXECUTE_GROUPS" ]), validationHandler(groups.schema), groups.post);
router.get("/groups/:guid", hasPrivelege([ "READ_GROUPDETAIL" ], groups.canGetByGuid), groups.getByGuid);
router.get("/groups/:guid/logins", hasPrivelege([ "READ_GROUPDETAIL" ], groups.logins.canGet), groups.logins.get);
router.get("/groups/:guid/merchants", hasPrivelege([ "READ_GROUPDETAIL" ], groups.merchants.canGet), groups.merchants.get);
router.get("/groups/:guid/shops", hasPrivelege([ "READ_GROUPDETAIL" ], groups.shops.canGet), groups.shops.get);
router.post("/groups/:guid/logins", hasPrivelege([ "EXECUTE_GROUPDETAIL" ], groups.logins.canPost), validationHandler(groups.logins.schema), groups.logins.post);

router.get("/merchants", hasPrivelege([ "READ_MERCHANTS" ], merchants.canGet), merchants.get);
router.post("/merchants", hasPrivelege([ "EXECUTE_MERCHANTS" ], merchants.canPost), validationHandler(merchants.schema), merchants.post);
router.get("/merchants/:guid", hasPrivelege([ "READ_MERCHANTDETAIL" ], merchants.canGetByGuid), merchants.getByGuid);
router.get("/merchants/:guid/logins", hasPrivelege([ "READ_MERCHANTDETAIL" ], merchants.logins.canGetByGuid), merchants.logins.get);
router.get("/merchants/:guid/shops", hasPrivelege([ "READ_MERCHANTDETAIL" ], merchants.shops.canGet), merchants.shops.get);
router.post("/merchants/:guid/logins", hasPrivelege([ "EXECUTE_MERCHANTDETAIL" ], merchants.logins.canPost), validationHandler(merchants.logins.schema), merchants.logins.post);
router.get("/merchants/:guid/blacklists", hasPrivelege([ "READ_MERCHANTDETAIL" ], merchants.blacklists.canGetByGuid), merchants.blacklists.get);
router.post("/merchants/:guid/blacklists", hasPrivelege([ "EXECUTE_MERCHANTDETAIL" ], merchants.blacklists.canPost), validationHandler(merchants.blacklists.schema), merchants.blacklists.post);

router.get("/currencies", currencies.get);
router.post("/currencies", validationHandler(currencies.schema), currencies.post);
router.get("/currencies/:guid", currencies.getByGuid);

router.get("/accounts", accounts.get);
router.post("/accounts", validationHandler(accounts.schema), accounts.post);
router.get("/accounts/:guid", accounts.getByGuid);

router.get("/cards", cards.get);
router.post("/cards", validationHandler(cards.schema), cards.post);
router.get("/cards/:guid", cards.getByGuid);

router.get("/gateways", gateways.get);
router.post("/gateways", validationHandler(gateways.schema), gateways.post);
router.get("/gateways/:guid", gateways.getByGuid);
router.get("/gateways/:guid/props/:name?", gateways.props.get);
router.post("/gateways/:guid/props", validationHandler(gateways.props.schema), gateways.props.post);

router.get("/shops", hasPrivelege([ "READ_SHOPS" ], shops.canGet), shops.get);
router.get("/shops/:guid", hasPrivelege([ "READ_SHOPDETAIL" ]), shops.getByGuid);
router.post("/shops", hasPrivelege([ "EXECUTE_SHOPS" ], shops.canPost), validationHandler(shops.schema), shops.post);

router.get("/shops/:guid/accounts", hasPrivelege([ "READ_SHOPDETAIL" ]), shops.accounts.get);
router.post("/shops/:guid/accounts", hasPrivelege([ "EXECUTE_SHOPDETAIL" ]), validationHandler(shops.accounts.schema), shops.accounts.post);

router.get("/shops/:guid/gateways", hasPrivelege([ "READ_SHOPDETAIL" ]), shops.gateways.get);
router.post("/shops/:guid/gateways", hasPrivelege([ "EXECUTE_SHOPDETAIL" ]), validationHandler(shops.gateways.schema), shops.gateways.post);

router.get("/shops/:guid/gateways/:gatewayGuid/props", hasPrivelege([ "READ_SHOPDETAIL" ]), shops.gateways.props.getLabels);
router.post("/shops/:guid/gateways/:gatewayGuid/props", hasPrivelege([ "EXECUTE_SHOPDETAIL" ]), validationHandler(shops.gateways.props.schema), shops.gateways.props.post);

router.get("/rates", hasPrivelege([ "READ_RATES" ]), rates.get);
router.post("/rates", hasPrivelege([ "EXECUTE_RATES" ]), validationHandler(rates.schema), rates.post);
router.get("/rates/shop/:shop_guid/gateway/:gateway_guid/currency/:currency_guid", hasPrivelege([ "READ_RATEDETAIL" ]), rates.getByGuid);
router.get("/rates/transactions/shop/:shop_guid/gateway/:gateway_guid/currency/:currency_guid", hasPrivelege([ "READ_RATEDETAIL" ]), rates.transactions.getByGuid);

router.get("/charges/one_time", hasPrivelege([ "READ_CHARGEONETIME" ], charges.one_time.canGet), charges.one_time.get);
router.get("/charges/one_time/shop/:shop_guid/gateway/:gateway_guid/currency/:currency_guid", hasPrivelege([ "READ_CHARGEONETIMEDETAIL" ], charges.one_time.canGetByGuid), charges.one_time.getByGuid);
router.post("/charges/one_time", hasPrivelege([ "EXECUTE_CHARGEONETIME" ], charges.one_time.canPost), validationHandler(charges.one_time.schema), charges.one_time.post);

router.get("/charges/periodic", hasPrivelege([ "READ_CHARGEPERIODIC" ], charges.periodic.canGet), charges.periodic.get);
router.get("/charges/periodic/shop/:shop_guid/gateway/:gateway_guid/currency/:currency_guid", hasPrivelege([ "READ_CHARGEPERIODICDETAIL" ], charges.periodic.canGetByGuid), charges.periodic.getByGuid);
router.post("/charges/periodic", hasPrivelege([ "EXECUTE_CHARGEPERIODIC" ], charges.periodic.canPost), validationHandler(charges.periodic.schema), charges.periodic.post);

router.get("/charges/conditional", hasPrivelege([ "READ_CHARGECONDITIONAL" ], charges.conditional.canGet), charges.conditional.get);
router.get("/charges/conditional/shop/:shop_guid/gateway/:gateway_guid/currency/:currency_guid", hasPrivelege([ "READ_CHARGECONDITIONALDETAIL" ], charges.conditional.canGetByGuid), charges.conditional.getByGuid);
router.post("/charges/conditional", hasPrivelege([ "EXECUTE_CHARGECONDITIONAL" ], charges.conditional.canPost), validationHandler(charges.conditional.schema), charges.conditional.post);

router.get("/blacklist", hasPrivelege([ "READ_MERCHANTSBLACKLIST" ]), blacklist.get);
router.post("/blacklist", hasPrivelege([ "EXECUTE_BLACKLISTS" ]), validationHandler(blacklist.schema), blacklist.post);
router.post("/blacklist/values", hasPrivelege([ "EXECUTE_BLACKLISTS" ]), validationHandler(blacklist.values.schema), blacklist.values.post);
router.get("/blacklist/merchants", hasPrivelege([ "READ_BLACKLISTS" ]), blacklist.merchants.get);
router.get("/blacklist/merchants/:guid", hasPrivelege([ "READ_BLACKLISTS" ]), blacklist.merchants.getByGuid);
router.post("/blacklist/merchants/", hasPrivelege([ "EXECUTE_BLACKLISTS" ]), validationHandler(blacklist.merchants.schema), blacklist.merchants.post);
router.get("/blacklist/global", hasPrivelege([ "READ_BLACKLISTS" ]), blacklist.global.get);
router.post("/blacklist/global", hasPrivelege([ "EXECUTE_BLACKLISTS" ]), validationHandler(blacklist.global.schema), blacklist.global.post);
router.get("/blacklist/:guid", hasPrivelege([ "READ_BLACKLISTDETAIL" ]), blacklist.getByGuid);

router.get("/transactions", hasPrivelege([ "READ_TRANSACTIONTEMPLATES" ]), transactions.get);
router.get("/transactions/gateways/:guid", hasPrivelege([ "READ_TRANSACTIONTEMPLATES" ]), transactions.gateways.get);
router.get("/transactions/processing", hasPrivelege([ "READ_TRANSACTIONSHISTORY" ]), transactions.processing.get);
router.get("/transactions/processing/:guid", hasPrivelege([ "READ_TRANSACTIONSHISTORY" ]), transactions.processing.getByGuid);
router.get("/transactions/:guid", hasPrivelege([ "READ_TRANSACTIONTEMPLATES" ]), transactions.getByGuid);
router.post("/transactions", hasPrivelege([ "EXECUTE_TRANSACTIONTEMPLATES" ]), validationHandler(transactions.schema), transactions.post);
router.get("/transactions/:guid/steps", hasPrivelege([ "READ_TRANSACTIONTEMPLATES" ]), transactions.steps.get);
router.post("/transactions/:guid/steps", hasPrivelege([ "EXECUTE_TRANSACTIONTEMPLATES" ]), validationHandler(transactions.steps.schema), transactions.steps.post);

router.get("/steps/processing/", hasPrivelege([ "READ_TRANSACTIONSHISTORY" ]), steps.processing.get);
router.get("/steps/processing/:guid", hasPrivelege([ "READ_TRANSACTIONSHISTORY" ]), steps.processing.getByGuid);
router.get("/steps/processing/:guid/params", hasPrivelege([ "READ_TRANSACTIONSHISTORY" ]), steps.processing.params.get);
router.get("/steps/processing/:guid/params/:name", hasPrivelege([ "READ_TRANSACTIONSHISTORY" ]), steps.processing.params.getByName);

router.get("/steps", hasPrivelege([ "READ_TRANSACTIONTEMPLATES" ]), steps.get);
router.get("/steps/:guid", hasPrivelege([ "READ_TRANSACTIONTEMPLATES" ]), steps.getByGuid);
router.post("/steps", hasPrivelege([ "EXECUTE_TRANSACTIONTEMPLATES" ]), validationHandler(steps.schema), steps.post);
router.get("/steps/:guid/params", hasPrivelege([ "READ_TRANSACTIONTEMPLATES" ]), steps.params.get);
router.post("/steps/:guid/params", hasPrivelege([ "EXECUTE_TRANSACTIONTEMPLATES" ]), validationHandler(steps.params.schema), steps.params.post);

module.exports = router;
