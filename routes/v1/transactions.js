const router = require("express").Router();
const { transactionValidator, getShopGatewayProps, antifraudMonitor } = require("../../middleware/transactions");
const { create } = require("../../controllers/v1/transactions");

router.post("/create", transactionValidator, antifraudMonitor, getShopGatewayProps, create.post);

module.exports = router;