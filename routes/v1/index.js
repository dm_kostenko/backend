const router = require("express").Router();
const { logger } = require("../../middleware");
const { verifyShop } = require("../../middleware/transactions");

router.use("/reports", require("./reports"));
router.use("/management", logger.reqResLog, require("./management"));
router.use("/transactions", verifyShop, require("./transactions"));
router.use("/secure", require("./secure"));

module.exports = router;