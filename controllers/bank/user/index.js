module.exports = {
  accounts: require("./accounts"),
  entities: require("./entities"),
  transactions: require("./transactions"),
  reports: require("./reports"),
  info: require("./info"),
  logins: require("./logins"),
  currencies: require("./currencies")
};