const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const {
    number,
    iban,
    currency,
  } = req.query;
  const login_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    const report = await db.report.currency({ 
      input_login_guid: login_guid,
      input_number: number,
      input_iban: iban,
      input_currency: currency,
    });

    res.status(200).json({ count: report[0] ? report[0].count : 0, data: report });
  }
  catch (err) {
    next(err);
  }
};