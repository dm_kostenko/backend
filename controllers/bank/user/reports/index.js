module.exports = {
  amountOfTransactions: require("./amountOfTransactions"),
  currency: require("./currency"),
  transactionHistory: require("./transactionHistory"),
};