const db = require("../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");
const { init } = require("../../../../modules/kafka/services");

module.exports.schema = () => {
  return Joi.object().keys({
    sender_number: Joi.string().length(11).required(),
    receiver_number: Joi.string().length(11).required(),
    receiver_currency: Joi.string().required(),
    sender_currency: Joi.string().required(),
    value: Joi.number().positive().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

    let transactions = await db.transaction_history.get({ input_login_guid: author_guid, input_type: "INTERNAL" });

    transactions.forEach(item => {
      item.value /= 100;
    })

    const numberOfTransactions = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfTransactions, data: transactions });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    let [ transaction ] = await db.transaction_history.get({ input_guid: guid, input_login_guid: author_guid, input_type: "INTERNAL" });
    if(transaction) {

      transaction.value /= 100;

      const [{
        from_currency_code, 
        to_currency_code,
        rate,
        date,
        from_amount,
        to_amount 
      }] = await db.transaction_history.getByAdmin({ 
        input_guid: guid,
        input_direction: "internal" 
      });

      transaction = {
        ...transaction,
        from_currency_code, 
        to_currency_code,
        rate,
        date,
        from_amount: from_amount / 100,
        to_amount: to_amount / 100
      };
    }

    res.status(200).json(transaction || {});
  } catch (err) {
    next(err);
  }
};

module.exports.post = (req, res, next) => init(req, res, next, "Internal");