const Joi = require("@hapi/joi");
const db = require("../../../../../helpers/db/bankApi");
const { EXTERNAL_TRANSACTION_UNAPPROVED, EXTERNAL_TRANSACTION_CANCELLED, EXTERNAL_TRANSACTION_UNCONFIRMED } = require("../../../../../helpers/constants/statuses").externalTransaction;
const { InvalidDataError, InternalError } = require("../../../../../errorTypes");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });


  return Joi.object().keys({
    // type: 
    status: Joi.equal([ EXTERNAL_TRANSACTION_CANCELLED, EXTERNAL_TRANSACTION_UNAPPROVED ]).required()  
  });
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { status, /* otp */ } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";
  const type = req.auth ? req.auth.type : undefined;

  try {
    const [ extTransaction ] = await db.external_transaction_history.get({ input_guid: guid });
    let individualTransactions;
    
    individualTransactions = await db.transaction_history.getByLogin({ 
      input_login_guid: author_guid,
      input_type: "FFCCTRNS" 
    });
    if (type === "individual"){
      const entitiesTransactions = await db.transaction_history.getByLogin({ 
        input_login_guid: author_guid,
        input_type: "FFCCTRNS" 
      });
      individualTransactions = [ ...individualTransactions, ...entitiesTransactions ];
    }
    if (!extTransaction || !individualTransactions.some(tr => tr.guid === guid))
      throw { message: `Cannot find transaction ${guid} by ${author_guid}` };

    if (extTransaction.status !== EXTERNAL_TRANSACTION_UNAPPROVED && extTransaction.status !== EXTERNAL_TRANSACTION_UNCONFIRMED)
      throw { message: `Status of transaction ${guid} cannot be changed because status is ${extTransaction ? extTransaction.status : "undefined"}` }; 

    if(type === "entity" && (
      (extTransaction.status === EXTERNAL_TRANSACTION_UNCONFIRMED)
      || 
      (status !== EXTERNAL_TRANSACTION_CANCELLED)))
      throw { message: `Cannot change status ${extTransaction.guid} from ${extTransaction.status} to ${status}` };

    if(type === "individual" && (
      (extTransaction.status !== EXTERNAL_TRANSACTION_UNCONFIRMED && extTransaction.status !== EXTERNAL_TRANSACTION_UNAPPROVED)
      || 
      (status !== EXTERNAL_TRANSACTION_UNAPPROVED && status !== EXTERNAL_TRANSACTION_CANCELLED)))
      throw { message: `Cannot change status ${extTransaction.guid} from ${extTransaction.status} to ${status}` };
    
    const [ upsertedMessage ] = await db.external_transaction_history.upsert({ input_guid: guid, input_status: status, input_author_guid: author_guid });

    return res.status(200).json(upsertedMessage);
  }
  catch (err) {
    next(err);
  }
};


