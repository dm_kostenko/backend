const Joi = require("@hapi/joi");
const db = require("../../../../../helpers/db/bankApi");
const cryptoRandomString = require("crypto-random-string");
const { mail } = require("../../../../../config/authorizationModule");
/// SEND OTP TO MAIL. DEPRECATED
module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";
  const type = req.auth ? req.auth.type : undefined;

  try {
    const { email } = req.auth;
    if (!email) { 
      throw { message: "Email is undefined" };
    }
    const [ extTransaction ] = await db.external_transaction_history.get({ input_guid: guid });
    let individualTransactions;
    if (type === "individual")
      individualTransactions = await db.transaction_history.getByLogin({ input_login_guid: author_guid });
    
    if (!extTransaction || !individualTransactions.some(tr => tr.guid === guid))
      throw { message: `Cannot find external transaction ${guid} by ${author_guid}` };

    
    if (extTransaction.status.toLowerCase() !== "unconfirmed")
      throw { message: `Status of transaction ${guid} is ${extTransaction.status}, but must be Unconfirmed` }; 

    let otp;
    const [ transaction_otp ] = await db.TFA.get({ input_transaction_guid: extTransaction.guid });
    if (transaction_otp && transaction_otp.otp)
      otp = transaction_otp.otp;
    else {
      otp = cryptoRandomString({ length: 10, characters: "0123456789" });
      await db.TFA.upsert({ input_transaction_guid: extTransaction.guid, input_otp: otp, input_author_guid: author_guid });
    }

    const { error } = await mail.send(otp, email, `Code for approving transaction ${extTransaction.guid}`);

    if (error)
      throw error;

    return res.status(200).json({ message: `Email sent to ${email}` });
  }
  catch (err) {
    next(err);
  }
};


