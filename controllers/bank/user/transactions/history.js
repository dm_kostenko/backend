const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  try {
    const { status, entity_guid, page, items, entity } = req.query;
    let { type } = req.query;
    const { lastDay, lastWeek, lastMonth, lastYear, fromDate, toDate } = req.query;
    let from, to;
    const author_guid = req.auth.loginGuid;
    let transactions;
    if(lastDay) {
      from = new Date();
      from.setDate(from.getDate() - 1)
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(lastWeek) {
      from = new Date();
      from.setDate(from.getDate() - 7);
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(lastMonth) {
      from = new Date();
      from.setMonth(from.getMonth() - 1);
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(lastYear) {
      from = new Date();
      const year = from.getFullYear();
      const month = from.getMonth();
      const day = from.getDate();
      from = new Date(year - 1, month, day)
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(fromDate && toDate) {
      from = fromDate;
      to = toDate;
    }
    if(type === "all")
      type = type.toUpperCase();
    if (req.auth.type === "entity" || !entity)
      transactions = await db.transaction_history.getByLogin({
        input_login_guid: author_guid,
        input_status: status,
        input_type: "ALL",
        input_page_number: page,
        input_items_count: items,
        input_from: from,
        input_to: to
      }); 
    else transactions = await db.transaction_history.getByLogin({
      input_login_guid: author_guid,
      input_type: type,
      input_status: status, 
      input_page_number: page, 
      input_items_count: items,
      input_from: from,
      input_to: to
    });

    transactions.forEach(item => {
      item.value /= 100;
      item.commission /= 100;
      if(item.from_amount && item.to_amount) {
        item.from_amount /= 100;
        item.to_amount /= 100;
      }
    });
    
    const numberOfTransactions = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfTransactions, data: transactions });
  }
  catch (err) {
    next(err);
  }
};


module.exports.getByGuid = async (req, res, next) => {
  try {
    const author_guid = req.auth.loginGuid;
    const { guid } = req.params;
    
    let [ transaction ] = await db.transaction_history.getByLogin({ input_guid: guid, input_login_guid: author_guid });

    transaction.value /= 100;

    res.status(200).json(transaction);
  }
  catch (err) {
    next(err);
  }
};

module.exports.otp = require("./history/otp");