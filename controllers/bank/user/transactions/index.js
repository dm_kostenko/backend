module.exports = {
  ffcctrns: require("./ffcctrns"),
  history: require("./history"),
  internal: require("./internal"),
  internalTemplates: require("./internalTemplates"),
  externalTemplates: require("./externalTemplates")
};