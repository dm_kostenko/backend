const db = require("../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.object().keys({
    name: Joi.string().required(), 
    from_account: Joi.string().length(11).required(), 
    to_account: Joi.string().length(20).required(),
    to_bic: Joi.string().required(),
    currency_code: Joi.string().required().length(3),
    amount: Joi.number().positive(),
    description: Joi.string().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const login_guid = req.auth ? req.auth.loginGuid : "emptyname";
    const { name, from_account, to_account, to_bic, currency_code, once, page_number, items_count } = req.query;
    let externalTemplates = await db.external_template.get({
      input_login_guid: login_guid,
      input_name: name,
      input_from_account: from_account,
      input_to_account: to_account,
      input_to_bic: to_bic,
      input_currency_code: currency_code,
      input_once: once ? once : 'false',
      input_page_number: page_number,
      input_items_count: items_count
    })

    externalTemplates.forEach(item => {
      item.amount /= 100;
    })
        
    if(externalTemplates.length < 1)
      res.status(200).json({ count: 0, data: [] });

    res.status(200).json({ count: externalTemplates[0].count || 0, data: externalTemplates });
  }
  catch(err) {
    next(err);
  }
}

module.exports.post = async (req, res, next) => {
  const { name, from_account, to_account, to_bic, currency_code, amount, description, once } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    const [ upsertedExternalTemplate ] = await db.external_template.upsert({
      input_login_guid: author_guid,
      input_name: name,
      input_from_account: from_account,
      input_to_account: to_account,
      input_to_bic: to_bic,
      input_currency_code: currency_code,
      input_amount: amount * 100,
      input_description: description,
      input_once: once ? once : 'false',
      input_author_guid: author_guid
    })

    res.status(200).json(upsertedExternalTemplate);
  }
  catch(err) {
    next(err);
  }
}