const db = require("../../../../helpers/db/bankApi");
const { isValidIBANNumber } = require("../../../../helpers/validate");
const Joi = require("@hapi/joi");
const { init } = require("../../../../modules/kafka/services");

module.exports.schema = (body) => {
  const { debtorAccount, creditorAccount } = body;
  if (debtorAccount && !isValidIBANNumber(debtorAccount))
    throw { message: "Nonvalid debtor IBAN account" };
  
  if (creditorAccount && !isValidIBANNumber(creditorAccount))
    throw { message: "Nonvalid creditor IBAN account" };

  return Joi.object().keys({
    // paymentEndToEndId: Joi.string(),
    // paymentInstructionId: Joi.string(),
    serviceLevel: Joi.string(),
    amount: Joi.number().positive().required(),
    chargeBearer: Joi.string(),
    debtorName: Joi.string().required(),
    debtorAccount: Joi.string().required(),
    debtorBic: Joi.string().required(),
    creditorName: Joi.string().required(),
    creditorAccount: Joi.string().required(),
    creditorBic: Joi.string().required(),
    remittanceInformation: Joi.string().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

    let transactions = await db.ffcctrns_transaction.get({ input_login_guid: author_guid });

    transactions.forEach(item => {
      item.grphdrttlintrbksttlmamt /= 100;
      item.cdttrftxinfintrbksttlmamt /= 100;
    })

    const numberOfLogins = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfLogins, data: transactions });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    let [ transaction ] = await db.ffcctrns_transaction.get({ input_cdttrftxinfpmtidtxid: guid });
    if(transaction) {

      transaction.grphdrttlintrbksttlmamt /= 100;
      transaction.cdttrftxinfintrbksttlmamt /= 100;

      const [{
        from_currency_code, 
        to_currency_code,
        rate,
        date,
        from_amount,
        to_amount 
      }] = await db.transaction_history.getByAdmin({ 
        input_guid: guid,
        input_type: "FFCCTRNS",
        input_direction: "all" 
      });

      transaction = {
        ...transaction,
        from_currency_code, 
        to_currency_code,
        rate,
        date,
        from_amount: from_amount / 100,
        to_amount: to_amount / 100
      };
    }
    
    res.status(200).json(transaction || {});
  } catch (err) {
    next(err);
  }
};

module.exports.post = (req, res, next) => init(req, res, next, "FFCCTRNS");

module.exports.status = require("./ffcctrns/status");