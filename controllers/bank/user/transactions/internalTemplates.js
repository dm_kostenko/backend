const db = require("../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.object().keys({
    name: Joi.string().required(), 
    from_account: Joi.string().length(11).required(), 
    to_account: Joi.string().length(11).required(),
    from_currency: Joi.string().length(3).required(),
    to_currency: Joi.string().length(3).required(),
    amount: Joi.number().positive()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const login_guid = req.auth ? req.auth.loginGuid : "emptyname";
    const { name, from_account, to_account, once, page_number, items_count } = req.query;
    let internalTemplates = await db.internal_template.get({
      input_login_guid: login_guid,
      input_name: name,
      input_from_account: from_account,
      input_to_account: to_account,
      input_once: once ? once : 'false',
      input_page_number: page_number,
      input_items_count: items_count
    })

    internalTemplates.forEach(item => {
      item.amount /= 100;
    })
        
    if(internalTemplates.length < 1)
      res.status(200).json({ count: 0, data: [] });

    res.status(200).json({ count: internalTemplates[0].count || 0, data: internalTemplates });
  }
  catch(err) {
    next(err);
  }
}

module.exports.post = async (req, res, next) => {
  const { name, from_account, to_account, amount, once } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    const [ upsertedInternalTemplate ] = await db.internal_template.upsert({
      input_login_guid: author_guid,
      input_name: name,
      input_from_account: from_account,
      input_to_account: to_account,
      input_from_currency_code: from_currency,
      input_to_currency_code: to_currency,
      input_amount: amount * 100,
      input_once: once ? once : 'false',
      input_author_guid: author_guid
    })

    res.status(200).json(upsertedInternalTemplate);
  }
  catch(err) {
    next(err);
  }
}