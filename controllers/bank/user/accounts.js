const db = require("../../../helpers/db/bankApi");

module.exports.getByGuid = async (req, res, next) => {
  const login_guid = req.auth.loginGuid;
  const { guid } = req.params;
  try {
    const accountCurrencies = await db.account.getByLogin({
      input_login_guid: login_guid, 
      input_number: guid
    });
  
    if (accountCurrencies.length < 1)
      return res.status(200).json({});

    const item = accountCurrencies[0];
    const data = {
      number: item.number,
      login_guid: item.login_guid,
      iban: item.iban,
      type: item.type,
      balances: accountCurrencies.map(accountCurrency => ({
        currency: accountCurrency.currency_code,
        balance: accountCurrency.balance / 100,
        reserved: accountCurrency.reserved / 100,
        monthly_limit: accountCurrency.monthly_limit / 100,
        daily_limit: accountCurrency.daily_limit / 100
      }))
    };

    res.status(200).json(data);
  }
  catch (err) {
    next(err);
  }
};


module.exports.get = async (req, res, next) => {
  const login_guid = req.auth ? req.auth.loginGuid : "emptyname";
  const {
    number,
    iban,
    type,
    currency, 
    page, 
    items,
    entities
  } = req.query;
  try {
    const accounts = entities ? 
      await db.account.getByLogin({ 
        input_login_guid: login_guid,
        input_number: number,
        input_iban: iban,
        input_type: type,
        input_currency_code: currency,
        input_page_number: page,
        input_items_count: items,
      })
      :  
      await db.account.getByLogin({ 
        input_login_guid: login_guid,
        input_number: number,
        input_iban: iban,
        input_type: type,
        input_currency_code: currency,
        input_page_number: page,
        input_items_count: items,
      }); 
      

    if (accounts.length < 1)
      return res.status(200).json({ count: 0, data: [] });
    const numbers = [ ...new Set(accounts.map(account => account.number)) ];
    
    const count = accounts[0].count || 0;
    
    const data = numbers.map(number => {
      const accountCurrencies = accounts.filter(account => account.number === number);
      const item = accounts.find(account => account.number === number);

      return {
        number: item.number,
        login_guid: item.login_guid,
        entity_guid: item.entity_guid,
        entity_name: item.entity_name,
        iban: item.iban,
        type: item.type,
        balances: accountCurrencies.map(accountCurrency => ({
          currency: accountCurrency.currency_code,
          balance: accountCurrency.balance / 100,
          reserved: accountCurrency.reserved / 100,
          monthly_limit: accountCurrency.monthly_limit / 100,
          daily_limit: accountCurrency.daily_limit / 100
        }))
      };
    });

    res.status(200).json({ count, data });
  }
  catch (err) {
    next(err);
  }
};