const db = require("../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const { code, name } = req.query;
  try {
    const currencies = await db.currency.get({
      input_code: code,
      input_name: name
    });
    const count = currencies ? currencies.length : 0;
    res.status(200).json({ count, data: currencies });
  }
  catch(err) {
    next(err);
  }
}

module.exports.rates = require("./currencies/rates");