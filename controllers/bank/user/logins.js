const { InvalidDataError } = require("../../../errorTypes");
const bcrypt = require("bcrypt");
const db = require("../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  
  return Joi.object().keys({
    email: Joi.string().email(),
    password: Joi.string().min(6).max(20),
    old_password: Joi.string().when("password", {
      is: Joi.string().min(6).max(20).required(),
      then: Joi.string().min(6).max(20).required(),
      otherwise: Joi.optional()
    }),
    phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const { logins: loginsGuids = [] } = req.auth;
    const logins = await Promise.all(loginsGuids.map(async loginGuid => {
      const [ login ] = await db.login.get({ input_guid: loginGuid });
      if (login) {
        const { guid, username, type, email } = login;
        return { guid, username, type, email };
      }
    }));
    
    res.status(200).json({ data: logins });
  } catch (err) {
    next(err);
  } 
};

module.exports.post = async (req, res, next) => {
  const { 
    email,
    old_password,
    phone,
  } = req.body;
  let { password } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  let connection;
  try {
    if (password) {
      if (!old_password) 
        throw new InvalidDataError("Not received old password");
      
      const [ login ] = await db.login.get({ input_guid: author_guid });

      if (!login) 
        throw new InvalidDataError("No response from database. guid from login undefined");

      if (!bcrypt.compareSync(old_password, login.password))
        throw new InvalidDataError("InvalidPassword");
    }

    await db.login.upsert({
      input_guid: author_guid,
      input_email: email,
      input_password: password ? bcrypt.hashSync(password, bcrypt.genSaltSync(10)) : undefined,
      input_phone: phone,
      input_author_guid: author_guid
    }, connection);
   
    res.status(200).json({ message: "Login successfully updated" });
  } 
  catch (err) {
    next(err);
  }
};