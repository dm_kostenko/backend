const db = require("../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../errorTypes");

module.exports.get = async (req, res, next) => {
  try {
    const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
    const entities = await db.entity_founder.get({ input_founder_guid: author_guid });

    res.status(200).json({ count: entities[0] ? entities[0].count : 0, data: entities });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  try {
    const { entities = [] } = req.auth;
    const { entityGuid } = req.body;
    if (!entities.includes(entityGuid))
      throw { message: "User cannot be logined as entity" };

    //todo auth!
    res.status(200).json({ count: entities[0] ? entities[0].count : 0, data: entities });
  }
  catch (err) {
    next(err);
  }
};
