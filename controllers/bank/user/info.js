const db = require("../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {

    let [ login ] = await db.login.get({ input_guid: author_guid });

    if (!login) return res.status(200).json({});

    // login = {
    //   guid: login.guid,
    //   username: login.username,
    //   type: login.type,
    //   email: login.email,
    //   auth_type: login.auth_type,
    //   phone: login.phone,
    //   created_at: login.created_at,
    //   created_by: login.created_by,
    //   updated_at: login.updated_at,
    //   updated_by: login.updated_by
    // };

    res.status(200).json(login);
  } catch (err) {
    next(err);
  } 
};