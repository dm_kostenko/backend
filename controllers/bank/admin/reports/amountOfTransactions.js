const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const {
    report_type,
    login_guid,
    currency,
    status,
    type,
    from_date,
    to_date,
    days,
  } = req.query;

  try {
    const report = await db.report.amount_of_transactions({ 
      input_report_type: report_type,
      input_login_guid: login_guid,
      input_currency: currency,
      input_status: status,
      input_type: type,
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days,
    });

    res.status(200).json({ count: report[0] ? report[0].count : 0, data: report });
  }
  catch (err) {
    next(err);
  }
};