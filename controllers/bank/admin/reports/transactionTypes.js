const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  try {
    const report = await db.report.transaction_types({});

    res.status(200).json({ report });
  }
  catch (err) {
    next(err);
  }
};