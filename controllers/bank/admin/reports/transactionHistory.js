const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const {
    guid,
    login_guid,
    currency,
    status,
    type,
    from_date,
    to_date,
    days,
    page,
    items,
  } = req.query;

  try {
    let report = await db.report.transaction_history({ 
      input_guid: guid, 
      input_login_guid: login_guid, 
      input_currency: currency, 
      input_status: status, 
      input_type: type, 
      input_from_date: from_date, 
      input_to_date: to_date, 
      input_days: days, 
      input_page_number: page, 
      input_items_count: items
    });

    report.forEach(item => {
      item.value /= 100;
    });

    res.status(200).json({ count: report[0] ? report[0].count : 0, data: report });
  }
  catch (err) {
    next(err);
  }
};