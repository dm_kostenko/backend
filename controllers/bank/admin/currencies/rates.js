const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const { currency_code } = req.query;
  try {
    const currenciesRates = await db.currency_rate.get({
      input_currency_code: currency_code
    });
    const count = currenciesRates ? currenciesRates.length : 0;
    res.status(200).json({ count, data: currenciesRates });
  }
  catch(err) {
    next(err);
  }
}