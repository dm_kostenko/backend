const db = require("../../../helpers/db/bankApi");
const bcrypt = require("bcrypt");
const { InvalidDataError, InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      username: Joi.string().alphanum().min(3).max(20),
      type: Joi.equal("admin", "individual", "entity"),
      email: Joi.string().email(),
      password: Joi.string().min(6).max(20),
      old_password: Joi.when("password", {
        is: Joi.string().min(6).max(20).required(),
        then: Joi.string().min(6).max(20).required(),
        otherwise: Joi.optional()
      }),
      auth_type: Joi.string().equal("login-password","login-password-mail"),
      phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
      founders: Joi.any().when("type", {
        is: Joi.valid("entity"), 
        then: Joi.array().items(Joi.string().guid()).required(), 
        otherwise: Joi.any() 
      }),
      privileges: Joi.array().items(Joi.string().guid())
    });
    
  return Joi.object().keys({
    username: Joi.string().alphanum().min(3).max(20).required(),
    email: Joi.string().email().required(),
    type: Joi.equal("admin", "individual", "entity").required(),
    password: Joi.string().min(6).max(20).required(),
    auth_type: Joi.string().equal("login-password","login-password-mail"),
    phone: Joi.string().trim().regex(/^[0-9]{7,11}$/).required(),
    founders: Joi.any().when("type", {
      is: Joi.valid("entity"), 
      then: Joi.array().items(Joi.string().guid()).required(), 
      otherwise: Joi.any() 
    }),
    privileges: Joi.array().items(Joi.string().guid()).required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let dataOnLogins = await db.login.get({
      input_guid: req.query.guid,
      input_username: req.query.username,
      input_type: req.query.type,
      input_rate_guid: req.query.rate_guid,
      input_email: req.query.email,
      input_auth_type: req.query.auth_type,
      input_enabled: req.query.enabled,
      input_phone: req.query.phone,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfLogins = dataOnLogins[0] ? dataOnLogins[0].count : 0;

    // const resData = await Promise.all(dataOnLogins.map(async recordLogin => {
    //   return {
    //     guid: recordLogin.guid,
    //     username: recordLogin.username,
    //     username_canonical: recordLogin.username_canonical,
    //     email: recordLogin.email,
    //     email_canonical: recordLogin.email_canonical,
    //     enabled: recordLogin.enabled,
    //     last_login: recordLogin.last_login,
    //     locked: recordLogin.locked,
    //     expired: recordLogin.expired,
    //     expires_at: recordLogin.expires_at,
    //     credentials_expired: recordLogin.credentials_expired,
    //     credentials_expire_at: recordLogin.credentials_expire_at,
    //     phone: recordLogin.phone,
    //     enabled_api: recordLogin.enabled_api,
    //     created_at: recordLogin.created_at,
    //     created_by: recordLogin.created_by,
    //     updated_at: recordLogin.updated_at,
    //     updated_by: recordLogin.updated_by
    //   };
    // }));

    res.status(200).json({ count: numberOfLogins, data: dataOnLogins });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    const { guid } = req.params;
    let [ login ]  = await db.login.get({ input_guid: guid });

    if (!login) 
      return res.status(200).json({});

    let privileges = await db.privilege.info({ input_login_guid: guid });

    privileges = privileges.map(privilegeRow => ({ name: privilegeRow.name, guid: privilegeRow.guid }));

    let loginTree = await db.login_tree.get({ input_guid: guid })

    login = {
      guid: login.guid,
      username: login.username,
      type: login.type,
      rate_guid: login.rate_guid,
      email: login.email,
      auth_type: login.auth_type,
      phone: login.phone,
      privileges,
      created_at: login.created_at,
      created_by: login.created_by,
      updated_at: login.updated_at,
      updated_by: login.updated_by
    };

    if(loginTree && loginTree.length > 0)
      login.loginTree = loginTree;

    res.status(200).json(login);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { 
    guid, 
    username,
    type,
    rate_guid,
    email,
    old_password,
    enabled = true,
    auth_type,
    phone,
    founders,
    privileges
  } = req.body;
  let { password } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  let connection;
  try {
    if (req.body.delete){
      const [ loginObj ] = await db.login.get({ input_guid: guid });
      if (!loginObj) 
        throw new InvalidDataError(`Doesn't exist "login" with guid = ${guid}`);

      await db.login.delete({ input_guid: guid });
      return res.status(200).json({ message: "Deleted" });
    }

    if (guid && password) {
      if (!old_password) 
        throw new InvalidDataError("Not received old password");
      
      const [ login ] = await db.login.get({ input_guid: guid });

      if (!login) 
        throw new InvalidDataError("No response from database. guid from login undefined");

      if (!bcrypt.compareSync(old_password, login.password))
        throw new InvalidDataError("InvalidPassword");
    }

    let loginData = {
      input_guid: guid,
      input_username: username,
      input_type: type,
      input_rate_guid: rate_guid,
      input_email: email,
      input_password: password ? bcrypt.hashSync(password, bcrypt.genSaltSync(10)) : undefined,
      input_enabled: enabled,
      input_auth_type: auth_type || "login-password",
      input_credentials_expired: false,
      input_phone: phone,
      input_author_guid: author_guid
    };

    connection = await db.getConnection();
    await db._transaction.begin(connection);
    console.log(loginData)
    const [ upsertedLogin ] = await db.login.upsert(loginData, connection);
   
    if (founders)
      await Promise.all(founders.map(async founder => await db.entity_founder.upsert({ 
        input_entity_guid: upsertedLogin.guid, 
        input_author_guid: author_guid, 
        input_founder_guid: founder,
      }, connection)));

    if (Array.isArray(privileges) && privileges.length > 0)
      await Promise.all(privileges.map(async privelege => await db.login_privilege.upsert({
        input_login_guid: upsertedLogin.guid,
        input_privilege_guid: privelege,
        input_author_guid: author_guid,
      }, connection)));

    await db._transaction.commit(connection);
    res.status(200).json({
      guid: upsertedLogin.guid,
      username: upsertedLogin.username,
      type: upsertedLogin.type,
      rate_guid: upsertedLogin.rate_guid,
      email: upsertedLogin.email,
      enabled: upsertedLogin.enabled,
      auth_type: upsertedLogin.auth_type,
      founders,
      phone: upsertedLogin.phone,
      created_at: upsertedLogin.created_at,
      created_by: upsertedLogin.created_by,
      updated_at: upsertedLogin.updated_at,
      updated_by: upsertedLogin.updated_by
    });
  } 
  catch (err) {
    if (connection)
      await db._transaction.rollback(connection);
    next(err);
  } finally {
    if (connection)
      connection.release();
  }
};