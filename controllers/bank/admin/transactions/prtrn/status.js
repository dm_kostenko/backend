const Joi = require("@hapi/joi");
const { getCETDate } = require("../../../../../helpers/target2Timing")
const db = require("../../../../../helpers/db/bankApi");
const { EXTERNAL_TRANSACTION_APPROVED, EXTERNAL_TRANSACTION_UNAPPROVED, EXTERNAL_TRANSACTION_DECLINED } = require("../../../../../helpers/constants/statuses").externalTransaction;
const { PRTRN } = require("../../../../../modules/messages");

module.exports.schema = () => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });


  return Joi.object().keys({
    // type: 
    status: Joi.equal([ EXTERNAL_TRANSACTION_APPROVED, EXTERNAL_TRANSACTION_DECLINED ]).required(), 
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required(),
  });
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { 
    system = "LITAS-MIG", /*LITAS-RLS or LITAS-MIG */
    status  
  } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  try {
    const [ extTransaction ] = await db.external_transaction_history.get({ input_guid: guid, input_type: "PRTRN" });

    if (!extTransaction || extTransaction.status !== EXTERNAL_TRANSACTION_UNAPPROVED)
      throw { message: `Transaction ${guid} status is ${extTransaction ? extTransaction.status : "undefined"}` };
    
    if (status === EXTERNAL_TRANSACTION_DECLINED) {
      const [ upsertedMessage ] = await db.external_transaction_history.upsert({
        input_guid: guid,
        input_status: status,
        input_author_guid: author_guid,
      });

      return res.status(200).json(upsertedMessage);
    }

    const [ transaction ] = await db.prtrn_transaction.get({ input_guid: guid });

    const message = new PRTRN({ 
      sender: transaction.txinforgnltxrefcdtragtfininstnidbic,
      receiver: transaction.txinforgnltxrefdbtragtfininstnidbic,
      msgId: transaction.txinfrtrid, 
      system 
    });

    message.addTransaction({
      returnmentId: transaction.txinfrtrid,
      originalMsgId: transaction.txinforgnlgrpinforgnlmsgid,
      originalMsgNmId: "pacs.008.001.02", //txinforgnlgrpinforgnlmsgnmid
      originalInstrId: transaction.txinforgnlinstrid,
      originalEndToEndId: transaction.txinforgnlendtoendid,
      originalTxId: transaction.txinforgnltxid,
      originalIntrBkSttlmAmt: transaction.txinforgnlintrbksttlmamt,
      rtrdIntrBkSttlmAmt: transaction.txinfrtrdintrbksttlmamt,
      rtrdInstdAmt: transaction.txinfrtrdinstdamt,
      chargeBearer: "SLEV", //txinfchrgbr
      сhrgsInfAmt: transaction.txinfchrgsinfamt,
      originatorClient: transaction.txinfrtrrsninforgtrnm,
      originatorBank: transaction.txinfrtrrsninforgtridorgidbicorbei,
      reasonForReturn: transaction.txinfrtrrsninfrsncd,  // allowed values: AC01, AC04, AC06, AG01, AG02, AM05, BE04, FOCR, MD07, MS02, MS03, RC01, RR01, RR02, RR03, RR04.
      rtrRsnInfAddtInf: transaction.txinfrtrrsninfaddtinf, //if reason: FOCR
      originalSettlementDate: getCETDate(transaction.txinforgnltxrefintrbksttlmdt).toISOString().slice(0,10),
      originalSettlementMethod: "CLRG", //txinforgnltxrefsttlminfsttlmmtd
      originalServiceLevel: "SEPA", //txinforgnltxrefpmttpinfsvclvlcd
      originalDebtorName: transaction.txinforgnltxrefdbtrnm,
      originalDebtorAccount: transaction.txinforgnltxrefdbtracctidiban,
      originalDebtorBic: transaction.txinforgnltxrefdbtragtfininstnidbic,
      originalCreditorName: transaction.txinforgnltxrefcdtrnm,
      originalCreditorAccount: transaction.txinforgnltxrefcdtracctidiban,
      originalCreditorBic: transaction.txinforgnltxrefcdtragtfininstnidbic,
    });


    message._calculateGroupHeader({});
    const { messageHeader, groupHeader } = message;
    await db.prtrn_transaction.upsert({
      input_guid: transaction.guid,
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: system,
      input_docid: messageHeader.msgId,
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      input_status: EXTERNAL_TRANSACTION_APPROVED,
      // input_reportmsgid,
      // input_reportdttm,
      // input_grsts,
      // input_rsnprty,
      input_grphdrmsgid: groupHeader.MsgId,
      input_grphdrcredttm: groupHeader.CreDtTm,
      input_grphdrnboftxs: groupHeader.NbOfTxs,
      input_grphdrttlintrbksttlmamt: groupHeader.TtlRtrdIntrBkSttlmAmt,
      input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
      input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
      input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
      input_data: message.toXml(),
      input_author_guid: author_guid,
    });

    const [ updatedStatus ] = await db.external_transaction_history.upsert({
      input_guid: guid,
      input_status: status,
      input_author_guid: author_guid,
    });

    res.status(200).json(updatedStatus);
  }
  catch (err) {
    next(err);
  }
};
