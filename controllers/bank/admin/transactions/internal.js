const db = require("../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");
const { init } = require("../../../../modules/kafka/services");

module.exports.schema = () => {
  return Joi.object().keys({
    sender_number: Joi.string().length(11).required(),
    receiver_number: Joi.string().length(11).required(),
    receiver_currency: Joi.string().required(),
    sender_currency: Joi.string().required(),
    value: Joi.number().positive().required(),
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const {
      guid,
      sender_iban,
      receiver_iban,
      currency,
      status,
      page,
      items, 
    } = req.query;
      
    let transactions = await db.internal_transaction_history.get({
      input_guid: guid,
      input_sender_iban: sender_iban,
      input_receiver_iban: receiver_iban,
      input_currency: currency,
      input_status: status,
      input_page_number: page,
      input_items_count: items
    });

    transactions.forEach(item => {
      item.value /= 100;
    });

    const numberOfTransactions = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfTransactions, data: transactions });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    const { guid } = req.params;
    let [ transaction ] = await db.internal_transaction_history.get({ input_guid: guid });

    transaction.value /= 100;
    transaction.commission /= 100;

    const [{
      from_currency_code, 
      to_currency_code,
      rate,
      date,
      from_amount,
      to_amount 
    }] = await db.transaction_history.getByAdmin({ 
      input_guid: guid,
      input_direction: "internal" 
    });

    transaction = {
      ...transaction,
      from_currency_code, 
      to_currency_code,
      rate,
      date,
      from_amount: from_amount / 100,
      to_amount: to_amount / 100
    };
    
    res.status(200).json(transaction);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = (req, res, next) => init(req, res, next, "Internal");