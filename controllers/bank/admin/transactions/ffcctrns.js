const db = require("../../../../helpers/db/bankApi");
const { isValidIBANNumber } = require("../../../../helpers/validate");
const Joi = require("@hapi/joi");
const { init } = require("../../../../modules/kafka/services");
const { EXTERNAL_TRANSACTION_WAITING } = require("../../../../helpers/constants/statuses").externalTransaction;


module.exports.schema = (body) => {
  const { debtorAccount, creditorAccount } = body;
  if (debtorAccount && !isValidIBANNumber(debtorAccount))
    throw { message: "Nonvalid debtor IBAN account" };
  
  if (creditorAccount && !isValidIBANNumber(creditorAccount))
    throw { message: "Nonvalid creditor IBAN account" };


  return Joi.object().keys({
    serviceLevel: Joi.string(),
    amount: Joi.number().positive().required(),
    amountWithRates: Joi.number().positive(),
    chargeBearer: Joi.string(),
    debtorName: Joi.string().required(),
    debtorAccount: Joi.string().required(),
    debtorBic: Joi.string().required().regex(/[A-Z0-9]{4,4}[A-Z]{2,2}[A-Z0-9]{2,2}([A-Z0-9]{3,3}){0,1}$/),
    // creditorName: Joi.string().required(),
    creditorAccount: Joi.string().required(),
    // creditorBic: Joi.string().required().regex(/[A-Z0-9]{4,4}[A-Z]{2,2}[A-Z0-9]{2,2}([A-Z0-9]{3,3}){0,1}$/),
    remittanceInformation: Joi.string().required()
  });
};


module.exports.otpSchema = () => {
  return Joi.object().keys({
    isNext: Joi.required()
  })
}

module.exports.get = async (req, res, next) => {
  try {
    const { msgId, status, page, items } = req.query;
    let transactions = await db.ffcctrns_transaction.get({ input_grphdrmsgid: msgId });
    let transactionsHistory = [];
    let count;
    if(status === EXTERNAL_TRANSACTION_WAITING) {
      transactionsHistory = await db.external_transaction_history.get({
        // input_type: "ffcctrns",
        // input_direction: "outgoing",
        input_status: EXTERNAL_TRANSACTION_WAITING,
        input_page_number: page,
        input_items_count: items
      })
      transactions = transactions.filter(tr => transactionsHistory.map(trh => trh.guid).includes(tr.guid))
      count = transactionsHistory[0] ? transactionsHistory[0].count : 0;
    }
    else {
      count = transactions[0] ? transactions[0].count : 0;
    }
    transactions.forEach(item => {
      item.grphdrttlintrbksttlmamt /= 100;
      item.cdttrftxinfintrbksttlmamt /= 100;
    })
    
    res.status(200).json({ count, data: transactions });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    let [ transactionHistory ] = await db.external_transaction_history.get({ input_guid: guid });
    if (!transactionHistory)
      return res.status(200).json({});

    transactionHistory.value /= 100;
    transactionHistory.commission /= 100;

    const [{
      from_currency_code, 
      to_currency_code,
      rate,
      date,
      from_amount,
      to_amount
    }] = await db.transaction_history.getByAdmin({ 
      input_guid: guid,
      input_type: "FFCCTRNS",
      input_direction: "all" 
    });
    let [ document ]  = await db.ffcctrns_transaction.get({ input_guid: transactionHistory.guid });

    document.grphdrttlintrbksttlmamt /= 100;
    document.cdttrftxinfintrbksttlmamt /= 100;

    transactionHistory = {
      ...transactionHistory,
      document,
      from_currency_code, 
      to_currency_code,
      rate,
      date,
      from_amount: from_amount / 100,
      to_amount: to_amount / 100
    };
    res.status(200).json(transactionHistory);
  } catch (err) {
    next(err);
  }
};


module.exports.post = (req, res, next) => init(req, res, next, "FFCCTRNS");

module.exports.status = require("./ffcctrns/status");
module.exports.fee = require("./ffcctrns/fee");
module.exports.array = require("./ffcctrns/array");