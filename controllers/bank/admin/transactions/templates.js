const db = require("../../../../helpers/db/bankApi");
const { checkIBANs } = require("../../../../helpers/parseIBAN");

module.exports.get = async (req, res, next) => {
  try {
    const login_guid = req.auth ? req.auth.loginGuid : "emptyname";
    const { account, once, page, items } = req.query;
    let templates = await db.template.get({
      input_from_account: account,
      input_login_guid: login_guid,
      input_once: once ? once : 'false',
      input_page_number: page,
      input_items_count: items
    })


    templates.forEach(item => {
      item.amount /= 100;
    })
        
    if(templates.length < 1)
      return res.status(200).json({ count: 0, data: [] });

    res.status(200).json({ count: templates[0].count || 0, data: templates });
  }
  catch(err) {
    next(err);
  }
}

const getBicAndName = async iban => {
  const code = iban.slice(4, 9)
  const [ record ] = await db.institute.get({
    input_code: code
  })
  if(record && record.bic && record.name)
    return {
      creditorBic: record.bic, 
      creditorName: record.name
    }
  else 
    return {
      empty: true
    }
}

module.exports.post = async (req, res, next) => {
  let connection;
  try {
    const login_guid = req.auth ? req.auth.loginGuid : "emptyname";
    let { 
      guids,
      del,
      name, 
      from_account, 
      to_account, 
      from_currency,
      to_currency,
      to_name,
      to_bic, 
      currency_code, 
      amount, 
      description,
      once 
    } = req.body;

    connection = await db.getConnection();

    if(del && guids && guids.length > 0) {
      for(let i = 0; i < guids.length; i++) {
        await db.template.delete({
          input_guid: guids[i]
        }, connection)
      }
      await db._transaction.commit(connection);
      res.status(200).json({ status: "ok" })
    }
    else {
      let { error, internal } = checkIBANs(to_account, from_account)
      if(error)
        throw { error }

      let upsertedTemplate
      if(internal)
        [ upsertedTemplate ] = await db.internal_template.upsert({
          input_login_guid: login_guid,
          input_name: name,
          input_from_account: from_account.slice(9),
          input_to_account: to_account.slice(9),
          input_from_currency_code: from_currency || "EUR",
          input_to_currency_code: to_currency || "EUR",
          input_amount: amount * 100,
          input_once: once ? once : 'false',
          input_author_guid: login_guid
        })
      else {
        if(!to_bic || !to_name) {
          let { creditorName, creditorBic, empty } = await getBicAndName(to_account)
          if(empty)
            return res.status(200).json({ empty })
          to_bic = creditorBic
          to_name = creditorName
        }
        [ upsertedTemplate ] = await db.external_template.upsert({
          input_login_guid: login_guid,
          input_name: name,
          input_from_account: from_account,
          input_to_account: to_account,
          input_to_bic: to_bic,
          input_to_name: to_name,
          input_currency_code: currency_code,
          input_amount: amount * 100,
          input_description: description,
          input_once: once ? once : 'false',
          input_author_guid: login_guid
        })
      }
      res.status(200).json(upsertedTemplate);
    }
  }
  catch(err) {
    if(connection)
      await db._transaction.rollback(connection); 
    next(err);
  }
  finally {
    if(connection)
      connection.release()
  }
}