const { checkIBANs } = require("../../../../../helpers/parseIBAN");
const { checkRates } = require("../../../../../modules/kafka/helpers")
const Joi = require("@hapi/joi");


module.exports.schema = () => {
  return Joi.object().keys({
    transactions: Joi.array().items(
      Joi.object().keys({
        senderCurrency: Joi.string().required(),
        receiverCurrency: Joi.string().required(),
        amount: Joi.number().required(),
        senderAccount: Joi.string().required(),
        receiverAccount: Joi.string().required(),
        senderName: Joi.string().required(),
        senderBIC: Joi.string().required(),
        receiverName: Joi.string().required(),
        receiverBIC: Joi.string().required()
      })
    )
  });
};

module.exports.post = async(req, res, next) => {
  try {
    const loginGuid = req.auth && req.auth.loginGuid ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

    let fees = []
    let fee = 0
    for(let i = 0; i < req.body.transactions.length; i++) {
      const {
        senderCurrency,
        receiverCurrency,
        amount,
        senderAccount,
        receiverAccount,
        senderName,
        senderBIC,
        receiverName,
        receiverBIC
      } = req.body.transactions[i];

      let { error, test, internal } = checkIBANs(receiverAccount, senderAccount)
      if(error)
        throw { error }

      if(!test) {
        if(internal) {
          const currentFee = await checkRates({
            senderCurrency,
            receiverCurrency,
            senderNumber: senderAccount,
            receiverNumber: receiverAccount,
            amount: Math.round(amount * 100),
            loginGuid
          }, "Internal")
          fee += currentFee
          fees.push(currentFee)
        }
        else {
          const currentFee = await checkRates({
            currency: senderCurrency,
            amount: Math.round(amount * 100),
            senderAccount,
            receiverAccount,
            senderName,
            senderBIC,
            receiverName,
            receiverBIC,
            loginGuid
          }, "outgoing_FFCCTRNS")
          fee += currentFee
          fees.push(currentFee)
        }
      }
      else
        fees.push(0)
    }
    res.status(200).json({ fees, totalFee: fee })

  }
  catch(err) {
    next(err)
  }
}