const db = require("../../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");
const { init } = require("../../../../../modules/kafka/services");


module.exports.schema = (body) => {


  return Joi.object().keys({
    transactions: Joi.array().items(
      Joi.object().keys({
        serviceLevel: Joi.string(),
        amount: Joi.number().positive().required(),
        amountWithRates: Joi.number().positive(),
        chargeBearer: Joi.string(),
        debtorName: Joi.string().required(),
        debtorAccount: Joi.string().required(),
        debtorBic: Joi.string().required().regex(/[A-Z0-9]{4,4}[A-Z]{2,2}[A-Z0-9]{2,2}([A-Z0-9]{3,3}){0,1}$/),
        // creditorName: Joi.string().required(),
        creditorAccount: Joi.string().required(),
        // creditorBic: Joi.string().required().regex(/[A-Z0-9]{4,4}[A-Z]{2,2}[A-Z0-9]{2,2}([A-Z0-9]{3,3}){0,1}$/),
        remittanceInformation: Joi.string().required()
      })
    )
  });
};

module.exports.post = async (req, res, next) => {
  const { transactions } = req.body
  let connection;
  try {
    connection = await db.getConnection();
    for(let i = 0; i < transactions.length; i++) {
      const { 
        status, 
        error,
        details 
      } = await init({
        ...req,
        body: {
          ...transactions[i],
          isNext: true
        }}, 
        null, 
        (err) => { throw(err) }, 
        "FFCCTRNS",
        connection, 
        true)
      if(error)
        throw { error, details }
      if(status !== "ok")
        throw { error: "Status isn't 'ok' " }
    }

    await db._transaction.commit(connection);
    res.status(200).json({ status: "ok" })
  }
  catch(err) {
    if(connection)
      await db._transaction.rollback(connection); 
    next(err)
  }
  finally {
    if(connection)
      connection.release()
  }
}