const Joi = require("@hapi/joi");
const db = require("../../../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../../../errorTypes");
const { EXTERNAL_TRANSACTION_UNAPPROVED, EXTERNAL_TRANSACTION_DECLINED, EXTERNAL_TRANSACTION_APPROVED } = require("../../../../../helpers/constants/statuses").externalTransaction;
const { creditTransfer } = require("../../../../../helpers/transactions");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });


  return Joi.object().keys({
    // type: 
    status: Joi.equal([ EXTERNAL_TRANSACTION_APPROVED, EXTERNAL_TRANSACTION_DECLINED ]).required(), 
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required(),
  });
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { 
    system = "LITAS-MIG", /*LITAS-RLS or LITAS-MIG */
    status  
  } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  let connection;
  try {
    const [ extTransaction ] = await db.external_transaction_history.get({ input_guid: guid });

    if (!extTransaction || extTransaction.status !== EXTERNAL_TRANSACTION_UNAPPROVED)
      throw { message: `transaction ${guid} status is ${extTransaction ? extTransaction.status : "undefined"}` };
    
    if (status === EXTERNAL_TRANSACTION_DECLINED) {
      const [ upsertedMessage ] = await db.external_transaction_history.upsert({
        input_guid: guid,
        input_status: status,
        input_author_guid: author_guid,
      });

      return res.status(200).json(upsertedMessage);
    }

    connection = await db.getConnection();
    await db._transaction.begin(connection);
    const updatedStatus = await creditTransfer.prepareXml({ guid, status, system, author_guid }, connection);
    await db._transaction.commit(connection);

    res.status(200).json(updatedStatus);
  } catch (err) {
    if (connection)
      await db._transaction.rollback(connection);

    next(err);
  } finally {
    if (connection)
      connection.release();
  }
};


