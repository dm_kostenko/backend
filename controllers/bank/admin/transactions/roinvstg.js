const db = require("../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");
const { init } = require("../../../../modules/kafka/services");

module.exports.schema = () => {
  return Joi.object().keys({
    OrgnlTxId: Joi.string().required(),   // FFCCTRNS
    CxlStsRsnInf: Joi.string().required(), // LEGL, CUST, ARDT, AC04, AM04, NOAS, NOOR
    CxlStsRsnInfAddtInf: Joi.any().when("CxlStsRsnInf", { is: Joi.valid("LEGL"), then: Joi.string().required(), otherwise: Joi.any() }),
    originatorClient: Joi.string(),
    originatorBank: Joi.string()
  })
}

module.exports.get = async (req, res, next) => {
  try {
    const { msgId } = req.query;
    let transactions = await db.roinvstg_transaction.get({ input_msgid: msgId });

    transactions.forEach(item => {
      item.cxldtlstxinfandstsorgnltxrefintrbksttlmamt /= 100;
    })

    const numberOfLogins = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfLogins, data: transactions });
  }
  catch (err) {
    next(err);
  }
};


module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    let [ transactionHistory ] = await db.external_transaction_history.get({ input_guid: guid });
    if (!transactionHistory)
      return res.status(200).json({});

    transactionHistory.value /= 100;

    let [ document ] = await db.roinvstg_transaction.get({ input_guid: transactionHistory.guid });

    document.cxldtlstxinfandstsorgnltxrefintrbksttlmamt /= 100;

    transactionHistory.document = document;

    res.status(200).json(transactionHistory || {});
  } catch (err) {
    next(err);
  }
};

module.exports.post = (req, res, next) => init(req, res, next, "ROINVSTG");

module.exports.status = require("./roinvstg/status");