const db = require("../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");
const { init } = require("../../../../modules/kafka/services");

module.exports.schema = (body) => {
  if (body.paymentTransactionId)
    return Joi.object().keys({
      paymentTransactionId: Joi.string().required(),
      status: Joi.string().required()
    });
  return Joi.object().keys({
    // returnmentId,
    // originalMsgId,
    // originalMsgNmId = "pacs.004.001.02",
    // originalInstrId,
    // originalEndToEndId,
    originalTxId: Joi.string().required(),
    // originalIntrBkSttlmAmt,
    // rtrdIntrBkSttlmAmt,
    // chargeBearer = "SLEV",
    originatorClient: Joi.string(),
    originatorBank: Joi.string(),
    reasonForReturn: Joi.string().required(),  // allowed values: AC01, AC04, AC06, AG01, AG02, AM05, BE04, FOCR, MD07, MS02, MS03, RC01, RR01, RR02, RR03, RR04.
    сhrgsInfAmt: Joi.string(), //Charges amount Fee for the operation. Can only be completed if TxInf/RtrRsnInf/Rsn/Cd=FOCR. 
    rtrRsnInfAddtInf: Joi.string(), //if reason: FOCR
    // originalSettlementDate,
    // originalSettlementMethod = "CLRG",
    // originalServiceLevel = "SEPA",
    // originalDebtorName,
    // originalDebtorAccount,
    // originalDebtorBic,
    // originalCreditorName,
    // originalCreditorAccount,
    // originalCreditorBic,
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const { msgId } = req.query;
    let transactions = await db.prtrn_transaction.get({ input_grphdrmsgid: msgId });

    transactions.forEach(item => {
      item.grphdrttlintrbksttlmamt /= 100;
      item.txinforgnlintrbksttlmamt /= 100;
      item.txinfrtrdintrbksttlmamt /= 100;
      item.txinfrtrdinstdamt /= 100;
      item.txinfchrgsinfamt /= 100;
    })
    const numberOfLogins = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfLogins, data: transactions });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    let [ transactionHistory ] = await db.external_transaction_history.get({ input_guid: guid });
    if (!transactionHistory)
      return res.status(200).json({});
    
    transactionHistory.value /= 100;

    let [ document ] = await db.prtrn_transaction.get({ input_guid: guid });

    document.grphdrttlintrbksttlmamt /= 100;
    document.txinforgnlintrbksttlmamt /= 100;
    document.txinfrtrdintrbksttlmamt /= 100;
    document.txinfrtrdinstdamt /= 100;
    document.txinfchrgsinfamt /= 100;

    transactionHistory.document = document;

    res.status(200).json(transactionHistory || {});
  } catch (err) {
    next(err);
  }
};

module.exports.post = (req, res, next) => init(req, res, next, "PRTRN");

module.exports.status = require("./prtrn/status");