const uuidv1 = require("uuid/v1");
const Joi = require("@hapi/joi");
const db = require("../../../../../helpers/db/bankApi");
const { 
  EXTERNAL_TRANSACTION_APPROVED, 
  EXTERNAL_TRANSACTION_DECLINED, 
  EXTERNAL_TRANSACTION_UNAPPROVED 
} = require("../../../../../helpers/constants/statuses").externalTransaction;
const { 
  XML_IMPORT_DECLINED,
  XML_IMPORT_ERRORED,
  XML_IMPORT_IMPORTED,
  XML_IMPORT_INVESTIGATED,
  XML_IMPORT_RECEIVED,
  XML_IMPORT_RETURNED
} = require("../../../../../helpers/constants/statuses").xmlImport;
const { FFPCRQST } = require("../../../../../modules/messages");
const { init } = require("../../../../../modules/kafka/services");
const { calcTime } = require("../../../../../helpers/target2Timing");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });


  return Joi.object().keys({
    // type: 
    status: Joi.equal([ EXTERNAL_TRANSACTION_APPROVED, EXTERNAL_TRANSACTION_DECLINED, XML_IMPORT_RETURNED, XML_IMPORT_INVESTIGATED ]).required(), 
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required(),
    isImported: Joi.bool().required(),
    cxlStsRsnInf: Joi.string(),
    cxlStsRsnAddtInf: Joi.string()
  });
};

module.exports.post = async (req, res, next) => {
  const { 
    system = "LITAS-MIG", /*LITAS-RLS or LITAS-MIG */
    status,
    isImported,
    cxlStsRsnInf,
    cxlStsRsnAddtInf
  } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  let connection;

  try {
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    if(isImported) {
      const { guid } = req.params;

      const [ ffpcrqstTransaction ] = await db.external_transaction_history.get({ input_guid: guid });

      if (!ffpcrqstTransaction || ffpcrqstTransaction.status !== XML_IMPORT_IMPORTED)
        throw { message: `Transaction ${guid} status is ${ffpcrqstTransaction ? ffpcrqstTransaction.status : "undefined"} instead of ${XML_IMPORT_IMPORTED}` };

      await db.external_transaction_history.upsert({
        input_guid: guid,
        input_status: status,
        input_author_guid: author_guid,
      }, connection);

      await db._transaction.commit(connection);

      const [ ffpcrqst ] = await db.ffpcrqst_transaction.get({ input_guid: guid })
      if(status === XML_IMPORT_RETURNED)
        init({
          body: {
            originalTxId: ffpcrqst.undrlygtxinforgnlgrpinforgnltxid,
            originatorClient: "TBF Finance, UAB",
            reasonForReturn: "FOCR",
            chrgsInfAmt: "0",
            rtrRsnInfAddtInf: "Return after cancellation request",
            isResponse: true,
            test
          },
          auth: req.auth
        }, res, next, "PRTRN");
      else if(status === XML_IMPORT_INVESTIGATED)
        init({
          body: {
            OrgnlTxId: ffpcrqst.undrlygtxinforgnlgrpinforgnltxid,
            CxlStsRsnInf: cxlStsRsnInf,
            originatorClient: "TBF Finance, UAB",
            CxlStsRsnInfAddtInf: cxlStsRsnInf === "LEGL" ? cxlStsRsnAddtInf : undefined
          },
          auth: req.auth
        }, res, next, "ROINVSTG");

    }
    else {
      const { guid } = req.params;

      const [ extTransaction ] = await db.external_transaction_history.get({ input_guid: guid, input_type: "FFPCRQST" });

      if (!extTransaction || extTransaction.status !== EXTERNAL_TRANSACTION_UNAPPROVED)
        throw { message: `Transaction ${guid}  status is ${extTransaction ? extTransaction.status : "undefined"}` };
      
      if (status === EXTERNAL_TRANSACTION_DECLINED){
        const [ upsertedMessage ] = await db.external_transaction_history.upsert({
          input_guid: guid,
          input_status: status,
          input_author_guid: author_guid,
        }, connection);

        await db._transaction.commit(connection);

        return res.status(200).json(upsertedMessage);
      }

      const [ transaction ] = await db.ffpcrqst_transaction.get({ input_guid: guid });

      const message = new FFPCRQST({ 
        sender: transaction.undrlygtxinforgnltxrefdbtragtfininstnidbic,
        receiver: transaction.undrlygtxinforgnltxrefcdtragtfininstnidbic,
        msgId: transaction.undrlygtxinfcxlid,
        system 
      });

      message.addTransaction({
        CancellationTrId: transaction.undrlygtxinfcxlid,
        OrgnlMsgId: transaction.undrlygtxinforgnlgrpinforgnlmsgid,
        OrgnlMsgNmId: transaction.undrlygtxinforgnlgrpinforgnlmsgnmid,
        OrgnlEndToEndId: transaction.undrlygtxinforgnlgrpinforgnlmsgendtoendid,
        OrgnlTxId: transaction.undrlygtxinforgnlgrpinforgnltxid,
        OrgnlIntrBkSttlmAmt: transaction.undrlygtxinforgnlintrbksttlmamt,
        OrgnlIntrBkSttlmDt: calcTime(transaction.undrlygtxinforgnlintrbksttlmdt).toISOString().slice(0,10),
        CxlRsnInf: transaction.undrlygtxinfcxlrsninfrsncd || transaction.undrlygtxinfcxlrsninfrsnprty, //Reason - DUPL, TECH or FRAD
        CxlRsnInfAddtInf: transaction.undrlygtxinfcxlrsninfrsnaddtinf, //If reason FRAD
        SttlmMtd: transaction.undrlygtxinforgnltxrefsttlminfsttlmmtd,
        ClrSysPrtry: transaction.undrlygtxinforgnltxrefsttlminfclrsysprtry, // LITAS-RLS or LITAS-MIG
        serviceLevel: transaction.undrlygtxinforgnltxrefpmttpinfsvclvlcd || "SEPA",
        debtorName: transaction.undrlygtxinforgnltxrefdbtrnm,
        debtorAccount: transaction.undrlygtxinforgnltxrefdbtracctidiban,
        debtorBic: transaction.undrlygtxinforgnltxrefdbtragtfininstnidbic,
        creditorName: transaction.undrlygtxinforgnltxrefcdtrnm,
        creditorAccount: transaction.undrlygtxinforgnltxrefcdtracctidiban, 
        creditorBic: transaction.undrlygtxinforgnltxrefcdtragtfininstnidbic,
        originatorClient: transaction.undrlygtxinfcxlrsninforgtrnm,
        originatorBank: transaction.undrlygtxinfcxlrsninforgtridorgidbicorbei
      });
      
      message._calculateAssgmntHeader({});
      const { messageHeader, assgmntHeader } = message;
      const [ upsertedMessage ] = await db.ffpcrqst_transaction.upsert({
        input_guid: transaction.guid,
        input_version: messageHeader.version,
        input_sender: messageHeader.sender,
        input_receiver: messageHeader.receiver,
        input_priority: messageHeader.priority,
        input_msgid: messageHeader.msgId,
        input_system: system,
        input_docid: messageHeader.msgId,
        input_datestype: messageHeader.date,
        input_businessarea: messageHeader.businessArea,
        // input_reportmsgid,
        // input_reportdttm,
        // input_grsts,
        // input_rsnprty,
        input_assgnmtid: assgmntHeader.Id,
        input_assgnmtcredttm: assgmntHeader.CreDtTm,
        input_ctrldatanboftxs: assgmntHeader.NbOfTxs,
        input_data: message.toXml(),
        input_author_guid: author_guid
      }, connection);

      const [ updatedStatus ] = await db.external_transaction_history.upsert({
        input_guid: guid,
        input_status: status,
        input_author_guid: author_guid,
      }, connection);

      await db._transaction.commit(connection);

      res.status(200).json(updatedStatus);
    }
  } 
  catch (err) {
    if (connection)
      await db._transaction.rollback(connection);
    next(err);
  } finally {
    if (connection)
      connection.release();
  }
};
