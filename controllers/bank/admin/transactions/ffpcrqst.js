const db = require("../../../../helpers/db/bankApi");
// const { InvalidDataError, InternalError } = require("../../../../errorTypes");
// const { EXTERNAL_TRANSACTION_UNAPPROVED } = require("../../../../helpers/constants/statuses").externalTransaction;
// const { paymentCancellation } = require("../../../../helpers/transactions");
const Joi = require("@hapi/joi");
const { init } = require("../../../../modules/kafka/services");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     paymentpaymentTransactionId: Joi.string().guid().required()
  //   });

  // if (body.paymentpaymentTransactionId)
  //   return Joi.object().keys({
  //     paymentpaymentTransactionId: Joi.string(),
  //     paymentEndToEndId: Joi.string(),
  //     paymentInstructionId: Joi.string(),
  //     serviceLevel: Joi.string(),
  //     amount: Joi.number(),
  //     chargeBearer: Joi.string(),
  //     debtorName: Joi.string(),
  //     debtorAccount: Joi.string(),
  //     debtorBic: Joi.string(),
  //     creditorName: Joi.string(),
  //     creditorAccount: Joi.string(),
  //     creditorBic: Joi.string(),
  //     remittanceInformation: Joi.string()
  //   });

  return Joi.object().keys({
    // CancellationTrId, generate
    // OrgnlMsgId,
    // OrgnlMsgNmId, 
    // OrgnlEndToEndId,
    OrgnlTxId: Joi.string().required(),
    // OrgnlIntrBkSttlmAmt,
    // OrgnlIntrBkSttlmDt,
    CxlRsnInf: Joi.string().required(), //Reason - DUPL, TECH or FRAD
    CxlRsnInfAddtInf: Joi.any().when("CxlRsnInf", { is: Joi.valid("FRAD"), then: Joi.string().required(), otherwise: Joi.any() }),
    // SttlmMtd = "CLRG",
    ClrSysPrtry: Joi.equal([ "LITAS-RLS", "LITAS-MIG" ]).required(), // LITAS-RLS or LITAS-MIG
    // serviceLevel = "SEPA",
    // debtorName,
    // debtorAccount,
    // debtorBic,
    // creditorName,
    // creditorAccount,
    // creditorBic,
    originatorClient: Joi.string(),
    originatorBank: Joi.string()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const { msgId } = req.query;
    let transactions = await db.ffpcrqst_transaction.get({ input_GrpHdrMsgId: msgId });
    transactions.forEach(item => {
      item.undrlygtxinforgnlintrbksttlmamt /= 100;
    })
    const numberOfLogins = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfLogins, data: transactions });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    let [ transactionHistory ] = await db.external_transaction_history.get({ input_guid: guid });
    if (!transactionHistory)
      return res.status(200).json({});

    transactionHistory.value /= 100;

    let [ document ] = await db.ffpcrqst_transaction.get({ input_guid: transactionHistory.guid });

    document.undrlygtxinforgnlintrbksttlmamt /= 100;

    transactionHistory.document = document;

    res.status(200).json(transactionHistory || {});
  } catch (err) {
    next(err);
  }
};

module.exports.post = (req, res, next) => init(req, res, next, "FFPCRQST");

module.exports.status = require("./ffpcrqst/status");