const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  try {
    const { guid, sender_iban, receiver_iban, currency, status, page, items } = req.query;
    const { lastDay, lastWeek, lastMonth, lastYear, fromDate, toDate } = req.query;
    let { type, direction } = req.query;
    let from, to;
    if(lastDay) {
      from = new Date();
      from.setDate(from.getDate() - 1)
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(lastWeek) {
      from = new Date();
      from.setDate(from.getDate() - 7);
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(lastMonth) {
      from = new Date();
      from.setMonth(from.getMonth() - 1);
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(lastYear) {
      from = new Date();
      const year = from.getFullYear();
      const month = from.getMonth();
      const day = from.getDate();
      from = new Date(year - 1, month, day)
      from = from.toISOString();
      to = new Date().toISOString();
    }
    else if(fromDate && toDate) {
      from = fromDate;
      to = toDate;
    }
    if(!direction)
      direction = "all";
    else if([ "internal", "all" ].includes(type ? type.toLowerCase() : type))
      type = undefined;
    if(type)
      type = type.toUpperCase();
    if(direction)
      direction = direction.toLowerCase();
    let transactions = await db.transaction_history.getByAdmin({
      input_type: type,
      input_direction: direction,
      input_page_number: page,
      input_items_count: items,
      input_from: from,
      input_to: to
    })

    transactions.forEach(item => {
      item.value /= 100;
      item.commission  /= 100;
      if(item.from_amount && item.to_amount) {
        item.from_amount /= 100;
        item.to_amount /= 100;
      }
    });

    for(let i = 0; i < transactions.length; i++) {
      if(transactions[i].type === "FFPCRQST") {
        const [ ffpcrqstTransaction ] = await db.ffpcrqst_transaction.get({
          input_guid: transactions[i].guid
        });
        const [{ guid }] = await db.ffcctrns_transaction.get({ input_cdttrftxinfpmtidtxid: ffpcrqstTransaction.undrlygtxinforgnlgrpinforgnltxid })
        transactions[i] = {
          ...transactions[i],
          originalTxGuid: guid
        }
      }
      else if(transactions[i].type === "FFCCTRNS") {
        const [{ msgid }] = await db.ffcctrns_transaction.get({
          input_guid: transactions[i].guid
        });
        transactions[i] = {
          ...transactions[i],
          msgid
        }
      }
    }
    
    const numberOfTransactions = transactions[0] ? transactions[0].count : 0;

    res.status(200).json({ count: numberOfTransactions, data: transactions });
  }
  catch (err) {
    next(err);
  }
};