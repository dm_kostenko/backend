const Joi = require("@hapi/joi");
const { getCETDate } = require("../../../../../helpers/target2Timing")
const db = require("../../../../../helpers/db/bankApi");
const { EXTERNAL_TRANSACTION_APPROVED, EXTERNAL_TRANSACTION_UNAPPROVED, EXTERNAL_TRANSACTION_DECLINED } = require("../../../../../helpers/constants/statuses").externalTransaction;
const { ROINVSTG } = require("../../../../../modules/messages");

module.exports.schema = () => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });


  return Joi.object().keys({
    // type: 
    status: Joi.equal([ EXTERNAL_TRANSACTION_APPROVED, EXTERNAL_TRANSACTION_DECLINED ]).required(), 
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required(),
  });
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { 
    system = "LITAS-MIG", /*LITAS-RLS or LITAS-MIG */
    status  
  } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  try {
    const [ extTransaction ] = await db.external_transaction_history.get({ input_guid: guid, input_type: "ROINVSTG" });

    if (!extTransaction || extTransaction.status !== EXTERNAL_TRANSACTION_UNAPPROVED)
      throw { message: `Transaction ${guid} status is ${extTransaction ? extTransaction.status : "undefined"}` };
    
    if (status === EXTERNAL_TRANSACTION_DECLINED) {
      const [ upsertedMessage ] = await db.external_transaction_history.upsert({
        input_guid: guid,
        input_status: status,
        input_author_guid: author_guid,
      });

      return res.status(200).json(upsertedMessage);
    }

    const [ transaction ] = await db.roinvstg_transaction.get({ input_guid: guid });

    const message = new ROINVSTG({
      sender: transaction.cxldtlstxinfandstsorgnltxrefcdtragtfininstnidbic,
      receiver: transaction.cxldtlstxinfandstsorgnltxrefdbtragtfininstnidbic,
      msgId: transaction.cxldtlstxinfandstscxlstsid,
      system
    });

    message.addTransactionCancellation({
      cancellationStsId: transaction.cxldtlstxinfandstscxlstsid,
      orgnlMsgId: transaction.cxldtlstxinfandstsorgnlgrpinforgnlmsgid,
      orgnlMsgNmId: "pacs.008.001.02",
      orgnlEndToEndId: transaction.cxldtlstxinfandstsorgnlendtoendid,
      orgnlTxId: transaction.cxldtlstxinfandstsorgnltxid,
      transactionCancellationSts: "RJCR",
      originatorClient: transaction.cxldtlstxinfandstscxlstsrsninforgtrnm,
      originatorBank: transaction.cxldtlstxinfandstscxlstsrsninforgtridorgidbicorbei,
      reasonForCancellation: transaction.cxldtlstxinfandstscxlstsrsninfrsncd || transaction.cxldtlstxinfandstscxlstsrsninfrsnprtry, 
      addInfForCancellation: transaction.cxldtlstxinfandstscxlstsrsninfaddtlinf,
      orgnlIntrBkSttlmAmt: transaction.cxldtlstxinfandstsorgnltxrefintrbksttlmamt,
      orgnlIntrBkSttlmDt: transaction.cxldtlstxinfandstsorgnltxrefintrbksttlmdt,
      sttlmMtd: "CLRG",
      clrSysPrtry: transaction.cxldtlstxinfandstsorgnltxrefsttlminfclrsysprtry,
      serviceLevel: "SEPA",
      debtorName: transaction.cxldtlstxinfandstsorgnltxrefdbtrnm,
      debtorAccount: transaction.cxldtlstxinfandstsorgnltxrefdbtracctidiban,
      debtorBic: transaction.cxldtlstxinfandstsorgnltxrefdbtragtfininstnidbic,
      creditorName: transaction.cxldtlstxinfandstsorgnltxrefcdtrnm,
      creditorAccount: transaction.cxldtlstxinfandstsorgnltxrefcdtracctidiban,
      creditorBic: transaction.cxldtlstxinfandstsorgnltxrefcdtragtfininstnidbic
    });

    const { messageHeader } = message;
    
    await db.roinvstg_transaction.upsert({
      input_guid: transaction.guid,
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: system,
      input_docid: messageHeader.msgId,
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      input_status: EXTERNAL_TRANSACTION_APPROVED,
      // input_reportmsgid,
      // input_reportdttm,
      // input_grsts,
      // input_rsnprty,
      input_data: message.toXml(),
      input_author_guid: author_guid
    })

    const [ updatedStatus ] = await db.external_transaction_history.upsert({
      input_guid: guid,
      input_status: status,
      input_author_guid: author_guid,
    });

    res.status(200).json(updatedStatus);

  }
  catch(err) {
    next(err)
  }
}