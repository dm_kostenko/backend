const db = require("../../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");


module.exports.schema = () => {
  return Joi.object().keys({
    direction: Joi.equal([ "incoming", "outgoing" ]).required(),
    transaction_type: Joi.equal([ "FFCCTRNS"/*, "INTERNAL" */]).required(),
    rule_type: Joi.equal([ "status" ]).required(),
    role: Joi.equal([ "all", "individual", "entity" ]).required(),
    success_status: Joi.string().required(),
    failed_status: Joi.string().required(),
    below: Joi.number().required()
  });
};

module.exports.get = async(req, res, next) => {
  try {
    // const { direction, transaction_type, rule_type, name, value } = req.params;
    const transactionsRules = await db.transaction_rule.get({
      // input_direction: direction,
      // input_transaction_type: transaction_type,
      // input_rule_type: rule_type,
      // input_name: name,
      // input_value: value
    });

    const outgoingSuccessEntityStatus = transactionsRules.find(item => item.name === "success_entity_status" && item.direction === "outgoing");
    const outgoingFailedEntityStatus = transactionsRules.find(item => item.name === "failed_entity_status" && item.direction === "outgoing");
    const outgoingBelowEntityRule = transactionsRules.find(item => item.name === "below_entity" && item.direction === "outgoing");
    const outgoingSuccessIndividualStatus = transactionsRules.find(item => item.name === "success_individual_status" && item.direction === "outgoing");
    const outgoingFailedIndividualStatus = transactionsRules.find(item => item.name === "failed_individual_status" && item.direction === "outgoing");
    const outgoingBelowIndividualRule = transactionsRules.find(item => item.name === "below_individual" && item.direction === "outgoing");

    const incomingSuccessStatus = transactionsRules.find(item => item.name === "success_status" && item.direction === "incoming");
    const incomingFailedStatus = transactionsRules.find(item => item.name === "failed_status" && item.direction === "incoming");
    const incomingBelowRule = transactionsRules.find(item => item.name === "below" && item.direction === "incoming");

    // const otherRules


    const rules = [
      {
        direction: "outgoing",
        transaction_type: "FFCCTRNS",
        rule_type: "status",
        role: "entity",
        success_status: outgoingSuccessEntityStatus.value,
        failed_status: outgoingFailedEntityStatus.value,
        below: outgoingBelowEntityRule.value / 100
      },
      {
        direction: "outgoing",
        transaction_type: "FFCCTRNS",
        rule_type: "status",
        role: "individual",
        success_status: outgoingSuccessIndividualStatus.value,
        failed_status: outgoingFailedIndividualStatus.value,
        below: outgoingBelowIndividualRule.value / 100
      },
      {
        direction: "incoming",
        transaction_type: "FFCCTRNS",
        rule_type: "status",
        role: "all",
        success_status: incomingSuccessStatus.value,
        failed_status: incomingFailedStatus.value,
        below: incomingBelowRule.value / 100
      }
    ];

    const count = transactionsRules[0] ? transactionsRules[0].count : 0;

    res.status(200).json({ count, data: rules });
  }
  catch(err) {
    next(err);
  }
};

module.exports.post = async(req, res, next) => {
  let connection;

  try {    
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    const { direction, transaction_type, rule_type, role, success_status, failed_status, below } = req.body;

    switch(direction) {
      case "outgoing":
        await db.transaction_rule.upsert({
          input_direction: "outgoing",
          input_transaction_type: transaction_type,
          input_rule_type: rule_type,
          input_name: `success_${role}_status`,
          input_value: success_status
        }, connection);

        await db.transaction_rule.upsert({
          input_direction: "outgoing",
          input_transaction_type: transaction_type,
          input_rule_type: rule_type,
          input_name: `failed_${role}_status`,
          input_value: failed_status
        }, connection);

        await db.transaction_rule.upsert({
          input_direction: "outgoing",
          input_transaction_type: transaction_type,
          input_rule_type: rule_type,
          input_name: `below_${role}`,
          input_value: below * 100
        }, connection);
        
        break;

      case "incoming":
        await db.transaction_rule.upsert({
          input_direction: "incoming",
          input_transaction_type: transaction_type,
          input_rule_type: rule_type,
          input_name: "success_status",
          input_value: success_status
        }, connection);

        await db.transaction_rule.upsert({
          input_direction: "incoming",
          input_transaction_type: transaction_type,
          input_rule_type: rule_type,
          input_name: "failed_status",
          input_value: failed_status
        }, connection);

        await db.transaction_rule.upsert({
          input_direction: "incoming",
          input_transaction_type: transaction_type,
          input_rule_type: rule_type,
          input_name: "below",
          input_value: below * 100
        }, connection);

        break;
      default: break;
    };

    await db._transaction.commit(connection);

    res.status(200).json(req.body);
  }
  catch(err) {
    next(err);
  }
};