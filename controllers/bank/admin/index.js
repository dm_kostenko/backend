module.exports = {
  accounts: require("./accounts"),
  documents: require("./documents"),
  reports: require("./reports"),
  entities: require("./entities"),
  inbox: require("./inbox"),
  outbox: require("./outbox"),
  logins: require("./logins"),
  messages: require("./messages"),
  transactions: require("./transactions"),
  privileges: require("./privileges"),
  currencies: require("./currencies"),
  rates: require("./rates"),
  rateRuleParams: require("./rateRuleParams"),
  aml: require("./aml")
};