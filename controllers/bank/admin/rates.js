const db = require("../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");


module.exports.schema = (body) => {

  // if(body.guid)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required(),
  //     name: Joi.string().required(), 
  //     description: Joi.string().required(),
  //     rules: Joi.array().items(Joi.object().keys({
  //       guid: Joi.string().guid().required(),
  //       type: Joi.equal([ "transaction", "currency", "system" ]).required(),
  //       rule_type: Joi
  //         .when("type", { is: "transaction", then: Joi.equal([ "FFCCTRNS", "PRTRN", "FFPCRQST", "ROINVSTG" ]).required() })
  //         .when("type", { is: "currency", then: Joi.equal("exchange").required() })
  //         .when("type", { is: "system", then: Joi.equal("recurring_payment").required() }),
  //       params: Joi
  //         .when("type",
  //           { 
  //             is: "transaction", 
  //             then: Joi.array().items(Joi.object().keys({
  //               guid: Joi.string().guid().required(),
  //               name: Joi.equal([ "party", "direction", "rate" ]).required(),
  //               value: Joi
  //                 .when("name", { is: "party", then: Joi.equal([ "sender", "receiver" ]).required() })
  //                 .when("name", { is: "direction", then: Joi.equal([ "incoming", "outgoing" ]).required() })
  //                 .when("name", { is: "rate", then: Joi.number().required() }),
  //               value_type: Joi.when("value", { is: "rate", then: Joi.equal([ "percent", "number" ]).required() })
  //             }))
  //           }
  //         )
  //         .when("type",
  //           { 
  //             is: "currency", 
  //             then: Joi.array().items(Joi.object().keys({
  //               guid: Joi.string().guid().required(),
  //               name: Joi.equal([ "from", "to", "rate" ]).required(),
  //               value: Joi
  //                 .when("name", { is: "from", then: Joi.string().required() })
  //                 .when("name", { is: "to", then: Joi.string().required() })
  //                 .when("name", { is: "rate", then: Joi.number().required() }),
  //               value_type: Joi.when("value", { is: "rate", then: Joi.equal([ "percent", "number" ]).required() })
  //             }))
  //           }
  //         )
  //         .when("type",
  //           { 
  //             is: "system", 
  //             then: Joi.array().items(Joi.object().keys({
  //               guid: Joi.string().guid().required(),
  //               name: Joi.equal([ "interval", "rate" ]).required(),
  //               value: Joi
  //                 .when("name", { is: "interval", then: Joi.equal([ "year", "month" ]).required() })
  //                 .when("name", { is: "rate", then: Joi.number().required() }),
  //               value_type: Joi.when("value", { is: "rate", then: Joi.equal("number").required() }),                
  //             }))
  //           }
  //         )
  //     }))
  //   })
    
  // return Joi.object().keys({
  //   name: Joi.string().required(), 
  //   description: Joi.string().required(),
  //   rules: Joi.array().items(Joi.object().keys({
  //     type: Joi.equal([ 
  //       "outgoing_FFCCTRNS", 
  //       "Internal",
  //       "currency_exchange", 
  //       "system" 
  //     ]).required(),
  //     rule_type: Joi
  //       .when("type", 
  //       { 
  //         is: "outgoing_FFCCTRNS", then: Joi.equal([ 
  //           "amount",
  //           "paymentTransactionId",
  //           "paymentInstructionId",
  //           "paymentEndToEndId",
  //           "currency",
  //           "senderAccount",
  //           "receiverAccount",
  //           "serviceLevel",
  //           "chargeBearer",
  //           "senderName",
  //           "senderBIC",
  //           "receiverName",
  //           "receiverBIC",
  //           "remittanceInformation"
  //         ]).required() 
  //       })
  //       .when("type", 
  //       { 
  //         is: "Internal", then: Joi.equal([ 
  //           "senderCurrency",
  //           "receiverCurrency",
  //           "senderNumber",
  //           "receiverNumber",
  //           "amount"
  //         ]).required() 
  //       })
  //       .when("type", { is: "currency_exchange", then: Joi.string().required() })           // USD -> EUR: "USD_EUR"
  //       .when("type", { is: "system", then: Joi.equal("recurring_payment").required() }),
  //     params: Joi
  //       .when("type",
  //         { 
  //           is: "outgoing_FFCCTRNS", 
  //           then: Joi.array().items(Joi.object().keys({
  //             name: Joi.equal([ "below", "above", "equals", "mask", "other", "rate" ]).required(),
  //             value: Joi
  //               .when("name", { is: "below", then: Joi.number().required() })
  //               .when("name", { is: "above", then: Joi.number().required() })
  //               .when("name", { is: "equals", then: Joi.required() })
  //               .when("name", { is: "mask", then: Joi.string().required() })
  //               .when("name", { is: "other", then: Joi.number().required() })
  //               .when("name", { is: "rate", then: Joi.number().required() }),
  //             value_type: Joi.equal([ "percent", "number", "string" ]).required()
  //           }))
  //         }
  //       )
  //       .when("type",
  //         { 
  //           is: "Internal", 
  //           then: Joi.array().items(Joi.object().keys({
  //             name: Joi.equal([ "below", "above", "equals", "mask", "other", "rate" ]).required(),
  //             value: Joi
  //               .when("name", { is: "below", then: Joi.number().required() })
  //               .when("name", { is: "above", then: Joi.number().required() })
  //               .when("name", { is: "equals", then: Joi.required() })
  //               .when("name", { is: "mask", then: Joi.string().required() })
  //               .when("name", { is: "other", then: Joi.number().required() })
  //               .when("name", { is: "rate", then: Joi.number().required() }),
  //             value_type: Joi.equal([ "percent", "number", "string" ]).required()
  //           }))
  //         }
  //       )
  //       .when("type",
  //         { 
  //           is: "currency_exchange", 
  //           then: Joi.array().items(Joi.object().keys({
  //             name: Joi.equal([ "above", "below", "other", "rate" ]).required(),
  //             value: Joi
  //               .when("name", { is: "below", then: Joi.number().required() })
  //               .when("name", { is: "above", then: Joi.number().required() })
  //               .when("name", { is: "other", then: Joi.number().required() })
  //               .when("name", { is: "rate", then: Joi.number().required() }),
  //             value_type: Joi.equal([ "percent", "number" ]).required()
  //           }))
  //         }
  //       )
  //       .when("type",
  //         { 
  //           is: "system", 
  //           then: Joi.array().items(Joi.object().keys({
  //             name: Joi.equal([ "interval", "rate" ]).required(),
  //             value: Joi
  //               .when("name", { is: "interval", then: Joi.equal([ "year", "month" ]).required() })
  //               .when("name", { is: "rate", then: Joi.number().required() }),
  //             value_type: Joi.when("value", { is: "rate", then: Joi.equal("number").required() }),                
  //           }))
  //         }
  //       )
  //   }))
  // })
  return true
};


module.exports.get = async (req, res, next) => {
  try {
    const { name, description, page, items } = req.query;
    const rates = await db.rate.get({
      input_name: name,
      input_description: description,
      input_page_number: page,
      input_items_count: items
    });

    if(rates.length < 1)
      return res.status(200).json({ count: 0, data: [] });
    
    res.status(200).json({ count: rates[0].count || 0, data: rates });    
  }
  catch(err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let { guid } = req.params;
    let name = undefined;
    if(guid === "default") {
      name = "Default rate";
      guid = undefined;
    }
    const [ rate ] = await db.rate.get({
      input_guid: guid,
      input_name: name
    });

    if(!rate)
      return res.status(200).json({});
    
    res.status(200).json(rate);    
  }
  catch(err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  let {
    name,
    description,
    rules
  } = req.body;
  const author_guid = req.auth && req.auth.loginGuid ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";
  let connection;

  try {
    
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    const isAllCurrencySet = !!rules.find(i => i.all)
    let newRules = rules
    if(isAllCurrencySet)
      newRules = newRules.filter(i => !i.all)

    let guidsString = '{';
    [ ...new Set(newRules.map(i => i.rate_rule_guid)) ].forEach(i => guidsString += `"${i}",`)

    guidsString = guidsString.substring(0, guidsString.length - 1) + '}'

    // copy rate without rules
    const [{ copy_rate: guid }] = await db.rate.copy({
      input_rate_guid: "d8c5b253-d583-d14a-20bf-249609336ee9",
      input_name: name,
      input_description: description,
      input_rate_rules: guidsString,
      input_author_guid: author_guid
    }, connection)

    if(rules.find(i => i.all)) {
      const aboveObjects = rules.filter(item => item.all && item.i === "above")
      const abovePercentRate = aboveObjects.find(item => item.value_type === "percent").value
      const aboveNumberRate = aboveObjects.find(item => item.value_type === "number").value
      const aboveValue = aboveObjects.find(item => item.name === "above").value

      const belowObjects = rules.filter(item => item.all && item.i === "below")
      const belowPercentRate = belowObjects.find(item => item.value_type === "percent").value
      const belowNumberRate = belowObjects.find(item => item.value_type === "number").value
      const belowValue = belowObjects.find(item => item.name === "below").value

      const otherObjects = rules.filter(item => item.all && item.i === "other")
      const otherPercentRate = otherObjects.find(item => item.value_type === "percent").value
      const otherNumberRate = otherObjects.find(item => item.value_type === "number").value

      await db.rate.setAllExchange({
        input_above: aboveValue,
        input_below: belowValue,
        input_number_rate_above: aboveNumberRate,
        input_number_rate_below: belowNumberRate,
        input_number_rate_other: otherNumberRate,
        input_percent_rate_above: abovePercentRate,
        input_percent_rate_below: belowPercentRate,
        input_percent_rate_other: otherPercentRate,
        input_rate_guid: guid
      }, connection)

      rules = rules.filter(i => !i.all)
    }

    let groupObj = {}

    rules.forEach(rule => {
      if(groupObj[`${rule.ruleType}__${rule.type}__${rule.i}`])
        groupObj[`${rule.ruleType}__${rule.type}__${rule.i}`].push({
          name: rule.name,
          value_type: rule.value_type,
          value: rule.value
        })
      else
        groupObj[`${rule.ruleType}__${rule.type}__${rule.i}`] = [{
          name: rule.name,
          value_type: rule.value_type,
          value: rule.value
        }]
    })

    const keys = Object.keys(groupObj)

    for(let i = 0; i < keys.length; i++) {
      const [ ruleType, type ] = keys[i].split("__")
      
      const [{ guid: ruleGuid }] = await db.rate_rule.upsert({
        input_rule_type: ruleType,
        input_type: type,
        input_author_guid: author_guid
      }, connection);

      await db.rate.addRule({
        input_rate_guid: guid,
        input_rate_rule_guid: ruleGuid,
        input_author_guid: author_guid
      }, connection);

      for(let j = 0; j < groupObj[keys[i]].length; j++)
        await db.rate_rule_param.upsert({
          input_rate_rule_guid: ruleGuid,
          input_name: groupObj[keys[i]][j].name,
          input_value_type: groupObj[keys[i]][j].value_type,
          input_value: [ "number", "percent" ].includes(groupObj[keys[i]][j].value_type) ? groupObj[keys[i]][j].value * 100 : groupObj[keys[i]][j].value,
          input_author_guid: author_guid
        }, connection);
    }

    await db._transaction.commit(connection);
    res.status(200).json({ guid });
  }
  catch(err) {
    if (connection)
      await db._transaction.rollback(connection);
    next(err);
  }
  finally {
    if (connection)
      connection.release();
  }
};

module.exports.rules = require("./rates/rules");
module.exports.params = require("./rates/params")