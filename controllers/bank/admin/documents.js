const db = require("../../../helpers/db/bankApi");
const { XML_IMPORT_RECEIVED } = require("../../../helpers/constants/statuses").xmlImport;

const author_guid = "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4"; //imported docs
module.exports.postIncomingXml = async (req, res, next) => {
  try {
    await db.inbox.upsert({
      input_type: "undefined",
      input_data: escape(req.body),
      input_status: XML_IMPORT_RECEIVED,
      // input_details: ,
      input_author_guid: author_guid,
    });
    
    res.json({ message: "Inserted" });
  } catch (err) {
    next(err);
  }
};

module.exports.ffcctrns= require("./documents/ffcctrns");
module.exports.ffpcrqst= require("./documents/ffpcrqst");
module.exports.prtrn = require("./documents/prtrn");