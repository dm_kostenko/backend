const db = require("../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  return Joi.object().keys({
    indiviual_guid: Joi.string().guid().required(),
    entity_guid: Joi.string().guid().required(),
  });
};

module.exports.get = async (req, res, next) => {
  try { 
    const { indiviual_guid, entity_guid, page, items } = req.query;
    const entities = await db.entity_founder.get({ 
      input_founder_guid: indiviual_guid,
      input_entity_guid: entity_guid,
      input_founder_type: 'individual',
      input_page_number: page,
      input_items_count: items
    });

    res.status(200).json({ count: entities[0] ? entities[0].count : 0, data: entities });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  try {
    const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
    const { indiviual_guid, entity_guid } = req.body;
    const entities = await db.entity_founder.upsert({ 
      input_founder_guid: indiviual_guid, 
      input_entity_guid: entity_guid,
      input_founder_type: 'individual',
      input_author_guid: author_guid,
    });

    res.status(200).json({ count: entities[0] ? entities[0].count : 0, data: entities });
  }
  catch (err) {
    next(err);
  }
};
