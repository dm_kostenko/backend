const db = require("../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const {
    guid,
    type,
    status,
    page,
    items,
  } = req.query;

  try {
    const documents = await db.inbox.get({
      input_guid: guid,
      input_type: type,
      input_status: status,
      input_page_number: page,
      input_items_count: items,
    });
  
    res.status(200).json({ count: documents[0] ? documents[0].count : 0, data: documents });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;

  try {
    const [ document ] = await db.inbox.get({ input_guid: guid });
  
    res.status(200).json(document);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { docs } = req.body;
  try {
    await Promise.all(docs.map(async(doc) => {
      await db.demon.add_inbox({
        input_data: escape(doc.xml),
        input_details: "details"
      })
    }))
    res.status(200).json({ status: "OK" })
  }
  catch(err) {
    next(err);
  }

}

module.exports.status = require("./inbox/status");