const amlApi = require("../../../../helpers/external/api/aml")

module.exports.get = async (req, res, next) => {
    const { id } = req.params;
  
    try {
      const data = await amlApi.getCertificate(id);
      res.status(200).json(data);
    }
    catch(err) {
      next(err)
    }
}