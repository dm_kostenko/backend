const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const {
    name
  } = req.query;

  try {
    let tags = await db.aml_tag.get({
      input_name: name
    });

    if (tags.length < 1)
      return res.status(200).json({ data: [] });

    tags = tags.sort((a,b) => new Date(b.created_at) - new Date(a.created_at))
    
    res.status(200).json({ data: tags });

  }
  catch(err) {
    next(err)
  }
}

module.exports.post = async (req, res, next) => {
  const { name, type, color, del } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    let upsertedTag;
    if(del)
      [ upsertedTag ] = await db.aml_tag.delete({
        input_name: name
      });
    else
      [ upsertedTag ] = await db.aml_tag.upsert({
        input_name: name,
        input_type: type,
        input_color: color,
        input_author_guid: author_guid
      });

    res.status(200).json(upsertedTag);

  }
  catch(err) {
    next(err)
  }
}


module.exports.case = require("./tags/case")