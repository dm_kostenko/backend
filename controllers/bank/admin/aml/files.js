const db = require("../../../../helpers/db/bankApi");
const { Base64 } = require("js-base64")

module.exports.get = async (req, res, next) => {
  let {
    caseId
  } = req.params;

  if(caseId === "all")
    caseId = undefined;

  try {
    let files = await db.aml_case_file.get({
      input_case_id: caseId
    });

    files.forEach(file => {
      file.data = file.data.toString()
    })

    if (files.length < 1)
      return res.status(200).json({ data: [] });

    res.status(200).json({ data: files });

  }
  catch(err) {
    next(err)
  }
}


module.exports.post = async (req, res, next) => {
  let {
    caseId
  } = req.params;

  const {
    guid,
    del
  } = req.body;

  if(caseId === "all")
    caseId = undefined;

  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  let connection;

  try {

    const files = req.files 
      ? Array.isArray(req.files.file) ? req.files.file : [ req.files.file ]
      : undefined
    connection = await db.getConnection();
    await db._transaction.begin(connection);
    
    if(files && files[0])
      for(let i = 0; i < files.length; i++) {
        await db.aml_case_file.upsert({
          input_case_id: caseId,
          input_name: files[i].name,
          input_data: Base64.encode(files[i].data),
          input_author_guid: author_guid
        }, connection)
      }
    else if(del && guid)
      await db.aml_case_file.delete({
        input_guid: guid
      })

    await db._transaction.commit(connection);
    res.status(200).json({ status: "success" })
  }
  catch(err) {
    if (connection)
      await db._transaction.rollback(connection);
    next(err)
  }
  finally {
    if (connection)
      connection.release();
  }
}