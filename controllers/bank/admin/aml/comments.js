const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const {
    page,
    items,
    entityId
  } = req.query;

  const { caseId } = req.params;

  try {
    const comments = await db.aml_comments.entityGet({
      input_case_id: caseId,
      input_entity_id: entityId,
      input_page_number: page,
      input_items_count: items
    });

    if (comments.length < 1)
      return res.status(200).json({ count: 0, data: [] });

    res.status(200).json({ count: comments[0].count || 0, data: comments });

  }
  catch(err) {
    next(err)
  }
}

module.exports.post = async (req, res, next) => {
  const { text, entityId } = req.body;
  const { caseId } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  const username = req.auth ? req.auth.username : "emptyname";


  try {
    const [ upsertedComment ] = await db.aml_comments.entityUpsert({
      input_case_id: caseId,
      input_entity_id: entityId,
      input_date: new Date().toISOString(),
      input_username: username,
      input_text: text,
      input_author_guid: author_guid
    });

    res.status(200).json(upsertedComment);

  }
  catch(err) {
    next(err)
  }
}
