const db = require("../../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  let {
    guid,
    caseId
  } = req.params;

  if(caseId === "all")
    caseId = undefined;

  const { name, value } = req.query;

  try {
    const tags = await db.aml_case_tag.get({
      input_case_id: caseId,
      input_tag_name: name,
      input_value: value
    });

    if (tags.length < 1)
      return res.status(200).json({ data: [] });

    res.status(200).json({ data: tags });

  }
  catch(err) {
    next(err)
  }
}

module.exports.post = async (req, res, next) => {
  const {
    caseId
  } = req.params;

  const { data } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  let connection;

  try {
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    let upsertedTag;
    for(let i = 0; i < data.length; i++) {
      if(data[i].del && data[i].guid)
        [ upsertedTag ] = await db.aml_case_tag.delete({
          input_guid: data[i].guid
        }, connection);
      else
        [ upsertedTag ] = await db.aml_case_tag.upsert({
          input_case_id: caseId,
          input_tag_name: data[i].name,
          input_value: data[i].value,
          input_author_guid: author_guid
        }, connection);
    }
    
    await db._transaction.commit(connection);
    res.status(200).json({ status: "Success" });
  }
  catch(err) {
    if (connection)
      await db._transaction.rollback(connection);
    next(err)
  }
  finally {
    if (connection)
      connection.release();
  }
}
