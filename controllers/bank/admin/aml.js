const db = require("../../../helpers/db/bankApi");
const amlApi = require("../../../helpers/external/api/aml")

module.exports.get = async (req, res, next) => {
  const {
    page,
    items,
    name,
    client_ref,
    risk_level,
    match_status,
    created_at_from,
    created_at_to,
    ...tagsParams
  } = req.query;

  try {
    const from = items * (page - 1);
    const to = parseInt(from) + parseInt(items);

    let { data } = await amlApi.get({
      name,
      client_ref,
      risk_level,
      match_status,
      created_at_from,
      created_at_to
    });

    const tags = await db.aml_case_tag.get({});
    const cases = await db.login_aml_case.get({});
    const logins = await db.login.get({});

    data = data.map(i => {
      let searcher = 'admin'
      const c = cases.find(c => c.case_id == i.id);
      if(c)
        searcher = logins.find(login => login.guid === c.login_guid).username
      return {
        ...i,
        tags: tags.filter(tag => tag.case_id == i.id),
        searcher
      }
    }).filter(i => {
      return Object.keys(tagsParams).every(tag => {
        return i.tags.find(caseTag => {
          return tagsParams[tag] === "undefined"
            ? caseTag.tag_name === tag
            : caseTag.tag_name === tag && caseTag.value === tagsParams[tag]
        })
      })
    })    

    const count = data.length;
    data = data.slice(from, to);

    res.status(200).json({ count, data });
  }
  catch(err) {
    next(err)
  }
}

module.exports.getById = async (req, res, next) => {
  const { id } = req.params;

  try {
    const data = await amlApi.getById(id);
    res.status(200).json(data);
  }
  catch(err) {
    next(err)
  }
}

module.exports.post = async (req, res, next) => {
  const {
    entityType,
    fuzziness,
    terms
  } = req.body;

  console.log(req.auth)

  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    const responses = await amlApi.post({
      terms,
      fuzziness,
      entityType
    })
    for(let i = 0; i < responses.length; i++) {
      await db.login_aml_case.upsert({
        input_login_guid: author_guid,
        input_case_id: responses[i].id,
        input_author_guid: author_guid
      })
    }
    res.status(200).json({ responses })
  }
  catch(err) {
    next(err)
  }
}

module.exports.postById = async (req, res, next) => {
  const {
    is_monitored  
  } = req.body;

  const { id } = req.params;

  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    const { code, status } = is_monitored !== undefined
    ? await amlApi.patchMonitor({
      id,
      is_monitored
    })
    : await amlApi.patchSearch({
      id,
      ...req.body
    })
    res.status(code).json({ status })
  }
  catch(err) {
    next(err)
  }
}

module.exports.comments = require("./aml/comments")
module.exports.tags = require("./aml/tags")
module.exports.files = require("./aml/files")
module.exports.certificate = require("./aml/certificate")