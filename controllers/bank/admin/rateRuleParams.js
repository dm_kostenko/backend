const db = require("../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");
const { internal_template } = require("../../../helpers/db/bankApi");

module.exports.schema = () => {
  return Joi.array().items(
    Joi.object().keys({
      rate_rule_guid: Joi.string().guid().required(),
      name: Joi.string().required(),
      value_type: Joi.string().required(),
      value: Joi.string().required()
    }))
}

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth && req.auth.loginGuid ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";
  let connection;

  try {
    
    connection = await db.getConnection();
    await db._transaction.begin(connection);

    let data = req.body;

    if(data.find(item => item.all)) {
      const aboveObjects = data.filter(item => item.all && item.i === "above")
      const abovePercentRate = aboveObjects.find(item => item.value_type === "percent").value
      const aboveNumberRate = aboveObjects.find(item => item.value_type === "number").value
      const aboveValue = aboveObjects.find(item => item.name === "above").value

      const belowObjects = data.filter(item => item.all && item.i === "below")
      const belowPercentRate = belowObjects.find(item => item.value_type === "percent").value
      const belowNumberRate = belowObjects.find(item => item.value_type === "number").value
      const belowValue = belowObjects.find(item => item.name === "below").value

      const otherObjects = data.filter(item => item.all && item.i === "other")
      const otherPercentRate = otherObjects.find(item => item.value_type === "percent").value
      const otherNumberRate = otherObjects.find(item => item.value_type === "number").value
      const rateGuid = otherObjects[0].rate_guid

      await db.rate.setAllExchange({
        input_above: aboveValue,
        input_below: belowValue,
        input_number_rate_above: aboveNumberRate,
        input_number_rate_below: belowNumberRate,
        input_number_rate_other: otherNumberRate,
        input_percent_rate_above: abovePercentRate,
        input_percent_rate_below: belowPercentRate,
        input_percent_rate_other: otherPercentRate,
        input_rate_guid: rateGuid
      }, connection)

      data = data.filter(i => !i.all)
    }

    data.forEach(async(param) => {
      try {
        await db.rate_rule_param.upsert({
          input_rate_rule_guid: param.rate_rule_guid,
          input_name: param.name,
          input_value_type: param.value_type,
          input_value: [ "number", "percent" ].includes(param.value_type) ? param.value * 100 : param.value,
          input_author_guid: author_guid
        })
      }
      catch(err) {
        throw(err)
      }
    })

    await db._transaction.commit(connection);
    res.status(200).json({});
  }
  catch(err) {
    if (connection)
      await db._transaction.rollback(connection);
    next(err);
  }
  finally {
    if (connection)
      connection.release();
  }
}