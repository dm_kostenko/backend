const db = require("../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      type: Joi.equal("admin", "individual", "entity").required(),
      name: Joi.string().required(),
      description: Joi.string()
    });

  return Joi.object().keys({
    type: Joi.equal("admin", "individual", "entity").required(),
    name: Joi.string().required(),
    description: Joi.string()
  });
};

module.exports.get = async (req, res, next) => {
  try { 
    const { name, type } = req.query;
    const priveleges = await db.privilege.get({ input_name: name, input_type: type });

    res.status(200).json({ count: priveleges[0] ? priveleges[0].count : 0, data: priveleges });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try { 
    const { guid } = req.params;
    const [ privilege = {} ] = await db.privilege.get({ input_guid: guid });

    res.status(200).json(privilege);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  try { 
    const { 
      guid,
      type,
      name,
      description
    } = req.params;

    const [ privilege ] = await db.privilege.upsert({
      input_guid: guid,
      input_type: type, 
      input_name: name,
      input_description: description
    });

    res.status(200).json(privilege);
  }
  catch (err) {
    next(err);
  }
};