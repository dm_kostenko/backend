const uuidv1 = require("uuid/v1");
const Joi = require("@hapi/joi");
const db = require("../../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../../errorTypes");
const { FFCCTRNS } = require("../../../../modules/messages");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });


  return Joi.object().keys({
    // type: 
    transactionId: Joi.string().guid(), 
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    // let dataOnLogins = await db.login.get({
    //   input_guid: req.query.guid,
    //   input_username: req.query.username,
    //   input_email: req.query.email,
    //   input_enabled: req.query.enabled,
    //   input_page_number: req.query.page,
    //   input_items_count: req.query.items
    // });
    const { 
      sender,
      receiver,
      page,
      items } = req.query;
    
    const documents = await db.ffcctrns_document.get({ 
      input_sender: sender, 
      input_receiver: receiver,
      input_page_number: page,
      input_items_count: items 
    });

    const count = documents[0] ? documents[0].count : 0;
    res.status(200).json({ count, data: documents });

  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    const [ document ] = await db.ffcctrns_document.get({ input_grphdrmsgid: guid });
    
    res.status(200).json(document);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const {
    // guid, 
    transactionId, 
    system = "LITAS-RLS" //LITAS-RLS or LITAS-MIG 
  } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  try {
    const [ transaction ] = await db.ffcctrns_transaction.get({ input_cdttrftxinfpmtidtxid: transactionId });
    if (!transaction || transaction.status !== "Pending")
      throw `transaction ${transactionId} cannot be processed because status is ${transaction ? transaction.status : "undefined"}`;
    let message = new FFCCTRNS({ 
      sender: "TBFULT21XXX",    // sender: transaction.cdttrftxinfdbtragtfininstnidbic, 
      receiver: "LIABLT2XMSD",  // receiver: transaction.cdttrftxinfcdtragtfininstnidbic, 
      msgId: uuidv1(), 
      system 
    });
  
    message.addTransaction({
      paymentInstructionId: transaction.cdttrftxinfpmtidinstrid,
      paymentEndToEndId: transaction.cdttrftxinfpmtidendtoendid,
      paymentTransactionId: transaction.cdttrftxinfpmtidtxid,
      serviceLevel: transaction.cdttrftxinfpmtidpmttpinfsvclvlcd,
      amount: transaction.cdttrftxinfintrbksttlmamt,
      chargeBearer: transaction.cdttrftxinfchrgbr,
      debtorName: transaction.cdttrftxinfdbtrnm,
      debtorAccount: transaction.cdttrftxinfdbtracctidiban,
      debtorBic: transaction.cdttrftxinfdbtragtfininstnidbic,
      creditorName: transaction.cdttrftxinfcdtrnm,
      creditorAccount: transaction.cdttrftxinfcdtracctidiban,
      creditorBic: transaction.cdttrftxinfcdtragtfininstnidbic,
      remittanceInformation: transaction.cdttrftxinfrmtinfstrdcdtrrefinfref
    });
    
    message._calculateGroupHeader({});
    const { messageHeader, groupHeader } = message;
    const [ upsertedMessage ] = await db.ffcctrns_transaction.upsert({
      input_guid: guid,
      input_cdttrftxinfpmtidtxid: transaction.cdttrftxinfpmtidtxid,
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: messageHeader.system,
      input_docid: messageHeader.msgId,
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      input_status: "Prepared",
      input_grphdrmsgid: groupHeader.MsgId,
      input_grphdrcredttm: groupHeader.CreDtTm,
      input_grphdrnboftxs: groupHeader.NbOfTxs,
      input_grphdrttlintrbksttlmamt: groupHeader.TtlIntrBkSttlmAmt,
      input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
      input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
      input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
      input_data: message.toXml(),
      input_author_guid: author_guid,
    });
    res.status(200).json(upsertedMessage);
  }
  catch (err) {
    next(err);
  }
};