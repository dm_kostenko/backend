const uuidv1 = require("uuid/v1");
const Joi = require("@hapi/joi");
const db = require("../../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../../errorTypes");
const { FFPCRQST } = require("../../../../modules/messages");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });

  // if (body.guid)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid(),
  //     username: Joi.string().alphanum().min(3).max(20),
  //     type: Joi.equal("admin", "user"),
  //     email: Joi.string().email(),
  //     auth_type: Joi.string().equal("login-password","login-password-mail"),
  //     phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
  //   });
  
  return Joi.object().keys({
    // type: 
    transactionId: Joi.string().guid(),
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    // let dataOnLogins = await db.login.get({
    //   input_guid: req.query.guid,
    //   input_username: req.query.username,
    //   input_email: req.query.email,
    //   input_enabled: req.query.enabled,
    //   input_page_number: req.query.page,
    //   input_items_count: req.query.items
    // });
    const { 
      sender,
      receiver,
      page,
      items } = req.query;
    
    const documents = await db.ffpcrqst_document.get({ 
      input_sender: sender, 
      input_receiver: receiver,
      input_page_number: page,
      input_items_count: items 
    });

    const count = documents[0] ? documents[0].count : 0;
    res.status(200).json({ count, data: documents });

  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    const [ document ] = await db.ffpcrqst_document.get({ input_assgnmtid: guid });
    
    res.status(200).json(document);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const {
    // guid, 
    transactionId,
    system = "LITAS-RLS" //LITAS-RLS or LITAS-MIG 
  } = req.body;

  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  try {
    const [ transaction ]  = await db.ffpcrqst_transaction.get({ input_undrlygtxinfcxlid: transactionId });
    if (!transaction || transaction.status !== "Pending")
      throw { message: `transaction ${transactionId} cannot be processed because status is ${transaction ? transaction.status : "undefined"}` };
    
    const message = new FFPCRQST({ 
      sender: "TBFULT21XXX",      //sender: transaction.undrlygtxinforgnltxrefdbtragtfininstnidbic,
      receiver: "LIABLT2XMSD",    //receiver: transaction.undrlygtxinforgnltxrefcdtragtfininstnidbic,
      msgId: uuidv1(), system });
    
    message.addTransaction({
      CancellationTrId: transactionId,
      OrgnlMsgId: transaction.undrlygtxinforgnlgrpinforgnlmsgid,
      OrgnlMsgNmId: transaction.undrlygtxinforgnlgrpinforgnlmsgnmid,
      OrgnlEndToEndId: transaction.undrlygtxinforgnlgrpinforgnlmsgendtoendid,
      OrgnlTxId: transaction.undrlygtxinforgnlgrpinforgnltxid,
      OrgnlIntrBkSttlmAmt: transaction.undrlygtxinforgnlintrbksttlmamt,
      OrgnlIntrBkSttlmDt: transaction.undrlygtxinforgnlintrbksttlmdt,
      CxlRsnInf: transaction.undrlygtxinfcxlrsninfrsncd || transaction.undrlygtxinfcxlrsninfrsnprty, //Reason - DUPL, TECH or FRAD
      CxlRsnInfAddtInf: transaction.undrlygtxinfcxlrsninfrsnaddtinf, //If reason FRAD
      SttlmMtd: transaction.undrlygtxinforgnltxrefsttlminfsttlmmtd,
      ClrSysPrtry: transaction.undrlygtxinforgnltxrefsttlminfclrsysprtry, // LITAS-RLS or LITAS-MIG
      serviceLevel: transaction.undrlygtxinforgnltxrefpmttpinfsvclvlcd || "SEPA",
      debtorName: transaction.undrlygtxinforgnltxrefdbtrnm,
      debtorAccount: transaction.undrlygtxinforgnltxrefdbtracctidiban,
      debtorBic: transaction.undrlygtxinforgnltxrefdbtragtfininstnidbic,
      creditorName: transaction.undrlygtxinforgnltxrefcdtrnm,
      creditorAccount: transaction.undrlygtxinforgnltxrefcdtracctidiban, 
      creditorBic: transaction.undrlygtxinforgnltxrefcdtragtfininstnidbic,
      originatorClient: transaction.undrlygtxinfcxlrsninforgtrnm,
      originatorBank: transaction.undrlygtxinfcxlrsninforgtridorgidbicorbei
    });
    
    message._calculateAssgmntHeader({});
    const { messageHeader, assgmntHeader } = message;

    const [ upsertedMessage ] = await db.ffpcrqst_document.upsert({
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: system,
      input_docid: messageHeader.msgId,
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      input_status: "Prepared",
      // input_reportmsgid,
      // input_reportdttm,
      // input_grsts,
      // input_rsnprty,
      input_assgnmtid: assgmntHeader.Id,
      input_assgnmtcredttm: assgmntHeader.CreDtTm,
      input_ctrldatanboftxs: assgmntHeader.NbOfTxs,
      input_data: message.toXml(),
      input_author_guid: author_guid,
    });

    await db.ffpcrqst_transaction.upsert({
      input_undrlygtxinfcxlid: transactionId,
      input_assgnmtid: messageHeader.msgId,
      input_status: "Approved",
      input_author_guid: author_guid,
    });
    res.status(200).json(upsertedMessage);
  }
  catch (err) {
    next(err);
  }
};

module.exports.xml = require("./ffpcrqst/xml");