const uuidv1 = require("uuid/v1");
const Joi = require("@hapi/joi");
const db = require("../../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../../errorTypes");
const { PRTRN } = require("../../../../modules/messages");

module.exports.schema = (body) => {
  // if (body.delete)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid().required()
  //   });

  // if (body.guid)
  //   return Joi.object().keys({
  //     guid: Joi.string().guid(),
  //     username: Joi.string().alphanum().min(3).max(20),
  //     type: Joi.equal("admin", "user"),
  //     email: Joi.string().email(),
  //     auth_type: Joi.string().equal("login-password","login-password-mail"),
  //     phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
  //   });
  
  return Joi.object().keys({
    // type: 
    transactionId: Joi.string().guid(),
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    // let dataOnLogins = await db.login.get({
    //   input_guid: req.query.guid,
    //   input_username: req.query.username,
    //   input_email: req.query.email,
    //   input_enabled: req.query.enabled,
    //   input_page_number: req.query.page,
    //   input_items_count: req.query.items
    // });
    const { 
      sender,
      receiver,
      page,
      items } = req.query;
    
    const documents = await db.prtrn_document.get({ 
      input_sender: sender, 
      input_receiver: receiver,
      input_page_number: page,
      input_items_count: items 
    });

    const count = documents[0] ? documents[0].count : 0;
    res.status(200).json({ count, data: documents });

  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid } = req.params;
  try {
    const [ document ] = await db.prtrn_document.get({ input_assgnmtid: guid });
    
    res.status(200).json(document);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const {
    // guid, 
    transactionId,
    system = "LITAS-RLS" //LITAS-RLS or LITAS-MIG 
  } = req.body;

  const author_guid = req.auth ? req.auth.loginGuid : "c77a7b58-ad1f-11e9-a2a3-2a2ae2dbcce4";

  try {
    const [ transaction ]  = await db.prtrn_transaction.get({ input_undrlygtxinfcxlid: transactionId });
    if (!transaction || transaction.status !== "Pending")
      throw { message: `transaction ${transactionId} cannot be processed because status is ${transaction ? transaction.status : "undefined"}` };
    
    const message = new PRTRN({ 
      sender: "TBFULT21XXX",    /*transaction.undrlygtxinforgnltxrefdbtragtfininstnidbic*/
      receiver: "LIABLT2XMSD",  /*transaction.undrlygtxinforgnltxrefcdtragtfininstnidbic*/
      msgId: uuidv1(), 
      system,
    });
    
    message.addTransaction({
      returnmentId: transactionId,
      originalMsgId: transaction.txinforgnlgrpinforgnlmsgid,
      originalMsgNmId: "pacs.004.001.02", //txinforgnlgrpinforgnlmsgnmid
      originalInstrId: transaction.txinforgnlinstrid,
      originalEndToEndId: transaction.txinforgnlendtoendid,
      originalTxId: transaction.txinforgnltxid,
      originalIntrBkSttlmAmt: transaction.txinforgnlintrbksttlmamt,
      rtrdIntrBkSttlmAmt: transaction.txinfrtrdintrbksttlmamt,
      rtrdInstdAmt: transaction.txinfrtrdinstdamt,
      chargeBearer: "SLEV", //txinfchrgbr
      сhrgsInfAmt: transaction.txinfchrgsinfamt,
      originatorClient: transaction.txinfrtrrsninforgtrnm,
      originatorBank: transaction.txinfrtrrsninforgtridorgidbicorbei,
      reasonForReturn: transaction.txinfrtrrsninfrsncd,  // allowed values: AC01, AC04, AC06, AG01, AG02, AM05, BE04, FOCR, MD07, MS02, MS03, RC01, RR01, RR02, RR03, RR04.
      rtrRsnInfAddtInf: transaction.txinfrtrrsninfaddtinf, //if reason: FOCR
      originalSettlementDate: transaction.txinforgnltxrefintrbksttlmdt,
      originalSettlementMethod: "CLRG", //txinforgnltxrefsttlminfsttlmmtd
      originalServiceLevel: "SEPA", //txinforgnltxrefpmttpinfsvclvlcd
      originalDebtorName: transaction.txinforgnltxrefdbtrnm,
      originalDebtorAccount: transaction.txinforgnltxrefdbtracctidiban,
      originalDebtorBic: transaction.txinforgnltxrefdbtragtfininstnidbic,
      originalCreditorName: transaction.txinforgnltxrefcdtrnm,
      originalCreditorAccount: transaction.txinforgnltxrefcdtragtfininstnidbic,
      originalCreditorBic: transaction.txinforgnltxrefcdtracctidiban,
    });
    
    message._calculateGroupHeader({});
    const { messageHeader, groupHeader } = message;

    const [ upsertedMessage ] = await db.prtrn_document.upsert({
      input_version: messageHeader.version,
      input_sender: messageHeader.sender,
      input_receiver: messageHeader.receiver,
      input_priority: messageHeader.priority,
      input_msgid: messageHeader.msgId,
      input_system: system,
      input_docid: messageHeader.msgId,
      input_datestype: messageHeader.date,
      input_businessarea: messageHeader.businessArea,
      input_status: "Prepared",
      // input_reportmsgid,
      // input_reportdttm,
      // input_grsts,
      // input_rsnprty,
      input_grphdrmsgid: groupHeader.MsgId,
      input_grphdrcredttm: groupHeader.CreDtTm,
      input_grphdrnboftxs: groupHeader.NbOfTxs,
      input_grphdrttlintrbksttlmamt: groupHeader.TtlRtrdIntrBkSttlmAmt,
      input_grphdrintrbksttlmdt: groupHeader.IntrBkSttlmDt,
      input_grphdrsttlminfsttlmmtd: groupHeader.SttlmMtd,
      input_grphdrsttlminfclrsysprtry: groupHeader.ClrSysPrtry,
      input_data: message.toXml(),
      input_author_guid: author_guid,
    });

    await db.prtrn_transaction.upsert({
      input_txinfrtrid: transactionId,
      input_grphdrmsgid: messageHeader.msgId,
      input_status: "Approved",
      input_author_guid: author_guid,
    });
    res.status(200).json(upsertedMessage);
  }
  catch (err) {
    next(err);
  }
};

module.exports.xml = require("./prtrn/xml");