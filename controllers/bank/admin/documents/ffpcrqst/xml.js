const db = require("../../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const { guid } = req.params;

  // const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    const [ message ] = await db.ffcctrns_document.get({ input_msgid: guid }); 
    if (!message)
      throw { message: "Message doesn't exist" };
    
    res.set("Content-Type", "text/xml").status(200).send(message.data);
  }
  catch (err) {
    next(err);
  }
};