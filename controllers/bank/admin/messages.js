const db = require("../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../errorTypes");
const { FFCCTRNS, FFPCRQST, PRTRN, ROINVSTG } = require("../../../modules/messages");
// const Joi = require("@hapi/joi");

// module.exports.schema = (body) => {
//   if (body.delete)
//     return Joi.object().keys({
//       guid: Joi.string().guid().required()
//     });

//   if (body.guid)
//     return Joi.object().keys({
//       guid: Joi.string().guid(),
//       username: Joi.string().alphanum().min(3).max(20),
//       type: Joi.equal("admin", "user"),
//       email: Joi.string().email(),
//       auth_type: Joi.string().equal("login-password","login-password-mail"),
//       phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
//     });

//   return Joi.object().keys({
//     type: 
//     transactions: 
//     sender:
//     receiver:
//   });
// };

module.exports.get = async (req, res, next) => {
  try {
    // let dataOnLogins = await db.login.get({
    //   input_guid: req.query.guid,
    //   input_username: req.query.username,
    //   input_email: req.query.email,
    //   input_enabled: req.query.enabled,
    //   input_page_number: req.query.page,
    //   input_items_count: req.query.items
    // });

    // const numberOfLogins = dataOnLogins[0] ? dataOnLogins[0].count : 0;

    // res.status(200).json({ count: numberOfLogins, data: dataOnLogins });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    // l 
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid, type, transactions: transactionsGuids, sender, receiver } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";

  try {
    let transactionsQuery, transactionsMessagesQuery;
    switch (type) {
    case "FFCCTRNS":
      transactionsQuery = db.ffcctrns_transaction;
      transactionsMessagesQuery = db.ffcctrns_document;
      break;
    case "FFPCRQST":
      transactionsQuery = db.ffpcrqst_transaction;
      transactionsMessagesQuery = db.ffpcrqst_document;
      break;
    case "PRTRN":
      transactionsQuery = db.prtrn_transaction;
      transactionsMessagesQuery = db.prtrn_document;
      break;
    case "ROINVSTG":
      transactionsQuery = db.roinvstg_transaction;
      transactionsMessagesQuery = db.roinvstg_document;
      break;
    default:
      throw `Unsupported transaction type ${type}`;
    }
    let transactions;
    if (transactionsGuids)
      transactions = await Promise.all(transactionsGuids.map(async guid => {
        const [ transaction ] = await transactionsQuery.get({ 
          input_cdttrftxinfpmtidinstrid: guid, 
          input_cdttrftxinfdbtragtfininstnidbic: sender, 
          input_cdttrftxinfcdtragtfininstnidbic: receiver
        });
        if (!transaction || transaction.status !== "rdy")
          throw `transaction ${guid} cannot be processed because status is ${transaction ? transaction.status : "undefined"}`;
        return transaction;
      }));
    else
      transactions = await transactionsQuery.get({ input_status: "rdy", input_cdttrftxinfdbtragtfininstnidbic: sender, input_cdttrftxinfcdtragtfininstnidbic: receiver});
    
    if (transactions.lentgh < 1)
      throw "There is no transactions to be processed";
    
    let message;
    switch (type) {
    case "FFCCTRNS":
      // message = new FFCCTRNS({ sender:   });
      break;
    case "FFPCRQST":
      
      break;
    case "PRTRN":
      
      break;
    case "ROINVSTG":
      
      break;
    default:
      throw `Unsupported transaction type ${type}`;
    }
    
  }
  catch (err) {
    next(err);
  }
};