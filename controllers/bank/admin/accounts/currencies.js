const db = require("../../../../helpers/db/bankApi");
const { InvalidDataError, InternalError } = require("../../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.object().keys({
    currency: Joi.string().length(3).required(),
    monthly_limit: Joi.number().positive(),
    daily_limit: Joi.number().positive()
  });
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { currency, monthly_limit, daily_limit } = req.body;
  const author_guid = req.auth.loginGuid;
  try {
    const [ existingAccount ] = await db.currency_balance.get({
      input_account_number: guid,
      input_currency_code: currency,
    });
    if (existingAccount)
      throw { message: "Subaccount already exist" };

    const [ updatedLimits ] = await db.currency_balance.upsert({
      input_account_number: guid,
      input_balance: 0,
      input_reserved: 0,
      input_currency_code: currency,
      input_monthly_limit: monthly_limit * 100,
      input_daily_limit: daily_limit * 100,
      input_author_guid: author_guid
    });

    res.status(200).json(updatedLimits);
  }
  catch (err) {
    next(err);
  }
};