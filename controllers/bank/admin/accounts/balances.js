const db = require("../../../../helpers/db/bankApi");
const { XmlMessageParser } = require("../../../../modules/messages")
const { iban } = require("../../../../config")

module.exports.get = async (req, res, next) => {
  const { number } = req.params;
  const { page, items, firstTwo } = req.query;

  try {
    let data = []
    if(firstTwo) {
      const [ customerBalance ] = await db.balance.get({
        input_account: iban.customer.external,
        input_type: "CBALPAT", // or "CBALPRI"
        input_page_number: 1,
        input_items_count: 1
      })
      const [ settlementBalance ] = await db.balance.get({
        input_account: iban.settlement.external,
        input_type: "CBALPAT", // or "CBALPRI"
        input_page_number: 1,
        input_items_count: 1
      })

      data = [
        settlementBalance,
        customerBalance
      ]
    }
    else {
      data = await db.balance.get({
        input_account: number,
        input_type: "CBALPAT", // or "CBALPRI"
        input_page_number: page,
        input_items_count: items
      })

      data = data.map(i => ({
        data: i.data,
        date: i.date,
        openingBalance: i.openingbalance / 100,
        closingBalance: i.closingbalance / 100
      }))

      // const docs = await db.inbox.get({
      //   input_type: 'BALANCE'
      // });

      // for(let i = 0; i < docs.length; i++) {
      //   const message = new XmlMessageParser(unescape(docs[i].data));
      //   const { date, iban, f, openingBalance, closingBalance } = await message.getBalanceData(number)

      //   if(iban === number && f)
      //     data.push({
      //       data: docs[i].data,
      //       date,
      //       openingBalance,
      //       closingBalance
      //     })
      // }

      let obj = {};

      data.forEach((item, index) => {
        if(!obj[item.date])
          obj[item.date] = index
      })

      const indexes = Object.keys(obj).map(i => obj[i])

      data = data.filter((item, index) => indexes.includes(index))/* .sort((a,b) => {
        return new Date(b.date) - new Date(a.date);
      }) */
    }

    res.status(200).json({ count: data.length, data });
  }
  catch (err) {
    next(err);
  }
};