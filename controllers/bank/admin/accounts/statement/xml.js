const db = require("../../../../../helpers/db/bankApi");
const { BTCSTMT } = require("../../../../../modules/messages");
const uuidv1 = require("uuid/v1");
const { iban } = require("../../../../../config");

module.exports.get = async (req, res, next) => {
  const { number } = req.params;
  const { lastDay, lastWeek, lastMonth, lastYear, fromDate, toDate } = req.query;

  try {

    let from, to;
    if(lastDay) {
      //from = new Date();
      //from.setDate(from.getDate() - 1)
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - 1).toISOString();
      to = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()).toISOString();
    }
    else if(lastWeek) {
      //from = new Date();
      //from.setDate(from.getDate() - 7);
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() - 6).toISOString();
      to = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() + 1).toISOString();
    }
    else if(lastMonth) {
      //from = new Date();
      //from.setMonth(from.getMonth() - 1);
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 1).toISOString();
      to = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0).toISOString();
    }
    else if(lastYear) {
      //from = new Date();
      //const year = from.getFullYear();
      //const month = from.getMonth();
      //const day = from.getDate();
      //from = new Date(year - 1, month, day)
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear() - 1, 0, 1).toISOString();
      to = new Date(currentDate.getFullYear(), 0, 0).toISOString();
    }
    else if(fromDate && toDate) {
      from = fromDate;
      to = toDate;
    }

    const balances = await db.balance.get({
      input_account: number,
      input_from: from,
      input_to: to
    });
    
    let _balances = {};

    if(balances.length > 0) {      
      _balances = {
        accountIBAN: balances[0].accountiban,
        currency: balances[0].currency,
        openingBalance: balances[balances.length - 1].openingbalance / 100,
        openingAvailableBalance: balances[balances.length - 1].openingavailablebalance / 100,
        closingBalance: balances[0].closingbalance / 100,
        closingAvailableBalance: balances[0].closingavailablebalance / 100,
        // startDate: balances[0].date,
        startDate: from ? from : balances[0].date,
        // endDate: balances[balances.length - 1].date        
        endDate: to ? to : new Date().toISOString()
      }
    }

    let statements = await db.btcdcntf.get({
      input_accountiban: number,
      input_from: from,
      input_to: to
    })

    if(number === iban.settlement.external) {
      let debitRecords = await db.debit.get({
        input_from: from,
        input_to: to
      })

      debitRecords.forEach(item => {
        item.debit = true;
      })

      statements = [ ...statements, ...debitRecords ];
      statements = statements.sort((a, b) => new Date(a.date) - new Date(b.date))
    }

    statements = statements.filter(item => item.amount && item.amount != "0").map(item => {
      item.amount /= 100;
      return item;
    })

    // console.log(statements)

    const date = new Date().toISOString().slice(0,19);

    const credits = statements.filter(i => i.creditdebitindex === "CRDT")
    const debits = statements.filter(i => i.creditdebitindex !== "CRDT")

    const document = new BTCSTMT({
      MsgId: uuidv1(),
      CreDtTm: date,
      StmtId: uuidv1(),
      StmtCreDtTm: date,
      StmtFrDtTm: from || statements[0].date.toISOString().slice(0,10),
      StmtToDtTm: to || date.slice(0,10),
      StmtAcctIdIban: number,
      // StmtAcctCcy: "",
      StmtAcctOwnrNm: "TBF Finance, UAB",
      StmtAcctOwnrIdOrgIdOthrId: "31800",
      StmtAcctOwnrIdOrgIdOthrSchmeNmCd: "COID",
      StmtAcctSvcrFinInstnIdBic: "LIABLT2XXXX",
      StmtAcctSvcrFinInstnIdNm: "Lietuvos bankas",
      StmtAcctSvcrFinInstnIdPstlAdrStrtNm: "Gedmino 6",
      StmtAcctSvcrFinInstnIdPstlAdrPstCd: "01103",
      StmtAcctSvcrFinInstnIdPstlAdrTwnNm: "Vilnius",
      StmtAcctSvcrFinInstnIdPstlAdrCtry: "LT",
      StmtAcctSvcrFinInstnIdOthrId: "10100",
      StmtAcctSvcrFinInstnIdOthrSchmeNmCd: "COID",
      StmtOpBalCd: "OPBD",
      StmtOpBalAmt: _balances.openingBalance,
      // StmtOpBalCcy: "",
      StmtOpBalCdtDbtInd: "CRDT",
      StmtOpBalDt: from ? from.slice(0,10) : statements[0].date.toISOString().slice(0,10),
      StmtClBalCd: "CLBD",
      StmtClBalAmt: _balances.closingBalance,
      // StmtClBalCcy: "",
      StmtClBalCdtDbtInd: "CRDT",
      StmtClBalDt: to ? to.slice(0,10) : date.slice(0,10),
      StmtTxsSummryTtlNtriesNbOfNtries: statements.length,
      StmtTxsSummryTtlCdtNtriesNbOfNtries: credits.length,
      StmtTxsSummryTtlCdtNtriesSum: credits.length > 0 ? credits.reduce((sum, item) => { return { amount: sum.amount + item.amount } }).amount.toFixed(2) : 0.00,
      StmtTxsSummryTtlDbtNtriesNbOfNtries: debits.length,
      StmtTxsSummryTtlDbtNtriesSum: debits.length > 0 ? debits.reduce((sum, item) => { return { amount: sum.amount + item.amount } }).amount.toFixed(2) : 0.00,
      StmtNtryArray: statements.map(item => {
        return {
          Amt: item.amount,
          CdtDbtInd: item.creditdebitindex || "DBIT",  // CRDT or DBIT
          Sts: "BOOK",
          BookgDt: item.date.toISOString().slice(0,10),
          ValDt: item.date.toISOString().slice(0,10),
          BkTxCd: item.domaincode || "PMNT",
          BkTxFmlyCd: item.domainfamilycode || "MDOP",
          BkTxSubFmlyCd: item.domainsubfamilycode || "COMI",
          NtryDtlsTxDtlsAcctSvcrRef: item.msgid,
          NtryDtlsTxDtlsTxId: item.msgid,
          NtryDtlsAmtDtlsInstdAmt: item.amount,
          NtryDtlsAmtDtlsTxAmt: item.amount,
          // NtryDtlsAmtDtlsTxCcy: "",
          NtryDtlsRltdPtiesDbtrNm: item.payername,
          NtryDtlsRltdPtiesDbtrAcctIban: item.payeraccount || item.debtoriban,
          NtryDtlsRltdAgtsDbtrAgtFinInstnIdBic: item.payerbic || item.debtorbic,
          NtryDtlsRltdAgtsDbtrAgtFinInstnIdNm: item.payername,
          // NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrStrtNm: "Mėnulio g. 7",
          // NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrPstCd: "04326",
          // NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrTwnNm: "Vilnius",
          // NtryDtlsRltdAgtsDbtrAgtFinInstnIdPstlAdrCtry: "LT",
          // NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrId: "300060819",
          // NtryDtlsRltdAgtsDbtrAgtFinInstnIdOthrSchmeNmCd: "COID",
          NtryDtlsRltdPtiesCdtrNm: item.payeename,
          NtryDtlsRltdPtiesCdtrAcctIban: item.payeeaccount || item.creditoriban,
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdBic: item.payeebic || item.creditorbic,
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdNm: item.payeebic ? "Lietuvos bankas" : "",
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrStrtNm: item.payeebic ? "Gedmino 6" : "",
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrPstCd: item.payeebic ? "01103" : "",
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrTwnNm: item.payeebic ? "Vilnius" : "",
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdPstlAdrCtry:item.payeebic ?  "LT" : "",
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrId: item.payeebic ? "10100" : "",
          NtryDtlsRltdAgtsCdtrAgtFinInstnIdOthrSchmeNmCd: item.payeebic ? "COID" : "",
          NtryDtlsRmtInfUstrd: item.remittanceinformationpaymentreason
        }
      })
    })

    res.status(200).json({ 
      document: escape(document.toXml())
    });
  }
  catch (err) {
    next(err);
  }
};