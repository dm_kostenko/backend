const db = require("../../../../helpers/db/bankApi");
const { iban } = require("../../../../config");

module.exports.get = async (req, res, next) => {
  const { number } = req.params;
  const { lastDay, lastWeek, lastMonth, lastYear, fromDate, toDate } = req.query;

  try {

    let from, to;
    if(lastDay) {
      //from = new Date();
      //from.setDate(from.getDate() - 1)
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - 1).toISOString();
      to = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()).toISOString();
    }
    else if(lastWeek) {
      //from = new Date();
      //from.setDate(from.getDate() - 7);
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() - 6).toISOString();
      to = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() + 1).toISOString();
    }
    else if(lastMonth) {
      //from = new Date();
      //from.setMonth(from.getMonth() - 1);
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 1).toISOString();
      to = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0).toISOString();
    }
    else if(lastYear) {
      //from = new Date();
      //const year = from.getFullYear();
      //const month = from.getMonth();
      //const day = from.getDate();
      //from = new Date(year - 1, month, day)
      //from = from.toISOString();
      //to = new Date().toISOString();
      const currentDate = new Date();
      from = new Date(currentDate.getFullYear() - 1, 0, 1).toISOString();
      to = new Date(currentDate.getFullYear(), 0, 0).toISOString();
    }
    else if(fromDate && toDate) {
      from = fromDate;
      to = toDate;
    }

    const balances = await db.balance.get({
      input_account: number,
      input_from: from,
      input_to: to
    });
    
    let _balances = {};

    if(balances.length > 0) {      
      _balances = {
        accountIBAN: balances[balances.length - 1].accountiban,
        currency: balances[balances.length - 1].currency,
        openingBalance: balances[balances.length - 1].openingbalance / 100,
        openingAvailableBalance: balances[balances.length - 1].openingavailablebalance / 100,
        closingBalance: balances[0].closingbalance / 100,
        closingAvailableBalance: balances[0].closingavailablebalance / 100,
        // startDate: balances[0].date,
        startDate: from ? from : balances[balances.length - 1].date,
        // endDate: balances[balances.length - 1].date        
        endDate: to ? to : new Date().toISOString()
      }
    }
    
    let statements = await db.btcdcntf.get({
      input_accountiban: number,
      input_from: from,
      input_to: to
    })
    let tx;
    for(let i = 0; i < statements.length; i++) {
      tx = await db.ffcctrns_transaction.get({
        input_cdttrftxinfpmtidtxid: statements[i].transactionid
      })
      tx = tx[0]
      if(tx)
        statements[i].remittanceinformationpaymentreason = tx.cdttrftxinfrmtinfstrdcdtrrefinfref
      else {
        tx = await db.ffpcrqst_transaction.get({
          input_msgid: statements[i].msgid
        })
        tx = tx[0]
        if(tx)
          statements[i].remittanceinformationpaymentreason = tx.undrlygtxinfcxlrsninfrsnaddtinf
        else {
          tx = await db.prtrn_transaction.get({
            input_msgid: statements[i].msgid
          })
          tx = tx[0]
          if(tx)
            statements[i].remittanceinformationpaymentreason = tx.txinfrtrrsninfaddtinf
        }
      }
    }

    if(number === iban.settlement.external) {
      let debitRecords = await db.debit.get({
        input_from: from,
        input_to: to
      })

      debitRecords.forEach(item => {
        item.debit = true;
      })

      statements = [ ...statements, ...debitRecords ];
      statements = statements.sort((a, b) => new Date(a.date) - new Date(b.date))
    }

    statements = statements.filter(item => item.amount && item.amount != "0").map(item => {
      item.amount /= 100;
      return item;
    })

    res.status(200).json({ 
      balances: _balances,
      statements
    });
  }
  catch (err) {
    next(err);
  }
};

module.exports.xml = require("./statement/xml")