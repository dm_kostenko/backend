const db = require("../../../../helpers/db/bankApi");
const { XmlMessageParser } = require("../../../../modules/messages")

module.exports.get = async (req, res, next) => {
  const {
    date: _date
  } = req.query;

  try {
    let data = [];
    const docs = await db.inbox.get({});
    for(let i = 0; i < docs.length; i++) {
      const message = new XmlMessageParser(unescape(docs[i].data));
      const { type, date } = await message.getMessageTypeAndDate()
      if(date.slice(0,10) === _date && [ "DEBIT", "FINSTA" ].includes(type))
        data.unshift({
          data: docs[i].data,
          type,
          date
        })
    }

    res.status(200).json({ count: data.length, data })
  }
  catch(err) {
    next(err)
  }
}