const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const { guid } = req.params;
  const { rule_type, type } = req.query;
  try {
    let ratesRules = await db.rate.getRules({
      input_guid: guid
    });

    // console.log(ratesRules)


    // for(let i = 0; i < ratesRules.length; i++) {
    //   let ratesRulesParams = await db.rate_rule_param.get({
    //     input_rate_rule_guid: ratesRules[i].guid,
    //     // input_name: name,
    //     // input_value: value,
    //     // input_value_type: value_type,
    //     // input_page_number: page,
    //     // input_items_count: items
    //   })
  
    //   if(ratesRulesParams.length < 1)
    //     res.status(200).json({ count: 0, data: [] });
  
    //   ratesRulesParams.forEach(param => {
    //     if([ "number", "percent" ].includes(param.value_type))
    //       param.value /= 100;
    //   })
  
    //   if(ratesRulesParams.length === 3) {
    //     const percentRate = ratesRulesParams.find(obj => obj.name === 'rate' && obj.value_type === 'percent');
    //     const numberRate = ratesRulesParams.find(obj => obj.name === 'rate' && obj.value_type === 'number');
    //     const compareObj = ratesRulesParams.find(obj => obj.name !== 'rate');
    //     ratesRulesParams = [
    //       {
    //         rate_rule_guid: percentRate.rate_rule_guid,
    //         percent_rate: percentRate.value,
    //         number_rate: numberRate.value,
    //         compare_action: compareObj.name,
    //         compare_value: compareObj.value,
    //         compare_value_type: compareObj.value_type
    //       }
    //     ]
    //   }
      
    //   ratesRules[i].params = ratesRulesParams;
    // }    

    if(ratesRules.length < 1)
      res.status(200).json({ count: 0, data: [] });

    res.status(200).json({ count: ratesRules.length || 0, data: ratesRules });
  }
  catch(err) {
    next(err);
  }
}

module.exports.params = require("./rules/params");