const db = require("../../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  const { guid: rateGuid } = req.params;
  const { rule_type, type } = req.query;
  try {
    let ratesRulesParams = await db.rate_rule_param.getByRate({
      input_rate_guid: rateGuid,
      input_rule_type: rule_type,
      input_type: type
    })

    if(ratesRulesParams.length < 1)
      return res.status(200).json({ count: 0, data: [] });

    ratesRulesParams.forEach(param => {
      if([ "number", "percent" ].includes(param.value_type))
        param.value /= 100;
    })

    let params = [];

    const guids = ratesRulesParams.filter(item => item.name !== 'rate').map(item => item.guid)
    guids.forEach(guid => {
      const ruleParams = ratesRulesParams.filter(item => item.guid === guid);
      const percentRate = ruleParams.find(obj => obj.name === 'rate' && obj.value_type === 'percent') || {};
      const numberRate = ruleParams.find(obj => obj.name === 'rate' && obj.value_type === 'number') || {};
      const compareObj = ruleParams.find(obj => obj.name !== 'rate') || {};
      params.push({
        rate_rule_guid: guid,
        percent_rate: percentRate.value,
        number_rate: numberRate.value,
        compare_action: compareObj.name,
        compare_value: compareObj.value,
        compare_value_type: compareObj.value_type
      })      
    })
    // if(ratesRulesParams.length === 3) {
    //   const percentRate = ratesRulesParams.find(obj => obj.name === 'rate' && obj.value_type === 'percent');
    //   const numberRate = ratesRulesParams.find(obj => obj.name === 'rate' && obj.value_type === 'number');
    //   const compareObj = ratesRulesParams.find(obj => obj.name !== 'rate');
    //   ratesRulesParams = [
    //     {
    //       rate_rule_guid: percentRate.rate_rule_guid,
    //       percent_rate: percentRate.value,
    //       number_rate: numberRate.value,
    //       compare_action: compareObj.name,
    //       compare_value: compareObj.value,
    //       compare_value_type: compareObj.value_type
    //     }
    //   ]
    // }

    res.status(200).json({ count: params.length || 0, data: params });
  }
  catch(err) {
    next(err);
  }
}