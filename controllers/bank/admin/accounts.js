const db = require("../../../helpers/db/bankApi");
const Joi = require("@hapi/joi");
const IBAN = require("../../../modules/IBAN-gen/index");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      number: Joi.string(),
      balance: Joi.number(),
      currency: Joi.string().required()
    });
  return Joi.object().keys({
    login_guid: Joi.string().guid().required(), 
    number: Joi.number(), 
    iban: Joi.string().length(20), 
    type: Joi.equal([ "card", "settlement" ]).required(),
    balance: Joi.number(),
    currency: Joi.string().required().length(3),
    monthly_limit: Joi.number().positive(), 
    daily_limit: Joi.number().positive()
  });
};

module.exports.get = async (req, res, next) => {
  const {
    login_guid, 
    number,
    iban,
    type,
    currency, 
    page, 
    items,
  } = req.query;

  try {
    let accounts = await db.account.getByLogin({
      input_login_guid: login_guid, 
      input_number: number,
      input_iban: iban,
      input_type: type,
      input_currency_code: currency, 
      input_page_number: page, 
      input_items_count: items,
    });
  
    if (accounts.length < 1)
      return res.status(200).json({ count: 0, data: [] });

    const numbers = [ ...new Set(accounts.map(account => account.number)) ];
    const data = numbers.map(number => {
      const accountCurrencies = accounts.filter(account => account.number === number);
      const item = accounts.find(account => account.number === number);
      return {
        number: item.number,
        login_guid: item.login_guid,
        iban: item.iban,
        type: item.type,
        balances: accountCurrencies.map(accountCurrency => ({
          currency: accountCurrency.currency_code,
          balance: accountCurrency.balance / 100,
          reserved: accountCurrency.reserved / 100,
          monthly_limit: accountCurrency.monthly_limit / 100,
          daily_limit: accountCurrency.daily_limit / 100
        }))
      };
    });


    res.status(200).json({ count: accounts[0].count || 0, data });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    const accountCurrencies = await db.account.getByLogin({ input_number: req.params.guid });
    if (accountCurrencies.length < 1)
      return res.status(200).json({});

    const item = accountCurrencies[0];
    const data = {
      number: item.number,
      login_guid: item.login_guid,
      iban: item.iban,
      type: item.type,
      balances: accountCurrencies.map(accountCurrency => ({
        currency: accountCurrency.currency_code,
        balance: accountCurrency.balance / 100,
        reserved: accountCurrency.reserved / 100,
        monthly_limit: accountCurrency.monthly_limit / 100,
        daily_limit: accountCurrency.daily_limit / 100
      }))
    };
    res.status(200).json(data);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { number, login_guid, currency, monthly_limit, daily_limit, type } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  let connection;
  try { 
    if (req.body.delete) {
      await db.account.delete({ input_guid: number, });
      return res.status(200).json({ message: "Deleted" });
    }
    
    let accountNumber = IBAN.generateAccount(number);
    let responseCheckUniqueAccount = await db.account.get({ input_number: accountNumber });
    while (responseCheckUniqueAccount.length !== 0) {
      accountNumber = IBAN.generateAccount();
      responseCheckUniqueAccount = await db.account.get({ input_number: accountNumber });
    }

    connection = await db.getConnection();
    await db._transaction.begin(connection);

    const [ upsertedAccount ] = await db.account.upsert({
      input_number: accountNumber,
      input_iban: IBAN.generateIBAN(accountNumber),
      input_type: type,
      input_author_guid: author_guid,
    }, connection);

    await db.login_account.upsert({ 
      input_account_number: upsertedAccount.number,
      input_login_guid: login_guid,
      input_author_guid: author_guid
    }, connection);

    await db.currency_balance.upsert({
      input_account_number: accountNumber,
      input_currency_code: currency,
      input_balance: 0,
      input_reserved: 0,
      input_monthly_limit: monthly_limit * 100,
      input_daily_limit: daily_limit * 100,
      input_author_guid: author_guid,
    }, connection);

    await db._transaction.commit(connection);
    res.status(200).json(upsertedAccount);
  }
  catch (err) {
    if (connection)
      await db._transaction.rollback(connection);
    next(err);
  }
  finally {
    if (connection)
      connection.release();
  }
};

module.exports.limits = require("./accounts/limits");
module.exports.currencies = require("./accounts/currencies");
module.exports.statement = require("./accounts/statement");
module.exports.pays = require("./accounts/pays");
module.exports.balances = require("./accounts/balances");