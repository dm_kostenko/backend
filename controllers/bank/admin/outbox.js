const db = require("../../../helpers/db/bankApi");

module.exports.get = async (req, res, next) => {
  try {
    const docs = await db.demon.get_outbox();
    res.status(200).json({ docs });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { docs } = req.body;
  try {
    await Promise.all(docs.map(async(doc) => {
      await db.demon.update_status({
        input_guid: doc.guid,
        input_status: "Sent"
      })
    }))
    res.status(200).json({ status: "OK" })
  }
  catch(err) {
    next(err);
  }
}