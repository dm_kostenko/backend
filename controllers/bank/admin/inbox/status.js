const Joi = require("@hapi/joi");
const { 
  XML_IMPORT_IMPORTED, 
  XML_IMPORT_DECLINED,
} = require("../../../../helpers/constants/statuses").xmlImport;
const { incomingXmlParser } = require("../../../../modules/kafka/services/MessageReceiver/config")


module.exports.schema = (body) => {
  return Joi.object().keys({
    status: Joi.equal([ XML_IMPORT_IMPORTED, XML_IMPORT_DECLINED ]).required(), 
    system: Joi.equal("LITAS-RLS", "LITAS-MIG").required(),
  });
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { status } = req.body;

  await incomingXmlParser(guid, status, { res, next }, null);
}