const logStorage = require("../../../../helpers/logStorageConnect");

module.exports.canGet = async (req) => req.auth.merchant ? true : false;

module.exports.get = async (req, res, next) => {
  try {
    const { request_id, transaction_guid, step_info, shop_guid, from_date, to_date, items, page } = req.query;
    const { merchant_guid } = req.auth.merchant;

    let { count, data } = await logStorage.transactionLog.get({
      request_id,
      merchant_guid,
      transaction_guid,
      step_info,
      shop_guid,
      from_date,
      to_date,
      page,
      items
    });

    data = data.map(obj => {
      return {
        request_id: obj.request_id,
        step_info: obj.step_info,
        transaction_guid: obj.transaction_guid,
        shop_guid: obj.shop_guid,
        shop_name: obj.shop_name,
        message: obj.message,
        createdAt: obj.createdAt
      };
    });

    res.status(200).json({ count, data });
  } catch (err) {
    next(err);
  }
};