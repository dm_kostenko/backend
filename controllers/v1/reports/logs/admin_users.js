const logStorage = require("../../../../helpers/logStorageConnect");

module.exports.canGet = async () => false;

module.exports.get = async (req, res, next) => {
  try {
    const { userType, from_date, to_date, guid, url, ip, resStatusCode, request_id, page, items } = req.query;
    
    let { count, data } = await logStorage.reqResLog.get({ from_date, to_date, userType, guid, url, ip, resStatusCode, request_id, page, items });

    data = data.map(obj => {
      return {
        request_id: obj.request_id,
        url: obj.url,
        ip: obj.ip,
        headers: obj.headers,
        parameters: obj.parameters,
        guid: obj.guid,
        reqBody: obj.reqBody,
        resBody: obj.resBody,
        resStatusCode: obj.resStatusCode,
        createdAt: obj.createdAt
      };
    });

    res.status(200).json({ count, data });
  }
  catch (err) {
    next(err);
  }
};