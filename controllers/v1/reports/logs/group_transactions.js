const logStorage = require("../../../../helpers/logStorageConnect");

module.exports.canGet = async (req) => req.auth.group ? true : false;

module.exports.get = async (req, res, next) => {
  try {
    const { request_id, merchant_guid, merchant_name,
      transaction_guid, step_info, shop_guid, from_date, to_date, items, page } = req.query;
    const { group_guid } = req.auth.group;

    let { count, data } = await logStorage.transactionLog.get({
      request_id,
      group_guid,
      merchant_guid,
      merchant_name,
      transaction_guid,
      step_info,
      shop_guid,
      from_date,
      to_date,
      page,
      items
    });

    data = data.map(obj => {
      return {
        request_id: obj.request_id,
        step_info: obj.step_info,
        transaction_guid: obj.transaction_guid,
        merchant_guid: obj.merchant_guid,
        merchant_name: obj.merchant_name, 
        shop_guid: obj.shop_guid,
        shop_name: obj.shop_name,
        message: obj.message,
        createdAt: obj.createdAt
      };
    });

    res.status(200).json({ count, data });
  } catch (err) {
    next(err);
  }
};