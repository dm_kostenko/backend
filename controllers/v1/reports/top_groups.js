const db = require("../../../helpers/db/api/");

module.exports.canGet = async (req) => req.auth.partner ? true : false;

module.exports.get = async (req, res, next) => {
  try {
    const { partner_guid, from_date, to_date, days, count } = req.query;

    const [ report ] = await db.report_top_groups.get({
      input_partner_guid: req.auth.partnerGuid || partner_guid,
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days,
      input_count: count
    });

    res.status(200).json(report);
  } catch (err) {
    next(err);
  }
};