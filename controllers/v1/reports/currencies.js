const db = require("../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    const { partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days } = req.query;

    const [ report ] = await db.report_currencies_of_transactions.get({
      input_partner_guid: req.auth.partnerGuid || partner_guid,
      input_group_guid: req.auth.groupGuid || group_guid,
      input_merchant_guid: req.auth.merchantGuid || merchant_guid,
      input_shop_guid: shop_guid,
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days,
    });


    let result = [];
    report.forEach(({ currency, success }) => {
      const index = result.findIndex(obj => obj.currency === currency);

      if(index >=0 ) result[index].count += success;
      else result.push({ currency, count: success });
    });
    
    res.status(200).json(result);
  } catch (err) {
    next(err);
  }
};