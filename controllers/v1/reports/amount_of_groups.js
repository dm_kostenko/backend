const db = require("../../../helpers/db/api/");

module.exports.canGet = async (req) => req.auth.partner ? true : false;

module.exports.get = async (req, res, next) => {
  try {
    const { partner_guid } = req.query;

    const [ report ] = await db.report_amount_of_groups.get({ input_partner_guid:  req.auth.partnerGuid || partner_guid });

    res.status(200).json(report);
  } catch (err) {
    next(err);
  }
};