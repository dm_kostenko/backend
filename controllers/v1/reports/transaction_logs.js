const db = require("../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    const { guid, status, type, partner_guid, partner_name, group_guid, group_name, merchant_guid, merchant_name, shop_guid, shop_name, account_guid, account_number, from_date, to_date, days, page, items } = req.query;

    let [ report ] = await db.report_transaction_logs.get({
      input_guid: guid,
      input_status: status,
      input_type: type,
      input_partner_guid: req.auth.partnerGuid || partner_guid,
      input_partner_name: partner_name,
      input_group_guid: req.auth.groupGuid || group_guid,
      input_group_name: group_name,
      input_merchant_guid: req.auth.merchantGuid || merchant_guid,
      input_merchant_name: merchant_name,
      input_shop_guid: shop_guid,
      input_shop_name: shop_name,
      input_account_guid: account_guid,
      input_account_number: account_number,
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days,
      input_page_number: page,
      input_items_count: items
    });
    const numberOfRows = report[0] ? report[0].count : 0;

    report.forEach(row => {
      delete row["count"];
    });

    res.status(200).json({ count: numberOfRows, data: report });
  } catch (err) {
    next(err);
  }
};