const db = require("../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    const { partner_guid, group_guid, merchant_guid, shop_guid } = req.query;

    const [ report ] = await db.report_daily_transactions.get({
      input_partner_guid: req.auth.partnerGuid || partner_guid,
      input_group_guid: req.auth.groupGuid || group_guid,
      input_merchant_guid: req.auth.merchantGuid || merchant_guid,
      input_shop_guid: shop_guid
    });

    res.status(200).json(report);
  } catch (err) {
    next(err);
  }
};