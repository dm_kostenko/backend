const db = require("../../../helpers/db/api/");

module.exports.canGet = async () => false;

module.exports.get = async (req, res, next) => {
  try {
    const { from_date, to_date, days } = req.query;

    const [ report ] = await db.report_new_clients.get({
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days
    });

    res.status(200).json(report);
  } catch (err) {
    next(err);
  }
};