const db = require("../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    const { partner_guid, group_guid, merchant_guid, shop_guid, from_date, to_date, days, page, items } = req.query;

    let [ report ] = await db.report_totals.get({
      input_partner_guid: req.auth.partnerGuid || partner_guid,
      input_group_guid: req.auth.groupGuid || group_guid,
      input_merchant_guid: req.auth.merchantGuid || merchant_guid,
      input_shop_guid: shop_guid,
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days,
      input_page_number: page,
      input_items_count: items
    });
    const numberOfRows = report[0] ? report[0].count : 0;

    report.forEach(row => {
      delete row["count"];
    });
    res.status(200).json({ count: numberOfRows, data: report });
  } catch (err) {
    next(err);
  }
};