const db = require("../../../helpers/db/api/");

module.exports.canGet = async () => false;

module.exports.get = async (req, res, next) => {
  try {
    const { status, from_date, to_date, days, page, items } = req.query;

    const [ report ] = await db.report_orders.get({
      input_status: status,
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days,
      input_page_number: page,
      input_items_count: items
    });
    const numberOfRows = report[0] ? report[0].count : 0;

    report.forEach(row => {
      delete row["count"];
    });

    res.status(200).json({ count: numberOfRows, data: report });
  } catch (err) {
    next(err);
  }
};