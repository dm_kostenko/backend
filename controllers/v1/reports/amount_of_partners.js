const db = require("../../../helpers/db/api/");

module.exports.canGet = async () => false;

module.exports.get = async (req, res, next) => {
  try {
    const [ report ] = await db.report_amount_of_partners.get();

    res.status(200).json(report);
  } catch (err) {
    next(err);
  }
};