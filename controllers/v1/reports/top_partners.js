const db = require("../../../helpers/db/api/");

module.exports.canGet = async () => false;

module.exports.get = async (req, res, next) => {
  try {
    const { from_date, to_date, days, count } = req.query;

    const [ report ] = await db.report_top_partners.get({
      input_from_date: from_date,
      input_to_date: to_date,
      input_days: days,
      input_count: count
    });

    res.status(200).json(report);
  } catch (err) {
    next(err);
  }
};