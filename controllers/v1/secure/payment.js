const db = require("../../../helpers/db/api/");
const fs = require("fs");
const Joi = require("@hapi/joi");
const path = require("path");
const { InvalidDataError } = require("../../../errorTypes");
const { fillHTMLTemplate } = require("../../../helpers/tools");

module.exports.schema = () => Joi.object().keys({
  number: Joi.string().creditCard().required(),
  verification_value: Joi.string().regex(/^[0-9]{3}$/).required(),
  holder: Joi.string().required(),
  exp_month: Joi.string().regex(/^[0-9]{2}$/).required(),
  exp_year: Joi.string().regex(/^[0-9]{4}$/).required()
});

module.exports.get = async (req, res, next) => {
  try {
    const { auth_key, success, fail } = req.query;
    if (!auth_key || (success && fail))
      throw new InvalidDataError("Not found");
    
    const [[ paymentSession ]] = await db.securePayment.get({ input_auth_key: auth_key });
    if (!paymentSession)
      throw new InvalidDataError("Not found");

    const paymentData = { ...JSON.parse(paymentSession.data), auth_key };
    if (success)
      paymentData.success = true;
    if (fail)
      paymentData.fail = true;

    const template = fs.readFileSync(path.normalize(__dirname + "/../../../templates/Payment.html"), "utf8");
    const page = fillHTMLTemplate(template, { data: paymentData });
    
    res.removeHeader("X-Frame-Options");
    res.send(page);
  } catch (err) {
    next(err);    
  }
};

module.exports.post = async (req, res, next) => {
  try {
    const { body, query } = req;
    const { auth_key } = query;

    if (!auth_key)
      throw new InvalidDataError("Auth_key required");

    const [[ paymentSession ]] = await db.securePayment.get({ input_auth_key: auth_key });
    if (!paymentSession)
      throw new InvalidDataError("Not found");

    req.url = `${req.baseUrl.split("/").slice(0,-1).join("/")}/transactions/create`;
    req.body = {
      ...JSON.parse(paymentSession.data), 
      credit_card:{ ...body },
    };
    req.auth = { shopGuid: paymentSession.shop_guid, auth_key };
    req.paymentForm = true;

    req.app._router.handle(req,res,next);
  } catch (err) {
    next(err);
  }
};