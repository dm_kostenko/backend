const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.array().items(Joi.object().keys({
    guid: Joi.string().guid().required()
  }));
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataOnPivileges ] = await db.privilege.byRoleInfo({
      input_role_guid: req.params.guid,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfPivileges = dataOnPivileges[0] ? dataOnPivileges[0].count : 0;

    dataOnPivileges = dataOnPivileges.map(pivilege => {
      return {
        guid: pivilege.guid,
        name: pivilege.name,
        type: pivilege.type,
        description: pivilege.description,
        created_at: pivilege.created_at,
        created_by: pivilege.created_by,
        updated_at: pivilege.updated_at,
        updated_by: pivilege.updated_by
      };
    });

    res.status(200).json({ count: numberOfPivileges, data: dataOnPivileges });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    const resData = await Promise.all(req.body.map(async rolePrivileges => {
      const privilege_guid = rolePrivileges.guid;

      if (rolePrivileges.delete){
        await db.rolePrivilege.delete({
          input_role_guid: guid,
          input_privilege_guid: privilege_guid,
        });

        return {
          privilege_guid: privilege_guid,
          role_guid: guid,
          message: "Deleted"
        };
      }

      const [[ resRecord ]] = await db.rolePrivilege.upsert({
        input_role_guid: guid,
        input_privilege_guid: privilege_guid,
        input_author_guid: author_guid
      });

      return resRecord;
    }));
    
    res.status(200).json(resData);
  } catch (err) {
    next(err);
  }
};
