const db = require("../../../helpers/db/api/");
const { InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      type: Joi.string().not().empty(),
      description: Joi.string().not().empty()
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    type: Joi.string().not().empty().required(),
    description: Joi.string().not().empty().required()
  });
};

module.exports.canGet = req => {
  if (req.auth.partner) {
    req.query.type = "Group";
    return true;
  }
  if (req.auth.group) {
    req.query.type = "Merchant";
    return true;
  }
  return false;
}

module.exports.canPost = () => false;

module.exports.canGetByGuid = req => req.auth.allRoles.includes(req.params.guid);

module.exports.get = async (req, res, next) => {
  try {
    let [ dataRoles ]  = await db.role.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_type: req.query.type,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfRoles = dataRoles[0] ? dataRoles[0].count : 0;

    dataRoles = dataRoles.map( record => {
      return {
        guid: record.guid,
        name: record.name,
        type: record.type,
        description: record.description,
        created_by: record.created_by,
        updated_by: record.updated_by,
        created_at: record.created_at,
        updated_at: record.updated_at
      };
    });

    res.status(200).json({ count: numberOfRoles, data: dataRoles });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ role ]]  = await db.role.info({ input_guid: req.params.guid });

    if (!role)
      return res.status(200).json({});

    role = {
      guid: role.guid,
      name: role.name,
      type: role.type,
      description: role.description,
      created_at: role.created_at,
      created_by: role.created_by,
      updated_at: role.updated_at,
      updated_by: role.updated_by,
    };

    res.status(200).json(role);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.role.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ role ]] = await db.role.upsert({
      input_guid: req.body.guid,
      input_name: req.body.name,
      input_type: req.body.type,
      input_description: req.body.description,
      input_author_guid: author_guid
    });

    if (!role) 
      throw new InternalError("No created or updated record in role");

    const resData = {
      guid: role.guid,
      name: role.name,
      type: role.type,
      description: role.description,
      created_at: role.created_at,
      created_by: role.created_by,
      updated_at: role.updated_at,
      updated_by: role.updated_by,
    };

    res.status(200).json(resData);
  }
  catch (err) {
    next(err);
  }
};

module.exports.privileges = require("./roles/privileges");
