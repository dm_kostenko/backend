const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      code: Joi.string().regex(/^[A-Z]{3}$/)
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    code: Joi.string().regex(/^[A-Z]{3}$/).required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataCurrencies ]  = await db.currency.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_code: req.query.code,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfcurrencies = dataCurrencies[0] ? dataCurrencies[0].count : 0;

    dataCurrencies = dataCurrencies.map(currency => {
      return {
        guid: currency.guid,
        name: currency.name,
        code: currency.code,
        created_at: currency.created_at,
        created_by: currency.created_by,
        updated_at: currency.updated_at,
        updated_by: currency.updated_by
      };
    });

    res.status(200).json({ count: numberOfcurrencies, data: dataCurrencies });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ currency ]]  = await db.currency.info({ input_guid: req.params.guid });
    if (!currency){
      return res.status(200).json({});
    }

    currency = {
      guid: currency.guid,
      name: currency.name,
      code: currency.code,
      created_at: currency.created_at,
      created_by: currency.created_by,
      updated_at: currency.updated_at,
      updated_by: currency.updated_by
    };
    
    res.status(200).json(currency);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.currency.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const currencyData = {
      input_guid: req.body.guid,
      input_name: req.body.name,
      input_code: req.body.code,
      input_author_guid: author_guid
    };

    const [[ currency ]] = await db.currency.upsert(currencyData);
    res.status(200).json(currency);
  }
  catch (err) {
    next(err);
  }
};
