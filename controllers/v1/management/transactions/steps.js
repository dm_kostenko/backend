const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      step_guid: Joi.string().guid().required()
    });

  return Joi.object().keys({
    step_guid: Joi.string().guid().required(),
    number: Joi.number().positive().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataSteps ] = await db.step.byTransactionInfo({ 
      input_transaction_guid: req.params.guid,
      input_number: req.query.number, 
      input_step_guid: req.query.step_guid,
      input_step_name: req.query.step_name,
      input_step_type: req.query.step_type,
      input_items_count: req.query.items, 
      input_page_number: req.query.page,
      input_gateway_guid: req.query.gateway_guid
    });

    const numberOfCount = dataSteps[0] ? dataSteps[0].count : 0;

    dataSteps = dataSteps.map(step => {
      return {
        number: step.number,
        guid: step.guid,
        name: step.name,
        type: step.type,
        created_by: step.created_by,
        updated_by: step.updated_by,
        created_at: step.created_at,
        updated_at: step.updated_at
      };
    });

    res.status(200).json({ count: numberOfCount, data: dataSteps });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { step_guid, number } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.transactionStep.delete({ input_transaction_guid: guid, input_step_guid: step_guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ transaction ]] = await db.transactionStep.upsert({
      input_transaction_guid: guid,
      input_step_guid: step_guid,
      input_number: number,
      input_author_guid: author_guid
    });
    
    delete transaction.guard_counter;
    res.status(200).json(transaction);
  }
  catch (err) {
    next(err);
  }
};