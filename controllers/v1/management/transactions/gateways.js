const db = require("../../../../helpers/db/api/");
const InternalError = require("../../../../errorTypes");

module.exports.get = async (req, res, next) => {
  try {
    let [ dataTransactions ] = await db.transaction.byGatewayGuid({ input_gateway_guid: req.params.guid });
    
    dataTransactions = dataTransactions.map( transaction => {

      //TODOs move this hardcode to config
      let failure;
      switch(transaction.name){
      case "Authorization": failure = true; break;
      case "Payment": failure = true; break;
      case "Capture": failure = true; break;
      case "Refund": failure = false; break;
      default: throw new InternalError(`Unknown transaction_name = ${transaction.name}`);
      }

      return {
        guid: transaction.guid,
        name: transaction.name,
        failure
      };
    });

    res.status(200).json(dataTransactions);
  }catch(err){
    next(err);
  }
};