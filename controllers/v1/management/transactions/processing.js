const db = require("../../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    let [ dataTransactions ] = await db.transactionProcessing.info({
      input_guid: req.query.guid,
      input_transaction_type: req.query.transaction_type,
      input_account_guid: req.query.account_guid,
      input_amount: req.query.amount,
      input_currency: req.query.currency,
      input_status: req.query.status, 
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfTransactions = dataTransactions[0] ? dataTransactions[0].count : 0;

    dataTransactions = dataTransactions.map(transaction => {
      return {
        guid: transaction.guid,
        account_guid: transaction.account_guid,
        transaction_type: transaction.transaction_type,
        amount: transaction.amount, 
        currency: transaction.currency, 
        status: transaction.status, 
        created_at: transaction.created_at,
        created_by: transaction.created_by,
        updated_at: transaction.updated_at,
        updated_by: transaction.updated_by,
      };
    });

    res.status(200).json({ count: numberOfTransactions, data: dataTransactions });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ transaction ]] = await db.transactionProcessing.info({ input_guid: req.params.guid });

    if (!transaction)
      return res.status(200).json({});

    transaction = {
      guid: transaction.guid,
      account_guid: transaction.account_guid,
      transaction_type: transaction.transaction_type,
      amount: transaction.amount, 
      currency: transaction.currency, 
      status: transaction.status, 
      created_at: transaction.created_at,
      created_by: transaction.created_by,
      updated_at: transaction.updated_at,
      updated_by: transaction.updated_by,
    };
    
    res.status(200).json(transaction);
  }
  catch (err) {
    next(err);
  }
};