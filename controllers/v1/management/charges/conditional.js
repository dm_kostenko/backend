const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      shop_guid: Joi.string().guid().required(),
      gateway_guid: Joi.string().required(),
      currency_guid: Joi.string().guid().required()      
    });

  return Joi.object().keys({
    shop_guid: Joi.string().guid().required(),
    currency_guid: Joi.string().guid().required(),
    gateway_guid: Joi.string().required(),
    name: Joi.string().required(),
    flat_rate: Joi.number().greater(0).less(100).allow(0).required(),
    percent: Joi.number().greater(0).less(100).allow(0).required(),
    condition_name: Joi.string().valid("Sum", "Transactions").required(),
    condition_value: Joi.number().required()
  });
};

module.exports.canGet = async () => false;
module.exports.canGetByGuid = async () => false;
module.exports.canPost = async () => false;

module.exports.get = async (req, res, next) => {
  try {
    let [ dataCharges ] = await db.chargeConditional.info({
      input_shop_name: req.query.shop_name,
      input_gateway_name: req.query.gateway_name,
      input_currency_name: req.query.currency_name,
      input_name: req.query.name,
      input_flat_rate: req.query.flat_rate / 100,
      input_percent: req.query.percent / 100,
      input_condition_name: req.query.condition_name,
      input_condition_value: req.query.condition_value,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfCharge = dataCharges[0] ? dataCharges[0].count : 0;

    dataCharges = dataCharges.map(charge => {
      return {
        shop_guid: charge.shop_guid,
        shop_name: charge.shop_name,
        currency_guid: charge.currency_guid,
        currency_name: charge.currency_name,
        gateway_guid: charge.gateway_guid,
        gateway_name: charge.gateway_name,
        name: charge.name,
        flat_rate: charge.flat_rate * 100,
        percent: charge.percent * 100,
        condition_name: charge.condition_name,
        condition_value: charge.condition_value,
        created_at: charge.created_at,
        created_by: charge.created_by,
        updated_at: charge.updated_at,
        updated_by: charge.updated_by,
      };
    });

    res.status(200).json({ count: numberOfCharge, data: dataCharges });
  } catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const primary_key = {
    input_shop_guid: req.params.shop_guid,
    input_gateway_guid: req.params.gateway_guid,
    input_currency_guid: req.params.currency_guid
  };

  try {
    const [[ charge ]] = await db.chargeConditional.info(primary_key);
    if (!charge) return res.status(200).json({});

    res.status(200).json({
      shop_guid: charge.shop_guid,
      shop_name: charge.shop_name,
      currency_guid: charge.currency_guid,
      currency_name: charge.currency_name,
      gateway_guid: charge.gateway_guid,
      gateway_name: charge.gateway_name,
      name: charge.name,
      flat_rate: charge.flat_rate * 100,
      percent: charge.percent * 100,
      condition_name: charge.condition_name,
      condition_value: charge.condition_value,
      created_at: charge.created_at,
      created_by: charge.created_by,
      updated_at: charge.updated_at,
      updated_by: charge.updated_by
    });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    const primary_key = {
      input_shop_guid: req.body.shop_guid,
      input_currency_guid: req.body.currency_guid,
      input_gateway_guid: req.body.gateway_guid
    };

    if (req.body.delete) {
      await db.chargeOneTime.delete(primary_key);
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ upsertedCharge ]] = await db.chargeConditional.upsert({
      ...primary_key,
      input_name: req.body.name,
      input_flat_rate: req.body.flat_rate / 100,
      input_percent: req.body.percent / 100,
      input_condition_name: req.body.condition_name,
      input_condition_value: req.body.condition_value,
      input_author_guid: author_guid
    });

    const [[ dataGateway ]] = await db.gateway.get({ input_guid: upsertedCharge.gateway_guid });
    const [[ dataCurrency ]] = await db.currency.get({ input_guid: upsertedCharge.currency_guid });
    const [[ dataShop ]] = await db.shop.get({ input_guid: upsertedCharge.shop_guid });

    res.status(200).json({
      ...upsertedCharge,
      flat_rate: upsertedCharge.flat_rate * 100,
      percent: upsertedCharge.percent * 100,
      gateway_name: dataGateway.name,
      currency_name: dataCurrency.name,
      shop_name: dataShop.name,
    });
  } catch (err) {
    next(err);
  }
};