const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");
const { InternalError } = require("../../../errorTypes");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      type: Joi.string().not().empty(),
      account_guid: Joi.string().guid(),
      card_number: Joi.string().creditCard(), //Luhn algoritm
      currency_guid: Joi.string().guid(),
      name_on_card: Joi.string().not().empty(),
      expired: Joi.equal(0,1),
      expires_at: Joi.date().iso(),
      balance: Joi.number()
    });
  return Joi.object().keys({
    type: Joi.string().not().empty(),
    account_guid: Joi.string().guid().required(),
    card_number: Joi.string().creditCard().required(), //Luhn algoritm
    currency_guid: Joi.string().guid().required(),
    name_on_card: Joi.string().not().empty().required(),
    expired: Joi.equal(0,1).required(),
    expires_at: Joi.date().iso().required(),
    balance: Joi.number().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataCards ]  = await db.card.info({
      input_guid: req.query.guid,
      input_type: req.query.type,
      input_account_guid: req.query.account_guid,
      input_account_number: req.query.account_number,
      input_card_number: req.query.card_number,
      input_currency_name: req.query.currency_name,
      input_name_on_card: req.query.name_on_card,
      input_balance: req.query.balance,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfCards = dataCards[0] ? dataCards[0].count : 0;

    dataCards = dataCards.map(card => {
      return{
        guid: card.guid,
        type: card.type,
        account_guid: card.account_guid,
        card_number: card.card_number,
        currency_guid: card.currency_guid,
        name_on_card: card.name_on_card,
        expired: card.expired,
        expires_at: card.expires_at,
        balance: card.balance,
        account_number: card.account_number,
        currency_name: card.currency_name,
        created_at: card.created_at,
        created_by: card.created_by,
        updated_at: card.updated_at,
        updated_by: card.updated_by,
      };
    });

    res.status(200).json({ count: numberOfCards, data: dataCards });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ card ]]  = await db.card.info({ input_guid: req.params.guid });

    card = {
      guid: card.guid,
      type: card.type,
      account_guid: card.account_guid,
      card_number: card.card_number,
      currency_guid: card.currency_guid,
      name_on_card: card.name_on_card,
      expired: card.expired,
      expires_at: card.expires_at,
      balance: card.balance,
      account_number: card.account_number,
      currency_name: card.currency_name,
      created_at: card.created_at,
      created_by: card.created_by,
      updated_at: card.updated_at,
      updated_by: card.updated_by,
    };

    res.status(200).json(card);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.card.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const cardData = {
      input_guid: req.body.guid,
      input_type: req.body.type,
      input_account_guid: req.body.account_guid,
      input_card_number: req.body.card_number,
      input_currency_guid: req.body.currency_guid,
      input_name_on_card: req.body.name_on_card,
      input_expired: req.body.expired,
      input_expires_at: req.body.expires_at,
      input_balance: req.body.balance,
      input_author_guid: author_guid
    };

    const [[ card ]] = await db.card.upsert(cardData);

    if (!card) 
      throw new InternalError("Record wasn't created for card");

    const [[ currency ]] = await db.currency.get({ input_guid: card.currency_guid });

    const [[ accountRow ]] = await db.account.get({ input_guid: card.account_guid });

    card.account_number = accountRow.number;
    card.currency_name = currency.name;

    res.status(200).json(card);
  }
  catch (err) {
    next(err);
  }
};
