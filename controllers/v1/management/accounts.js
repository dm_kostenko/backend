const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");
const { InternalError } = require("../../../errorTypes");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      currency_guid: Joi.string().guid(),
      number: Joi.string().not().empty(),
      balance: Joi.number()
    });
  return Joi.object().keys({
    currency_guid: Joi.string().guid().required(),
    number: Joi.string().not().empty().required(),
    balance: Joi.number().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataAccounts ]  = await db.account.info({
      input_guid: req.query.guid,
      input_currency_name: req.query.currency_name,
      input_number: req.query.number,
      input_balance: req.query.balance,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfAccounts = dataAccounts[0] ? dataAccounts[0].count : 0;

    dataAccounts = dataAccounts.map( account => {
      return{
        guid: account.guid,
        currency_guid: account.currency_guid,
        currency_name: account.currency_name,
        number: account.number,
        balance: account.balance,
        created_at: account.created_at,
        created_by: account.created_by,
        updated_at: account.updated_at,
        updated_by: account.updated_by,
      };
    });

    res.status(200).json({ count: numberOfAccounts, data: dataAccounts });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ account ]]  = await db.account.info({ input_guid: req.params.guid });

    if (!account)
      return res.status(200).json({});

    account = {
      guid: account.guid,
      currency_guid: account.currency_guid,
      currency_name: account.currency_name,
      number: account.number,
      balance: account.balance,
      created_at: account.created_at,
      created_by: account.created_by,
      updated_at: account.updated_at,
      updated_by: account.updated_by,
    };

    res.status(200).json(account);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {

    if (req.body.delete) {
      await db.account.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const accountData = {
      input_guid: req.body.guid,
      input_currency_guid: req.body.currency_guid,
      input_number: req.body.number,
      input_balance: req.body.balance,
      input_author_guid: author_guid
    };

    const [[ account ]] = await db.account.upsert(accountData);

    if (!account) 
      throw new InternalError("Record wasn't created for account");

    const [[ currency ]] = await db.currency.get({ input_guid: account.currency_guid });

    account.currency_name = currency.name;

    res.status(200).json(account);
  }
  catch (err) {
    next(err);
  }
};
