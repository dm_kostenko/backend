const db = require("../../../../helpers/db/api/");

module.exports.canGet = async (req) => req.auth.partner ? req.params.guid === req.auth.partnerGuid : false;

module.exports.get = async (req, res, next) => {
  try {
    let [ dataOnGroups ] = await db.group.byPartnerInfo({
      input_partner_guid: req.params.guid,
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfGroups = dataOnGroups[0] ? dataOnGroups[0].count : 0;

    dataOnGroups = dataOnGroups.map(group => {
      return {
        guid: group.guid,
        name: group.name,
        type: group.type,
        created_at: group.created_at,
        created_by: group.created_by,
        updated_at: group.updated_at,
        updated_by: group.updated_by
      };
    });

    res.status(200).json({ count: numberOfGroups, data: dataOnGroups });
  } catch (err) {
    next(err);
  }
};