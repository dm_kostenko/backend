const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      login_guid: Joi.string().guid().required()
    });

  return Joi.object().keys({
    login_guid: Joi.string().guid().required(),
    role_guid: Joi.string().guid().required(),
  });
};

module.exports.canGet = async (req) => req.auth.partner ? req.params.guid === req.auth.partnerGuid : false;
module.exports.canPost = async (req) => false;

module.exports.get = async (req, res, next) => {
  try {
    const { guid, username, email, enabled, page, items } = req.query;

    let [ dataOnLogins ] = await db.login.byPartnerInfo({
      input_partner_guid: req.params.guid,
      input_guid: guid,
      input_username: username,
      input_email: email,
      input_enabled: enabled,
      input_page_number: page,
      input_items_count: items
    });

    const numberOfLogins = dataOnLogins[0] ? dataOnLogins[0].count : 0;

    const resData = dataOnLogins.map(recordLogin => {
      return {
        guid: recordLogin.guid,
        username: recordLogin.username,
        email: recordLogin.email,
        role : {
          guid: recordLogin.role_guid,
          name: recordLogin.role_name,
          description: recordLogin.role_description,
          created_at : recordLogin.role_created_at,
          created_by: recordLogin.role_created_by,
          updated_at : recordLogin.role_updated_at,
          updated_by : recordLogin.role_updated_by,
        },
        created_at: recordLogin.created_at,
        created_by: recordLogin.created_by,
        updated_at: recordLogin.updated_at,
        updated_by: recordLogin.updated_by,
      };
    });

    res.status(200).json({ count: numberOfLogins, data: resData });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid: partner_guid } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.loginPartner.delete({
        input_login_guid: req.body.login_guid,
        input_partner_guid: partner_guid
      });

      return res.status(200).json({ message: "Deleted" });
    }

    const [[ upsertedUser ]] = await db.loginPartner.upsert({
      input_login_guid: req.body.login_guid,
      input_partner_guid: partner_guid,
      input_role_guid: req.body.role_guid,
      input_author_guid: author_guid
    });
    
    res.status(200).json({
      guid: upsertedUser.login_guid,
      partner_guid: upsertedUser.partner_guid,
      role_guid: upsertedUser.role_guid,
      created_at: upsertedUser.created_at,
      created_by: upsertedUser.created_by,
      updated_at: upsertedUser.updated_at,
      updated_by: upsertedUser.updated_by,
    });
  } catch (err) {
    next(err);
  }
};
