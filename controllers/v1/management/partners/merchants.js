const db = require("../../../../helpers/db/api/");

module.exports.canGet = async req => req.auth.partner ? req.params.guid === req.auth.partnerGuid : false;

module.exports.get = async (req, res, next) => {
  try {
    let [ dataMerchants ] = await db.merchant.byPartnerInfo({
      input_partner_guid: req.params.guid,
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_group_name: req.query.group_name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfMerchants = dataMerchants[0] ? dataMerchants[0].count : 0;

    dataMerchants = dataMerchants.map(merchant => {
      return {
        guid: merchant.guid,
        name: merchant.name,
        type: merchant.type,
        group_guid: merchant.group_guid,
        group_name: merchant.group_name,
        created_at: merchant.created_at,
        created_by: merchant.created_by,
        updated_at: merchant.updated_at,
        updated_by: merchant.updated_by,
      };
    });

    res.status(200).json({ count: numberOfMerchants, data: dataMerchants });
  } catch (err) {
    next(err);
  }
};
