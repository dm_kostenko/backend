const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");

//TODOs add guid validation to gateway_guid
module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      shop_guid: Joi.string().guid().required(),
      gateway_guid: Joi.string().required(),
      currency_guid: Joi.string().guid().required()      
    });

  return Joi.object().keys({
    shop_guid: Joi.string().guid().required(),
    currency_guid: Joi.string().guid().required(),
    gateway_guid: Joi.string().required(),
    hold_rate: Joi.number().greater(0).less(100).allow(0).required(),
    hold_period: Joi.number().positive().integer().required(),
    apply_from: Joi.date().iso().required(),
    apply_to: Joi.date().iso(),
    note: Joi.string().allow(""),
    transaction_rates: Joi.array().required().items(
      Joi.object().required().keys({
        transaction_guid: Joi.string().guid().required(),
        buy: Joi.object().required().keys({
          success: Joi.number().greater(0).less(100).allow(0).required(),
          failure: Joi.number().greater(0).less(100).allow(0).allow(null)
        }),
        sell: Joi.object().required().keys({
          success: Joi.number().greater(0).less(100).allow(0).required(),
          failure: Joi.number().greater(0).less(100).allow(0).allow(null)
        })
      })
    )
  });
};

module.exports.canGet = async () => false;
module.exports.canGetByGuid = async () => false;
module.exports.canPost = async () => false;

module.exports.get = async (req, res, next) => {
  try {
    const { apply_from, apply_to, hold_rate } = req.query;
    let [ dataRates ] = await db.rate.info({
      input_shop_name: req.query.shop_name,
      input_gateway_name: req.query.gateway_name,
      input_currency_name: req.query.currency_name,
      input_apply_to: apply_to ? apply_to.replace(/T/g," ") : undefined,
      input_apply_from: apply_from ? apply_from.replace(/T/g," ") : undefined,
      input_hold_rate: hold_rate ? hold_rate / 100 : undefined,
      input_hold_period: req.query.hold_period,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfRates = dataRates[0] ? dataRates[0].count : 0;

    dataRates = dataRates.map(rate => {
      return {
        shop_guid: rate.shop_guid,
        shop_name: rate.shop_name,
        currency_guid: rate.currency_guid,
        currency_name: rate.currency_name,
        gateway_guid: rate.gateway_guid,
        gateway_name: rate.gateway_name,
        hold_rate: rate.hold_rate * 100,
        hold_period: rate.hold_period,
        apply_from: rate.apply_from,
        apply_to: rate.apply_to,
        note: rate.note,
        created_at: rate.created_at,
        created_by: rate.created_by,
        updated_at: rate.updated_at,
        updated_by: rate.updated_by,
      };
    });

    res.status(200).json({ count: numberOfRates, data: dataRates });
  } catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const primary_key = {
    input_shop_guid: req.params.shop_guid,
    input_gateway_guid: req.params.gateway_guid,
    input_currency_guid: req.params.currency_guid
  };

  try {
    const [[ rate ]] = await db.rate.info(primary_key);
    if (!rate) return res.status(200).json({});

    res.status(200).json({
      shop_guid: rate.shop_guid,
      shop_name: rate.shop_name,
      currency_guid: rate.currency_guid,
      currency_name: rate.currency_name,
      gateway_guid: rate.gateway_guid,
      gateway_name: rate.gateway_name,
      hold_rate: rate.hold_rate * 100,
      hold_period: rate.hold_period,
      apply_from: rate.apply_from,
      apply_to: rate.apply_to,
      note: rate.note,
      created_at: rate.created_at,
      created_by: rate.created_by,
      updated_at: rate.updated_at,
      updated_by: rate.updated_by
    });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    const primary_key = {
      input_shop_guid: req.body.shop_guid,
      input_currency_guid: req.body.currency_guid,
      input_gateway_guid: req.body.gateway_guid
    };

    if (req.body.delete) {
      await db.rate.delete(primary_key);
      await db.transactionRate.delete(primary_key);
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ upsertedRate ]] = await db.rate.upsert({
      ...primary_key,
      input_hold_rate: req.body.hold_rate / 100,
      input_hold_period: req.body.hold_period,
      input_apply_from: req.body.apply_from,
      input_apply_to: req.body.apply_to,
      input_note: req.body.note,
      input_author_guid: author_guid
    });

    const upsertedTransactionRates = await Promise.all(req.body.transaction_rates.map( async transaction_rate => {
      const [[ rateBuy ]] = await db.transactionRate.upsert({
        ...primary_key,
        input_transaction_guid: transaction_rate.transaction_guid,
        input_type: "buy",
        input_success: transaction_rate.buy.success / 100,
        input_failure: transaction_rate.buy.failure ? transaction_rate.buy.failure / 100 : undefined,
        input_author_guid: author_guid
      });

      const [[ rateSell ]] = await db.transactionRate.upsert({
        ...primary_key,
        input_transaction_guid: transaction_rate.transaction_guid,
        input_type: "sell",
        input_success: transaction_rate.sell.success / 100,
        input_failure: transaction_rate.sell.failure ? transaction_rate.sell.failure / 100 : undefined,
        input_author_guid: author_guid
      });

      return {
        transaction_guid: transaction_rate.transaction_guid,
        buy: {
          success: rateBuy.success * 100,
          failure: rateBuy.failure ? rateBuy.failure * 100 : undefined,
          created_at: rateBuy.created_at,
          created_by: rateBuy.created_by,
          updated_at: rateBuy.updated_at,
          updated_by: rateBuy.updated_by
        },
        sell: {
          success: rateSell.success * 100,
          failure: rateSell.failure ? rateSell.failure * 100 : undefined,
          created_at: rateSell.created_at,
          created_by: rateSell.created_by,
          updated_at: rateSell.updated_at,
          updated_by: rateSell.updated_by
        }
      };
    }));

    const [[ dataGateway ]] = await db.gateway.get({ input_guid: upsertedRate.gateway_guid });
    const [[ dataCurrency ]] = await db.currency.get({ input_guid: upsertedRate.currency_guid });
    const [[ dataShop ]] = await db.shop.get({ input_guid: upsertedRate.shop_guid });

    res.status(200).json({
      ...upsertedRate,
      gateway_name: dataGateway.name,
      currency_name: dataCurrency.name,
      shop_name: dataShop.name,
      transaction_rates: upsertedTransactionRates
    });
  } catch (err) {
    next(err);
  }
};

module.exports.transactions = require("./rates/transactions");