const db = require("../../../helpers/db/api/");
const { InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      type: Joi.string().not().empty(),
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    type: Joi.string().not().empty().required(),
  });
};

module.exports.canGet = async req => false;
module.exports.canGetByGuid = async req => false;
module.exports.canPost = async req => false;

module.exports.get = async (req, res, next) => {
  try {
    let [ dataPartners ] = await db.partner.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfPartners = dataPartners[0] ? dataPartners[0].count : 0;

    dataPartners = dataPartners.map( partner => {
      return {
        guid: partner.guid,
        name: partner.name,
        type: partner.type,
        created_at: partner.created_at,
        created_by: partner.created_by,
        updated_at: partner.updated_at,
        updated_by: partner.updated_by,
      };
    });

    res.status(200).json({ count: numberOfPartners, data: dataPartners });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ partner ]]  = await db.partner.info({ input_guid: req.params.guid });

    if (!partner) return res.status(200).json({});

    res.status(200).json({
      guid: partner.guid,
      name: partner.name,
      type: partner.type,
      created_at: partner.created_at,
      created_by: partner.created_by,
      updated_at: partner.updated_at,
      updated_by: partner.updated_by,
    });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.partner.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const partnerData = {
      input_guid: req.body.guid,
      input_name: req.body.name,
      input_type: req.body.type,
      input_author_guid: author_guid
    };

    const [[ partner ]] = await db.partner.upsert(partnerData);
    if (!partner) throw new InternalError("Record wasn't created for partner");

    res.status(200).json({
      guid: partner.guid,
      name: partner.name,
      type: partner.type,
      created_at: partner.created_at,
      created_by: partner.created_by,
      updated_at: partner.updated_at,
      updated_by: partner.updated_by,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.logins = require("./partners/logins");
module.exports.groups = require("./partners/groups");
module.exports.merchants = require("./partners/merchants");
module.exports.shops = require("./partners/shops");
