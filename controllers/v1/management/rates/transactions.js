const db = require("../../../../helpers/db/api/");

module.exports.getByGuid = async (req, res, next) => {
  try {
    const primary_key = {
      input_shop_guid: req.params.shop_guid,
      input_gateway_guid: req.params.gateway_guid,
      input_currency_guid: req.params.currency_guid
    };
    const [ rates ] = await db.transactionRate.get(primary_key);
    if (!rates) return res.status(200).json([]);
  
    const arrayRates = await Promise.all(rates.map( async rate => {
      const [[ transaction ]] = await db.transaction.get({ input_guid: rate.transaction_guid });

      return {
        shop_guid: rate.shop_guid,
        currency_guid: rate.currency_guid,
        gateway_guid: rate.gateway_guid,
        transaction_guid: rate.transaction_guid,
        transaction_name: transaction.name,
        type: rate.type,
        success: rate.success * 100,
        failure: rate.failure ? rate.failure * 100 : undefined,
        created_at: rate.created_at,
        created_by: rate.created_by,
        updated_at: rate.updated_at,
        updated_by: rate.updated_by
      };
    }));

    res.status(200).json(arrayRates);
  } catch (err) {
    next(err);
  }
};