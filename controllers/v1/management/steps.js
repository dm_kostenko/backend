const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      step_guid: Joi.string().guid().required()
    });
  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      gateway_guid: Joi.string().guid(),
      name: Joi.string().not().empty()
    });
  return Joi.object().keys({
    gateway_guid: Joi.string().guid().required(),
    name: Joi.string().not().empty().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataSteps ] = await db.step.info({
      input_guid: req.query.guid,  
      input_name: req.query.name,
      input_gateway_guid: req.query.gateway_guid, 
      input_items_count: req.query.items,
      input_page_number: req.query.page
    });

    const numberOfSteps = dataSteps[0] ? dataSteps[0].count : 0;

    dataSteps = dataSteps.map(step => {
      return {
        guid: step.guid,
        name: step.name,
        gateway_guid: step.gateway_guid,
        created_at: step.created_at,
        created_by: step.created_by,
        updated_at: step.updated_at,
        updated_by: step.updated_by,
      };
    });

    res.status(200).json({ count: numberOfSteps, data: dataSteps });
  } catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ step ]] = await db.step.info({ input_guid: req.params.guid });

    if (!step)
      return res.status(200).json({});
    
    step = {
      guid: step.guid,
      name: step.name,
      gateway_guid: step.gateway_guid,
      created_at: step.created_at,
      created_by: step.created_by,
      updated_at: step.updated_at,
      updated_by: step.updated_by,
    };

    res.status(200).json(step);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { name, gateway_guid, guid } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.step.delete({ input_guid: guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ step ]] = await db.step.upsert({
      input_guid: guid,
      input_name: name,
      input_gateway_guid: gateway_guid,
      input_author_guid: author_guid
    });
    
    delete step.guard_counter;
    res.status(200).json(step);
  } catch (err) {
    next(err);
  }
};

module.exports.params = require("./steps/params");
module.exports.processing = require("./steps/processing");