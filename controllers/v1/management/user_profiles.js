const userProfileStorage = require("../../../helpers/userProfileStorage");

module.exports.getByGuid = async (req, res, next) => {
  try {
    let user_profile = await userProfileStorage.get(req.params.guid);

    user_profile = {
      first_name: user_profile.first_name,
      last_name: user_profile.last_name,
      email: user_profile.email,
      phone: user_profile.phone,
      company_name: user_profile.company_name,
      note: user_profile.note,
      created_at: user_profile.createdAt,
      created_by: user_profile.createdBy,
      updated_at: user_profile.updatedAt,
      updated_by: user_profile.updatedBy
    };

    res.status(200).json(user_profile);
  } catch (err) {
    next(err);
  }
};