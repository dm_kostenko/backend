const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid().required(),
      name: Joi.string().not().empty(),
      type: Joi.string().not().empty()
    });

  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    type: Joi.string().not().empty().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataTransactions ] = await db.transaction.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_type: req.query.type,
      input_items_count: req.query.items,
      input_page_number: req.query.page
    });

    const numberOfShops = dataTransactions[0] ? dataTransactions[0].count : 0;

    dataTransactions = dataTransactions.map( transaction => {
      return {
        guid: transaction.guid,
        type: transaction.type,
        name: transaction.name,
        created_at: transaction.created_at,
        created_by: transaction.created_by,
        updated_at: transaction.updated_at,
        updated_by: transaction.updated_by
      };
    });

    res.status(200).json({ count: numberOfShops, data: dataTransactions });
  } catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ transaction ]] = await db.transaction.info({ input_guid: req.params.guid });

    if (!transaction)
      return res.status(200).json({});
  
    transaction = {
      guid: transaction.guid,
      type: transaction.type,
      name: transaction.name,
      created_at: transaction.created_at,
      created_by: transaction.created_by,
      updated_at: transaction.updated_at,
      updated_by: transaction.updated_by
    };

    res.status(200).json(transaction);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { name, type, guid } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.transaction.delete({ input_guid: guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ transaction ]] = await db.transaction.upsert({
      input_guid: guid,
      input_name: name,
      input_type: type,
      input_author_guid: author_guid
    });
    
    delete transaction.guard_counter;
    res.status(200).json(transaction);
  } catch (err) {
    next(err);
  }
};

module.exports.processing = require("./transactions/processing");
module.exports.steps = require("./transactions/steps");
module.exports.gateways = require("./transactions/gateways");