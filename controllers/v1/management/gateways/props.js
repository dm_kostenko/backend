const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      name: Joi.string().not().empty().required()
    });

  if (body.name)
    return Joi.object().keys({
      name: Joi.string().not().empty(),
      label: Joi.string().not().empty()
    });

  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    label: Joi.string().not().empty().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataGatewayProps ]  = await db.gatewayProp.info({
      input_gateway_guid: req.params.guid,
      input_name: req.params.name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });      

    const numberOfProps = dataGatewayProps[0] ? dataGatewayProps[0].count : 0;
    dataGatewayProps = dataGatewayProps.map(gatewayProp => ({
      gateway_guid: gatewayProp.gateway_guid,
      name: gatewayProp.name,
      label: gatewayProp.label,
      created_at: gatewayProp.created_at,
      created_by: gatewayProp.created_by,
      updated_at: gatewayProp.updated_at,
      updated_by: gatewayProp.updated_by,
    }));

    if (req.params.name && dataGatewayProps)
      res.status(200).json(dataGatewayProps[0]); //if name -> should return object, not array. So, destructurize 
    else
      res.status(200).json({ count: numberOfProps, data: dataGatewayProps });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {  
      await db.gatewayProp.delete({
        input_gateway_guid: req.params.guid,
        input_name: req.body.name
      });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ upsertedProp ]]  = await db.gatewayProp.upsert({
      input_gateway_guid: req.params.guid,
      input_name: req.body.name,
      input_label: req.body.label,
      input_author_guid: author_guid
    });

    res.status(200).json(upsertedProp);
  }
  catch (err) {
    next(err);
  }
};
