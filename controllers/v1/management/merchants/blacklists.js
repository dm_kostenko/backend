const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      blacklist_rule_guid: Joi.string().guid().required(),
    });

  return Joi.object().keys({
    blacklist_rule_guid: Joi.string().guid().required(),
    type: Joi.string().required()
  });
};

module.exports.canGet = async req => req.auth.merchant ? req.params.guid = req.auth.merchantGuid : false;
module.exports.canPost = async req => req.auth.merchant ? req.params.guid = req.auth.merchantGuid : false;

module.exports.get = async (req, res, next) => {
  const { page, items } = req.query;
  
  try {
    let [ blacklistMerchants ] = await db.blacklistMerchant.byMerchantInfo({
      input_merchant_guid: req.params.guid,
      input_page_number: page,
      input_items_count: items
    });
    
    if (!blacklistMerchants)
      return res.status(200).json({});

    const numberOfblacklistMerchants = blacklistMerchants[0] ? blacklistMerchants[0].count : 0;

    blacklistMerchants = blacklistMerchants.map(obj => {
      return {
        merchant_guid: obj.merchant_guid,
        blacklist_rule_guid: obj.blacklist_rule_guid,
        blacklist_rule_name: obj.blacklist_rule_name,
        type: obj.type,
        created_at: obj.created_at,
        created_by: obj.created_by,
        updated_at: obj.updated_at,
        updated_by: obj.created_by
      };
    });

    res.status(200).json({ count: numberOfblacklistMerchants, data: blacklistMerchants });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid: merchant_guid } = req.params;
  const { blacklist_rule_guid, type } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.blacklistMerchant.delete({
        input_blacklist_rule_guid: req.body.blacklist_rule_guid,
        input_merchant_guid: merchant_guid,
      });

      return res.status(200).json({ message: "Deleted" });
    }

    const [[ blacklistMerchant ]] = await db.blacklistMerchant.upsert({
      input_blacklist_rule_guid: blacklist_rule_guid,
      input_merchant_guid: merchant_guid,
      input_type: type,
      input_author_guid: author_guid
    });

    res.status(200).json(blacklistMerchant);
  }
  catch (err) {
    next(err);
  }
};