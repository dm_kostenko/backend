const db = require("../../../../helpers/db/api/");

module.exports.canGet = async req => req.auth.merchant && req.params.guid === req.auth.merchantGuid;

module.exports.get = async (req, res, next) => {
  try {
    let [ dataShops ] = await db.shop.byMerchantInfo({
      input_merchant_guid: req.params.guid,
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_merchant_name: req.query.merchant_name,
      input_enabled: req.query.enabled,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfShops = dataShops[0] ? dataShops[0].count : 0;

    dataShops = dataShops.map(shop => {
      return {
        guid: shop.guid,
        merchant_guid: shop.merchant_guid,
        merchant_name: shop.merchant_name,
        name: shop.name,
        url: shop.url,
        email: shop.email,
        phone: shop.phone,
        enabled: shop.enabled,
        note: shop.note,
        enable_checkout: shop.enable_checkout,
        checkout_method: shop.checkout_method,
        antiFraudMonitor: shop.antiFraudMonitor,
        antiFraudMonitorValue: shop.antiFraudMonitorValue,
        created_at: shop.created_at,
        created_by: shop.created_by,
        updated_at: shop.updated_at,
        updated_by: shop.updated_by
      };
    });

    res.status(200).json({ count: numberOfShops, data: dataShops });
  } catch (err) {
    next(err);
  }
};
