const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");
const { InternalError } = require("../../../../errorTypes");

module.exports.schema = () => {
  return Joi.object().keys({
    guid: Joi.string().guid().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataAccounts ] = await db.account.byLoginInfo({
      input_login_guid : req.params.guid,
      input_guid: req.query.guid,
      input_currency_name: req.query.currency_name,
      input_number: req.query.number,
      input_balance: req.query.balance,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfAccounts = dataAccounts[0] ? dataAccounts[0].count : 0;

    dataAccounts = dataAccounts.map(account => {
      return{
        guid: account.guid,
        currency_guid: account.currency_guid,
        currency_name: account.currency_name,
        number: account.number,
        balance: account.balance,
        created_at: account.created_at,
        created_by: account.created_by,
        updated_at: account.updated_at,
        updated_by: account.updated_by,
      };
    });

    res.status(200).json({ count: numberOfAccounts, data: dataAccounts });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid: login_guid } = req.params;
  const { guid: account_guid } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {

    if (req.body.delete) {
      await db.loginAccount.delete({
        input_login_guid: login_guid,
        input_account_guid: account_guid
      });

      return res.status(200).json({ message: "Deleted" });
    }

    const [[ loginAccount ]] = await db.loginAccount.upsert({
      input_login_guid: login_guid,
      input_account_guid: account_guid,
      input_author_guid: author_guid
    });

    const [[ account ]] = await db.account.get({ input_guid: account_guid });

    if (!account || !loginAccount) 
      throw new InternalError("Didn't write info into account or login_account");

    const resData = {
      login_guid: login_guid,
      guid: account.guid,
      currency_guid: account.currency_guid,
      number: account.number,
      balance: account.balance,
      created_at: loginAccount.created_at,
      created_by: loginAccount.created_by,
      updated_at: loginAccount.updated_at,
      updated_by: loginAccount.updated_by
    };

    res.status(200).json(resData);
  }
  catch (err) {
    next(err);
  }
};
