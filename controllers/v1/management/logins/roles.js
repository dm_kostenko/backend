const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.object().keys({
    guid: Joi.string().guid().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ defaultRoleRecords ] = await db.defaultRole.byLoginInfo({
      input_login_guid: req.params.guid,
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfRoles = defaultRoleRecords[0] ? defaultRoleRecords[0].count : 0;

    defaultRoleRecords = defaultRoleRecords.map(role => {
      return{
        guid: role.guid,
        name: role.name,
        description: role.description,
        created_at: role.created_at,
        created_by: role.created_by,
        updated_at: role.updated_at,
        updated_by: role.updated_by,
      };
    });

    res.status(200).json({ count: numberOfRoles, data: defaultRoleRecords });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid: login_guid } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {

    if (req.body.delete) {
      await db.defaultLoginRole.delete({
        input_login_guid: login_guid,
        input_role_guid: req.body.guid
      });

      return res.status(200).json({ message: "Deleted" });
    }

    const [[ dataLoginRole ]] = await db.defaultLoginRole.upsert({
      input_login_guid: login_guid,
      input_role_guid: req.body.guid,
      input_author_guid: author_guid
    });

    const [[ role ]] = await db.role.get({ input_guid: dataLoginRole.role_guid });

    const resDb = {
      login_guid: dataLoginRole.login_guid,
      guid: dataLoginRole.role_guid,
      name: role.name,
      type: role.type,
      description: role.description,
      created_at: dataLoginRole.created_at,
      created_by: dataLoginRole.created_by,
      updated_at: dataLoginRole.updated_at,
      updated_by: dataLoginRole.updated_by
    };

    res.status(200).json(resDb);
  }
  catch (err) {
    next(err);
  }
};
