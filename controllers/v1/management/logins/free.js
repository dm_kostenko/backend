const db = require("../../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    const { entity, enabled, page, items } = req.query;

    let [ dataOnLogins ] = await db.free_logins.info({
      input_entity: entity,
      input_enabled: enabled,
      input_page_number: page,
      input_items_count: items
    });

    const numberOfLogins = dataOnLogins[0] ? dataOnLogins[0].count : 0;

    const resData = await Promise.all(dataOnLogins.map(async recordLogin => {
      return {
        guid: recordLogin.guid,
        username: recordLogin.username
      };
    }));

    res.status(200).json({ count: numberOfLogins, data: resData });
  }
  catch (err) {
    next(err);
  }
};