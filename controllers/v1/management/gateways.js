const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      description: Joi.string().not().empty()
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    description: Joi.string().not().empty().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataGateways ]  = await db.gateway.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfGateways = dataGateways[0] ? dataGateways[0].count : 0;

    dataGateways = dataGateways.map(gateway => {
      return {
        guid: gateway.guid,
        name: gateway.name,
        description: gateway.description,
        created_by: gateway.created_by,
        updated_by: gateway.updated_by,
        created_at: gateway.created_at,
        updated_at: gateway.updated_at
      };
    });

    res.status(200).json({ count: numberOfGateways, data: dataGateways });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ gateway ]]  = await db.gateway.info({ input_guid: req.params.guid });

    if (!gateway)
      return res.status(200).json({});

    gateway = {
      guid: gateway.guid,
      name: gateway.name,
      description: gateway.description,
      created_by: gateway.created_by,
      updated_by: gateway.updated_by,
      created_at: gateway.created_at,
      updated_at: gateway.updated_at
    };

    res.status(200).json(gateway);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.gateway.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const gatewayData = {
      input_guid: req.body.guid,
      input_name: req.body.name,
      input_description: req.body.description,
      input_author_guid: author_guid
    };

    const [[ gateway ]] = await db.gateway.upsert(gatewayData);

    const resData = {
      guid: gateway.guid,
      name: gateway.name,
      description: gateway.description,
      created_by: gateway.created_by,
      updated_by: gateway.updated_by,
      created_at: gateway.created_at,
      updated_at: gateway.updated_at
    };

    res.status(200).json(resData);
  }
  catch (err) {
    next(err);
  }
};

module.exports.props = require("./gateways/props");
