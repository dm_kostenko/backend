const db = require("../../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    let [ processedSteps ] = await db.stepProcessing.info({
      input_name: req.query.name,
      input_type: req.query.type,
      input_transaction_processing_guid: req.query.transaction_processing_guid,
      input_status: req.query.status,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });
    const numberOfSteps = processedSteps[0] ? processedSteps[0].count : 0;

    processedSteps = processedSteps.map(step => ({
      guid: step.guid,
      name: step.name,
      transaction_processing_guid: step.transaction_processing_guid,
      type: step.type,
      status: step.status, 
      created_at: step.created_at,
      created_by: step.created_by,
      updated_at: step.updated_at,
      updated_by: step.updated_by,
    }));

    res.status(200).json({ count: numberOfSteps, data: processedSteps });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ processedStep ]] = await db.stepProcessing.get({ input_guid: req.params.guid });

    if (!processedStep)
      return res.status(200).json({});

    processedStep = {
      guid: processedStep.guid,
      name: processedStep.name,
      transaction_processing_guid: processedStep.transaction_processing_guid,
      type: processedStep.type,
      status: processedStep.status, 
      created_at: processedStep.created_at,
      created_by: processedStep.created_by,
      updated_at: processedStep.updated_at,
      updated_by: processedStep.updated_by,
    };
    
    res.status(200).json(processedStep);
  }
  catch (err) {
    next(err);
  }
};

module.exports.params = require("./processing/params");