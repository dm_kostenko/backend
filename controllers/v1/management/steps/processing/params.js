const db = require("../../../../../helpers/db/api/");

module.exports.get = async (req, res, next) => {
  try {
    let [ processedStepParams ] = await db.stepParamProcessing.get({
      input_step_processing_guid: req.params.guid,
      input_value: req.query.value,
      input_items_count: req.query.items,
      input_page_number: req.query.page,
    });

    const numberOfParams = processedStepParams[0] ? processedStepParams[0].count : 0;

    processedStepParams = processedStepParams.map(param => ({
      step_processing_guid: param.step_processing_guid,
      name: param.name,
      value: param.value,
      created_at: param.created_at,
      created_by: param.created_by,
      updated_at: param.updated_at,
      updated_by: param.updated_by,
    }));

    res.status(200).json({ count: numberOfParams, data: processedStepParams });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByName = async (req, res, next) => {
  try {
    let [[ processedStepParam ]] = await db.stepParamProcessing.get({ input_guid: req.params.guid, input_name: req.params.name });

    if (!processedStepParam)
      return res.status(200).json({});

    processedStepParam = {
      step_processing_guid: processedStepParam.step_processing_guid,
      name: processedStepParam.name,
      value: processedStepParam.value,
      created_at: processedStepParam.created_at,
      created_by: processedStepParam.created_by,
      updated_at: processedStepParam.updated_at,
      updated_by: processedStepParam.updated_by,
    };
    
    res.status(200).json(processedStepParam);
  }
  catch (err) {
    next(err);
  }
};