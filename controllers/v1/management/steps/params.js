const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      name: Joi.string().not().empty().required()
    });
  if (body.name)
    return Joi.object().keys({
      name: Joi.string().not().empty(),
      value: Joi.string().not().empty()
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    value: Joi.string().not().empty().required()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataParams ] = await  db.stepParam.info({ 
      input_step_guid: req.params.guid, 
      input_name: req.query.name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const paramsCount = dataParams[0] ? dataParams[0].count : 0;

    dataParams = dataParams.map(param => {
      return {
        step_guid: param.step_guid,
        name: param.name,
        value: param.value,
        created_by: param.created_by,
        updated_by: param.updated_by,
        created_at: param.created_at,
        updated_at: param.updated_at
      };
    });

    res.status(200).json({ count: paramsCount, data: dataParams });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const { name, value } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.stepParam.delete({ input_name: name, input_step_guid: guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ param ]] = await db.stepParam.upsert({
      input_step_guid: guid,
      input_name: name,
      input_value: value,
      input_author_guid: author_guid
    });
    
    delete param.guard_counter;
    res.status(200).json(param);
  }
  catch (err) {
    next(err);
  }
};