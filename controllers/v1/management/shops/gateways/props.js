const db = require("../../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      name: Joi.string().not().empty().required()
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    value: Joi.string()
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ shopGatewayProps ]  = await db.shopGatewayProp.get({
      input_shop_guid: req.params.guid,
      input_gateway_guid: req.params.gatewayGuid,
      input_name: req.query.name,
      input_items_count: req.query.items,
      input_page_number: req.query.page
    });

    const numberOfProps = shopGatewayProps[0] ? shopGatewayProps[0].count : 0;

    shopGatewayProps = shopGatewayProps.map(shopGatewayProp => ({
      shop_guid: shopGatewayProp.gateway_guid,
      gateway_guid: shopGatewayProp.shop_guid,
      name: shopGatewayProp.name,
      value: shopGatewayProp.value,
      guard_counter: shopGatewayProp.guard_counter,
      created_by: shopGatewayProp.created_by,
      updated_by: shopGatewayProp.updated_by,
      created_at: shopGatewayProp.created_at,
      updated_at: shopGatewayProp.updated_at
    }));

    res.status(200).json({ count: numberOfProps, data: shopGatewayProps });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getLabels = async (req, res, next) => {
  try {
    let [ shopGatewayProps ]  = await db.shopGatewayProp.info({
      input_shop_guid: req.params.guid,
      input_gateway_guid: req.params.gatewayGuid
    });

    const numberOfProps = shopGatewayProps[0] ? shopGatewayProps[0].count : 0;

    shopGatewayProps = shopGatewayProps.map(shopGatewayProp => ({
      name: shopGatewayProp.name,
      label: shopGatewayProp.label,
      value: shopGatewayProp.value,
    }));

    res.status(200).json({ count: numberOfProps, data: shopGatewayProps });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.shopGatewayProp.delete({ 
        input_shop_guid: req.params.guid, 
        input_gateway_guid: req.params.gatewayGuid, 
        input_name: req.body.name });
      return res.status(200).json({ message: "Deleted" });
    }

    const shopGatewayProp = {
      input_shop_guid: req.params.guid,
      input_gateway_guid: req.params.gatewayGuid,
      input_name: req.body.name,
      input_value: req.body.value,
      input_author_guid: author_guid
    };

    res.status(200).json(await db.shopGatewayProp.upsert(shopGatewayProp));
  }
  catch (err) {
    next(err);
  }
};
