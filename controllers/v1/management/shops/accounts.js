const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.array().items(Joi.object().keys({
    guid: Joi.string().guid().required()
  }));
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataAccounts ] = await db.account.byShopInfo({
      input_shop_guid : req.params.guid,
      input_guid: req.query.guid,
      input_currency_name: req.query.currency_name,
      input_number: req.query.number,
      input_balance: req.query.balance,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfAccounts = dataAccounts[0] ? dataAccounts[0].count : 0;

    dataAccounts = dataAccounts.map(account => {
      return{
        guid: account.guid,
        currency_guid: account.currency_guid,
        currency_name: account.currency_name,
        number: account.number,
        balance: account.balance,
        created_at: account.created_at,
        created_by: account.created_by,
        updated_at: account.updated_at,
        updated_by: account.updated_by,
      };
    });

    res.status(200).json({ count: numberOfAccounts, data: dataAccounts });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid: shop_guid } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    const resData = await Promise.all(req.body.map(async obj => {
      const account_guid = obj.guid;

      if (obj.delete) {
        await db.shopAccount.delete({
          input_shop_guid: shop_guid,
          input_account_guid: account_guid
        });

        return {
          shop_guid: shop_guid,
          account_guid: account_guid,
          message: "Deleted"
        };
      }

      const [[ upsertedData ]] = await db.shopAccount.upsert({
        input_shop_guid: shop_guid,
        input_account_guid: account_guid,
        input_author_guid: author_guid
      });

      return {
        shop_guid: upsertedData.shop_guid,
        account_guid: upsertedData.account_guid,
        created_at: upsertedData.created_at,
        created_by: upsertedData.created_by,
        updated_at: upsertedData.updated_at,
        updated_by: upsertedData.updated_by,
      };
    }));

    res.status(200).json(resData);
  } catch (err) {
    next(err);
  }
};
