const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.array().items(Joi.alternatives().try(
    Joi.object().keys({
      guid: Joi.string().guid().required(),
      billing_descriptor: Joi.string().not().empty().required(),
      routing_string: Joi.string().not().empty().required(),
      payment_amount_limit: Joi.number().required(),
      monthly_amount_limit: Joi.number().required(),
      supported_brands: Joi.string().required(),
      supported_currencies: Joi.string().required(),
      generate_statement: Joi.equal(0,1).required(),
      enabled: Joi.equal(0,1).required()
    }),
    Joi.object().keys({
      delete: Joi.equal(true).required(),
      guid: Joi.string().guid().required()
    })));
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataShopGateways ]  = await db.gateway.byShopInfo({
      input_shop_guid : req.params.guid,
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfGateways = dataShopGateways[0] ? dataShopGateways[0].count : 0;

    dataShopGateways = dataShopGateways.map(shopGateway => {
      return {
        //shop gateway info
        billing_descriptor: shopGateway.billing_descriptor,
        routing_string: shopGateway.routing_string,
        payment_amount_limit: shopGateway.payment_amount_limit,
        monthly_amount_limit: shopGateway.monthly_amount_limit,
        supported_brands: shopGateway.supported_brands,
        supported_currencies: shopGateway.supported_currencies,
        generate_statement: shopGateway.generate_statement,
        enabled: shopGateway.enabled,
        created_at: shopGateway.created_at,
        created_by: shopGateway.created_by,
        updated_at: shopGateway.updated_at,
        updated_by: shopGateway.updated_by,
        //gateway info
        guid: shopGateway.gateway_guid,
        name: shopGateway.gateway_name,
        description: shopGateway.gateway_description,
      };
    });

    res.status(200).json({ count: numberOfGateways, data: dataShopGateways });
  } catch (err) {
    next(err);
  }
};


module.exports.post = async (req, res, next) => {
  const { guid } = req.params;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    const resData = await Promise.all(req.body.map(async shopGateways => {
      const gateway_guid = shopGateways.guid;

      if (shopGateways.delete){
        await db.shopGateway.delete({
          input_shop_guid: guid,
          input_gateway_guid: gateway_guid
        });

        return {
          gateway_guid: gateway_guid,
          shop_guid: guid,
          message: "Deleted"
        };
      }

      const [[ resRecord ]] = await db.shopGateway.upsert({
        input_shop_guid: guid,
        input_gateway_guid: gateway_guid,
        input_billing_descriptor: shopGateways.billing_descriptor,
        input_routing_string: shopGateways.routing_string,
        input_payment_amount_limit: shopGateways.payment_amount_limit,
        input_monthly_amount_limit: shopGateways.monthly_amount_limit,
        input_supported_brands: shopGateways.supported_brands,
        input_supported_currencies: shopGateways.supported_currencies,
        input_generate_statement: shopGateways.generate_statement,
        input_enabled: shopGateways.enabled,
        input_author_guid: author_guid
      });

      return resRecord;
    }));

    res.status(200).json(resData);
  } catch (err) {
    next(err);
  }
};

module.exports.props = require("./gateways/props");