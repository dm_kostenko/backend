const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");
const { InternalError } = require("../../../errorTypes");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      type: Joi.string().not().empty(),
      partner_guid: Joi.string().guid().allow("")
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    type: Joi.string().not().empty().required(),
    partner_guid: Joi.string().guid().allow("")
  });
};

module.exports.canGet = async req => false;

module.exports.canGetByGuid = async (req) => {
  if (req.auth.partner){
    const [ groups ] = await db.group.byPartnerInfo({ input_partner_guid: req.auth.partnerGuid });
    return groups.some(({ guid }) => guid === req.params.guid);
  }
  return false;
};

module.exports.canPost = async (req) => {
  if (req.auth.partner){
    req.body.partner_guid = req.auth.partnerGuid;
    if (req.body.guid){
      const [ groups ] = await db.group.byPartnerInfo({ input_partner_guid: req.auth.partnerGuid });
      return groups.some(({ guid }) => guid === req.body.guid);
    } else return true;
  }
  return false;
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataGroups ] = await db.group.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_partner_guid: req.query.partner_guid,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfGroups = dataGroups[0] ? dataGroups[0].count : 0;
    
    dataGroups = dataGroups.map(group => {
      return {
        guid: group.guid,
        name: group.name,
        type: group.type,
        partner_guid: group.partner_guid,
        partner_name: group.partner_name,
        created_at: group.created_at,
        created_by: group.created_by,
        updated_at: group.updated_at,
        updated_by: group.updated_by,
      };
    });

    res.status(200).json({ count: numberOfGroups, data: dataGroups });
  } catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ group ]] = await db.group.info({ input_guid: req.params.guid });

    if (!group) return res.status(200).json({});

    res.status(200).json({
      guid: group.guid,
      name: group.name,
      type: group.type,
      partner_guid: group.partner_guid,
      partner_name: group.partner_name,
      created_at: group.created_at,
      created_by: group.created_by,
      updated_at: group.updated_at,
      updated_by: group.updated_by,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  const { guid } = req.body;
  try {
    if (req.body.delete) {
      await db.group.delete({ input_guid: guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ group ]] = await db.group.upsert({
      input_guid: guid,
      input_name: req.body.name,
      input_type: req.body.type,
      input_partner_guid: req.body.partner_guid,
      input_author_guid: author_guid
    });

    if (!group) throw new InternalError("Record wasn't created for group");

    let partner;
    if (group.partner_guid) 
      [[ partner ]] = await db.partner.get({ input_guid: group.partner_guid });

    res.status(200).json({
      guid: group.guid,
      name: group.name,
      type: group.type,
      partner_guid: group.partner_guid,
      partner_name: partner ? partner.name : undefined,
      created_at: group.created_at,
      created_by: group.created_by,
      updated_at: group.updated_at,
      updated_by: group.updated_by,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.logins = require("./groups/logins");
module.exports.merchants = require("./groups/merchants");
module.exports.shops = require("./groups/shops");
