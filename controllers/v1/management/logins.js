const db = require("../../../helpers/db/api/");
const userProfileStorage = require("../../../helpers/userProfileStorage");
const bcrypt = require("bcrypt");
const { InvalidDataError, InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      username: Joi.string().alphanum().min(3).max(20),
      email: Joi.string().email(),
      password: Joi.string().min(6).max(20),
      old_password: Joi.string().min(6).max(20),
      auth_type: Joi.string().equal("login-password","login-password-mail"),
      enabled: Joi.equal(0,1),
      phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
      user_profile: Joi.object().keys({
        first_name: Joi.string().not().empty(),
        last_name: Joi.string().not().empty(),
        email: Joi.string().email(),
        phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
        company_name: Joi.string().not().empty(),
        note: Joi.string().allow(""),
      })
    });

  return Joi.object().keys({
    username: Joi.string().alphanum().min(3).max(20).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(20).required(),
    enabled: Joi.equal(0,1).required(),
    auth_type: Joi.string().equal("login-password","login-password-mail"),
    phone: Joi.string().trim().regex(/^[0-9]{7,11}$/).required(),
    user_profile: Joi.object().keys({
      first_name: Joi.string().not().empty().required(),
      last_name: Joi.string().not().empty().required(),
      email: Joi.string().email().required(),
      phone: Joi.string().trim().regex(/^[0-9]{7,11}$/).required(),
      company_name: Joi.string().not().empty().required(),
      note: Joi.string().allow("").required(),
    })
  });
};

module.exports.canGet = async req => false;

module.exports.canGetByGuid = async req => req.auth.loginGuid === req.params.guid;

module.exports.canPost = async req => {
  req.body.username = undefined;
  req.body.enabled = undefined;
  return req.auth.loginGuid === req.body.guid;
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataOnLogins ] = await db.login.info({
      input_guid: req.query.guid,
      input_username: req.query.username,
      input_email: req.query.email,
      input_enabled: req.query.enabled,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfLogins = dataOnLogins[0] ? dataOnLogins[0].count : 0;

    const resData = await Promise.all(dataOnLogins.map(async recordLogin => {
      return {
        guid: recordLogin.guid,
        username: recordLogin.username,
        username_canonical: recordLogin.username_canonical,
        email: recordLogin.email,
        email_canonical: recordLogin.email_canonical,
        enabled: recordLogin.enabled,
        last_login: recordLogin.last_login,
        locked: recordLogin.locked,
        expired: recordLogin.expired,
        expires_at: recordLogin.expires_at,
        credentials_expired: recordLogin.credentials_expired,
        credentials_expire_at: recordLogin.credentials_expire_at,
        phone: recordLogin.phone,
        enabled_api: recordLogin.enabled_api,
        created_at: recordLogin.created_at,
        created_by: recordLogin.created_by,
        updated_at: recordLogin.updated_at,
        updated_by: recordLogin.updated_by
      };
    }));

    res.status(200).json({ count: numberOfLogins, data: resData });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ dataDbLogin ]]  = await db.login.info({ input_guid: req.params.guid });

    if (!dataDbLogin) return res.status(200).json({});

    dataDbLogin = {
      guid: dataDbLogin.guid,
      user_profile_id: dataDbLogin.user_profile_id,
      username: dataDbLogin.username,
      username_canonical: dataDbLogin.username_canonical,
      email: dataDbLogin.email,
      email_canonical: dataDbLogin.email_canonical,
      enabled: dataDbLogin.enabled,
      auth_type: dataDbLogin.auth_type,
      last_login: dataDbLogin.last_login,
      locked: dataDbLogin.locked,
      expired: dataDbLogin.expired,
      expires_at: dataDbLogin.expires_at,
      credentials_expired: dataDbLogin.credentials_expired,
      credentials_expire_at: dataDbLogin.credentials_expire_at,
      phone: dataDbLogin.phone,
      enabled_api: dataDbLogin.enabled_api,
      created_at: dataDbLogin.created_at,
      created_by: dataDbLogin.created_by,
      updated_at: dataDbLogin.updated_at,
      updated_by: dataDbLogin.updated_by
    };

    res.status(200).json(dataDbLogin);
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  let session = 0, dbConnection;

  try {
    if (req.body.delete){
      const [[ loginObj ]] = await db.login.get({ input_guid: guid });
      if (!loginObj) 
        throw new InvalidDataError(`Doesn't exist "login" with guid = ${guid}`);

      if (loginObj.user_profile_id) await userProfileStorage.delete(loginObj.user_profile_id);
      await db.login.delete({ input_guid: guid });
      
      return res.status(200).json({ message: "Deleted" });
    }

    // session = await userProfileStorage.startTransaction();

    let user_profile_id;
    if (req.body.user_profile){
      if (guid){
        const [[ loginObj ]] = await db.login.get({ input_guid: guid });
        if (!loginObj) 
          throw new InvalidDataError(`Doesn't exist "login" with guid = ${guid}`);
        user_profile_id = loginObj.user_profile_id;
      }

      user_profile_id = await userProfileStorage.upsert({
        id: user_profile_id,
        first_name: req.body.user_profile.first_name,
        last_name: req.body.user_profile.last_name,
        email: req.body.user_profile.email,
        phone: req.body.user_profile.phone,
        company_name: req.body.user_profile.company_name,
        note: req.body.user_profile.note,
        _author: author_guid
      }); //}, { session });

      session = user_profile_id;
    }

    let loginData = {
      input_guid: guid,
      input_user_profile_id : user_profile_id,
      input_username: req.body.username,
      input_email: req.body.email,
      input_enabled: req.body.enabled,
      input_auth_type: req.body.auth_type,
      input_locked: "0",
      input_expired: "1",
      input_expires_at: "9999-12-31 23:59:59",
      input_confirmation_token: "hard code",
      input_credentials_expired: "1",
      input_credentials_expire_at: "9999-12-31 23:59:59",
      input_phone: req.body.phone,
      input_enabled_api: "1",
      input_allowed_ips: "hard code",
      input_hash_key: "hard code",
      input_author_guid: author_guid
    };

    // update password
    if (req.body.password) {
      if (req.body.guid){
        if (!req.body.old_password) throw new InvalidDataError("Not received old password");
        
        const [[ dbObj ]] = await db.login.get({ input_guid: guid });

        if (!dbObj) throw new InvalidDataError("No response from database. guid from login undefined");

        if (!bcrypt.compareSync(req.body.old_password, dbObj.password))
          throw new InvalidDataError("InvalidPassword");
      }
      
      loginData.input_salt = bcrypt.genSaltSync(10);
      loginData.input_password = bcrypt.hashSync(req.body.password, loginData.input_salt);
      loginData.input_password_requested_at = new Date().toISOString().slice(0, 19).replace("T", " ");
    }

    dbConnection = await db.getConnection();
    await dbConnection.beginTransaction();
 
    // create or update record in `login`
    const [[ dataDbLogin ]] = await db.login.upsert(loginData, dbConnection);
    if (!dataDbLogin && !dataDbLogin.guid) 
      throw new InvalidDataError("No response from database. guid from login undefined");
    
    if (!req.body.guid){
      const [[ basicRole ]] = await db.role.get({ input_name: "user" });
      if (!basicRole)
        throw new InternalError("Basic role \"user\" not found and cannot be attached");

      await db.defaultLoginRole.upsert({ input_login_guid: dataDbLogin.guid, input_role_guid: basicRole.guid, input_author_guid: author_guid }, dbConnection)
    }
    await dbConnection.commit();
    //await userProfileStorage.commitTransaction(session);
    const user_profile = user_profile_id ? await userProfileStorage.get(user_profile_id) : undefined;

    res.status(200).json({
      guid: dataDbLogin.guid,
      username: dataDbLogin.username,
      username_canonical: dataDbLogin.username_canonical,
      email: dataDbLogin.email,
      email_canonical: dataDbLogin.email_canonical,
      enabled: dataDbLogin.enabled,
      auth_type: dataDbLogin.auth_type,
      last_login: dataDbLogin.last_login,
      locked: dataDbLogin.locked,
      expired: dataDbLogin.expired,
      expires_at: dataDbLogin.expires_at,
      credentials_expired: dataDbLogin.credentials_expired,
      credentials_expire_at: dataDbLogin.credentials_expire_at,
      phone: dataDbLogin.phone,
      enabled_api: dataDbLogin.enabled_api,
      user_profile: user_profile ? {
        first_name: user_profile.first_name,
        last_name: user_profile.last_name,
        email: user_profile.email,
        phone: user_profile.phone,
        company_name: user_profile.company_name,
        note: user_profile.note
      } : undefined,
      created_at: dataDbLogin.created_at,
      created_by: dataDbLogin.created_by,
      updated_at: dataDbLogin.updated_at,
      updated_by: dataDbLogin.updated_by
    });
  }
  catch (err) {
    //if (session) await userProfileStorage.abortTransaction(session);
    if (session) 
      await userProfileStorage.delete(session);

    if (dbConnection)
      await dbConnection.rollback();

    next(err);
  }
  finally {
    if (dbConnection)
      dbConnection.end();
  }
};

module.exports.free = require("./logins/free");
module.exports.accounts = require("./logins/accounts");
module.exports.roles = require("./logins/roles");