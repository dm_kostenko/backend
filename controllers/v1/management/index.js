module.exports = {
  groups: require("./groups"),
  logins: require("./logins"),
  user_profiles: require("./user_profiles"),
  merchants: require("./merchants"),
  partners: require("./partners"),
  privileges: require("./privileges"),
  roles: require("./roles"),
  currencies: require("./currencies"),
  accounts: require("./accounts"),
  cards: require("./cards"),
  gateways: require("./gateways"),
  shops: require("./shops"),
  rates: require("./rates"),
  charges: require("./charges"),
  blacklist: require("./blacklist"),
  transactions: require("./transactions"),
  steps: require("./steps")
};
