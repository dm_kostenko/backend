const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = body => {
  if (body.delete)
    return Joi.object().keys({
      blacklist_rule_guid: Joi.string().guid().required(),
      merchant_guid: Joi.string().guid().required(),
    });
  return Joi.object().keys({
    delete: Joi.boolean(),
    blacklist_rule_guid: Joi.string().guid().required(),
    merchant_guid: Joi.string().guid().required(),
    type: Joi.string().required(),
  });
};

module.exports.get = async (req, res, next) => {
  try {
    const { merchant_guid, blacklist_rule_name, rule_guid, type, items, page } = req.query;

    let [ blacklistMerchantRules ] = await db.blacklistMerchant.info({
      input_blacklist_rule_guid: rule_guid,
      input_blacklist_rule_name: blacklist_rule_name,
      input_merchant_guid: merchant_guid,
      input_type: type,
      input_items_count: items,
      input_page_number: page
    });

    const numberOfRules = blacklistMerchantRules[0] ? blacklistMerchantRules[0].count : 0;
    blacklistMerchantRules = blacklistMerchantRules.map(rule => ({
      merchant_guid: rule.merchant_guid,
      blacklist_rule_name: rule.blacklist_rule_name,
      blacklist_rule_guid: rule.blacklist_rule_guid,
      type: rule.type,
      created_by: rule.created_by,
      updated_by: rule.updated_by,
      created_at: rule.created_at,
      updated_at: rule.updated_at
    }));

    res.status(200).json({ count: numberOfRules, data: blacklistMerchantRules });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    const { merchant_guid } = req.params;
    const { items, page } = req.query;

    let [ blacklistMerchantRules ] = await db.blacklistMerchant.byMerchantInfo({ input_merchant_guid: merchant_guid, input_items_count: items, input_page_number: page });

    if (!blacklistMerchantRules.length)
      return res.status(200).json([]);

    const numberOfRules = blacklistMerchantRules[0] ? blacklistMerchantRules[0].count : 0;

    blacklistMerchantRules = blacklistMerchantRules.map(rule => ({
      merchant_guid: rule.merchant_guid,
      blacklist_rule_guid: rule.blacklist_rule_guid,
      blacklist_rule_name: rule.blacklist_rule_name,
      type: rule.type,
      created_by: rule.created_by,
      updated_by: rule.updated_by,
      created_at: rule.created_at,
      updated_at: rule.updated_at
    }));

    res.status(200).json({ count: numberOfRules, data: blacklistMerchantRules });
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    const { blacklist_rule_guid, merchant_guid, type } = req.body;

    if (req.body.delete) {
      await db.blacklistMerchant.delete({ input_blacklist_rule_guid: blacklist_rule_guid, input_merchant_guid: merchant_guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ rule ]] = await db.blacklistMerchant.upsert({
      input_blacklist_rule_guid: blacklist_rule_guid,
      input_merchant_guid: merchant_guid,
      input_type: type,
      input_author_guid: author_guid
    });

    res.status(200).json(rule);
  }
  catch (err) {
    next(err);
  }
};