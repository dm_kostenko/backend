const db = require("../../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = () => {
  return Joi.object().keys({
    blacklist_rule_guid: Joi.string().guid().required(),
    value: Joi.alternatives().try(
      Joi.string().ip({ version: [ "ipv4", "ipv6" ] }).required(),
      Joi.string().creditCard().required(),
      Joi.string().regex(/^[A-Z]{2}$/).required()
    )
  });
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.rule.delete({
        input_blacklist_rule_guid: req.body.blacklist_rule_guid,
        input_value: req.body.value
      });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ rule ]] = await db.rule.upsert({
      input_blacklist_rule_guid: req.body.blacklist_rule_guid,
      input_value: req.body.value,
      input_author_guid: author_guid
    });

    res.status(200).json(rule);
  }
  catch (err) {
    next(err);
  }
};