const db = require("../../../helpers/db/api/");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty().required(),
      type: Joi.string().valid("ip", "card", "country").required(),
      description: Joi.string()
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    type: Joi.string().valid("ip", "card", "country").required(),
    description: Joi.string(),
    values: Joi.array().required().items(Joi
      .when("type", { is: "ip", then: Joi.string().ip({ version: [ "ipv4", "ipv6" ] }).required() })
      .when("type", { is: "card", then: Joi.string().creditCard().required() })
      .when("type", { is: "country", then: Joi.string().regex(/^[A-Z]{2}$/).required() }),
    )
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ blacklistRules ] = await db.blacklistRule.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_type: req.query.type,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });
    
    const numberOfRules = blacklistRules[0] ? blacklistRules[0].count : 0;

    blacklistRules = await Promise.all(blacklistRules.map(async rule => {
      const [ rules ] = await db.rule.get({ input_blacklist_rule_guid: rule.guid });
      const values = rules.map(rule => rule.value);
      return {
        guid: rule.guid,
        name: rule.name,
        type: rule.type,
        values,
        description: rule.description,
        created_by: rule.created_by,
        updated_by: rule.updated_by,
        created_at: rule.created_at,
        updated_at: rule.updated_at
      };
    }));
    res.status(200).json({ count: numberOfRules, data: blacklistRules });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  const { guid: blacklist_rule_guid } = req.params;
  try {
    const [[ blacklistRule ]]  = await db.blacklistRule.get({ input_guid: blacklist_rule_guid });

    if (!blacklistRule)
      return res.status(200).json({});

    const [ ruleValues ] = await db.rule.get({ input_blacklist_rule_guid: blacklist_rule_guid });
    const values = ruleValues.map(ruleValue => ruleValue.value);

    const resData = {
      guid: blacklistRule.guid,
      name: blacklistRule.name,
      type: blacklistRule.type,
      description: blacklistRule.description,
      created_by: blacklistRule.created_by,
      updated_by: blacklistRule.updated_by,
      created_at: blacklistRule.created_at,
      updated_at: blacklistRule.updated_at,
      values
    };

    res.status(200).json(resData);
  }
  catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.blacklistRule.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ blacklistRule ]] = await db.blacklistRule.upsert({
      input_guid: req.body.guid,
      input_name: req.body.name,
      input_type: req.body.type,
      input_description: req.body.description,
      input_author_guid: author_guid
    });

    if (!req.body.guid){
      await Promise.all(req.body.values.map(async value => 
        await db.rule.upsert({ input_blacklist_rule_guid: blacklistRule.guid, input_value: value, input_author_guid: author_guid })
      ));
    }

    const resData = {
      guid: blacklistRule.guid,
      name: blacklistRule.name,
      type: blacklistRule.type,
      description: blacklistRule.description,
      created_by: blacklistRule.created_by,
      updated_by: blacklistRule.updated_by,
      created_at: blacklistRule.created_at,
      updated_at: blacklistRule.updated_at
    };

    res.status(200).json(resData);
  }
  catch (err) {
    next(err);
  }
};

module.exports.global = require("./blacklist/global");
module.exports.merchants = require("./blacklist/merchants");
module.exports.values = require("./blacklist/values");