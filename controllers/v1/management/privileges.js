const db = require("../../../helpers/db/api/");
const { InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      type: Joi.string().not().empty(),
      description: Joi.string().not().empty(),
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    type: Joi.string().not().empty().required(),
    description: Joi.string().not().empty().required(),
  });
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataPrivileges ]  = await db.privilege.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_type: req.query.type,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfPrivileges = dataPrivileges[0] ? dataPrivileges[0].count : 0;

    dataPrivileges = dataPrivileges.map( record => {
      return {
        guid: record.guid,
        name: record.name,
        type: record.type,
        description: record.description,
        created_by: record.created_by,
        updated_by: record.updated_by,
        created_at: record.created_at,
        updated_at: record.updated_at
      };
    });

    res.status(200).json({ count: numberOfPrivileges, data: dataPrivileges });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ privilege ]]  = await db.privilege.info({ input_guid: req.params.guid });

    if (!privilege)
      return res.status(200).json({});

    privilege = {
      guid: privilege.guid,
      name: privilege.name,
      type: privilege.type,
      description: privilege.description,
      updated_by: privilege.updated_by,
      created_by: privilege.created_by,
      updated_at: privilege.updated_at,
      created_at: privilege.created_at
    };

    res.status(200).json(privilege);
  }
  catch (err) {
    next(err);
  }
};
module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.privilege.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const privilegeData = {
      input_guid: req.body.guid,
      input_name: req.body.name,
      input_type: req.body.type,
      input_description: req.body.description,
      input_author_guid: author_guid,
    };

    const [[ privilege ]] = await db.privilege.upsert(privilegeData);
    if (!privilege) 
      throw new InternalError("Record wasn't created for privilege");

    const resData = {
      guid: privilege.guid,
      name: privilege.name,
      type: privilege.type,
      description: privilege.description,
      created_by: privilege.created_by,
      updated_by: privilege.updated_by,
      created_at: privilege.created_at,
      updated_at: privilege.updated_at
    };
    res.status(200).json(resData);
  } catch (err) {
    next(err);
  }
};
