const db = require("../../../helpers/db/api/");
const { InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      type: Joi.string().not().empty(),
      group_guid: Joi.string().guid().allow(""),
    });
  return Joi.object().keys({
    name: Joi.string().not().empty().required(),
    type: Joi.string().not().empty().required(),
    group_guid: Joi.string().guid().allow(""),
  });
};

module.exports.canGet = async req => false;

module.exports.canGetByGuid = async (req) => {
  if (req.auth.group){
    const [ merchants ] = await db.merchant.byGroupInfo({ input_group_guid: req.auth.groupGuid });
    return merchants.some(({ guid }) => guid === req.params.guid);
  }
  else if (req.auth.partner){
    const [ merchants ] = await db.merchant.byPartnerInfo({ input_partner_guid: req.auth.partnerGuid });
    return merchants.some(({ guid }) => guid === req.params.guid);
  }
  else if (req.auth.merchant)
    return req.auth.merchantGuid === req.params.guid;

  return false;
};

module.exports.canPost = async (req) => {
  if (req.auth.group){
    req.body.group_guid = req.auth.groupGuid;
    if (req.body.guid){
      const [ merchants ] = await db.merchant.byGroupInfo({ input_group_guid: req.auth.groupGuid });
      return merchants.some(({ guid }) => guid === req.body.guid);
    } else return true;
  }
  else if (req.auth.partner){
    const [ merchants ] = await db.merchant.byPartnerInfo({ input_partner_guid: req.auth.partnerGuid });

    const guidBool = req.body.guid ? merchants.some(({ guid }) => guid === req.body.guid) : true;
    const merchantGuidBool = req.body.group_guid ? merchants.some(({ group_guid }) => group_guid === req.body.group_guid) : true;
    return guidBool && merchantGuidBool;
  }
  return false;
};

module.exports.get = async (req, res, next) => {
  try {
    let [ dataMerchants ] = await db.merchant.info({
      input_guid: req.query.guid,
      input_name: req.query.name,
      input_group_name: req.query.group_name,
      input_page_number: req.query.page,
      input_items_count: req.query.items
    });

    const numberOfMerchants = dataMerchants[0] ? dataMerchants[0].count : 0;

    dataMerchants = dataMerchants.map(merchant => {
      return {
        guid: merchant.guid,
        name: merchant.name,
        type: merchant.type,
        group_guid: merchant.group_guid,
        group_name: merchant.group_name,
        created_at: merchant.created_at,
        created_by: merchant.created_by,
        updated_at: merchant.updated_at,
        updated_by: merchant.updated_by,
      };
    });

    res.status(200).json({ count: numberOfMerchants, data: dataMerchants });
  } catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let [[ merchant ]] = await db.merchant.info({ input_guid: req.params.guid });
    if (!merchant) return res.status(200).json({});

    res.status(200).json({
      guid: merchant.guid,
      name: merchant.name,
      type: merchant.type,
      group_guid: merchant.group_guid,
      group_name: merchant.group_name,
      monthly_amount_limit: merchant.monthly_amount_limit,
      created_at: merchant.created_at,
      created_by: merchant.created_by,
      updated_at: merchant.updated_at,
      updated_by: merchant.updated_by,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const { guid } = req.body;
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.merchant.delete({ input_guid: guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const upsertData = {
      name: req.body.name,
      type: req.body.type,
      group_guid: req.body.group_guid,
      monthly_amount_limit: req.body.monthly_amount_limit || null
    };

    const [[ merchant ]] = await db.merchant.upsert({
      input_guid: guid,
      input_name: upsertData.name,
      input_type: upsertData.type,
      input_group_guid: upsertData.group_guid,
      input_monthly_amount_limit: upsertData.monthly_amount_limit,
      input_author_guid: author_guid,
    });
    if (!merchant) 
      throw new InternalError("Record wasn't created for merchant");

    let group;
    if (merchant.group_guid)
      [[ group ]] = await db.group.get({ input_guid: merchant.group_guid });
    
    const responseData =  {

      guid: merchant.guid,
      name: merchant.name,
      type: merchant.type,
      group_guid: merchant.group_guid,
      group_name: group ? group.name : undefined,
      created_at: merchant.created_at,
      created_by: merchant.created_by,
      updated_at: merchant.updated_at,
      updated_by: merchant.updated_by,
    };

    res.status(200).json(responseData);
  } catch (err) {
    next(err);
  }
};

module.exports.logins = require("./merchants/logins");
module.exports.shops = require("./merchants/shops");
module.exports.blacklists = require("./merchants/blacklists");