const db = require("../../../helpers/db/api/");
const { generateSecret } = require("../../../helpers/tools");
const aes = require("../../../helpers/aes");
const { InternalError } = require("../../../errorTypes");
const Joi = require("@hapi/joi");

module.exports.schema = (body) => {
  if (body.delete)
    return Joi.object().keys({
      guid: Joi.string().guid().required()
    });

  if (body.guid)
    return Joi.object().keys({
      guid: Joi.string().guid(),
      merchant_guid: Joi.string().guid(),
      name: Joi.string().not().empty(),
      url: Joi.string().uri(),
      email: Joi.string().email(),
      phone: Joi.string().trim().regex(/^[0-9]{7,11}$/),
      enabled: Joi.equal(0,1),
      note: Joi.string().allow(""),
      enable_checkout: Joi.equal(0,1),
      checkout_method: Joi.string().allow(""),
      antiFraudMonitor: Joi.equal(0,1),
      antiFraudMonitorValue: Joi.number(),
      generateSecret: Joi.equal(0,1)
    });

  return Joi.object().keys({
    merchant_guid: Joi.string().guid().required(),
    name: Joi.string().not().empty().required(),
    url: Joi.string().uri().required(),
    email: Joi.string().email().required(),
    phone: Joi.string().trim().regex(/^[0-9]{7,11}$/).required(),
    enabled: Joi.equal(0,1).required(),
    note: Joi.string().allow(""),
    enable_checkout: Joi.equal(0,1),
    checkout_method: Joi.string().allow(""),
    antiFraudMonitor: Joi.equal(0,1).required(),
    antiFraudMonitorValue: Joi.number()
  });
};

module.exports.canGet = async (req) => {
  if (req.query.gateway_guid) return false;
  return true;
};

module.exports.canPost = async (req) => {
  let merchants = true;
  let shops = true;
  if (req.auth.partner) {
    if (req.body.merchant_guid) {
      [ merchants ] = await db.merchant.byPartnerInfo({
        input_partner_guid: req.auth.partnerGuid,
        input_guid: req.body.merchant_guid
      });
    } 
    if (req.body.guid){
      [ shops ] = await db.shop.byPartnerInfo({
        input_partner_guid: req.auth.partnerGuid,
        input_guid: req.body.guid
      });
    }
  }
  else if (req.auth.group) {
    if (req.body.merchant_guid) {
      [ merchants ] = await db.merchant.byGroupInfo({
        input_group_guid: req.auth.groupGuid,
        input_guid: req.body.merchant_guid
      });
    } 
    if (req.body.guid){
      [ shops ] = await db.shop.byGroupInfo({
        input_group_guid: req.auth.groupGuid,
        input_guid: req.body.guid
      });
    }
  }
  else if (req.auth.merchant) {
    req.body.merchant_guid = req.auth.merchantGuid;
    if (req.body.guid) {
      [ shops ] = await db.shop.byMerchantInfo({
        input_merchant_guid: req.auth.merchantGuid,
        input_guid: req.body.guid
      });
    }
  }

  return merchants && shops;
};

module.exports.get = async (req, res, next) => {
  try {
    let dataShops;

    if (req.query.gateway_guid) 
      [ dataShops ] = await db.shop.byGatewayInfo({
        input_guid: req.query.guid,
        input_name: req.query.name,
        input_merchant_name: req.query.merchant_name,
        input_enabled: req.query.enabled,
        input_gateway_guid: req.query.gateway_guid,
        input_page_number: req.query.page,
        input_items_count: req.query.items
      });
    else {
      if (req.auth && req.auth.merchant) [ dataShops ] = await db.shop.byMerchantInfo({
        input_merchant_guid: req.auth.merchantGuid,
        input_guid: req.query.guid,
        input_name: req.query.name,
        input_enabled: req.query.enabled,
        input_page_number: req.query.page,
        input_items_count: req.query.items
      });
      else if (req.auth && req.auth.group) [ dataShops ] = await db.shop.byGroupInfo({
        input_group_guid: req.auth.groupGuid,
        input_guid: req.query.guid,
        input_name: req.query.name,
        input_merchant_name: req.query.merchant_name,
        input_enabled: req.query.enabled,
        input_page_number: req.query.page,
        input_items_count: req.query.items,
      });
      else if (req.auth && req.auth.partner) [ dataShops ] = await db.shop.byPartnerInfo({
        input_partner_guid: req.auth.partnerGuid,
        input_guid: req.query.guid,
        input_name: req.query.name,
        input_merchant_name: req.query.merchant_name,
        input_enabled: req.query.enabled,
        input_page_number: req.query.page,
        input_items_count: req.query.items
      });
      else [ dataShops ] = await db.shop.info({
        input_guid: req.query.guid,
        input_name: req.query.name,
        input_merchant_name: req.query.merchant_name,
        input_enabled: req.query.enabled,
        input_page_number: req.query.page,
        input_items_count: req.query.items
      }); 
    }

    const numberOfShops = dataShops[0] ? dataShops[0].count : 0;

    dataShops = dataShops.map(shop => ({
      guid: shop.guid,
      merchant_guid: shop.merchant_guid,
      merchant_name: shop.merchant_name,
      name: shop.name,
      url: shop.url,
      email: shop.email,
      phone: shop.phone,
      enabled: shop.enabled,
      note: shop.note,
      enable_checkout: shop.enable_checkout,
      checkout_method: shop.checkout_method,
      antiFraudMonitor: shop.antiFraudMonitor,
      antiFraudMonitorValue: shop.antiFraudMonitorValue,
      created_at: shop.created_at,
      created_by: shop.created_by,
      updated_at: shop.updated_at,
      updated_by: shop.updated_by
    }));

    res.status(200).json({ count: numberOfShops, data: dataShops });
  }
  catch (err) {
    next(err);
  }
};

module.exports.getByGuid = async (req, res, next) => {
  try {
    let shop;
    if (req.auth && req.auth.merchant) [[ shop ]] = await db.shop.byMerchantInfo({
      input_guid: req.params.guid,
      input_merchant_guid: req.auth.merchantGuid
    });
    else if (req.auth && req.auth.group) [[ shop ]] = await db.shop.byGroupInfo({
      input_guid: req.params.guid,
      input_group_guid: req.auth.groupGuid
    });
    else if (req.auth && req.auth.partner) [[ shop ]] = await db.shop.byPartnerInfo({
      input_guid: req.params.guid,
      input_partner_guid: req.auth.partnerGuid
    });
    else [[ shop ]] = await db.shop.info({
      input_guid: req.params.guid
    });

    if (!shop) return res.status(200).json({});

    res.status(200).json({
      guid: shop.guid,
      merchant_guid: shop.merchant_guid,
      merchant_name: shop.merchant_name,
      name: shop.name,
      url: shop.url,
      email: shop.email,
      phone: shop.phone,
      enabled: shop.enabled,
      secret: (req.auth && req.auth.merchantGuid === shop.merchant_guid) ? aes.decode(shop.secret) : undefined,
      note: shop.note,
      hash_key: (req.auth && req.auth.merchantGuid === shop.merchant_guid) ? aes.decode(shop.hash_key) : undefined,
      enable_checkout: shop.enable_checkout,
      checkout_method: shop.checkout_method,
      antiFraudMonitor: shop.antiFraudMonitor,
      antiFraudMonitorValue: shop.antiFraudMonitorValue,
      created_at: shop.created_at,
      created_by: shop.created_by,
      updated_at: shop.updated_at,
      updated_by: shop.updated_by
    });
  } catch (err) {
    next(err);
  }
};

module.exports.post = async (req, res, next) => {
  const author_guid = req.auth ? req.auth.loginGuid : "emptyname";
  try {
    if (req.body.delete) {
      await db.shop.delete({ input_guid: req.body.guid });
      return res.status(200).json({ message: "Deleted" });
    }

    const [[ shop ]] = await db.shop.upsert({
      input_guid: req.body.guid,
      input_merchant_guid: req.body.merchant_guid,
      input_name: req.body.name,
      input_url: req.body.url,
      input_email: req.body.email,
      input_phone: req.body.phone,
      input_enabled: req.body.enabled,
      input_secret: !req.body.guid || req.body.generateSecret ? aes.encode(generateSecret()) : undefined,
      input_note: req.body.note,
      input_hash_key: !req.body.guid || req.body.generateSecret ? aes.encode(generateSecret()) : undefined,
      input_enable_checkout: req.body.enable_checkout,
      input_checkout_method: req.body.checkout_method,
      input_antiFraudMonitor: req.body.antiFraudMonitor,
      input_antiFraudMonitorValue: req.body.antiFraudMonitorValue,
      input_author_guid: author_guid
    });

    if (!shop) throw new InternalError("No created or updated record in shops");

    const [[ merchant ]] = await db.merchant.get({ input_guid: shop.merchant_guid });

    res.status(200).json({
      guid: shop.guid,
      merchant_guid: shop.merchant_guid,
      merchant_name: merchant ? merchant.name : undefined,
      name: shop.name,
      url: shop.url,
      email: shop.email,
      phone: shop.phone,
      enabled: shop.enabled,
      note: shop.note,
      enable_checkout: shop.enable_checkout,
      checkout_method: shop.checkout_method,
      antiFraudMonitor: shop.antiFraudMonitor,
      antiFraudMonitorValue: shop.antiFraudMonitorValue,
      created_at: shop.created_at,
      created_by: shop.created_by,
      updated_at: shop.updated_at,
      updated_by: shop.updated_by
    });
  }
  catch (err) {
    next(err);
  }
};

module.exports.accounts = require("./shops/accounts");
module.exports.gateways = require("./shops/gateways");
