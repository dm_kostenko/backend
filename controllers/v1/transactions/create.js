const eventsBus = require("../../../modules/transactionsProcessor");
const db = require("../../../helpers/db/api/");
const emitLog = require("../../../modules/logStorage/emitLog");

module.exports.post = async (req, res, next) => {
  try { 
    const { template, paymentForm } = req;
    const { shopGuid, auth_key } = req.auth;   
    const { succeed_url, failed_url } = req.body; 

    eventsBus.once(`transactionSucceed${req.id}`, async guid => {
      if (!res.headersSent){
        const message = { guid, status: "Success", succeed_url };

        emitLog.emit("transactionLog", {
          step_info: "Response to client: Success",
          request_id: req.id,
          transaction_guid: guid,
          shop_guid: shopGuid,
          message
        });

        res.json(message);
      }
      if (paymentForm)
        await db.securePayment.delete({ input_auth_key: auth_key });
    });

    eventsBus.once(`transactionFailed${req.id}`, async guid => {
      if (!res.headersSent){
        let message = { guid, status: "Failed", failed_url };
        
        emitLog.emit("transactionLog", {
          step_info: "Response to client: Failed",
          request_id: req.id,
          transaction_guid: guid,
          shop_guid: shopGuid,
          message
        });

        res.json(message);
      }
      if (paymentForm)
        await db.securePayment.delete({ input_auth_key: auth_key });
    });

    eventsBus.once(`transactionRedirect3dRequired${req.id}`, async (guid, data) => {
      if (!res.headersSent){
        const message = { guid, succeed_url, status: "Redirect3dRequired", ...data };
        
        emitLog.emit("transactionLog", {
          step_info: "Response to client: Redirect3dRequired",
          request_id: req.id,
          transaction_guid: guid,
          shop_guid: shopGuid,
          message
        });
        res.json(message);
      }
      if (paymentForm) 
        await db.securePayment.delete({ input_auth_key: auth_key });
    });

    eventsBus.emit("transactionCreate", { 
      params: req.body,
      requestId: req.id,
      initiator: shopGuid,
      template
    });
    
    emitLog.emit("transactionLog", {
      step_info: "Request from client: Create transaction",
      request_id: req.id,
      shop_guid: shopGuid,
      message: { params: req.body, template: template }
    });

    setTimeout(() => {
      if (!res.headersSent){
        const error = { error: "Transaction wasn't created" };
        
        emitLog.emit("transactionLog", {
          step_info: "Response to client: Timeout is over",
          request_id: req.id,
          shop_guid: shopGuid,
          message: error
        });

        res.status(500).json(error);
      }
      eventsBus.removeAllListeners(`transactionFailed${req.id}`);
      eventsBus.removeAllListeners(`transactionSucceed${req.id}`);
    }, 40000);
  } catch (err) {
    next(err);    
  }
};
